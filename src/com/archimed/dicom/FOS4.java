
package com.archimed.dicom;

import java.io.IOException;
import java.io.OutputStream;

class FOS4 extends FOS3 {
	FOS4(OutputStream outputstream) {
		super(outputstream);
	}

	FOS4(OutputStream outputstream, int i) {
		super(outputstream, i);
	}

	int _mthfor(Object object) throws IOException {
		return this.a((String) object);
	}

	int _mthdo(Object object) throws IOException {
		int i = ((Integer) object).intValue();
		return this.a(i >>> 16) + this.a(i);
	}

	int _mthbyte(Object object) throws IOException {
		return this._mthdo(Float.floatToIntBits(((Float) object).floatValue()));
	}

	int _mthtry(Object object) throws IOException {
		return this.a(Double.doubleToLongBits(((Double) object).doubleValue()));
	}

	int _mthcase(Object object) throws IOException {
		return this._mthdo(((Long) object).intValue());
	}

	int _mthint(Object object) throws IOException {
		return this.a(((Short) object).intValue());
	}

	int _mthnew(Object object) throws IOException {
		return this._mthdo(((Long) object).intValue());
	}

	int _mthif(Object object) throws IOException {
		return this.a(((Integer) object).intValue());
	}

	int a(Object object) throws IOException {
		return this.a(((Integer) object).intValue());
	}

	int a(Object object, int i) throws IOException {
		switch (i) {
		case 17:
			return _mthfor(object);
		case 5:
			return _mthdo(object);
		case 26:
			return _mthbyte(object);
		case 20:
			return _mthtry(object);
		case 19:
			return _mthcase(object);
		case 23:
			return _mthint(object);
		case 1:
			return _mthnew(object);
		case 3:
			return _mthif(object);
		case 21:
			return a(object);
		default:
			return 0;
		}
	}
}