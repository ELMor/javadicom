/*
 * DicomImage - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;

public class DicomImage extends DicomObject {
	public void patientData(String string, String string_0_, String string_1_,
			String string_2_) throws DicomException {
		try {
			set(147, string);
			set(148, string_0_);
			set(152, string_2_);
		} catch (DicomException dicomexception) {
			System.err.println(dicomexception);
		}
		try {
			set(150, string_1_);
		} catch (DicomException dicomexception) {
			throw new DicomException("Invalid Date format");
		}
	}

	public void generalStudyData(String string, String string_3_,
			String string_4_, String string_5_, String string_6_,
			String string_7_) throws DicomException {
		set(425, string);
		set(64, string_3_);
		set(70, string_4_);
		set(88, string_5_);
		set(427, string_6_);
		set(77, string_7_);
	}

	public void generalSeriesData(String string, String string_8_,
			String string_9_) throws DicomException {
		set(81, string);
		set(426, string_8_);
		set(428, string_9_);
	}

	public void generalEquipmentData(String string) {
		try {
			set(84, string);
		} catch (DicomException dicomexception) {
			/* empty */
		}
	}

	public void generalImageData(String string) throws DicomException {
		try {
			set(430, string);
		} catch (DicomException dicomexception) {
			throw new DicomException("Invalid Integer format");
		}
	}

	public void imagePixelData(int i, int i_10_, int i_11_, int i_12_,
			int i_13_, byte[] is) throws DicomException {
		set(461, new Integer(1));
		set(462, "MONOCHROME2");
		set(466, new Integer(i));
		set(467, new Integer(i_10_));
		if (i_11_ != 8 && i_11_ != 16 && i_11_ != 32)
			throw new DicomException("BitsAllocated " + i_11_
					+ ": other than 8, 16 or 32");
		if (i_12_ > i_11_)
			throw new DicomException("BitsStored > BitsAllocated");
		if (i_13_ >= i_12_)
			throw new DicomException("HighBit >= BitsStored");
		if (is == null)
			throw new DicomException("PixelData empty");
		set(475, new Integer(i_11_));
		set(476, new Integer(i_12_));
		set(477, new Integer(i_13_));
		set(478, new Integer(0));
		set(1184, is);
	}

	public void imagePixelData(int i, int i_14_, int i_15_, int i_16_,
			int i_17_, int[] is) throws DicomException {
		set(461, new Integer(1));
		set(462, "MONOCHROME2");
		set(466, new Integer(i));
		set(467, new Integer(i_14_));
		if (i_15_ != 8 && i_15_ != 16 && i_15_ != 24 && i_15_ != 32)
			throw new DicomException("BitsAllocated " + i_15_
					+ ": other than 8, 16 or 32");
		if (i_16_ > i_15_)
			throw new DicomException("BitsStored > BitsAllocated");
		if (i_17_ >= i_16_)
			throw new DicomException("HighBit >= BitsStored");
		if (is == null)
			throw new DicomException("PixelData empty");
		set(475, new Integer(i_15_));
		set(476, new Integer(i_16_));
		set(477, new Integer(i_17_));
		set(478, new Integer(0));
		int i_18_ = i_15_ / 8;
		byte[] is_19_ = new byte[is.length * i_18_];
		for (int i_20_ = 0; i_20_ < is.length; i_20_++) {
			for (int i_21_ = 0; i_21_ < i_18_; i_21_++)
				is_19_[i_18_ * i_20_ + i_21_] = (byte) ((is[i_20_] & 255 << 8 * i_21_) >> 8 * i_21_);
		}
		set(1184, is_19_);
	}

	public void imagePixelData(int i, int i_22_, int i_23_, byte[] is)
			throws DicomException {
		if (i_23_ != 0 && i_23_ != 1)
			throw new DicomException("Planar Configuration has to be 0 or 1");
		if (is == null)
			throw new DicomException("PixelData empty");
		set(461, new Integer(3));
		set(462, "RGB");
		set(466, new Integer(i));
		set(467, new Integer(i_22_));
		set(475, new Integer(8));
		set(476, new Integer(8));
		set(477, new Integer(7));
		set(478, new Integer(0));
		set(463, new Integer(i_23_));
		set(1184, is);
	}

	public void imagePixelData(int i, int i_24_, int i_25_, int[] is)
			throws DicomException {
		if (i_25_ != 0 && i_25_ != 1)
			throw new DicomException("Planar Configuration has to be 0 or 1");
		if (is == null)
			throw new DicomException("PixelData empty");
		set(461, new Integer(3));
		set(462, "RGB");
		set(466, new Integer(i));
		set(467, new Integer(i_24_));
		set(475, new Integer(8));
		set(476, new Integer(8));
		set(477, new Integer(7));
		set(478, new Integer(0));
		set(463, new Integer(i_25_));
		byte[] is_26_ = new byte[is.length * 3];
		if (i_25_ == 0) {
			for (int i_27_ = 0; i_27_ < is.length; i_27_++) {
				is_26_[3 * i_27_] = (byte) ((is[i_27_] & 0xff0000) >> 16);
				is_26_[3 * i_27_ + 1] = (byte) ((is[i_27_] & 0xff00) >> 8);
				is_26_[3 * i_27_ + 2] = (byte) (is[i_27_] & 0xff);
			}
		} else {
			for (int i_28_ = 0; i_28_ < is.length; i_28_++) {
				is_26_[i_28_] = (byte) ((is[i_28_] & 0xff0000) >> 16);
				is_26_[is.length + i_28_] = (byte) ((is[i_28_] & 0xff00) >> 8);
				is_26_[2 * is.length + i_28_] = (byte) (is[i_28_] & 0xff);
			}
		}
		set(1184, is_26_);
	}

	public void imagePixelData(int i, int i_29_, byte[] is, byte[] is_30_,
			byte[] is_31_, byte[] is_32_) throws DicomException {
		if (is == null)
			throw new DicomException("PixelData empty");
		if (is_30_ == null)
			throw new DicomException("Red Palette empty");
		if (is_31_ == null)
			throw new DicomException("Green Palette empty");
		if (is_32_ == null)
			throw new DicomException("Blue Palette empty");
		set(466, new Integer(i));
		set(467, new Integer(i_29_));
		set(475, new Integer(8));
		set(476, new Integer(8));
		set(477, new Integer(7));
		set(494, new Integer(256), 0);
		set(494, new Integer(0), 1);
		set(494, new Integer(16), 2);
		set(495, new Integer(256), 0);
		set(495, new Integer(0), 1);
		set(495, new Integer(16), 2);
		set(496, new Integer(256), 0);
		set(496, new Integer(0), 1);
		set(496, new Integer(16), 2);
		set(461, new Integer(1));
		set(462, "PALETTE COLOR");
		set(478, new Integer(0));
		byte[] is_33_ = new byte[512];
		byte[] is_34_ = new byte[512];
		byte[] is_35_ = new byte[512];
		for (int i_36_ = 0; i_36_ < is_30_.length; i_36_++)
			is_33_[2 * i_36_ + 1] = is_30_[i_36_];
		for (int i_37_ = 0; i_37_ < is_31_.length; i_37_++)
			is_34_[2 * i_37_ + 1] = is_31_[i_37_];
		for (int i_38_ = 0; i_38_ < is_32_.length; i_38_++)
			is_35_[2 * i_38_ + 1] = is_32_[i_38_];
		set(498, is_33_);
		set(499, is_34_);
		set(500, is_35_);
		set(1184, is);
	}

	public void sopCommonData(String string, String string_39_) {
		try {
			if (string == null)
				set(62, "1.2.840.10008.5.1.4.1.1.7");
			else
				set(62, string);
			set(63, string_39_);
		} catch (DicomException dicomexception) {
			/* empty */
		}
	}
}