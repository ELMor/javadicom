/*
 * ImageIO - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;

import java.awt.Toolkit;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.IOException;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.dicom.codec.Compression;

public class ImageIO {
	DicomImage dcm;

	int w;

	int h;

	int ba;

	int bs;

	int f;

	int max;

	int pc;

	int mask;

	int pr;

	String pi;

	boolean decode = false;

	int encoding;

	void setEncoding(int i) {
		switch (i) {
		case 8193:
		case 8194:
		case 8195:
			encoding = 0;
			break;
		default:
			encoding = i;
		}
	}

	Object getPixels(int i) {
		if (encoding != 0) {
			byte[] is;
			try {
				is = Compression.decompressFrame(encoding, (byte[]) dcm.get(
						1184, i + 1), w, h);
			} catch (DicomException dicomexception) {
				if (Debug.DEBUG > 0)
					dicomexception.printStackTrace();
				return null;
			} catch (IOException ioexception) {
				if (Debug.DEBUG > 0)
					ioexception.printStackTrace();
				return null;
			}
			return is;
		}
		byte[] is;
		if (f == 1)
			is = (byte[]) dcm.get(1184);
		else if (ba > 8) {
			is = new byte[w * h * ba / 8];
			System.arraycopy(dcm.get(1184), w * h * i * ba / 8, is, 0, w * h
					* ba / 8);
		} else if (pi.equals("RGB")) {
			is = new byte[w * h * 3];
			System.arraycopy(dcm.get(1184), w * h * i * 3, is, 0, w * h * 3);
		} else {
			is = new byte[w * h];
			System.arraycopy(dcm.get(1184), w * h * i, is, 0, w * h);
		}
		if (pi.equals("PALETTE COLOR"))
			return is;
		if (pi.startsWith("MONOCHROME")) {
			if (ba == 8)
				return is;
			if (ba == 16) {
				int[] is_0_ = new int[w * h];
				if (pr == 1) {
					for (int i_1_ = 0; i_1_ < w * h; i_1_++) {
						is_0_[i_1_] = ByteGetter.get(is[2 * i_1_]);
						is_0_[i_1_] += (is[2 * i_1_ + 1] << 8) + 32768;
						is_0_[i_1_] &= mask;
						if (i_1_ > 131072 && i_1_ < 131584)
							System.out.println(is_0_[i_1_]);
					}
				} else {
					for (int i_2_ = 0; i_2_ < w * h; i_2_++) {
						is_0_[i_2_] = ByteGetter.get(is[2 * i_2_]);
						is_0_[i_2_] += ByteGetter.get(is[2 * i_2_ + 1]) << 8;
						is_0_[i_2_] &= mask;
					}
				}
				return is_0_;
			}
			if (ba == 24) {
				int[] is_3_ = new int[w * h];
				for (int i_4_ = 0; i_4_ < w * h; i_4_++) {
					is_3_[i_4_] = ByteGetter.get(is[3 * i_4_]);
					is_3_[i_4_] += ByteGetter.get(is[3 * i_4_ + 1]) << 8;
					is_3_[i_4_] += ByteGetter.get(is[3 * i_4_ + 2]) << 16;
					is_3_[i_4_] &= mask;
				}
				return is_3_;
			}
			if (ba == 32) {
				int[] is_5_ = new int[w * h];
				for (int i_6_ = 0; i_6_ < w * h; i_6_++) {
					is_5_[i_6_] = ByteGetter.get(is[4 * i_6_]);
					is_5_[i_6_] += ByteGetter.get(is[4 * i_6_ + 1]) << 8;
					is_5_[i_6_] += ByteGetter.get(is[4 * i_6_ + 2]) << 16;
					is_5_[i_6_] += ByteGetter.get(is[4 * i_6_ + 3]) << 24;
					is_5_[i_6_] &= mask;
				}
				return is_5_;
			}
			System.out.println("BA: " + ba);
		}
		if (pi.equals("RGB")) {
			int[] is_7_ = new int[w * h];
			if (pc == 0) {
				for (int i_8_ = 0; i_8_ < w * h; i_8_++) {
					is_7_[i_8_] = ByteGetter.get(is[3 * i_8_]) << 16;
					is_7_[i_8_] += ByteGetter.get(is[3 * i_8_ + 1]) << 8;
					is_7_[i_8_] += ByteGetter.get(is[3 * i_8_ + 2]);
				}
			} else {
				for (int i_9_ = 0; i_9_ < w * h; i_9_++) {
					is_7_[i_9_] = ByteGetter.get(is[i_9_]) << 16;
					is_7_[i_9_] += ByteGetter.get(is[w * h + i_9_]) << 8;
					is_7_[i_9_] += ByteGetter.get(is[2 * w * h + i_9_]);
				}
			}
			return is_7_;
		}
		return null;
	}

	byte[] getPalette(int i, int i_10_) throws DicomException {
		int i_11_ = dcm.getI(i_10_, 0);
		int i_12_ = dcm.getI(i_10_, 1);
		byte[] is = new byte[256];
		int i_13_;
		if ((i_13_ = dcm.getSize(i)) == 1) {
			byte[] is_14_ = (byte[]) dcm.get(i);
			int i_15_ = ByteGetter.get(is_14_[0]);
			i_15_ += ByteGetter.get(is_14_[1]) << 8;
			for (i_13_ = 0; i_13_ < i_12_; i_13_++)
				is[i_13_] = (byte) (i_15_ >> 8);
			for (int i_16_ = 0; i_16_ < i_11_; i_16_++) {
				i_15_ = ByteGetter.get(is_14_[2 * i_16_]);
				i_15_ += ByteGetter.get(is_14_[2 * i_16_ + 1]) << 8;
				is[i_16_ + i_12_] = (byte) (i_15_ >> 8);
			}
		} else {
			int i_17_ = dcm.getI(i, 0);
			for (i_13_ = 0; i_13_ < i_12_; i_13_++)
				is[i_13_] = (byte) (i_17_ >> 8);
			for (int i_18_ = 0; i_18_ < i_13_; i_18_++) {
				i_17_ = dcm.getI(i, i_18_);
				is[i_18_ + i_12_] = (byte) (i_17_ >> 8);
			}
		}
		return is;
	}

	int calcMax(int[] is) {
		int i = 0;
		for (int i_19_ = 0; i_19_ < is.length; i_19_++) {
			if ((is[i_19_] & mask) > i)
				i = is[i_19_] & mask;
		}
		return i;
	}

	int calcMax(byte[] is) {
		int i = 0;
		if (ba == 8)
			i = 255;
		else if (ba == 16) {
			boolean bool = false;
			for (int i_20_ = 0; i_20_ < is.length / 2; i_20_++) {
				int i_21_ = ByteGetter.get(is[2 * i_20_]) + (ByteGetter.get(is[2 * i_20_ + 1]) << 8);
				if ((i_21_ & mask) > i)
					i = i_21_ & mask;
			}
		} else if (ba == 32) {
			boolean bool = false;
			for (int i_22_ = 0; i_22_ < is.length / 4; i_22_++) {
				int i_23_ = (ByteGetter.get(is[2 * i_22_]) + (ByteGetter.get(is[2 * i_22_ + 1]) << 8)
						+ (ByteGetter.get(is[2 * i_22_ + 2]) << 16) + (ByteGetter
						.get(is[2 * i_22_ + 3]) << 24));
				if ((i_23_ & mask) > i)
					i = i_23_ & mask;
			}
		}
		return i;
	}

	public ImageIO(DicomImage dicomimage) throws DicomException {
		dcm = dicomimage;
		w = dcm.getI(467);
		h = dcm.getI(466);
		ba = dcm.getI(475);
		bs = dcm.getI(476);
		pr = dcm.getI(478);
		mask = (1 << bs) - 1;
		pc = dcm.getI(463);
		if ((pi = dcm.getS(462)) != null)
			pi = pi.trim();
		if ((f = dcm.getI(464)) == 2147483647)
			f = 1;
		if ((max = dcm.getI(480)) == 2147483647)
			max = 0;
		try {
			int i = 0;
			Object object = null;
			String string;
			try {
				string = dcm.getFileMetaInformation().getS(31);
			} catch (Exception exception) {
				string = null;
			}
			if (string != null)
				i = UID.getUIDEntry(string).getConstant();
			setEncoding(i);
		} catch (UnknownUIDException unknownuidexception) {
			throw new DicomException("ImageIO not possible: "
					+ unknownuidexception.getMessage());
		}
	}

	public ImageProducer getImageProducer() throws DicomException {
		return getImageProducer(0);
	}

	public ImageProducer getImageProducer(int i) throws DicomException {
		if (i >= f)
			throw new DicomException("Index exceeds number of frames");
		if (encoding == 8196)
			return Toolkit.getDefaultToolkit().createImage(
					(byte[]) dcm.get(1184, i + 1)).getSource();
		Object object = getPixels(i);
		if (object == null)
			throw new DicomException("Error ocurred building ImageProducer "
					+ i);
		if (pi.trim().equals("MONOCHROME1")) {
			if (ba == 8) {
				GrayColorModel graycolormodel = new GrayColorModel(8);
				return new MemoryImageSource(w, h, graycolormodel,
						(byte[]) object, 0, w);
			}
			if (max == 0)
				max = calcMax((byte[]) dcm.get(1184));
			GrayColorModel graycolormodel = new GrayColorModel(ba, max);
			return new MemoryImageSource(w, h, graycolormodel, (int[]) object,
					0, w);
		}
		if (pi.trim().equals("MONOCHROME2")) {
			if (ba == 8) {
				byte[] is = new byte[256];
				byte[] is_24_ = new byte[256];
				byte[] is_25_ = new byte[256];
				for (int i_26_ = 0; i_26_ < 256; i_26_++)
					is[i_26_] = is_24_[i_26_] = is_25_[i_26_] = (byte) i_26_;
				IndexColorModel indexcolormodel = new IndexColorModel(8, 256,
						is, is_24_, is_25_, -1);
				return new MemoryImageSource(w, h, indexcolormodel,
						(byte[]) object, 0, w);
			}
			if (max == 0)
				max = calcMax((byte[]) dcm.get(1184));
			GrayColorModel graycolormodel = new GrayColorModel(ba, max);
			return new MemoryImageSource(w, h, graycolormodel, (int[]) object,
					0, w);
		}
		if (pi.trim().equals("PALETTE COLOR")) {
			byte[] is = getPalette(498, 494);
			byte[] is_27_ = getPalette(499, 495);
			byte[] is_28_ = getPalette(500, 496);
			IndexColorModel indexcolormodel = new IndexColorModel(8, 256, is,
					is_27_, is_28_);
			return new MemoryImageSource(w, h, indexcolormodel,
					(byte[]) object, 0, w);
		}
		if (pi.equals("RGB")) {
			DirectColorModel directcolormodel = new DirectColorModel(24,
					16711680, 65280, 255);
			return new MemoryImageSource(w, h, directcolormodel,
					(int[]) object, 0, w);
		}
		return null;
	}

	public Vector getImageProducers() throws DicomException {
		Vector vector = new Vector(f);
		for (int i = 0; i < f; i++)
			vector.addElement(getImageProducer(i));
		return vector;
	}

	public synchronized void setImageProducer(ImageProducer imageproducer) {
		DicomImageConsumer var_b = new DicomImageConsumer(this);
		imageproducer.startProduction(var_b);
		if (!var_b.status) {
			try {
				this.wait();
			} catch (InterruptedException interruptedexception) {
				interruptedexception.printStackTrace();
			}
		}
		imageproducer.removeConsumer(var_b);
	}

	public int size() {
		return f;
	}

	public DicomImage getSource() {
		return dcm;
	}

	boolean isDecoded() {
		return decode;
	}
}