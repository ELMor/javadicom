/*
 * GrayColorModel - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.image;

import java.awt.image.ColorModel;

public class GrayColorModel extends ColorModel {
	private int a = 0;

	private int _flddo = 0;

	private int[] _fldif;

	private void a() {
		_fldif = new int[a + 1 - _flddo];
		if (a + 1 == 256) {
			for (int i = 0; i < 256; i++)
				_fldif[i] = (~0xffffff | (i & 0xff) << 16 | (i & 0xff) << 8 | i & 0xff);
		} else {
			boolean bool = false;
			for (int i = _flddo; i < a + 1; i++) {
				int i_0_ = (i - _flddo) * 255 / (a - _flddo);
				_fldif[i - _flddo] = (~0xffffff | (i_0_ & 0xff) << 16
						| (i_0_ & 0xff) << 8 | i_0_ & 0xff);
			}
		}
	}

	private void a(int i, int i_1_) {
		for (int i_2_ = 0; i_2_ < _fldif.length; i_2_++) {
			if (i_2_ < i_1_ - _flddo)
				_fldif[i_2_] = -16777216;
			else if (i_2_ > i_1_ - _flddo + i)
				_fldif[i_2_] = -1;
			else {
				int i_3_ = (i_2_ - (i_1_ - _flddo)) * 255 / i;
				_fldif[i_2_] = (~0xffffff | (i_3_ & 0xff) << 16
						| (i_3_ & 0xff) << 8 | i_3_ & 0xff);
			}
		}
	}

	public GrayColorModel(int i) {
		this(i, (1 << i) - 1);
	}

	public GrayColorModel(int i, int i_4_) {
		super(i);
		pixel_bits = i;
		a = i_4_;
		a();
	}

	public GrayColorModel(int i, int i_5_, int i_6_) {
		super(i);
		pixel_bits = i;
		_flddo = i_5_;
		a = i_6_;
		a();
	}

	public void setWindowLevel(WL wl) {
		a(wl.window, wl.level);
	}

	public int getAlpha(int i) {
		return 255;
	}

	public int getRed(int i) {
		return _fldif[i - _flddo] & 0xff;
	}

	public int getGreen(int i) {
		return _fldif[i - _flddo] & 0xff;
	}

	public int getBlue(int i) {
		return _fldif[i - _flddo] & 0xff;
	}

	public int getRGB(int i) {
		return _fldif[i - _flddo];
	}

	public int getMax() {
		return a;
	}

	public int getMin() {
		return _flddo;
	}
}