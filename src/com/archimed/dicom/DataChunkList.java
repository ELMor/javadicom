/*
 * f - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Enumeration;
import java.util.NoSuchElementException;

class DataChunkList implements Enumeration {
	boolean returnIntegers;

	int size;

	DataChunk[] holder;

	DataChunk nextElement;

	DataChunkList(DataChunk[] var_ps, boolean bool) {
		holder = var_ps;
		returnIntegers = bool;
		size = var_ps.length;
	}

	public boolean hasMoreElements() {
		if (nextElement != null)
			return true;
		while_8_: do {
			do {
				if (size-- <= 0)
					break while_8_;
			} while ((nextElement = holder[size]) == null);
			return true;
		} while (false);
		return false;
	}

	public Object nextElement() {
		if (nextElement == null) {
			while (size-- > 0 && (nextElement = holder[size]) == null) {
				/* empty */
			}
		}
		if (nextElement != null) {
			DataChunk var_p = nextElement;
			nextElement = var_p.next;
			return returnIntegers ? (Object) new Integer(var_p.data2) : var_p.ref;
		}
		throw new NoSuchElementException("IntHashtableEnumerator");
	}
}