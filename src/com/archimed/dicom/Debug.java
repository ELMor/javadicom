/*
 * Debug - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class Debug {
	public static int DEBUG = 0;

	public static void printMessage(String string, int i, int i_0_) {
		System.err.println(string + ": (" + ObjPrinter.a(i, 4) + "," + ObjPrinter.a(i_0_, 4)
				+ ")  " + DDict.getDescription(DDict.lookupDDict(i, i_0_))
				+ ".");
	}

	public static void printMessage(String string) {
		System.err.println(string);
	}
}