/*
 * m - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Enumeration;
import java.util.Vector;

class Meter {
	static int nextPar(int i) {
		if ((i & 0x1) == 1)
			return i + 1;
		return i;
	}

	private static int lessSize(Vector vector) {
		if (vector.size() > 0)
			return vector.size() - 1;
		return 0;
	}

	private static int allStringSizes(Vector vector) {
		int i = 0;
		for (int i_0_ = 0; i_0_ < vector.size(); i_0_++)
			i += ((String) vector.elementAt(i_0_)).length();
		return i;
	}

	private static int allFloatSizes(Vector vector) {
		int i = 0;
		for (int i_1_ = 0; i_1_ < vector.size(); i_1_++)
			i += ((Float) vector.elementAt(i_1_)).toString().length();
		return i;
	}

	private static int allIntegerSizes(Vector vector) {
		int i = 0;
		for (int i_2_ = 0; i_2_ < vector.size(); i_2_++)
			i += ((Integer) vector.elementAt(i_2_)).toString().length();
		return i;
	}

	private static int allPersonSizes(Vector vector) {
		int i = 0;
		for (int i_3_ = 0; i_3_ < vector.size(); i_3_++)
			i += ((Person) vector.elementAt(i_3_)).toDICOMString('^').length();
		return i;
	}

	private static int dateRangeSize(Vector vector) {
		boolean bool = false;
		if (vector.size() == 0)
			return 0;
		int i;
		try {
			i = ((DDateRange) vector.elementAt(0)).toDICOMString().length();
		} catch (ClassCastException classcastexception) {
			i = vector.size() * 8;
		}
		return i;
	}

	private static int byteArrayInVectorSize(Vector vector) {
		int i = 0;
		for (int i_4_ = 0; i_4_ < vector.size(); i_4_++)
			i += ((byte[]) vector.elementAt(i_4_)).length;
		return i;
	}

	static int dicomObjectLength(DicomObject dicomobject, int i, boolean bool) {
		boolean bool_5_ = false;
		int i_6_;
		if (bool)
			i_6_ = 16;
		else
			i_6_ = 8;
		Enumeration enumeration = dicomobject.enumerateVRs(false, i, bool);
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) enumeration.nextElement();
			i_6_ += is8193Bool(var_c.val, var_c._fldif, i, bool);
			i_6_ += ddictTypeLength(var_c.val, var_c._fldif, i, bool);
		}
		return i_6_;
	}

	private static int vectorOfDicomObjectLength(Vector vector, int i, boolean bool) {
		int acum = 0;
		for (int i_8_ = 0; i_8_ < vector.size(); i_8_++) {
			DicomObject dicomobject = (DicomObject) vector.elementAt(i_8_);
			dicomobject.length = dicomObjectLength(dicomobject, i, bool);
			acum += dicomobject.length;
		}
		return acum;
	}

	private static int length02(Vector vector, boolean bool) {
		int i;
		if (bool) {
			i = 8;
			int i_9_ = 16;
		} else {
			i = 0;
			int i_10_ = 8;
		}
		return i;
	}

	private static int length03(Vector vector) {
		if (vector.size() == 1 || vector.size() == 0)
			return 0;
		return 8 + vector.size() * 8;
	}

	private static int vectorOfFloatLength(Vector vector) {
		int i = 0;
		for (int i_11_ = 0; i_11_ < vector.size(); i_11_++)
			i += ((float[]) vector.elementAt(i_11_)).length * 4;
		return i;
	}

	static int ddictTypeLength(Vector vector, int i, int i_12_, boolean bool) {
		int i_13_ = DDict.getTypeFixed(i);
		boolean bool_14_ = false;
		if (i_13_ != 0)
			return i_13_ * vector.size();
		int aux;
		switch (i) {
		case 2:
		case 4:
		case 6:
		case 7:
		case 9:
		case 12:
		case 13:
		case 18:
		case 27:
		case 28:
			aux = allStringSizes(vector);
			break;
		case 16:
			if (Jdt.isDSAsString())
				aux = allStringSizes(vector);
			else
				aux = allFloatSizes(vector);
			break;
		case 15:
			aux = allIntegerSizes(vector);
			break;
		case 14:
			aux = allPersonSizes(vector);
			break;
		case 11:
			aux = dateRangeSize(vector);
			break;
		case 10:
			return nextPar(vectorOfDicomObjectLength(vector, i_12_, bool) + length02(vector, bool));
		case 8:
		case 22:
		case 24:
			return nextPar(byteArrayInVectorSize(vector) + length03(vector));
		case 29:
			aux = vectorOfFloatLength(vector);
			break;
		default:
			aux = byteArrayInVectorSize(vector);
		}
		aux += lessSize(vector);
		return nextPar(aux);
	}

	private static int is8193(int i, int i_16_) {
		if (i_16_ == 8193)
			return 8;
		switch (i) {
		case 0:
		case 8:
		case 10:
		case 22:
		case 24:
			return 12;
		default:
			return 8;
		}
	}

	static int is8193Bool(Vector vector, int i, int i_17_, boolean bool) {
		return is8193(i, i_17_);
	}

	static int a(VRGroup var_d, int i, boolean bool) {
		int i_18_ = 0;
		Enumeration enumeration = var_d.elements();
		while (enumeration.hasMoreElements()) {
			VR var_c = (VR) enumeration.nextElement();
			i_18_ += is8193Bool(var_c.val, var_c._fldif, i, bool);
			i_18_ += ddictTypeLength(var_c.val, var_c._fldif, i, bool);
		}
		return i_18_;
	}
}