/*
 * DDictEntry - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class DDictEntry {
	int g;

	int e;

	int type;

	final String desc;

	final String vm;

	public DDictEntry(int i, int i_0_, int i_1_, String string, String string_2_) {
		g = i;
		e = i_0_;
		type = i_1_;
		desc = string;
		vm = string_2_;
	}

	public int getGroup() {
		return g;
	}

	public int getElement() {
		return e;
	}

	public String getDescription() {
		return desc;
	}

	public int getType() {
		return type;
	}

	public String getVM() {
		return vm;
	}
}