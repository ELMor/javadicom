/*
 * DicomException - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class DicomException extends Exception {
	public DicomException() {
		/* empty */
	}

	public DicomException(String string) {
		super(string);
	}

	public DicomException(int i, int i_0_, String string) {
		super("(" + i + "," + i_0_ + "): " + string);
	}

	public DicomException(int i, String string) {
		super(DDict.getDescription(i) + ": " + string);
	}
}