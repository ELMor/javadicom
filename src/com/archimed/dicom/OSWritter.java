/*
 * a - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

class OSWritter extends FOS4 {
	Vector _fldbyte;

	OSWritter(OutputStream outputstream) {
		super(outputstream);
	}

	OSWritter(OutputStream outputstream, int i) {
		super(outputstream, i);
	}

	private int _mthfor(int i) {
		switch (i) {
		case 2:
		case 8:
		case 22:
		case 24:
			return 0;
		default:
			return 32;
		}
	}

	int _mthif(Vector vector, int i) throws IOException {
		int i_0_ = 0;
		for (int i_1_ = 0; i_1_ < vector.size(); i_1_++)
			i_0_ += this.a(vector.elementAt(i_1_), i);
		return i_0_;
	}

	String _mthif(Object object, int i) {
		switch (i) {
		case 2:
		case 4:
		case 6:
		case 7:
		case 9:
		case 12:
		case 13:
		case 18:
		case 27:
		case 28:
			return (String) object;
		case 16:
			return object.toString();
		case 15:
			return ((Integer) object).toString();
		case 14:
			return ((Person) object).toDICOMString('^');
		case 11: {
			String string;
			try {
				string = ((DDate) object).toDICOMString();
			} catch (ClassCastException classcastexception) {
				return ((DDateRange) object).toDICOMString();
			}
			return string;
		}
		case 0:
			return new String((byte[]) object);
		default:
			return null;
		}
	}

	int a(Vector vector, int i) throws IOException {
		int i_2_ = 0;
		for (int i_3_ = 0; i_3_ < vector.size(); i_3_++) {
			String string = _mthif(vector.elementAt(i_3_), i);
			i_2_ += this.a(string);
			if (i_3_ != vector.size() - 1) {
				write(92);
				i_2_++;
			}
		}
		return i_2_;
	}

	int _mthdo(Vector vector, int i) throws IOException {
		byte[] is = (byte[]) vector.elementAt(0);
		if (i == 24 && this._mthif() == 8195) {
			byte[] is_4_ = new byte[is.length];
			System.arraycopy(is, 0, is_4_, 0, is_4_.length);
			ByteSwapper.swapBytes(is_4_);
			write(is_4_);
			return is_4_.length;
		}
		write(is);
		return is.length;
	}

	int a(Vector vector) throws IOException {
		float[] fs = (float[]) vector.elementAt(0);
		for (int i = 0; i < fs.length; i++)
			this._mthdo(Float.floatToIntBits(fs[i]));
		return fs.length * 4;
	}

	int _mthfor(Vector vector, int i) throws IOException {
		int i_5_ = DDict.getTypeFixed(i);
		int i_6_ = 0;
		if (i_5_ != 0)
			return _mthif(vector, i);
		switch (i) {
		case 0:
		case 8:
		case 22:
		case 24:
			i_6_ += _mthdo(vector, i);
			break;
		case 29:
			i_6_ += a(vector);
			break;
		default:
			i_6_ += a(vector, i);
		}
		if (i_6_ % 2 == 1) {
			write(_mthfor(i));
			i_6_++;
		}
		return i_6_;
	}
}