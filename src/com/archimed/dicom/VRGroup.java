/*
 * d - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Dictionary;
import java.util.Enumeration;

class VRGroup extends Dictionary implements Cloneable {
	private DataChunk[] _flddo;

	private int _fldif;

	private int a;

	private float _fldfor;

	public VRGroup(int i, float f) {
		if (i <= 0 || (double) f <= 0.0)
			throw new IllegalArgumentException();
		_fldfor = f;
		_flddo = new DataChunk[i];
		a = (int) ((float) i * f);
	}

	public VRGroup(int i) {
		this(i, 0.75F);
	}

	public VRGroup() {
		this(101, 0.75F);
	}

	public int size() {
		return _fldif;
	}

	public boolean isEmpty() {
		return _fldif == 0;
	}

	public synchronized Enumeration keys() {
		return new DataChunkList(_flddo, true);
	}

	public synchronized Enumeration elements() {
		return new DataChunkList(_flddo, false);
	}

	public synchronized boolean a(Object object) {
		if (object == null)
			throw new NullPointerException();
		DataChunk[] var_ps = _flddo;
		int i = var_ps.length;
		while (i-- > 0) {
			for (DataChunk var_p = var_ps[i]; var_p != null; var_p = var_p.next) {
				if (var_p.ref.equals(object))
					return true;
			}
		}
		return false;
	}

	public synchronized boolean _mthdo(int i) {
		DataChunk[] var_ps = _flddo;
		int i_0_ = i;
		int i_1_ = (i_0_ & 0x7fffffff) % var_ps.length;
		for (DataChunk var_p = var_ps[i_1_]; var_p != null; var_p = var_p.next) {
			if (var_p.data1 == i_0_ && var_p.data2 == i)
				return true;
		}
		return false;
	}

	public synchronized Object _mthif(int i) {
		DataChunk[] var_ps = _flddo;
		int i_2_ = i;
		int i_3_ = (i_2_ & 0x7fffffff) % var_ps.length;
		for (DataChunk var_p = var_ps[i_3_]; var_p != null; var_p = var_p.next) {
			if (var_p.data1 == i_2_ && var_p.data2 == i)
				return var_p.ref;
		}
		return null;
	}

	public Object get(Object object) {
		if (!(object instanceof Integer))
			throw new InternalError("key is not an Integer");
		Integer integer = (Integer) object;
		int i = integer.intValue();
		return _mthif(i);
	}

	protected void _mthif() {
		int i = _flddo.length;
		DataChunk[] var_ps = _flddo;
		int i_4_ = i * 2 + 1;
		DataChunk[] var_ps_5_ = new DataChunk[i_4_];
		a = (int) ((float) i_4_ * _fldfor);
		_flddo = var_ps_5_;
		int i_6_ = i;
		while (i_6_-- > 0) {
			DataChunk var_p = var_ps[i_6_];
			while (var_p != null) {
				DataChunk var_p_7_ = var_p;
				var_p = var_p.next;
				int i_8_ = (var_p_7_.data1 & 0x7fffffff) % i_4_;
				var_p_7_.next = var_ps_5_[i_8_];
				var_ps_5_[i_8_] = var_p_7_;
			}
		}
	}

	public synchronized Object a(int i, Object object) {
		if (object == null)
			throw new NullPointerException();
		DataChunk[] var_ps = _flddo;
		int i_9_ = i;
		int i_10_ = (i_9_ & 0x7fffffff) % var_ps.length;
		for (DataChunk var_p = var_ps[i_10_]; var_p != null; var_p = var_p.next) {
			if (var_p.data1 == i_9_ && var_p.data2 == i) {
				Object object_11_ = var_p.ref;
				var_p.ref = object;
				return object_11_;
			}
		}
		if (_fldif >= a) {
			_mthif();
			return a(i, object);
		}
		DataChunk var_p = new DataChunk();
		var_p.data1 = i_9_;
		var_p.data2 = i;
		var_p.ref = object;
		var_p.next = var_ps[i_10_];
		var_ps[i_10_] = var_p;
		_fldif++;
		return null;
	}

	public Object put(Object object, Object object_12_) {
		if (!(object instanceof Integer))
			throw new InternalError("key is not an Integer");
		Integer integer = (Integer) object;
		int i = integer.intValue();
		return a(i, object_12_);
	}

	public synchronized Object a(int i) {
		DataChunk[] var_ps = _flddo;
		int i_13_ = i;
		int i_14_ = (i_13_ & 0x7fffffff) % var_ps.length;
		DataChunk var_p = var_ps[i_14_];
		DataChunk var_p_15_ = null;
		for (/**/; var_p != null; var_p = var_p.next) {
			if (var_p.data1 == i_13_ && var_p.data2 == i) {
				if (var_p_15_ != null)
					var_p_15_.next = var_p.next;
				else
					var_ps[i_14_] = var_p.next;
				_fldif--;
				return var_p.ref;
			}
			var_p_15_ = var_p;
		}
		return null;
	}

	public Object remove(Object object) {
		if (!(object instanceof Integer))
			throw new InternalError("key is not an Integer");
		Integer integer = (Integer) object;
		int i = integer.intValue();
		return a(i);
	}

	public synchronized void a() {
		DataChunk[] var_ps = _flddo;
		int i = var_ps.length;
		while (--i >= 0)
			var_ps[i] = null;
		_fldif = 0;
	}

	public synchronized Object clone() {
		VRGroup var_d_16_;
		try {
			VRGroup var_d_17_ = (VRGroup) super.clone();
			var_d_17_._flddo = new DataChunk[_flddo.length];
			int i = _flddo.length;
			while (i-- > 0)
				var_d_17_._flddo[i] = _flddo[i] != null ? (DataChunk) _flddo[i].clone()
						: null;
			var_d_16_ = var_d_17_;
		} catch (CloneNotSupportedException clonenotsupportedexception) {
			throw new InternalError();
		}
		return var_d_16_;
	}

	public synchronized String toString() {
		int i = size() - 1;
		StringBuffer stringbuffer = new StringBuffer();
		Enumeration enumeration = keys();
		Enumeration enumeration_18_ = elements();
		stringbuffer.append("{");
		for (int i_19_ = 0; i_19_ <= i; i_19_++) {
			String string = enumeration.nextElement().toString();
			String string_20_ = enumeration_18_.nextElement().toString();
			stringbuffer.append(string + "=" + string_20_);
			if (i_19_ < i)
				stringbuffer.append(", ");
		}
		stringbuffer.append("}");
		return stringbuffer.toString();
	}
}