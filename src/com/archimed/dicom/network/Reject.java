/*
 * Reject - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import com.archimed.tool.StringTool;

public class Reject extends Response {
	public static int REJECTED_PERMANENT = 1;

	public static int REJECTED_TRANSIENT = 2;

	public static int DICOM_UL_SERVICE_USER = 1;

	public static int DICOM_UL_SERVICE_PROVIDER_ACSE = 2;

	public static int DICOM_UL_SERVICE_PROVIDER_PRESENTATION = 3;

	public static int USER_NO_REASON_GIVEN = 1;

	public static int USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED = 2;

	public static int USER_CALLING_AETITLE_NOT_RECOGNIZED = 3;

	public static int USER_CALLED_AETITLE_NOT_RECOGNIZED = 7;

	public static int ACSE_NO_REASON_GIVEN = 1;

	public static int ACSE_PROTOCOL_NOT_SUPPORTED = 2;

	public static int PRESENTATION_TEMPORARY_CONGESTION = 1;

	public static int PRESENTATION_LOCAL_LIMIT_EXCEEDED = 2;

	private int result;

	private int source;

	private int reason;

	public Reject(int i, int i_0_, int i_1_) {
		result = i;
		source = i_0_;
		reason = i_1_;
	}

	public int getResult() {
		return result;
	}

	public int getSource() {
		return source;
	}

	public int getReason() {
		return reason;
	}

	public String toString() {
		int i = 15;
		String string = "******************* ASSOCIATE REJECT *******************\n";
		string += StringTool.fstr("result:", i);
		if (result == REJECTED_PERMANENT)
			string += "REJECTED_PERMANENT";
		else if (result == REJECTED_TRANSIENT)
			string += "REJECTED_TRANSIENT";
		else
			string += "UNKNOWN";
		string += "\n";
		string += StringTool.fstr("source:", i);
		if (source == DICOM_UL_SERVICE_USER)
			string += "DICOM_UL_SERVICE_USER";
		else if (source == DICOM_UL_SERVICE_PROVIDER_ACSE)
			string += "DICOM_UL_SERVICE_PROVIDER_ACSE";
		else if (source == DICOM_UL_SERVICE_PROVIDER_PRESENTATION)
			string += "DICOM_UL_SERVICE_PROVIDER_PRESENTATION";
		else
			string += "UNKNOWN";
		string += "\n";
		string += StringTool.fstr("reason:", i);
		if (reason == USER_NO_REASON_GIVEN)
			string += "NO_REASON_GIVEN";
		else if (reason == USER_APPLICATIONCONTEXTNAME_NOT_SUPPORTED)
			string += "APPLICATIONCONTEXTNAME_NOT_SUPPORTED";
		else if (reason == USER_CALLING_AETITLE_NOT_RECOGNIZED)
			string += "CALLING_AETITLE_NOT_RECOGNIZED";
		else if (reason == USER_CALLED_AETITLE_NOT_RECOGNIZED)
			string += "CALLED_AETITLE_NOT_RECOGNIZED";
		else if (reason == ACSE_NO_REASON_GIVEN)
			string += "NO_REASON_GIVEN";
		else if (reason == ACSE_PROTOCOL_NOT_SUPPORTED)
			string += "PROTOCOL_NOT_SUPPORTED";
		else if (reason == PRESENTATION_TEMPORARY_CONGESTION)
			string += "TEMPORARY_CONGESTION";
		else if (reason == PRESENTATION_LOCAL_LIMIT_EXCEEDED)
			string += "LOCAL_LIMIT_EXCEEDED";
		else
			string += "UNKNOWN";
		string += "\n";
		string += "********************************************************";
		return string;
	}
}