/*
 * n - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.StringTool;

class Type64 {
	private byte a = 64;

	private UIDEntry id;

	public Type64() {
		/* empty */
	}

	public Type64(UIDEntry uidentry) {
		id = uidentry;
	}

	public UIDEntry getUID() {
		return id;
	}

	public int length() {
		return id.getValue().length() + 4;
	}

	void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(a);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(id.getValue().length());
		dataoutputstream.write(id.getValue().getBytes());
	}

	int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != 64)
			throw new IllegalValueException(
					"itemtype field of received Transfer Syntax Item not " + a);
		datainputstream.read();
		char c = datainputstream.readChar();
		byte[] is = new byte[c];
		datainputstream.readFully(is);
		is = StringTool.trimZeros(is);
		String string = new String(is);
		try {
			id = UID.getUIDEntry(string);
		} catch (UnknownUIDException unknownuidexception) {
			id = new UIDEntry(0, string, "unknown uid", "??", 3);
		}
		return '\004' + c;
	}
}