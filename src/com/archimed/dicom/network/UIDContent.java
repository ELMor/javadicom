/*
 * g - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import com.archimed.dicom.UIDEntry;

public class UIDContent {
	private UIDEntry uid;

	private byte[] content;

	public UIDEntry getUID() {
		return uid;
	}

	public void setUID(UIDEntry uidentry) {
		uid = uidentry;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] is) {
		content = is;
	}
}