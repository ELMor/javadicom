/*
 * q - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;

import com.archimed.dicom.IllegalValueException;

class Type3 {
	private int tipo = 3;

	private int data1;

	private int data4;

	private int data2;

	private int data3;

	public Type3() {
		/* empty */
	}

	public Type3(int i, int i_0_, int i_1_) {
		data4 = i;
		data2 = i_0_;
		data3 = i_1_;
	}

	public Reject getReject() {
		Reject reject = new Reject(data4, data2, data3);
		return reject;
	}

	public void read(InputStream inputstream) throws IOException,
			IllegalValueException {
		PushbackInputStream pushbackinputstream = new PushbackInputStream(
				inputstream);
		DataInputStream datainputstream = new DataInputStream(
				pushbackinputstream);
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"PDU-type field of Associate Reject PDU not " + tipo);
		datainputstream.read();
		data1 = datainputstream.readInt();
		datainputstream.read();
		data4 = inputstream.read();
		data2 = inputstream.read();
		data3 = inputstream.read();
	}

	public void write(OutputStream outputstream) throws IOException {
		DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
		outputstream.write(tipo);
		outputstream.write(0);
		dataoutputstream.writeInt(4);
		outputstream.write(0);
		outputstream.write(data4);
		outputstream.write(data2);
		outputstream.write(data3);
		outputstream.flush();
	}
}