/*
 * e - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.StringTool;

class Type86 extends ItemType {
	private int tipo = 86;

	private UIDEntry uidEntry;

	private byte[] content;

	public Type86() {
		content = new byte[0];
	}

	public Type86(UIDEntry uidentry, byte[] is) {
		content = new byte[0];
		if (is == null)
			throw new NullPointerException();
		uidEntry = uidentry;
		content = is;
	}

	public byte[] getBytes() {
		return content;
	}

	public UIDEntry getUIDEntry() {
		return uidEntry;
	}

	public int getSize() {
		return 6 + uidEntry.getValue().length() + content.length;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(2 + uidEntry.getValue().length()
				+ content.length);
		dataoutputstream.writeChar(uidEntry.getValue().length());
		dataoutputstream.write(uidEntry.getValue().getBytes());
		dataoutputstream.write(content);
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException, UnknownUIDException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received Implementation Version Name Item  not "
							+ tipo);
		datainputstream.read();
		char c = datainputstream.readChar();
		char c_0_ = datainputstream.readChar();
		byte[] is = new byte[c_0_];
		datainputstream.read(is);
		is = StringTool.trimZeros(is);
		String string = new String(is);
		try {
			uidEntry = UID.getUIDEntry(string);
		} catch (UnknownUIDException unknownuidexception) {
			uidEntry = new UIDEntry(0, string, "unknown uid", "??", 1);
		}
		content = new byte[c - c_0_ - '\002'];
		datainputstream.read(content);
		return '\004' + c;
	}
}