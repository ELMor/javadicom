/*
 * o - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.StringTool;

class Type16 {
	private byte tipo = 16;

	private String id = "1.2.840.10008.3.1.1.1";

	int length() {
		return id.length() + 4;
	}

	void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(id.length());
		dataoutputstream.write(id.getBytes());
	}

	protected int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		boolean bool = false;
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received Application Context Item  not "
							+ tipo);
		datainputstream.read();
		char c = datainputstream.readChar();
		byte[] is = new byte[c];
		datainputstream.readFully(is);
		is = StringTool.trimZeros(is);
		if (!id.equals(new String(is)))
			throw new IllegalValueException(
					"received Application Context Name  not " + id);
		return '\004' + c;
	}
}