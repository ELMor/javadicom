/*
 * d - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;

import com.archimed.dicom.IllegalValueException;

class Type7 {
	private int tipo = 7;

	private int _fldif;

	private int data1;

	private int data2;

	public Type7() {
		/* empty */
	}

	public Type7(int i, int i_0_) {
		data1 = i;
		data2 = i_0_;
	}

	public Abort getAbort() {
		Abort abort = new Abort(data1, data2);
		return abort;
	}

	public void read(InputStream inputstream) throws IOException,
			IllegalValueException {
		PushbackInputStream pushbackinputstream = new PushbackInputStream(
				inputstream);
		DataInputStream datainputstream = new DataInputStream(
				pushbackinputstream);
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException("PDU-type field of Abort PDU not "
					+ tipo);
		inputstream.read();
		_fldif = datainputstream.readInt();
		inputstream.read();
		data1 = inputstream.read();
		data2 = inputstream.read();
	}

	public void write(OutputStream outputstream) throws IOException {
		DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
		outputstream.write(tipo);
		outputstream.write(0);
		dataoutputstream.writeInt(4);
		outputstream.write(0);
		outputstream.write(0);
		outputstream.write(data1);
		outputstream.write(data2);
	}
}