/*
 * b - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;

class Type81 extends ItemType {
	private int tipo = 81;

	private int data1;

	public Type81() {
		/* empty */
	}

	public Type81(int i) {
		data1 = i;
	}

	public int getSize() {
		return 8;
	}

	public int getData() {
		return data1;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(4);
		dataoutputstream.writeInt(data1);
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received Implementation Class UID Item  not "
							+ tipo);
		datainputstream.read();
		char c = datainputstream.readChar();
		data1 = datainputstream.readInt();
		return c + '\004';
	}
}