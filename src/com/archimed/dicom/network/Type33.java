/*
 * h - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;

class Type33 {
	private byte tipo = 33;

	private int _fldint;

	private int size;

	private int mode;

	Type64 t64;

	public Type33() {
		t64 = new Type64();
	}

	public Type33(int i, int i_0_, Type64 var_n) {
		t64 = new Type64();
		_fldint = i;
		mode = i_0_;
		t64 = var_n;
		size = 4;
		if (var_n != null)
			size += var_n.length();
	}

	public int _mthif() {
		return _fldint;
	}

	public int getMode() {
		return mode;
	}

	public UIDEntry getUID() {
		return t64.getUID();
	}

	public int length() {
		return size + 4;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException,
			IllegalValueException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(size);
		dataoutputstream.write(_fldint);
		dataoutputstream.write(0);
		dataoutputstream.write(mode);
		dataoutputstream.write(0);
		if (t64 != null)
			t64.write(dataoutputstream);
	}

	public int read(DataInputStream datainputstream) throws IllegalValueException,
			IOException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"itemtype field of received Presentation Context Item  not "
							+ tipo);
		datainputstream.read();
		size = datainputstream.readChar();
		_fldint = datainputstream.read();
		datainputstream.read();
		mode = datainputstream.read();
		datainputstream.read();
		if (mode == 0)
			t64.read(datainputstream);
		else
			datainputstream.readFully(new byte[size - 4]);
		return size + 4;
	}
}