/*
 * i - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;

class Type2 {
	private int tipo = 2;

	private int size;

	private int aux;

	private String called;

	private String calling;

	private Type16 t16;

	private Type80 t80;

	private Type33 t33;

	private Vector vec;

	private Acknowledge ack;

	public Type2() {
		vec = new Vector();
	}

	public Type2(Acknowledge acknowledge) throws IllegalValueException {
		called = acknowledge.getCalledTitle();
		calling = acknowledge.getCallingTitle();
		t16 = new Type16();
		vec = new Vector();
		for (int i = 0; i < acknowledge.getPresentationContexts(); i++) {
			UIDEntry uidentry = acknowledge.getTransferSyntax(i);
			if (uidentry == null)
				t33 = new Type33(acknowledge.getID(i), acknowledge.getResult(i),
						null);
			else
				t33 = new Type33(acknowledge.getID(i), acknowledge.getResult(i),
						new Type64(uidentry));
			vec.addElement(t33);
		}
		t80 = new Type80();
		Type81 var_b = new Type81(acknowledge.getMaxPduSize());
		Type82 var_a = new Type82(acknowledge.getImplementationClassUID());
		Type85 var_k = new Type85(acknowledge.getImplementationVersionName());
		t80.add(var_b);
		t80.add(var_a);
		t80.add(var_k);
		for (int i = 0; i < acknowledge.getRoles(); i++) {
			UIDEntry uidentry = acknowledge.getAbstractSyntaxForRole(i);
			int i_0_ = acknowledge.getScuRole(uidentry.getConstant());
			int i_1_ = acknowledge.getScpRole(uidentry.getConstant());
			if (i_0_ != -1 && i_1_ != -1) {
				Type84 var_j = new Type84(uidentry, i_0_, i_1_);
				t80.add(var_j);
			}
		}
		UIDContent[] var_gs = acknowledge.getExtendedNegotiations();
		if (var_gs != null) {
			for (int i = 0; i < var_gs.length; i++) {
				Type86 var_e = new Type86(var_gs[i].getUID(), var_gs[i].getContent());
				t80.add(var_e);
			}
		}
		if (acknowledge.getMaxOperationsInvoked() != 1
				|| acknowledge.getMaxOperationsPerformed() != 1) {
			Type83 var_r = new Type83();
			var_r.setData1(acknowledge.getMaxOperationsInvoked());
			var_r.setData2(acknowledge.getMaxOperationsPerformed());
			t80.add(var_r);
		}
		size = 68;
		size += t16.length();
		for (int i = 0; i < vec.size(); i++)
			size += ((Type33) vec.elementAt(i)).length();
		size += t80.length();
	}

	public Acknowledge getAck() {
		return ack;
	}

	private void parse() throws IllegalValueException {
		ack = new Acknowledge();
		ack.setCalledTitle(called);
		ack.setCallingTitle(calling);
		Vector vector = t80.getVector();
		for (int i = 0; i < vec.size(); i++) {
			Type33 t33 = (Type33) vec.elementAt(i);
			if (t33.getMode() == 0)
				ack.addPresentationContext(t33._mthif(),
						t33.getMode(), t33.getUID().getConstant());
			else
				ack.addPresentationContext(t33._mthif(),
						t33.getMode(), 8193);
		}
		Vector v2 = new Vector();
		for (int i = 0; i < vector.size(); i++) {
			ItemType it = (ItemType) vector.elementAt(i);
			if (it instanceof Type82)
				ack.setImplementationClassUID(((Type82) it).getData());
			else if (it instanceof Type85)
				ack.setImplementationVersionName(((Type85) it).getName());
			else if (it instanceof Type81)
				ack.setMaxPduSize(((Type81) it).getData());
			else if (it instanceof Type84) {
				Type84 var_j = (Type84) it;
				ack.setScuScpRoleSelection(var_j.getUID().getConstant(),
						var_j._mthnew(), var_j._mthtry());
			} else if (it instanceof Type83) {
				Type83 var_r = (Type83) it;
				ack.setMaxOperationsInvoked(var_r.getData1());
				ack.setMaxOperationsPerformed(var_r.getData2());
			} else if (it instanceof Type86) {
				Type86 var_e = (Type86) it;
				UIDContent var_g = new UIDContent();
				var_g.setUID(var_e.getUIDEntry());
				var_g.setContent(var_e.getBytes());
				v2.add(var_g);
			}
		}
		UIDContent[] var_gs = new UIDContent[v2.size()];
		for (int i = 0; i < v2.size(); i++)
			var_gs[i] = (UIDContent) v2.elementAt(i);
		ack.setExtendedNegotiations(var_gs);
	}

	public int length() {
		return size + 6;
	}

	public Vector getVector() {
		return vec;
	}

	public void write(OutputStream outputstream) throws IOException,
			IllegalValueException {
		DataOutputStream dataoutputstream = new DataOutputStream(outputstream);
		outputstream.write(tipo);
		outputstream.write(0);
		dataoutputstream.writeInt(length() - 6);
		dataoutputstream.writeChar(1);
		dataoutputstream.writeChar(0);
		outputstream.write(called.getBytes());
		for (int i = 0; i < 16 - called.length(); i++)
			outputstream.write(32);
		outputstream.write(calling.getBytes());
		for (int i = 0; i < 16 - calling.length(); i++)
			outputstream.write(32);
		outputstream.write(new byte[32]);
		t16.write(dataoutputstream);
		for (int i = 0; i < vec.size(); i++) {
			Type33 var_h = (Type33) vec.elementAt(i);
			var_h.write(dataoutputstream);
		}
		t80.write(dataoutputstream);
	}

	public void read(InputStream inputstream) throws IOException,
			IllegalValueException, UnknownUIDException {
		PushbackInputStream pushbackinputstream = new PushbackInputStream(
				inputstream);
		DataInputStream datainputstream = new DataInputStream(
				pushbackinputstream);
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException(
					"PDU-type field of Associate Acknowledge PDU not "
							+ tipo);
		datainputstream.read();
		size = datainputstream.readInt();
		aux = datainputstream.readChar();
		size -= 2;
		datainputstream.readFully(new byte[66]);
		size -= 66;
		Type16 var_o = new Type16();
		size -= var_o.read(datainputstream);
		while (size > 0) {
			i = datainputstream.read();
			if (i == 33) {
				pushbackinputstream.unread(33);
				t33 = new Type33();
				size -= t33.read(datainputstream);
				vec.addElement(t33);
			} else if (i == 80) {
				pushbackinputstream.unread(80);
				t80 = new Type80();
				size -= t80.read(pushbackinputstream);
			}
		}
		parse();
	}
}