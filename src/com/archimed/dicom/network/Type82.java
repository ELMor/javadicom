/*
 * a - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.tool.StringTool;

class Type82 extends ItemType {
	private int _fldvoid = 82;

	private String data;

	public Type82() {
		/* empty */
	}

	public Type82(String string) {
		data = string;
	}

	public int getSize() {
		return 4 + data.length();
	}

	public String getData() {
		return data;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(_fldvoid);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(data.length());
		dataoutputstream.write(data.getBytes());
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != _fldvoid)
			throw new IllegalValueException(
					"itemtype field of received Implementation Class UID Item  not "
							+ _fldvoid);
		datainputstream.read();
		char c = datainputstream.readChar();
		byte[] is = new byte[c];
		datainputstream.readFully(is);
		is = StringTool.trimZeros(is);
		data = new String(is);
		return '\004' + c;
	}
}