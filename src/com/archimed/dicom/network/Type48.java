/*
 * l - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.tool.StringTool;

class Type48 {
	private byte tipo = 48;

	private UIDEntry id;

	public Type48(UIDEntry uidentry) {
		id = uidentry;
	}

	public Type48() {
		/* empty */
	}

	public UIDEntry getUID() {
		return id;
	}

	public int length() {
		return id.getValue().length() + 4;
	}

	public void write(DataOutputStream dataoutputstream) throws IOException {
		dataoutputstream.write(tipo);
		dataoutputstream.write(0);
		dataoutputstream.writeChar(id.getValue().length());
		dataoutputstream.write(id.getValue().getBytes());
	}

	public int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException {
		int i = datainputstream.read();
		if (i != tipo)
			throw new IllegalValueException("wrong itemtype of abstract syntax");
		datainputstream.read();
		char c = datainputstream.readChar();
		byte[] is = new byte[c];
		datainputstream.readFully(is);
		is = StringTool.trimZeros(is);
		String string = new String(is);
		try {
			id = UID.getUIDEntry(string);
		} catch (UnknownUIDException unknownuidexception) {
			id = new UIDEntry(0, string, "unknown uid", "??", 1);
		}
		return '\004' + c;
	}
}