/*
 * Request - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.util.Vector;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.tool.StringTool;

public class Request {
	public static final int NO_SUPPORT_SCU_ROLE = 0;

	public static final int SUPPORT_SCU_ROLE = 1;

	public static final int NO_SUPPORT_SCP_ROLE = 0;

	public static final int SUPPORT_SCP_ROLE = 1;

	public static final int DEFAULT = -1;

	private String _flddo;

	private String _fldgoto;

	private Vector _fldelse;

	private Vector a = new Vector();

	private Vector _fldnull = new Vector();

	private String _fldtry;

	private String _fldchar;

	private Vector _fldif;

	private int _fldnew;

	private int _fldfor;

	private int _fldcase;

	private Vector _fldint;

	private Vector _fldbyte;

	private Vector _fldlong;

	private UIDContent[] _fldvoid;

	public Request() {
		_fldelse = new Vector();
		_fldif = new Vector();
		_fldfor = 1;
		_fldcase = 1;
		_fldint = new Vector();
		_fldbyte = new Vector();
		_fldlong = new Vector();
		setImplementationClassUID(Association.implclassuid);
		setImplementationVersionName(Association.implversionname);
	}

	public String getCallingTitle() {
		return _fldgoto;
	}

	public String getCalledTitle() {
		return _flddo;
	}

	public void setCalledTitle(String string) {
		_flddo = string;
	}

	public void setCallingTitle(String string) {
		_fldgoto = string;
	}

	public String getImplementationClassUID() {
		return _fldtry;
	}

	public String getImplementationVersionName() {
		return _fldchar;
	}

	void setImplementationClassUID(String string) {
		_fldtry = string;
	}

	void setImplementationVersionName(String string) {
		_fldchar = string;
	}

	public int getMaxPduSize() {
		return _fldnew;
	}

	public void setMaxPduSize(int i) {
		_fldnew = i;
	}

	public void setMaxOperationsInvoked(int i) {
		_fldfor = i;
	}

	public int getMaxOperationsInvoked() {
		return _fldfor;
	}

	public void setMaxOperationsPerformed(int i) {
		_fldcase = i;
	}

	public int getMaxOperationsPerformed() {
		return _fldcase;
	}

	public int getScuRole(int i) throws IllegalValueException {
		int i_0_ = _fldint.indexOf(UID.getUIDEntry(i));
		if (i_0_ == -1)
			return -1;
		return ((Integer) _fldbyte.elementAt(i_0_)).intValue();
	}

	UIDEntry getAbstractSyntaxForRole(int i) {
		return (UIDEntry) _fldint.elementAt(i);
	}

	int getRoles() {
		return _fldint.size();
	}

	public int getScpRole(int i) throws IllegalValueException {
		int i_1_ = _fldint.indexOf(UID.getUIDEntry(i));
		if (i_1_ == -1)
			return -1;
		return ((Integer) _fldlong.elementAt(i_1_)).intValue();
	}

	public byte getPresentationContextID(int i) {
		return ((Byte) _fldelse.elementAt(i)).byteValue();
	}

	public int getID(int i) {
		return ((Integer) _fldelse.elementAt(i)).intValue();
	}

	public int getPresentationContexts() {
		return _fldelse.size();
	}

	public UIDEntry getAbstractSyntax(int i) {
		return (UIDEntry) a.elementAt(i);
	}

	public int getTransferSyntaxes(int i) {
		return ((Vector) _fldnull.elementAt(i)).size();
	}

	public UIDEntry getTransferSyntax(int i, int i_2_) {
		return (UIDEntry) ((Vector) _fldnull.elementAt(i)).elementAt(i_2_);
	}

	public byte[] getExtendedNegotiationData(int i) {
		return (byte[]) _fldif.elementAt(i);
	}

	public UIDContent[] getExtendedNegotiations() {
		return _fldvoid;
	}

	public void setExtendedNegotiations(UIDContent[] var_gs) {
		if (var_gs != null)
			_fldvoid = var_gs;
	}

	public void addPresentationContext(int i, int[] is)
			throws IllegalValueException {
		addPresentationContext(2 * a.size() + 1, i, is);
	}

	void addPresentationContext(int i, int i_3_, int[] is)
			throws IllegalValueException {
		UIDEntry uidentry = UID.getUIDEntry(i_3_);
		UIDEntry[] uidentrys = new UIDEntry[is.length];
		for (int i_4_ = 0; i_4_ < is.length; i_4_++)
			uidentrys[i_4_] = UID.getUIDEntry(is[i_4_]);
		addPresentationContext(i, uidentry, uidentrys);
	}

	void addPresentationContext(int i, UIDEntry uidentry, UIDEntry[] uidentrys) {
		a.addElement(uidentry);
		_fldif.addElement(null);
		_fldelse.addElement(new Integer(i));
		Vector vector = new Vector();
		for (int i_5_ = 0; i_5_ < uidentrys.length; i_5_++)
			vector.addElement(uidentrys[i_5_]);
		_fldnull.addElement(vector);
	}

	public void setExtendedNegotiationData(int i, byte[] is) {
		_fldif.setElementAt(is, i);
	}

	public void setScuScpRoleSelection(int i, int i_6_, int i_7_)
			throws IllegalValueException {
		UIDEntry uidentry = UID.getUIDEntry(i);
		int i_8_ = _fldint.indexOf(uidentry);
		if (i_8_ != -1) {
			_fldbyte.setElementAt(new Integer(i_6_), i_8_);
			_fldlong.setElementAt(new Integer(i_7_), i_8_);
		} else {
			_fldint.addElement(uidentry);
			_fldbyte.addElement(new Integer(i_6_));
			_fldlong.addElement(new Integer(i_7_));
		}
	}

	public String toString() {
		int i = 30;
		String string = "****************** ASSOCIATE REQUEST *******************\n";
		string += StringTool.fstr("called title:", i) + _flddo + "\n";
		string += StringTool.fstr("calling title:", i) + _fldgoto + "\n";
		string += (StringTool.fstr("implementation class UID:", i) + _fldtry + "\n");
		string += (StringTool.fstr("implementation version name:", i)
				+ _fldchar + "\n");
		string += StringTool.fstr("max PDU size:", i) + _fldnew + "\n";
		string += StringTool.fstr("max operations invoked:", i) + _fldfor
				+ "\n";
		string += (StringTool.fstr("max operations performed:", i) + _fldcase + "\n");
		string += (StringTool.fstr("# presentation contexts:", i)
				+ _fldelse.size() + "\n");
		string += (StringTool.fstr("nr", 5) + StringTool.fstr("pcid", 6)
				+ StringTool.fstr("abstract syntax", 32)
				+ StringTool.fstr("trasyn", 12)
				+ StringTool.fstr("description", 55) + "\n");
		for (int i_9_ = 0; i_9_ < getPresentationContexts(); i_9_++) {
			string += (StringTool.fstr(i_9_ + "", 5)
					+ StringTool.fstr("" + getID(i_9_), 6) + StringTool.fstr(
					getAbstractSyntax(i_9_).getValue(), 32));
			String string_10_ = "";
			for (int i_11_ = 0; i_11_ < getTransferSyntaxes(i_9_); i_11_++) {
				if (i_11_ > 0)
					string_10_ += ",";
				string_10_ += getTransferSyntax(i_9_, i_11_).getShortName();
			}
			string += StringTool.fstr(string_10_, 12);
			string += (StringTool.fstr(getAbstractSyntax(i_9_).getName(), 55) + "\n");
		}
		string += "non-default roles set:\n";
		boolean bool = false;
		try {
			for (int i_12_ = 0; i_12_ < getRoles(); i_12_++) {
				UIDEntry uidentry = getAbstractSyntaxForRole(i_12_);
				if (getScpRole(uidentry.getConstant()) != -1
						|| getScuRole(uidentry.getConstant()) != -1) {
					bool = true;
					string += StringTool.fstr(uidentry.getName() + "", 55);
					String string_13_ = "";
					if (getScuRole(uidentry.getConstant()) == 1)
						string_13_ = "SCU";
					else if (getScuRole(uidentry.getConstant()) == 0)
						string_13_ = "NO-SCU";
					else if (getScuRole(uidentry.getConstant()) == -1)
						string_13_ = "DEFAULT";
					string_13_ += "/";
					if (getScpRole(uidentry.getConstant()) == 1)
						string_13_ += "SCP";
					else if (getScpRole(uidentry.getConstant()) == 0)
						string_13_ += "NO-SCP";
					else if (getScpRole(uidentry.getConstant()) == -1)
						string_13_ += "DEFAULT";
					string += StringTool.fstr(string_13_, 30) + "\n";
				}
			}
			if (!bool)
				string += "none\n";
		} catch (IllegalValueException illegalvalueexception) {
			illegalvalueexception.printStackTrace();
		}
		string += "********************************************************";
		return string;
	}
}