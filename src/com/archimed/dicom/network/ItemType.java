/*
 * m - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

abstract class ItemType {
	public abstract int getSize();

	public abstract void write(DataOutputStream dataoutputstream)
			throws IOException;

	public abstract int read(DataInputStream datainputstream) throws IOException,
			IllegalValueException, UnknownUIDException;
}