/*
 * Association - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.util.Vector;

import com.archimed.dicom.Debug;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.TransferSyntax;
import com.archimed.dicom.UID;
import com.archimed.dicom.UIDEntry;
import com.archimed.dicom.UnknownUIDException;

public class Association {
	private static final int _fldint = 8;

	private static final int b = 9;

	public static final int PDATA_PDU = 10;

	public static final int RELEASE_REQUEST = 11;

	public static final int ABORT = 12;

	public static final int RELEASE_RESPONSE = 13;

	public static final int ASSOCIATE_REQUEST = 14;

	public static final int ASSOCIATE_ACKNOWLEDGE = 15;

	public static final int ASSOCIATE_REJECT = 16;

	static String implclassuid = "1.2.826.0.1.3680043.2.60.0.1";

	static String implversionname = "softlink_jdt116";

	private String _fldbyte;

	private String e;

	private int pduSize;

	private InputStream is;

	private OutputStream os;

	private PushbackInputStream pbis;

	private Request req;

	private Acknowledge ack;

	private ByteArrayOutputStream baos1;

	private ByteArrayOutputStream baos2;

	private Type1 t1;

	private Type2 t2;

	private Vector vec;

	private boolean waitingForInput = false;

	private boolean c = false;

	private int _fldlong;

	private boolean seqUndefLen = true;

	public Association(InputStream inputstream, OutputStream outputstream) {
		is = inputstream;
		os = outputstream;
		pbis = new PushbackInputStream(inputstream);
		baos1 = new ByteArrayOutputStream();
		baos2 = new ByteArrayOutputStream();
	}

	public void setImplementationVersionName(String string) {
		if (this != null) {
			/* empty */
		}
		implversionname = string;
	}

	public String getImplementationVersionName() {
		if (this != null) {
			/* empty */
		}
		return implversionname;
	}

	public void setImplementationClassUID(String string) {
		if (this != null) {
			/* empty */
		}
		implclassuid = string;
	}

	public String getImplementationClassUID() {
		if (this != null) {
			/* empty */
		}
		return implclassuid;
	}

	public void setSequenceUndefLen(boolean bool) {
		seqUndefLen = bool;
	}

	public boolean isSequenceUndefLen() {
		return seqUndefLen;
	}

	public DicomObject receiveCommand() throws IOException,
			IllegalValueException, DicomException, UnknownUIDException {
		byte[] is = receiveCommandAsByteArray();
		if (is != null) {
			DicomObject dicomobject = new DicomObject();
			dicomobject.read(new ByteArrayInputStream(is), 8193, true);
			Object object = null;
			return dicomobject;
		}
		return null;
	}

	public byte[] receiveCommandAsByteArray() throws IOException,
			IllegalValueException {
		if (waitingForInput == true) {
			byte[] is = baos1.toByteArray();
			baos1.reset();
			waitingForInput = false;
			return is;
		}
		if (communicate() == 8) {
			byte[] is = baos1.toByteArray();
			baos1.reset();
			waitingForInput = false;
			return is;
		}
		return null;
	}

	public DicomObject receiveData() throws IOException, IllegalValueException,
			DicomException, UnknownUIDException {
		byte[] is = receiveDataAsByteArray();
		int i = 8193;
		int i_0_ = getCurrentPresentationContext();
		if (Debug.DEBUG > 0)
			Debug.printMessage("current presentation context: " + i_0_);
		try {
			if (ack != null) {
				for (int i_1_ = 0; i_1_ < ack.getPresentationContexts(); i_1_++) {
					if (i_0_ == ack.getID(i_1_))
						i = ack.getTransferSyntax(i_1_).getConstant();
				}
			}
			if (Debug.DEBUG > 0)
				Debug.printMessage("current transfer syntax: "
						+ TransferSyntax.getUIDEntry(i).getName());
		} catch (Throwable throwable) {
			if (Debug.DEBUG > 0) {
				throwable.printStackTrace();
				System.err
						.println("could not derive transfer syntax from current pc "
								+ i_0_ + ", trying with ImplicitLittleEndian");
			}
		}
		if (is != null) {
			DicomObject dicomobject = new DicomObject();
			dicomobject.read(new ByteArrayInputStream(is), i, true);
			Object object = null;
			return dicomobject;
		}
		return null;
	}

	public byte[] receiveDataAsByteArray() throws IOException,
			IllegalValueException {
		if (c == true) {
			byte[] is = baos2.toByteArray();
			baos2.reset();
			c = false;
			return is;
		}
		if (communicate() == 9) {
			byte[] is = baos2.toByteArray();
			baos2.reset();
			c = false;
			return is;
		}
		return null;
	}

	public void sendAssociateResponse(Response response) throws IOException,
			IllegalValueException {
		if (response instanceof Abort) {
			Abort abort = (Abort) response;
			Type7 var_d = new Type7(abort.getSource(), abort.getReason());
			var_d.write(os);
		} else if (response instanceof Reject) {
			Reject reject = (Reject) response;
			Type3 var_q = new Type3(reject.getResult(), reject.getSource(), reject
					.getReason());
			var_q.write(os);
		} else if (response instanceof Acknowledge) {
			ack = (Acknowledge) response;
			t2 = new Type2(ack);
			t2.write(os);
			vec = t2.getVector();
		}
	}

	public void sendReleaseResponse() throws IOException, IllegalValueException {
		DataOutputStream dataoutputstream = new DataOutputStream(os);
		os.write(6);
		os.write(0);
		dataoutputstream.writeInt(4);
		dataoutputstream.writeInt(0);
	}

	public void send(int i, DicomObject dicomobject, DicomObject dicomobject_2_)
			throws IOException, IllegalValueException, DicomException {
		int i_3_ = -1;
		UIDEntry uidentry = UID.getUIDEntry(i);
		if (uidentry.getType() != 4 && uidentry.getType() != 1)
			throw new IllegalValueException(
					"first argument should be Meta SOP Class or SOP Class");
		while_3_: for (int i_4_ = 0; i_4_ < req.getPresentationContexts(); i_4_++) {
			uidentry = req.getAbstractSyntax(i_4_);
			if (uidentry.getConstant() == i) {
				i_3_ = req.getID(i_4_);
				for (int i_5_ = 0; i_5_ < ack.getPresentationContexts(); i_5_++) {
					int i_6_ = ack.getID(i_5_);
					if (i_6_ == i_3_ && ack.getResult(i_5_) == 0)
						break while_3_;
				}
			}
		}
		if (i_3_ == -1)
			throw new IllegalValueException(
					"no accepted presentation context with specified abstract syntax");
		sendInPresentationContext(i_3_, dicomobject, dicomobject_2_);
	}

	public void send(byte i, DicomObject dicomobject, DicomObject dicomobject_7_)
			throws IOException, IllegalValueException, DicomException {
		sendInPresentationContext((int) i, dicomobject, dicomobject_7_);
	}

	public void sendInPresentationContext(byte i, DicomObject dicomobject,
			DicomObject dicomobject_8_) throws IOException,
			IllegalValueException, DicomException {
		sendInPresentationContext((int) i, dicomobject, dicomobject_8_);
	}

	public void sendInPresentationContext(int i, DicomObject dicomobject,
			DicomObject dicomobject_9_) throws IOException,
			IllegalValueException, DicomException {
		int i_10_ = -1;
		for (int i_11_ = 0; i_11_ < ack.getPresentationContexts(); i_11_++) {
			if (i == ack.getID(i_11_))
				i_10_ = i_11_;
		}
		if (i_10_ == -1)
			throw new IllegalValueException(
					"presentation context with specified id not present in Acknowledge");
		if (ack.getResult(i_10_) != 0)
			throw new IllegalValueException(
					"not an accepted presentation context");
		UIDEntry uidentry = ack.getTransferSyntax(i_10_);
		if (uidentry == null)
			System.out.println("NULL");
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		com.archimed.tool.BAOS2 var_c = new com.archimed.tool.BAOS2();
		FOS var_c_12_ = new FOS(os, pduSize, i, (byte) 1);
		dicomobject.write(var_c_12_, false, 8193, seqUndefLen);
		var_c_12_.close();
		if (dicomobject_9_ != null) {
			var_c_12_ = new FOS(os, pduSize, i, (byte) 0);
			dicomobject_9_
					.write(var_c_12_, false, uidentry.getConstant(), true);
			var_c_12_.close();
		}
	}

	private void a(byte[] is, int i, int i_13_, byte i_14_, byte i_15_)
			throws IOException {
		int i_16_ = i;
		BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(os,
				8192);
		DataOutputStream dataoutputstream = new DataOutputStream(
				bufferedoutputstream);
		int i_17_ = i_13_;
		int i_18_;
		if (pduSize != 0)
			i_18_ = pduSize - 6;
		else
			i_18_ = i_17_;
		for (/**/; i_17_ > i_18_; i_17_ -= i_18_) {
			dataoutputstream.writeByte(4);
			dataoutputstream.writeByte(0);
			dataoutputstream.writeInt(pduSize);
			dataoutputstream.writeInt(pduSize - 4);
			dataoutputstream.writeByte(i_14_);
			dataoutputstream.writeByte(i_15_);
			dataoutputstream.write(is, i_16_, i_18_);
			i_16_ += i_18_;
		}
		dataoutputstream.writeByte(4);
		dataoutputstream.writeByte(0);
		dataoutputstream.writeInt(i_17_ + 6);
		dataoutputstream.writeInt(i_17_ + 2);
		dataoutputstream.writeByte(i_14_);
		dataoutputstream.writeByte(i_15_ + 2);
		dataoutputstream.write(is, i_16_, i_17_);
		bufferedoutputstream.flush();
	}

	public int peek() throws IOException, IllegalValueException {
		int i = pbis.read();
		if (i == -1)
			throw new IOException("end of stream reached");
		pbis.unread(i);
		if (i == 1)
			return 14;
		if (i == 2)
			return 15;
		if (i == 3)
			return 16;
		if (i == 4)
			return 10;
		if (i == 5)
			return 11;
		if (i == 6)
			return 13;
		if (i == 7)
			return 12;
		throw new IllegalValueException("Unexpected byte read: " + i);
	}

	private int communicate() throws IOException, IllegalValueException {
		DataInputStream datainputstream = new DataInputStream(pbis);
		DataOutputStream dataoutputstream = new DataOutputStream(os);
		int i = 4;
		do {
			int i_19_ = pbis.read();
			if (Debug.DEBUG > 0) {
				System.out.println("incoming PDU");
				System.out.println("PDU type: " + i_19_);
			}
			if (i_19_ != i)
				throw new IllegalValueException(
						"first byte of PDATA-PDU not 0x04 but '" + i_19_ + "'");
			datainputstream.read();
			int i_20_ = datainputstream.readInt();
			if (Debug.DEBUG > 0)
				System.out.println("PDU length: " + i_20_);
			while_4_: do {
				for (;;) {
					if (i_20_ <= 0)
						break while_4_;
					int i_21_ = datainputstream.readInt();
					if (Debug.DEBUG > 0)
						System.out.println("item length: " + i_21_);
					byte[] is = new byte[i_21_ - 2];
					i_20_ = i_20_ - i_21_ - 4;
					_fldlong = datainputstream.readUnsignedByte();
					if (Debug.DEBUG > 0)
						System.out.println("presentation context id: "
								+ _fldlong);
					byte i_22_ = datainputstream.readByte();
					if (Debug.DEBUG > 0)
						System.out.println("header value: " + i_22_);
					datainputstream.readFully(is);
					if (i_22_ == 0)
						baos2.write(is, 0, is.length);
					else if (i_22_ == 1)
						baos1.write(is, 0, is.length);
					else if (i_22_ == 2) {
						baos2.write(is, 0, is.length);
						c = true;
					} else {
						if (i_22_ != 3)
							break;
						baos1.write(is, 0, is.length);
						waitingForInput = true;
					}
				}
				throw new IllegalValueException(
						"illegal value for header of PDV item");
			} while (false);
			if (waitingForInput)
				return 8;
		} while (!c);
		return 9;
	}

	public int getCurrentPresentationContext() {
		return _fldlong;
	}

	public UIDEntry getCurrentAbstractSyntax() throws IllegalValueException {
		for (int i = 0; i < req.getPresentationContexts(); i++) {
			if (_fldlong == req.getID(i))
				return req.getAbstractSyntax(i);
		}
		throw new IllegalValueException(
				"no abstract syntax in request found for received presentation context id: "
						+ _fldlong);
	}

	public Request receiveAssociateRequest() throws IOException,
			UnknownUIDException, IllegalValueException {
		t1 = new Type1();
		t1.read(pbis);
		pduSize = t1.getReq().getMaxPduSize();
		req = t1.getReq();
		return req;
	}

	public Response receiveAssociateResponse() throws IOException,
			UnknownUIDException, IllegalValueException {
		int i = pbis.read();
		pbis.unread(i);
		if (i == 2) {
			t2 = new Type2();
			t2.read(pbis);
			pduSize = t2.getAck().getMaxPduSize();
			vec = t2.getVector();
			ack = t2.getAck();
			return t2.getAck();
		}
		if (i == 3) {
			Type3 t3 = new Type3();
			t3.read(pbis);
			return t3.getReject();
		}
		if (i == 7) {
			Type7 t7 = new Type7();
			t7.read(pbis);
			return t7.getAbort();
		}
		throw new IllegalValueException(
				"first byte of PDATA-PDU not 0x04 but '" + i + "'");
	}

	public void sendAssociateRequest(Request request) throws IOException,
			IllegalValueException {
		req = request;
		req.setImplementationClassUID(implclassuid);
		req.setImplementationVersionName(implversionname);
		t1 = new Type1(req);
		ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
		DataOutputStream dataoutputstream = new DataOutputStream(
				bytearrayoutputstream);
		t1.write(dataoutputstream);
		os.write(bytearrayoutputstream.toByteArray());
	}

	public void sendReleaseRequest() throws IOException, IllegalValueException {
		DataOutputStream dataoutputstream = new DataOutputStream(os);
		os.write(5);
		os.write(0);
		dataoutputstream.writeInt(4);
		dataoutputstream.writeInt(0);
	}

	public void receiveReleaseRequest() throws IOException,
			IllegalValueException {
		DataInputStream datainputstream = new DataInputStream(pbis);
		int i = pbis.read();
		if (i != 5)
			throw new IllegalValueException(
					"wrong first byte for expected Release Request: " + i);
		pbis.read();
		datainputstream.readInt();
		datainputstream.readInt();
	}

	public void receiveReleaseResponse() throws IOException,
			IllegalValueException {
		DataInputStream datainputstream = new DataInputStream(pbis);
		int i = pbis.read();
		if (i != 6)
			throw new IllegalValueException(
					"wrong first byte for expected Release Response: " + i);
		pbis.read();
		datainputstream.readInt();
		datainputstream.readInt();
	}

	public void sendAbort(int i, int i_23_) throws IOException {
		Type7 t7 = new Type7(i, i_23_);
		t7.write(os);
	}

	public Abort receiveAbort() throws IllegalValueException, IOException {
		Type7 t7 = new Type7();
		t7.read(pbis);
		return t7.getAbort();
	}
}