/*
 * Abort - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom.network;

public class Abort extends Response {
	public static int DICOM_UL_SERVICE_USER = 0;

	public static int DICOM_UL_SERVICE_PROVIDER = 2;

	public static int REASON_NOT_SPECIFIED = 0;

	public static int UNRECOGNIZED_PDU = 1;

	public static int UNEXPECTED_PDU = 2;

	public static int UNRECOGNIZED_PDU_PARAMETER = 4;

	public static int UNEXPECTED_PDU_PARAMETER = 5;

	public static int INVALID_PDU_PARAMETER_VALUE = 6;

	private int source;

	private int reason;

	public Abort(int i, int i_0_) {
		source = i;
		reason = i_0_;
	}

	public int getSource() {
		return source;
	}

	public int getReason() {
		return reason;
	}

	public String toString() {
		String string = "[abort, source: ";
		if (source == DICOM_UL_SERVICE_USER)
			string += "DICOM_UL_SERVICE_USER";
		else if (source == DICOM_UL_SERVICE_PROVIDER)
			string += "DICOM_UL_SERVICE_PROVIDER";
		else
			string += "UNKNOWN";
		string += ", reason: ";
		if (reason == REASON_NOT_SPECIFIED)
			string += "REASON_NOT_SPECIFIED";
		else if (reason == UNRECOGNIZED_PDU)
			string += "UNRECOGNIZED_PDU";
		else if (reason == UNEXPECTED_PDU)
			string += "UNEXPECTED_PDU";
		else if (reason == UNRECOGNIZED_PDU_PARAMETER)
			string += "UNRECOGNIZED_PDU_PARAMETER";
		else if (reason == UNEXPECTED_PDU_PARAMETER)
			string += "UNEXPECTED_PDU_PARAMETER";
		else if (reason == INVALID_PDU_PARAMETER_VALUE)
			string += "INVALID_PDU_PARAMETER_VALUE";
		else
			string += "UNKNOWN";
		string += "]";
		return string;
	}
}