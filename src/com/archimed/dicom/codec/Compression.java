
package com.archimed.dicom.codec;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UID;
import com.archimed.dicom.UnknownUIDException;
import com.archimed.utils.helpers.StreamReader;
import com.archimed.utils.img.JPEG;
import com.archimed.utils.img.Writer;

public class Compression {
	DicomObject dcm;

	public Compression() {
		dcm = null;
	}

	public Compression(DicomObject dicomobject) throws DicomException {
		setDicomObject(dicomobject);
	}

	public void decompress() throws DicomException, IOException {
		if (dcm == null)
			throw new DicomException(
					"Use setDicomObject first to specify a DicomObject");
		int i;
		try {
			DicomObject dicomobject = dcm.getFileMetaInformation();
			if (dicomobject == null)
				throw new DicomException("no file meta information");
			i = UID.getUIDEntry(dicomobject.getS(31)).getConstant();
		} catch (UnknownUIDException unknownuidexception) {
			throw new DicomException("Decompression not possible: "
					+ unknownuidexception.getMessage());
		} catch (Exception exception) {
			throw new DicomException(
					"Decompression not possible: no transfersyntax found");
		}
		int i467 = dcm.getI(467);
		int i466 = dcm.getI(466);
		int i1184 = dcm.getSize(1184) - 1;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		dcm.deleteItem(1184, 0);
		for (int j = 0; j < i1184; j++) {
			byte[] is = decompressFrame(i, (byte[]) dcm.deleteItem(1184, 0),i467, i466);
			baos.write(is);
		}
		dcm.set(1184, baos.toByteArray());
		try {
			dcm.getFileMetaInformation().set(31,UID.getUIDEntry(8194).getValue());
		} catch (IllegalValueException illegalvalueexception) {
			/* empty */
		}
	}

	public static byte[] decompressFrame(int tipo, byte[] frame, int i_4_, int i_5_)
			throws DicomException, IOException {
		if (tipo == 8197) {
			JPEG var_c = new JPEG();
			byte[] ret = var_c.process01(frame);
			return ret;
		}
		if (tipo == 8198) {
			ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(
					frame);
			StreamReader var_b = new StreamReader();
			var_b.a(i_4_, i_5_);
			return var_b.read2DArray(bytearrayinputstream);
		}
		if (tipo == 8196) {
			Image image = Toolkit.getDefaultToolkit().createImage(frame);
			PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, i_4_,
					i_5_, false);
			try {
				pixelgrabber.grabPixels();
			} catch (InterruptedException interruptedexception) {
				interruptedexception.printStackTrace();
			}
			byte[] is_7_;
			try {
				is_7_ = (byte[]) pixelgrabber.getPixels();
			} catch (ClassCastException classcastexception) {
				return a((int[]) pixelgrabber.getPixels());
			}
			return is_7_;
		}
		throw new DicomException(
				"unsupported transfer syntax for decompression");
	}

	private static byte[] a(int[] is) {
		byte[] is_8_ = new byte[is.length * 3];
		for (int i = 0; i < is.length; i++) {
			is_8_[3 * i] = (byte) ((is[i] & 0xff0000) >> 16);
			is_8_[3 * i + 1] = (byte) ((is[i] & 0xff00) >> 8);
			is_8_[3 * i + 2] = (byte) (is[i] & 0xff);
		}
		return is_8_;
	}

	public void compress(int i) throws IOException, DicomException {
		if (dcm == null)
			throw new DicomException(
					"Use setDicomObject first to specify a DicomObject");
		if (i != 8197 && i != 8198)
			throw new DicomException(
					"unsupported transfer syntax for compression");
		int i_9_;
		try {
			DicomObject dicomobject = dcm.getFileMetaInformation();
			if (dicomobject == null)
				i_9_ = 8193;
			else if (dicomobject.getSize(31) > 0)
				i_9_ = UID.getUIDEntry(dicomobject.getS(31)).getConstant();
			else
				i_9_ = 8193;
		} catch (UnknownUIDException unknownuidexception) {
			throw new DicomException("Decompression not possible: "
					+ unknownuidexception.getMessage());
		}
		if (i_9_ != 8195 && i_9_ != 8194 && i_9_ != 8193)
			throw new DicomException(
					"associated DicomObject already has a compressed transfer syntax");
		int i_10_ = dcm.getI(467);
		int i_11_ = dcm.getI(466);
		int i_12_ = 1;
		if (dcm.getSize(464) > 0)
			i_12_ = dcm.getI(464);
		byte[] is = (byte[]) dcm.get(1184);
		dcm.deleteItem(1184, 0);
		dcm.set(1184, new byte[0], 0);
		int i_13_ = dcm.getI(475);
		if (i_12_ == 1)
			dcm.set(1184, compressFrame(i, is, i_10_, i_11_), 1);
		else {
			byte[] is_14_;
			if (i_13_ == 8)
				is_14_ = new byte[i_10_ * i_11_];
			else if (i_13_ == 16)
				is_14_ = new byte[i_10_ * i_11_ * 2];
			else if (i_13_ == 24)
				is_14_ = new byte[i_10_ * i_11_ * 3];
			else
				throw new DicomException(
						"unsupported value for Bits Allocated: " + i_13_
								+ " (should be 8,16 or 24)");
			for (int i_15_ = 0; i_15_ < i_12_; i_15_++) {
				System.arraycopy(is, i_15_ * is_14_.length, is_14_, 0,
						is_14_.length);
				dcm.append(1184, compressFrame(i, is_14_, i_10_, i_11_));
			}
		}
		DicomObject dicomobject = dcm.getFileMetaInformation();
		try {
			if (dicomobject != null)
				dicomobject.set(31, UID.getUIDEntry(i).getValue());
		} catch (IllegalValueException illegalvalueexception) {
			System.out.println("unexpected error: " + illegalvalueexception);
		}
	}

	public static byte[] compressFrame(int i, byte[] is, int i_16_, int i_17_)
			throws IOException, DicomException {
		if (i == 8197) {
			Writer var_g = new Writer();
			if (is.length == i_16_ * i_17_)
				return var_g._mthif(is, i_16_, i_17_);
			if (is.length == i_16_ * i_17_ * 2)
				return var_g.a(is, i_16_, i_17_);
			if (is.length == i_16_ * i_17_ * 3)
				throw new DicomException(
						"24 bit color lossless jpeg compression unsupported");
			throw new DicomException(
					"length of frame to compress is not equal to width*height or width*height*2 or width*height*3");
		}
		throw new DicomException("unsupported transfer syntax for compression");
	}

	public void setDicomObject(DicomObject dicomobject) throws DicomException {
		dcm = dicomobject;
	}

	public DicomObject getDicomObject() {
		return dcm;
	}
}