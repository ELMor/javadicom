/*
 * MetaSOPClass - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class MetaSOPClass extends UID {
	public static final int DetachedPatientManagement = 12289;

	public static final int DetachedResultsManagement = 12290;

	public static final int DetachedStudyManagement = 12291;

	public static final int BasicGrayscalePrintManagement = 12292;

	public static final int ReferencedGrayscalePrintManagement = 12293;

	public static final int BasicColorPrintManagement = 12294;

	public static final int ReferencedColorPrintManagement = 12295;

	public static final int PullStoredPrintManagement = 12296;

	public static final int GeneralPurposeWorklistManagement = 12297;

	static {
		entries.put(new Integer(12289), new UIDEntry(12289,
				"1.2.840.10008.3.1.2.1.4",
				"Detached Patient Management Meta SOP Class", "DP", 4));
		entries.put(new Integer(12290), new UIDEntry(12290,
				"1.2.840.10008.3.1.2.5.4",
				"Detached Results Management Meta SOP Class", "DR", 4));
		entries.put(new Integer(12291), new UIDEntry(12291,
				"1.2.840.10008.3.1.2.5.5",
				"Detached Study Management Meta SOP Class", "DS", 4));
		entries.put(new Integer(12292), new UIDEntry(12292,
				"1.2.840.10008.5.1.1.9",
				"Basic Grayscale Print Management Meta SOP Class", "BG", 4));
		entries.put(new Integer(12293),
				(new UIDEntry(12293, "1.2.840.10008.5.1.1.9.1",
						"Referenced Grayscale Print Management Meta SOP Class",
						"RG", 4)));
		entries.put(new Integer(12294), new UIDEntry(12294,
				"1.2.840.10008.5.1.1.18",
				"Basic Color Print Management Meta SOP Class", "BC", 4));
		entries.put(new Integer(12295), new UIDEntry(12295,
				"1.2.840.10008.5.1.1.18.1",
				"Referenced Color Print Management Meta SOP Class", "RC", 4));
		entries.put(new Integer(12296), new UIDEntry(12296,
				"1.2.840.10008.5.1.1.32",
				"Pull Stored Print Management Meta SOP Class", "PS", 4));
		entries.put(new Integer(12297), new UIDEntry(12297,
				"1.2.840.10008.5.1.4.32",
				"General Purpose Worklist Management Meta SOP Class", "GW", 4));
	}
}