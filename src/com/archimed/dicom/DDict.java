
package com.archimed.dicom;

import com.archimed.tool.MyVector;
import com.archimed.tool.DataChunk2Holder;
import java.util.Enumeration;


public class DDict {

	public static final int tUN = 0;

	public static final int tUNKNOWN = 0;

	public static final int tUL = 1;

	public static final int tUI = 2;

	public static final int tUS = 3;

	public static final int tAE = 4;

	public static final int tAT = 5;

	public static final int tLO = 6;

	public static final int tSH = 7;

	public static final int tOB = 8;

	public static final int tCS = 9;

	public static final int tSQ = 10;

	public static final int tDA = 11;

	public static final int tTM = 12;

	public static final int tST = 13;

	public static final int tPN = 14;

	public static final int tIS = 15;

	public static final int tDS = 16;

	public static final int tAS = 17;

	public static final int tLT = 18;

	public static final int tSL = 19;

	public static final int tFD = 20;

	public static final int tUS_SS = 21;

	public static final int tOW_OB = 22;

	public static final int tSS = 23;

	public static final int tOW = 24;

	public static final int tNONE = 25;

	public static final int tFL = 26;

	public static final int tUT = 27;

	public static final int tDT = 28;

	public static final int tOF = 29;

	public static final int dCommandGroupLength = 0;

	public static final int dAffectedSOPClassUID = 1;

	public static final int dRequestedSOPClassUID = 2;

	public static final int dCommandField = 3;

	public static final int dMessageID = 4;

	public static final int dMessageIDBeingRespondedTo = 5;

	public static final int dMoveDestination = 6;

	public static final int dPriority = 7;

	public static final int dDataSetType = 8;

	public static final int dStatus = 9;

	public static final int dOffendingElement = 10;

	public static final int dErrorComment = 11;

	public static final int dErrorID = 12;

	public static final int dAffectedSOPInstanceUID = 13;

	public static final int dRequestedSOPInstanceUID = 14;

	public static final int dEventTypeID = 15;

	public static final int dAttributeIdentifierList = 16;

	public static final int dModificationList = 17;

	public static final int dActionTypeID = 18;

	public static final int dNumberOfRemainingSuboperations = 19;

	public static final int dNumberOfCompletedSuboperations = 20;

	public static final int dNumberOfFailedSuboperations = 21;

	public static final int dNumberOfWarningSuboperations = 22;

	public static final int dMoveOriginatorApplicationEntityTitle = 23;

	public static final int dMoveOriginatorMessageID = 24;

	public static final int dMessageSetID = 25;

	public static final int dEndMessageSet = 26;

	public static final int dMetaElementGroupLength = 27;

	public static final int dFileMetaInformationVersion = 28;

	public static final int dMediaStorageSOPClassUID = 29;

	public static final int dMediaStorageSOPInstanceUID = 30;

	public static final int dTransferSyntaxUID = 31;

	public static final int dImplementationClassUID = 32;

	public static final int dImplementationVersionName = 33;

	public static final int dSourceApplicationEntityTitle = 34;

	public static final int dPrivateInformationCreatorUID = 35;

	public static final int dPrivateInformation = 36;

	public static final int dFileSetGroupLength = 37;

	public static final int dFileSetID = 38;

	public static final int dFileSetDescriptorFileID = 39;

	public static final int dFileSetCharacterSet = 40;

	public static final int dRootDirectoryFirstRecord = 41;

	public static final int dRootDirectoryLastRecord = 42;

	public static final int dFileSetConsistencyFlag = 43;

	public static final int dDirectoryRecordSequence = 44;

	public static final int dNextDirectoryRecordOffset = 45;

	public static final int dRecordInUseFlag = 46;

	public static final int dLowerLevelDirectoryOffset = 47;

	public static final int dDirectoryRecordType = 48;

	public static final int dPrivateRecordUID = 49;

	public static final int dReferencedFileID = 50;

	public static final int dDirectoryRecordOffset = 51;

	public static final int dReferencedSOPClassUIDInFile = 52;

	public static final int dReferencedSOPInstanceUIDInFile = 53;

	public static final int dReferencedTransferSyntaxUIDInFile = 54;

	public static final int dNumberOfReferences = 55;

	public static final int dIdentifyingGroupLength = 56;

	public static final int dSpecificCharacterSet = 57;

	public static final int dImageType = 58;

	public static final int dInstanceCreationDate = 59;

	public static final int dInstanceCreationTime = 60;

	public static final int dInstanceCreatorUID = 61;

	public static final int dSOPClassUID = 62;

	public static final int dSOPInstanceUID = 63;

	public static final int dStudyDate = 64;

	public static final int dSeriesDate = 65;

	public static final int dAcquisitionDate = 66;

	public static final int dContentDate = 67;

	public static final int dImageDate = 67;

	public static final int dOverlayDate = 68;

	public static final int dCurveDate = 69;

	public static final int dStudyTime = 70;

	public static final int dSeriesTime = 71;

	public static final int dAcquisitionTime = 72;

	public static final int dContentTime = 73;

	public static final int dImageTime = 73;

	public static final int dOverlayTime = 74;

	public static final int dCurveTime = 75;

	public static final int dNuclearMedicineSeriesType = 76;

	public static final int dAccessionNumber = 77;

	public static final int dQueryRetrieveLevel = 78;

	public static final int dRetrieveAETitle = 79;

	public static final int dFailedSOPInstanceUIDList = 80;

	public static final int dModality = 81;

	public static final int dModalitiesInStudy = 82;

	public static final int dConversionType = 83;

	public static final int dManufacturer = 84;

	public static final int dInstitutionName = 85;

	public static final int dInstitutionAddress = 86;

	public static final int dInstitutionCodeSequence = 87;

	public static final int dReferringPhysiciansName = 88;

	public static final int dReferringPhysiciansAddress = 89;

	public static final int dReferringPhysiciansTelephoneNumber = 90;

	public static final int dCodeValue = 91;

	public static final int dCodingSchemeDesignator = 92;

	public static final int dCodeMeaning = 93;

	public static final int dStationName = 94;

	public static final int dStudyDescription = 95;

	public static final int dProcedureCodeSequence = 96;

	public static final int dSeriesDescription = 97;

	public static final int dInstitutionalDepartmentName = 98;

	public static final int dPhysiciansOfRecord = 99;

	public static final int dPerformingPhysiciansName = 100;

	public static final int dNameOfPhysiciansReadingStudy = 101;

	public static final int dOperatorsName = 102;

	public static final int dAdmittingDiagnosisDescription = 103;

	public static final int dAdmittingDiagnosisCodeSequence = 104;

	public static final int dManufacturerModelName = 105;

	public static final int dReferencedResultsSequence = 106;

	public static final int dReferencedStudySequence = 107;

	public static final int dReferencedStudyComponentSequence = 108;

	public static final int dReferencedSeriesSequence = 109;

	public static final int dReferencedPatientSequence = 110;

	public static final int dReferencedVisitSequence = 111;

	public static final int dReferencedOverlaySequence = 112;

	public static final int dReferencedImageSequence = 113;

	public static final int dReferencedCurveSequence = 114;

	public static final int dReferencedSOPClassUID = 115;

	public static final int dReferencedSOPInstanceUID = 116;

	public static final int dReferencedFrameNumber = 117;

	public static final int dTransactionUID = 118;

	public static final int dFailureReason = 119;

	public static final int dFailedSOPSequence = 120;

	public static final int dReferencedSOPSequence = 121;

	public static final int dDerivationDescription = 122;

	public static final int dSourceImageSequence = 123;

	public static final int dStageName = 124;

	public static final int dStageNumber = 125;

	public static final int dNumberOfStages = 126;

	public static final int dViewNumber = 127;

	public static final int dNumberOfEventTimers = 128;

	public static final int dNumberOfViewsInStage = 129;

	public static final int dEventElapsedTime = 130;

	public static final int dEventTimerName = 131;

	public static final int dStartTrim = 132;

	public static final int dStopTrim = 133;

	public static final int dRecommendedDisplayFrameRate = 134;

	public static final int dTransducerPosition = 135;

	public static final int dTransducerOrientation = 136;

	public static final int dAnatomicStructure = 137;

	public static final int dAnatomicRegionSequence = 138;

	public static final int dAnatomicRegionModifierSequence = 139;

	public static final int dPrimaryAnatomicStructureSequence = 140;

	public static final int dPrimaryAnatomicStructureModifierSequence = 141;

	public static final int dTransducerPositionSequence = 142;

	public static final int dTransducerPositionModifierSequence = 143;

	public static final int dTransducerOrientationSequence = 144;

	public static final int dTransducerOrientationModifierSequence = 145;

	public static final int dPatientGroupLength = 146;

	public static final int dPatientName = 147;

	public static final int dPatientID = 148;

	public static final int dIssuerOfPatientID = 149;

	public static final int dPatientBirthDate = 150;

	public static final int dPatientBirthTime = 151;

	public static final int dPatientSex = 152;

	public static final int dPatientInsurancePlanCodeSequence = 153;

	public static final int dOtherPatientID = 154;

	public static final int dOtherPatientNames = 155;

	public static final int dPatientBirthName = 156;

	public static final int dPatientAge = 157;

	public static final int dPatientSize = 158;

	public static final int dPatientWeight = 159;

	public static final int dPatientAddress = 160;

	public static final int dPatientMotherBirthName = 161;

	public static final int dMilitaryRank = 162;

	public static final int dBranchOfService = 163;

	public static final int dMedicalRecordLocator = 164;

	public static final int dMedicalAlerts = 165;

	public static final int dContrastAllergies = 166;

	public static final int dCountryOfResidence = 167;

	public static final int dRegionOfResidence = 168;

	public static final int dPatientTelephoneNumber = 169;

	public static final int dEthnicGroup = 170;

	public static final int dOccupation = 171;

	public static final int dSmokingStatus = 172;

	public static final int dAdditionalPatientHistory = 173;

	public static final int dPregnancyStatus = 174;

	public static final int dLastMenstrualDate = 175;

	public static final int dPatientReligiousPreference = 176;

	public static final int dPatientComments = 177;

	public static final int dAcquisitionGroupLength = 178;

	public static final int dContrastBolusAgent = 179;

	public static final int dContrastBolusAgentSequence = 180;

	public static final int dContrastBolusAdministrationRouteSequence = 181;

	public static final int dBodyPartExamined = 182;

	public static final int dScanningSequence = 183;

	public static final int dSequenceVariant = 184;

	public static final int dScanOptions = 185;

	public static final int dMRAcquisitionType = 186;

	public static final int dSequenceName = 187;

	public static final int dAngioFlag = 188;

	public static final int dInterventionDrugInformationSequence = 189;

	public static final int dInterventionDrugStopTime = 190;

	public static final int dInterventionDrugDose = 191;

	public static final int dInterventionDrugCodeSequence = 192;

	public static final int dAdditionalDrugSequence = 193;

	public static final int dRadionuclide = 194;

	public static final int dRadiopharmaceutical = 195;

	public static final int dEnergyWindowCenterline = 196;

	public static final int dEnergyWindowTotalWidth = 197;

	public static final int dInterventionDrugName = 198;

	public static final int dInterventionDrugStartTime = 199;

	public static final int dInterventionalTherapySequence = 200;

	public static final int dTherapyType = 201;

	public static final int dInterventionalStatus = 202;

	public static final int dTherapyDescription = 203;

	public static final int dCineRate = 204;

	public static final int dSliceThickness = 205;

	public static final int dKVP = 206;

	public static final int dCountsAccumulated = 207;

	public static final int dAcquisitionTerminationCondition = 208;

	public static final int dEffectiveDuration = 209;

	public static final int dAcquisitionStartCondition = 210;

	public static final int dAcquisitionStartConditionData = 211;

	public static final int dAcquisitionTerminationConditionData = 212;

	public static final int dRepetitionTime = 213;

	public static final int dEchoTime = 214;

	public static final int dInversionTime = 215;

	public static final int dNumberOfAverages = 216;

	public static final int dImagingFrequency = 217;

	public static final int dImagedNucleus = 218;

	public static final int dEchoNumber = 219;

	public static final int dMagneticFieldStrength = 220;

	public static final int dSpacingBetweenSlices = 221;

	public static final int dNumberOfPhaseEncodingSteps = 222;

	public static final int dDataCollectionDiameter = 223;

	public static final int dEchoTrainLength = 224;

	public static final int dPercentSampling = 225;

	public static final int dPercentPhaseFieldOfView = 226;

	public static final int dPixelBandwidth = 227;

	public static final int dDeviceSerialNumber = 228;

	public static final int dPlateID = 229;

	public static final int dSecondaryCaptureDeviceID = 230;

	public static final int dDateOfSecondaryCapture = 231;

	public static final int dTimeOfSecondaryCapture = 232;

	public static final int dSecondaryCaptureDeviceManufacturer = 233;

	public static final int dSecondaryCaptureDeviceManufacturerModelName = 234;

	public static final int dSecondaryCaptureDeviceSoftwareVersion = 235;

	public static final int dSoftwareVersion = 236;

	public static final int dVideoImageFormatAcquired = 237;

	public static final int dDigitalImageFormatAcquired = 238;

	public static final int dProtocolName = 239;

	public static final int dContrastBolusRoute = 240;

	public static final int dContrastBolusVolume = 241;

	public static final int dContrastBolusStartTime = 242;

	public static final int dContrastBolusStopTime = 243;

	public static final int dContrastBolusTotalDose = 244;

	public static final int dSyringeCounts = 245;

	public static final int dContrastFlowRates = 246;

	public static final int dContrastFlowDurations = 247;

	public static final int dContrastBolusIngredient = 248;

	public static final int dContrastBolusIngredientConcentration = 249;

	public static final int dSpatialResolution = 250;

	public static final int dTriggerTime = 251;

	public static final int dTriggerSourceOrType = 252;

	public static final int dNominalInterval = 253;

	public static final int dFrameTime = 254;

	public static final int dFramingType = 255;

	public static final int dFrameTimeVector = 256;

	public static final int dFrameDelay = 257;

	public static final int dRadionuclideRoute = 258;

	public static final int dRadionuclideVolume = 259;

	public static final int dRadiopharmaceuticalStartTime = 260;

	public static final int dRadiopharmaceuticalStopTime = 261;

	public static final int dRadionuclideTotalDose = 262;

	public static final int dRadionuclideHalfLife = 263;

	public static final int dRadionuclidePositronFraction = 264;

	public static final int dRadiopharmaceuticalSpecificActivity = 265;

	public static final int dBeatRejectionFlag = 266;

	public static final int dLowRRValue = 267;

	public static final int dHighRRValue = 268;

	public static final int dIntervalsAcquired = 269;

	public static final int dIntervalsRejected = 270;

	public static final int dPVCRejection = 271;

	public static final int dSkipBeats = 272;

	public static final int dHeartRate = 273;

	public static final int dCardiacNumberOfImages = 274;

	public static final int dTriggerWindow = 275;

	public static final int dReconstructionDiameter = 276;

	public static final int dDistanceSourceToDetector = 277;

	public static final int dDistanceSourceToPatient = 278;

	public static final int dEstimatedRadiographicMagnificationFactor = 279;

	public static final int dGantryDetectorTilt = 280;

	public static final int dGantryDetectorSlew = 281;

	public static final int dTableHeight = 282;

	public static final int dTableTraverse = 283;

	public static final int dTableMotion = 284;

	public static final int dTableVerticalIncrement = 285;

	public static final int dTableLateralIncrement = 286;

	public static final int dTableLongitudinalIncrement = 287;

	public static final int dTableAngle = 288;

	public static final int dRotationDirection = 289;

	public static final int dAngularPosition = 290;

	public static final int dRadialPosition = 291;

	public static final int dScanArc = 292;

	public static final int dAngularStep = 293;

	public static final int dCenterOfRotationOffset = 294;

	public static final int dRotationOffset = 295;

	public static final int dFieldOfViewShape = 296;

	public static final int dFieldOfViewDimension = 297;

	public static final int dExposureTime = 298;

	public static final int dXRayTubeCurrent = 299;

	public static final int dExposure = 300;

	public static final int dAveragePulseWidth = 301;

	public static final int dRadiationSetting = 302;

	public static final int dRadiationMode = 303;

	public static final int dImageAreaDoseProduct = 304;

	public static final int dFilterType = 305;

	public static final int dTypeOfFilters = 306;

	public static final int dIntensifierSize = 307;

	public static final int dImagerPixelSpacing = 308;

	public static final int dGrid = 309;

	public static final int dGeneratorPower = 310;

	public static final int dCollimatorGridName = 311;

	public static final int dCollimatorType = 312;

	public static final int dFocalDistance = 313;

	public static final int dXFocusCenter = 314;

	public static final int dYFocusCenter = 315;

	public static final int dFocalSpot = 316;

	public static final int dDateOfLastCalibration = 317;

	public static final int dTimeOfLastCalibration = 318;

	public static final int dConvolutionKernel = 319;

	public static final int dActualFrameDuration = 320;

	public static final int dCountRate = 321;

	public static final int dPreferredPlaybackSequencing = 322;

	public static final int dReceivingCoil = 323;

	public static final int dTransmittingCoil = 324;

	public static final int dPlateType = 325;

	public static final int dPhosphorType = 326;

	public static final int dScanVelocity = 327;

	public static final int dWholeBodyTechnique = 328;

	public static final int dScanLength = 329;

	public static final int dAcquisitionMatrix = 330;

	public static final int dPhaseEncodingDirection = 331;

	public static final int dFlipAngle = 332;

	public static final int dVariableFlipAngleFlag = 333;

	public static final int dSAR = 334;

	public static final int ddBdt = 335;

	public static final int dAcquisitionDeviceProcessingDescription = 336;

	public static final int dAcquisitionDeviceProcessingCode = 337;

	public static final int dCassetteOrientation = 338;

	public static final int dCassetteSize = 339;

	public static final int dExposuresOnPlate = 340;

	public static final int dRelativeXrayExposure = 341;

	public static final int dTomoLayerHeight = 342;

	public static final int dTomoAngle = 343;

	public static final int dTomoTime = 344;

	public static final int dPositionerMotion = 345;

	public static final int dPositionerPrimaryAngle = 346;

	public static final int dPositionerSecondaryAngle = 347;

	public static final int dPositionerPrimaryAngleIncrement = 348;

	public static final int dPositionerSecondaryAngleIncrement = 349;

	public static final int dDetectorPrimaryAngle = 350;

	public static final int dDetectorSecondaryAngle = 351;

	public static final int dShutterShape = 352;

	public static final int dShutterLeftVerticalEdge = 353;

	public static final int dShutterRightVerticalEdge = 354;

	public static final int dShutterUpperHorizontalEdge = 355;

	public static final int dShutterLowerHorizontalEdge = 356;

	public static final int dCenterOfCircularShutter = 357;

	public static final int dRadiusOfCircularShutter = 358;

	public static final int dVerticesOfThePolygonalShutter = 359;

	public static final int dCollimatorShape = 360;

	public static final int dCollimatorLeftVerticalEdge = 361;

	public static final int dCollimatorRightVerticalEdge = 362;

	public static final int dCollimatorUpperHorizontalEdge = 363;

	public static final int dCollimatorLowerHorizontalEdge = 364;

	public static final int dCenterOfCircularCollimator = 365;

	public static final int dRadiusOfCircularCollimator = 366;

	public static final int dVerticesOfThePolygonalCollimator = 367;

	public static final int dOutputPower = 368;

	public static final int dTransducerData = 369;

	public static final int dFocusDepth = 370;

	public static final int dPreprocessingFunction = 371;

	public static final int dPostprocessingFunction = 372;

	public static final int dMechanicalIndex = 373;

	public static final int dBoneThermalIndex = 374;

	public static final int dCranialThermalIndex = 375;

	public static final int dSoftTissueThermalIndex = 376;

	public static final int dSoftTissueFocusThermalIndex = 377;

	public static final int dSoftTissueSurfaceThermalIndex = 378;

	public static final int dDepthOfScanField = 379;

	public static final int dPatientPosition = 380;

	public static final int dViewPosition = 381;

	public static final int dImageTransformationMatrix = 382;

	public static final int dImageTranslationVector = 383;

	public static final int dSensitivity = 384;

	public static final int dSequenceOfUltrasoundRegions = 385;

	public static final int dRegionSpatialFormat = 386;

	public static final int dRegionDataType = 387;

	public static final int dRegionFlags = 388;

	public static final int dRegionLocationMinX0 = 389;

	public static final int dRegionLocationMinY0 = 390;

	public static final int dRegionLocationMaxX1 = 391;

	public static final int dRegionLocationMaxY1 = 392;

	public static final int dReferencePixelX0 = 393;

	public static final int dReferencePixelY0 = 394;

	public static final int dPhysicalUnitsXDirection = 395;

	public static final int dPhysicalUnitsYDirection = 396;

	public static final int dReferencePixelPhysicalValueX = 397;

	public static final int dReferencePixelPhysicalValueY = 398;

	public static final int dPhysicalDeltaX = 399;

	public static final int dPhysicalDeltaY = 400;

	public static final int dTransducerFrequency = 401;

	public static final int dTransducerType = 402;

	public static final int dPulseRepetitionFrequency = 403;

	public static final int dDopplerCorrectionAngle = 404;

	public static final int dSteeringAngle = 405;

	public static final int dDopplerSampleVolumeXPosition = 406;

	public static final int dDopplerSampleVolumeYPosition = 407;

	public static final int dTMLinePositionX0 = 408;

	public static final int dTMLinePositionY0 = 409;

	public static final int dTMLinePositionX1 = 410;

	public static final int dTMLinePositionY1 = 411;

	public static final int dPixelComponentOrganization = 412;

	public static final int dPixelComponentMask = 413;

	public static final int dPixelComponentRangeStart = 414;

	public static final int dPixelComponentRangeStop = 415;

	public static final int dPixelComponentPhysicalUnits = 416;

	public static final int dPixelComponentDataType = 417;

	public static final int dNumberOfTableBreakPoints = 418;

	public static final int dTableOfXBreakPoints = 419;

	public static final int dTableOfYBreakPoints = 420;

	public static final int dNumberOfTableEntries = 421;

	public static final int dTableOfPixelValues = 422;

	public static final int dTableOfParameterValues = 423;

	public static final int dImageGroupLength = 424;

	public static final int dStudyInstanceUID = 425;

	public static final int dSeriesInstanceUID = 426;

	public static final int dStudyID = 427;

	public static final int dSeriesNumber = 428;

	public static final int dAcquisitionNumber = 429;

	public static final int dInstanceNumber = 430;

	public static final int dImageNumber = 430;

	public static final int dIsotopeNumber = 431;

	public static final int dPhaseNumber = 432;

	public static final int dIntervalNumber = 433;

	public static final int dTimeSlotNumber = 434;

	public static final int dAngleNumber = 435;

	public static final int dPatientOrientation = 436;

	public static final int dOverlayNumber = 437;

	public static final int dCurveNumber = 438;

	public static final int dLUTNumber = 439;

	public static final int dImagePositionPatient = 440;

	public static final int dImageOrientationPatient = 441;

	public static final int dFrameOfReferenceUID = 442;

	public static final int dLaterality = 443;

	public static final int dTemporalPositionIdentifier = 444;

	public static final int dNumberOfTemporalPositions = 445;

	public static final int dTemporalResolution = 446;

	public static final int dSeriesInStudy = 447;

	public static final int dImagesInAcquisition = 448;

	public static final int dAcquisitionsInStudy = 449;

	public static final int dPositionReferenceIndicator = 450;

	public static final int dSliceLocation = 451;

	public static final int dOtherStudyNumbers = 452;

	public static final int dNumberOfPatientRelatedStudies = 453;

	public static final int dNumberOfPatientRelatedSeries = 454;

	public static final int dNumberOfPatientRelatedImages = 455;

	public static final int dNumberOfStudyRelatedSeries = 456;

	public static final int dNumberOfStudyRelatedImages = 457;

	public static final int dNumberOfSeriesRelatedImages = 458;

	public static final int dImageComments = 459;

	public static final int dImagePresentationGroupLength = 460;

	public static final int dSamplesPerPixel = 461;

	public static final int dPhotometricInterpretation = 462;

	public static final int dPlanarConfiguration = 463;

	public static final int dNumberOfFrames = 464;

	public static final int dFrameIncrementPointer = 465;

	public static final int dRows = 466;

	public static final int dColumns = 467;

	public static final int dPlanes = 468;

	public static final int dUltrasoundColorDataPresent = 469;

	public static final int dPixelSpacing = 470;

	public static final int dZoomFactor = 471;

	public static final int dZoomCenter = 472;

	public static final int dPixelAspectRatio = 473;

	public static final int dCorrectedImage = 474;

	public static final int dBitsAllocated = 475;

	public static final int dBitsStored = 476;

	public static final int dHighBit = 477;

	public static final int dPixelRepresentation = 478;

	public static final int dSmallestImagePixelValue = 479;

	public static final int dLargestImagePixelValue = 480;

	public static final int dSmallestPixelValueInSeries = 481;

	public static final int dLargestPixelValueInSeries = 482;

	public static final int dSmallestImagePixelValueInPlane = 483;

	public static final int dLargestImagePixelValueInPlane = 484;

	public static final int dPixelPaddingValue = 485;

	public static final int dPixelIntensityRelationship = 486;

	public static final int dWindowCenter = 487;

	public static final int dWindowWidth = 488;

	public static final int dRescaleIntercept = 489;

	public static final int dRescaleSlope = 490;

	public static final int dRescaleType = 491;

	public static final int dWindowCenterWidthExplanation = 492;

	public static final int dRecommendedViewingMode = 493;

	public static final int dRedPaletteColorLookupTableDescriptor = 494;

	public static final int dGreenPaletteColorLookupTableDescriptor = 495;

	public static final int dBluePaletteColorLookupTableDescriptor = 496;

	public static final int dPaletteColorLookupTableUID = 497;

	public static final int dRedPaletteColorLookupTableData = 498;

	public static final int dGreenPaletteColorLookupTableData = 499;

	public static final int dBluePaletteColorLookupTableData = 500;

	public static final int dSegmentedRedPaletteColorLookupTableData = 501;

	public static final int dSegmentedGreenPaletteColorLookupTableData = 502;

	public static final int dSegmentedBluePaletteColorLookupTableData = 503;

	public static final int dLossyImageCompression = 504;

	public static final int dModalityLUTSequence = 505;

	public static final int dLUTDescriptor = 506;

	public static final int dLUTExplanation = 507;

	public static final int dModalityLUTType = 508;

	public static final int dLUTData = 509;

	public static final int dVOILUTSequence = 510;

	public static final int dBiPlaneAcquisitionSequence = 511;

	public static final int dRepresentativeFrameNumber = 512;

	public static final int dFrameNumbersOfInterestFOI = 513;

	public static final int dFrameOfInterestDescription = 514;

	public static final int dMaskPointers = 515;

	public static final int dRWavePointer = 516;

	public static final int dMaskSubtractionSequence = 517;

	public static final int dMaskOperation = 518;

	public static final int dApplicableFrameRange = 519;

	public static final int dMaskFrameNumbers = 520;

	public static final int dContrastFrameAveraging = 521;

	public static final int dMaskSubPixelShift = 522;

	public static final int dTIDOffset = 523;

	public static final int dMaskOperationExplanation = 524;

	public static final int dStudyGroupLength = 525;

	public static final int dStudyStatusID = 526;

	public static final int dStudyPriorityID = 527;

	public static final int dStudyIDIssuer = 528;

	public static final int dStudyVerifiedDate = 529;

	public static final int dStudyVerifiedTime = 530;

	public static final int dStudyReadDate = 531;

	public static final int dStudyReadTime = 532;

	public static final int dScheduledStudyStartDate = 533;

	public static final int dScheduledStudyStartTime = 534;

	public static final int dScheduledStudyStopDate = 535;

	public static final int dScheduledStudyStopTime = 536;

	public static final int dScheduledStudyLocation = 537;

	public static final int dScheduledStudyLocationAETitle = 538;

	public static final int dReasonForStudy = 539;

	public static final int dRequestingPhysician = 540;

	public static final int dRequestingService = 541;

	public static final int dStudyArrivalDate = 542;

	public static final int dStudyArrivalTime = 543;

	public static final int dStudyCompletionDate = 544;

	public static final int dStudyCompletionTime = 545;

	public static final int dStudyComponentStatusID = 546;

	public static final int dRequestedProcedureDescription = 547;

	public static final int dRequestedProcedureCodeSequence = 548;

	public static final int dRequestedContrastAgent = 549;

	public static final int dStudyComments = 550;

	public static final int dVisitGroupLength = 551;

	public static final int dReferencedPatientAliasSequence = 552;

	public static final int dVisitStatusID = 553;

	public static final int dAdmissionID = 554;

	public static final int dIssuerOfAdmissionID = 555;

	public static final int dRouteOfAdmissions = 556;

	public static final int dScheduledAdmissionDate = 557;

	public static final int dScheduledAdmissionTime = 558;

	public static final int dScheduledDischargeDate = 559;

	public static final int dScheduledDischargeTime = 560;

	public static final int dScheduledPatientInstitutionResidence = 561;

	public static final int dAdmittingDate = 562;

	public static final int dAdmittingTime = 563;

	public static final int dDischargeDate = 564;

	public static final int dDischargeTime = 565;

	public static final int dDischargeDiagnosisDescription = 566;

	public static final int dDischargeDiagnosisCodeSequence = 567;

	public static final int dSpecialNeeds = 568;

	public static final int dCurrentPatientLocation = 569;

	public static final int dPatientInstitutionResidence = 570;

	public static final int dPatientState = 571;

	public static final int dVisitComments = 572;

	public static final int dModalityWorklistGroupLength = 573;

	public static final int dScheduledStationAETitle = 574;

	public static final int dScheduledProcedureStepStartDate = 575;

	public static final int dScheduledProcedureStepStartTime = 576;

	public static final int dScheduledProcedureStepEndDate = 577;

	public static final int dScheduledProcedureStepEndTime = 578;

	public static final int dScheduledPerformingPhysiciansName = 579;

	public static final int dScheduledProcedureStepDescription = 580;

	public static final int dScheduledActionItemCodeSequence = 581;

	public static final int dScheduledProcedureStepID = 582;

	public static final int dScheduledStationName = 583;

	public static final int dScheduledProcedureStepLocation = 584;

	public static final int dPreMedication = 585;

	public static final int dScheduledProcedureStepStatus = 586;

	public static final int dScheduledProcedureStepSequence = 587;

	public static final int dCommentsOnTheScheduledProcedureStep = 588;

	public static final int dRequestedProcedureID = 589;

	public static final int dReasonForTheRequestedProcedure = 590;

	public static final int dRequestedProcedurePriority = 591;

	public static final int dPatientTransportArrangements = 592;

	public static final int dRequestedProcedureLocation = 593;

	public static final int dPlacerOrderNumberProcedure = 594;

	public static final int dFillerOrderNumberProcedure = 595;

	public static final int dConfidentialityCode = 596;

	public static final int dReportingPriority = 597;

	public static final int dNamesOfIntendedRecipientsOfResults = 598;

	public static final int dRequestedProcedureComments = 599;

	public static final int dReasonForTheImagingServiceRequest = 600;

	public static final int dIssueDateOfImagingServiceRequest = 601;

	public static final int dIssueTimeOfImagingServiceRequest = 602;

	public static final int dPlacerOrderNumberImagingServiceRequest = 603;

	public static final int dFillerOrderNumberImagingServiceRequest = 604;

	public static final int dOrderEnteredBy = 605;

	public static final int dOrderEnterersLocation = 606;

	public static final int dOrderCallbackPhoneNumber = 607;

	public static final int dImagingServiceRequestComments = 608;

	public static final int dConfidentialityConstraintOnPatientData = 609;

	public static final int dCalibrationObject = 610;

	public static final int dDeviceSequence = 611;

	public static final int dDeviceLength = 612;

	public static final int dDeviceDiameter = 613;

	public static final int dDeviceDiameterUnits = 614;

	public static final int dDeviceVolume = 615;

	public static final int dInterMarkerDistance = 616;

	public static final int dDeviceDescription = 617;

	public static final int dGroupLength = 618;

	public static final int dEnergyWindowVector = 619;

	public static final int dNumberOfEnergyWindows = 620;

	public static final int dEnergyWindowInformationSequence = 621;

	public static final int dEnergyWindowRangeSequence = 622;

	public static final int dEnergyWindowLowerLimit = 623;

	public static final int dEnergyWindowUpperLimit = 624;

	public static final int dRadiopharmaceuticalInformationSequence = 625;

	public static final int dResidualSyringeCounts = 626;

	public static final int dEnergyWindowName = 627;

	public static final int dDetectorVector = 628;

	public static final int dNumberOfDetectors = 629;

	public static final int dDetectorInformationSequence = 630;

	public static final int dPhaseVector = 631;

	public static final int dNumberOfPhases = 632;

	public static final int dPhaseInformationSequence = 633;

	public static final int dNumberOfFramesInPhase = 634;

	public static final int dPhaseDelay = 635;

	public static final int dPauseBetweenFrames = 636;

	public static final int dRotationVector = 637;

	public static final int dNumberOfRotations = 638;

	public static final int dRotationInformationSequence = 639;

	public static final int dNumberOfFramesInRotation = 640;

	public static final int dRRIntervalVector = 641;

	public static final int dNumberOfRRIntervals = 642;

	public static final int dGatedInformationSequence = 643;

	public static final int dDataInformationSequence = 644;

	public static final int dTimeSlotVector = 645;

	public static final int dNumberOfTimeSlots = 646;

	public static final int dTimeSlotInformationSequence = 647;

	public static final int dTimeSlotTime = 648;

	public static final int dSliceVector = 649;

	public static final int dNumberOfSlices = 650;

	public static final int dAngularViewVector = 651;

	public static final int dTimeSliceVector = 652;

	public static final int dNumberOfTimeSlices = 653;

	public static final int dStartAngle = 654;

	public static final int dTypeOfDetectorMotion = 655;

	public static final int dTriggerVector = 656;

	public static final int dNumberOfTriggersInPhase = 657;

	public static final int dViewCodeSequence = 658;

	public static final int dViewAngulationModifierCodeSequence = 659;

	public static final int dRadionuclideCodeSequence = 660;

	public static final int dAdministrationRouteCodeSequence = 661;

	public static final int dRadiopharmaceuticalCodeSequence = 662;

	public static final int dCalibrationDataSequence = 663;

	public static final int dEnergyWindowNumber = 664;

	public static final int dImageID = 665;

	public static final int dPatientOrientationCodeSequence = 666;

	public static final int dPatientOrientationModifierCodeSequence = 667;

	public static final int dPatientGantryRelationshipCodeSequence = 668;

	public static final int dSeriesType = 669;

	public static final int dUnits = 670;

	public static final int dCountsSource = 671;

	public static final int dReprojectionMethod = 672;

	public static final int dRandomsCorrectionMethod = 673;

	public static final int dAttenuationCorrectionMethod = 674;

	public static final int dDecayCorrection = 675;

	public static final int dReconstructionMethod = 676;

	public static final int dDetectorLinesOfResponseUsed = 677;

	public static final int dScatterCorrectionMethod = 678;

	public static final int dAxialAcceptance = 679;

	public static final int dAxialMash = 680;

	public static final int dTransverseMash = 681;

	public static final int dDetectorElementSize = 682;

	public static final int dCoincidenceWindowWidth = 683;

	public static final int dSecondaryCountsType = 684;

	public static final int dFrameReferenceTime = 685;

	public static final int dPrimaryPromptsCountsAccumulated = 686;

	public static final int dSecondaryCountsAccumulated = 687;

	public static final int dSliceSensitivityFactor = 688;

	public static final int dDecayFactor = 689;

	public static final int dDoseCalibrationFactor = 690;

	public static final int dScatterFractionFactor = 691;

	public static final int dDeadTimeFactor = 692;

	public static final int dImageIndex = 693;

	public static final int dCountsIncluded = 694;

	public static final int dDeadTimeCorrectionFlag = 695;

	public static final int dStorageGroupLength = 696;

	public static final int dStorageMediaFilesetID = 697;

	public static final int dStorageMediaFilesetUID = 698;

	public static final int dIconImage = 699;

	public static final int dTopicTitle = 700;

	public static final int dTopicSubject = 701;

	public static final int dTopicAuthor = 702;

	public static final int dTopicKeyWords = 703;

	public static final int dFilmSessionGroupLength = 704;

	public static final int dNumberOfCopies = 705;

	public static final int dPrintPriority = 706;

	public static final int dMediumType = 707;

	public static final int dFilmDestination = 708;

	public static final int dFilmSessionLabel = 709;

	public static final int dMemoryAllocation = 710;

	public static final int dReferencedFilmBoxSequence = 711;

	public static final int dFilmBoxGroupLength = 712;

	public static final int dImageDisplayFormat = 713;

	public static final int dAnnotationDisplayFormatID = 714;

	public static final int dFilmOrientation = 715;

	public static final int dFilmSizeID = 716;

	public static final int dMagnificationType = 717;

	public static final int dSmoothingType = 718;

	public static final int dBorderDensity = 719;

	public static final int dEmptyImageDensity = 720;

	public static final int dMinDensity = 721;

	public static final int dMaxDensity = 722;

	public static final int dTrim = 723;

	public static final int dConfigurationInformation = 724;

	public static final int dReferencedFilmSessionSequence = 725;

	public static final int dReferencedImageBoxSequence = 726;

	public static final int dReferencedBasicAnnotationBoxSequence = 727;

	public static final int dImageBoxGroupLength = 728;

	public static final int dImageBoxPosition = 729;

	public static final int dPolarity = 730;

	public static final int dRequestedImageSize = 731;

	public static final int dPreformattedGrayscaleImageSequence = 732;

	public static final int dPreformattedColorImageSequence = 733;

	public static final int dReferencedImageOverlayBoxSequence = 734;

	public static final int dReferencedVOILUTBoxSequence = 735;

	public static final int dAnnotationGroupLength = 736;

	public static final int dAnnotationPosition = 737;

	public static final int dTextString = 738;

	public static final int dOverlayBoxGroupLength = 739;

	public static final int dReferencedOverlayPlaneSequence = 740;

	public static final int dReferencedOverlayPlaneGroups = 741;

	public static final int dOverlayMagnificationType = 742;

	public static final int dOverlaySmoothingType = 743;

	public static final int dOverlayForegroundDensity = 744;

	public static final int dOverlayMode = 745;

	public static final int dThresholdDensity = 746;

	public static final int dRETIRED_ReferencedImageBoxSequence = 747;

	public static final int dPrintJobGroupLength = 748;

	public static final int dPrintJobID = 749;

	public static final int dExecutionStatus = 750;

	public static final int dExecutionStatusInfo = 751;

	public static final int dCreationDate = 752;

	public static final int dCreationTime = 753;

	public static final int dOriginator = 754;

	public static final int dDestinationAE = 755;

	public static final int dOwnerID = 756;

	public static final int dNumberOfFilms = 757;

	public static final int dReferencedPrintJobSequence = 758;

	public static final int dPrinterGroupLength = 759;

	public static final int dPrinterStatus = 760;

	public static final int dPrinterStatusInfo = 761;

	public static final int dPrinterName = 762;

	public static final int dPrintQueueID = 763;

	public static final int dQueueStatus = 764;

	public static final int dPrintJobDescriptionSequence = 765;

	public static final int dQueueReferencedPrintJobSequence = 766;

	public static final int dRTImageLabel = 767;

	public static final int dRTImageName = 768;

	public static final int dRTImageDescription = 769;

	public static final int dReportedValuesOrigin = 770;

	public static final int dRTImagePlane = 771;

	public static final int dXRayImageReceptorAngle = 772;

	public static final int dRTImageOrientation = 773;

	public static final int dImagePlanePixelSpacing = 774;

	public static final int dRTImagePosition = 775;

	public static final int dRadiationMachineName = 776;

	public static final int dRadiationMachineSAD = 777;

	public static final int dRadiationMachineSSD = 778;

	public static final int dRTImageSID = 779;

	public static final int dSourceToReferenceObjectDistance = 780;

	public static final int dFractionNumber = 781;

	public static final int dExposureSequence = 782;

	public static final int dMetersetExposure = 783;

	public static final int dDVHType = 784;

	public static final int dDoseUnits = 785;

	public static final int dDoseType = 786;

	public static final int dDoseComment = 787;

	public static final int dNormalizationPoint = 788;

	public static final int dDoseSummationType = 789;

	public static final int dGridFrameOffsetVector = 790;

	public static final int dDoseGridScaling = 791;

	public static final int dRTDoseROISequence = 792;

	public static final int dDoseValue = 793;

	public static final int dDVHNormalizationPoint = 794;

	public static final int dDVHNormalizationDoseValue = 795;

	public static final int dDVHSequence = 796;

	public static final int dDVHDoseScaling = 797;

	public static final int dDVHVolumeUnits = 798;

	public static final int dDVHNumberOfBins = 799;

	public static final int dDVHData = 800;

	public static final int dDVHReferencedROISequence = 801;

	public static final int dDVHROIContributionType = 802;

	public static final int dDVHMinimumDose = 803;

	public static final int dDVHMaximumDose = 804;

	public static final int dDVHMeanDose = 805;

	public static final int dStructureSetLabel = 806;

	public static final int dStructureSetName = 807;

	public static final int dStructureSetDescription = 808;

	public static final int dStructureSetDate = 809;

	public static final int dStructureSetTime = 810;

	public static final int dReferencedFrameOfReferenceSequence = 811;

	public static final int dRTReferencedStudySequence = 812;

	public static final int dRTReferencedSeriesSequence = 813;

	public static final int dContourImageSequence = 814;

	public static final int dStructureSetROISequence = 815;

	public static final int dROINumber = 816;

	public static final int dReferencedFrameOfReferenceUID = 817;

	public static final int dROIName = 818;

	public static final int dROIDescription = 819;

	public static final int dROIDisplayColor = 820;

	public static final int dROIVolume = 821;

	public static final int dRTRelatedROISequence = 822;

	public static final int dRTROIRelationship = 823;

	public static final int dROIGenerationAlgorithm = 824;

	public static final int dROIGenerationDescription = 825;

	public static final int dROIContourSequence = 826;

	public static final int dContourSequence = 827;

	public static final int dContourGeometricType = 828;

	public static final int dContourSlabThickness = 829;

	public static final int dContourOffsetVector = 830;

	public static final int dNumberOfContourPoints = 831;

	public static final int dContourData = 832;

	public static final int dRTROIObservationsSequence = 833;

	public static final int dObservationNumber = 834;

	public static final int dReferencedROINumber = 835;

	public static final int dROIObservationLabel = 836;

	public static final int dRTROIIdentificationCodeSequence = 837;

	public static final int dROIObservationDescription = 838;

	public static final int dRelatedRTROIObservationsSequence = 839;

	public static final int dRTROIInterpretedType = 840;

	public static final int dROIInterpreter = 841;

	public static final int dROIPhysicalPropertiesSequence = 842;

	public static final int dROIPhysicalProperty = 843;

	public static final int dROIPhysicalPropertyValue = 844;

	public static final int dFrameOfReferenceRelationshipSequence = 845;

	public static final int dRelatedFrameOfReferenceUID = 846;

	public static final int dFrameOfReferenceTransformationType = 847;

	public static final int dFrameOfReferenceTransformationMatrix = 848;

	public static final int dFrameOfReferenceTransformationComment = 849;

	public static final int dRTPlanLabel = 850;

	public static final int dRTPlanName = 851;

	public static final int dRTPlanDescription = 852;

	public static final int dRTPlanDate = 853;

	public static final int dRTPlanTime = 854;

	public static final int dTreatmentProtocols = 855;

	public static final int dTreatmentIntent = 856;

	public static final int dTreatmentSites = 857;

	public static final int dRTPlanGeometry = 858;

	public static final int dPrescriptionDescription = 859;

	public static final int dDoseReferenceSequence = 860;

	public static final int dDoseReferenceNumber = 861;

	public static final int dDoseReferenceStructureType = 862;

	public static final int dDoseReferenceDescription = 863;

	public static final int dDoseReferencePointCoordinates = 864;

	public static final int dNominalPriorDose = 865;

	public static final int dDoseReferenceType = 866;

	public static final int dConstraintWeight = 867;

	public static final int dDeliveryWarningDose = 868;

	public static final int dDeliveryMaximumDose = 869;

	public static final int dTargetMinimumDose = 870;

	public static final int dTargetPrescriptionDose = 871;

	public static final int dTargetMaximumDose = 872;

	public static final int dTargetUnderdoseVolumeFraction = 873;

	public static final int dOrganAtRiskFullVolumeDose = 874;

	public static final int dOrganAtRiskLimitDose = 875;

	public static final int dOrganAtRiskMaximumDose = 876;

	public static final int dOrganAtRiskOverdoseVolumeFraction = 877;

	public static final int dToleranceTableSequence = 878;

	public static final int dToleranceTableNumber = 879;

	public static final int dToleranceTableLabel = 880;

	public static final int dGantryAngleTolerance = 881;

	public static final int dBeamLimitingDeviceAngleTolerance = 882;

	public static final int dBeamLimitingDeviceToleranceSequence = 883;

	public static final int dBeamLimitingDevicePositionTolerance = 884;

	public static final int dPatientSupportAngleTolerance = 885;

	public static final int dTableTopEccentricAngleTolerance = 886;

	public static final int dTableTopVerticalPositionTolerance = 887;

	public static final int dTableTopLongitudinalPositionTolerance = 888;

	public static final int dTableTopLateralPositionTolerance = 889;

	public static final int dRTPlanRelationship = 890;

	public static final int dFractionGroupSequence = 891;

	public static final int dFractionGroupNumber = 892;

	public static final int dNumberOfFractionsPlanned = 893;

	public static final int dNumberOfFractionsPerDay = 894;

	public static final int dRepeatFractionCycleLength = 895;

	public static final int dFractionPattern = 896;

	public static final int dNumberOfBeams = 897;

	public static final int dBeamDoseSpecificationPoint = 898;

	public static final int dBeamDose = 899;

	public static final int dBeamMeterset = 900;

	public static final int dNumberOfBrachyApplicationSetups = 901;

	public static final int dBrachyApplicationSetupDoseSpecificationPoint = 902;

	public static final int dBrachyApplicationSetupDose = 903;

	public static final int dBeamSequence = 904;

	public static final int dTreatmentMachineName = 905;

	public static final int dPrimaryDosimeterUnit = 906;

	public static final int dSource_AxisDistance = 907;

	public static final int dBeamLimitingDeviceSequence = 908;

	public static final int dRTBeamLimitingDeviceType = 909;

	public static final int dSourceToBeamLimitingDeviceDistance = 910;

	public static final int dNumberOfLeafJawPairs = 911;

	public static final int dLeafPositionBoundaries = 912;

	public static final int dBeamNumber = 913;

	public static final int dBeamName = 914;

	public static final int dBeamDescription = 915;

	public static final int dBeamType = 916;

	public static final int dRadiationType = 917;

	public static final int dReferenceImageNumber = 918;

	public static final int dPlannedVerificationImageSequence = 919;

	public static final int dImagingDeviceSpecificAcquisitionParameters = 920;

	public static final int dTreatmentDeliveryType = 921;

	public static final int dNumberOfWedges = 922;

	public static final int dWedgeSequence = 923;

	public static final int dWedgeNumber = 924;

	public static final int dWedgeType = 925;

	public static final int dWedgeID = 926;

	public static final int dWedgeAngle = 927;

	public static final int dWedgeFactor = 928;

	public static final int dWedgeOrientation = 929;

	public static final int dSourceToWedgeTrayDistance = 930;

	public static final int dNumberOfCompensators = 931;

	public static final int dMaterialID = 932;

	public static final int dTotalCompensatorTrayFactor = 933;

	public static final int dCompensatorSequence = 934;

	public static final int dCompensatorNumber = 935;

	public static final int dCompensatorID = 936;

	public static final int dSourceToCompensatorTrayDistance = 937;

	public static final int dCompensatorRows = 938;

	public static final int dCompensatorColumns = 939;

	public static final int dCompensatorPixelSpacing = 940;

	public static final int dCompensatorPosition = 941;

	public static final int dCompensatorTransmissionData = 942;

	public static final int dCompensatorThicknessData = 943;

	public static final int dNumberOfBoli = 944;

	public static final int dNumberOfBlocks = 945;

	public static final int dTotalBlockTrayFactor = 946;

	public static final int dBlockSequence = 947;

	public static final int dBlockTrayID = 948;

	public static final int dSourceToBlockTrayDistance = 949;

	public static final int dBlockType = 950;

	public static final int dBlockDivergence = 951;

	public static final int dBlockNumber = 952;

	public static final int dBlockName = 953;

	public static final int dBlockThickness = 954;

	public static final int dBlockTransmission = 955;

	public static final int dBlockNumberOfPoints = 956;

	public static final int dBlockData = 957;

	public static final int dApplicatorSequence = 958;

	public static final int dApplicatorID = 959;

	public static final int dApplicatorType = 960;

	public static final int dApplicatorDescription = 961;

	public static final int dCumulativeDoseReferenceCoefficient = 962;

	public static final int dFinalCumulativeMetersetWeight = 963;

	public static final int dNumberOfControlPoints = 964;

	public static final int dControlPointSequence = 965;

	public static final int dControlPointIndex = 966;

	public static final int dNominalBeamEnergy = 967;

	public static final int dDoseRateSet = 968;

	public static final int dWedgePositionSequence = 969;

	public static final int dWedgePosition = 970;

	public static final int dBeamLimitingDevicePositionSequence = 971;

	public static final int dLeafJawPositions = 972;

	public static final int dGantryAngle = 973;

	public static final int dGantryRotationDirection = 974;

	public static final int dBeamLimitingDeviceAngle = 975;

	public static final int dBeamLimitingDeviceRotationDirection = 976;

	public static final int dPatientSupportAngle = 977;

	public static final int dPatientSupportRotationDirection = 978;

	public static final int dTableTopEccentricAxisDistance = 979;

	public static final int dTableTopEccentricAngle = 980;

	public static final int dTableTopEccentricRotationDirection = 981;

	public static final int dTableTopVerticalPosition = 982;

	public static final int dTableTopLongitudinalPosition = 983;

	public static final int dTableTopLateralPosition = 984;

	public static final int dIsocenterPosition = 985;

	public static final int dSurfaceEntryPoint = 986;

	public static final int dSourceToSurfaceDistance = 987;

	public static final int dCumulativeMetersetWeight = 988;

	public static final int dPatientSetupSequence = 989;

	public static final int dPatientSetupNumber = 990;

	public static final int dPatientAdditionalPosition = 991;

	public static final int dFixationDeviceSequence = 992;

	public static final int dFixationDeviceType = 993;

	public static final int dFixationDeviceLabel = 994;

	public static final int dFixationDeviceDescription = 995;

	public static final int dFixationDevicePosition = 996;

	public static final int dShieldingDeviceSequence = 997;

	public static final int dShieldingDeviceType = 998;

	public static final int dShieldingDeviceLabel = 999;

	public static final int dShieldingDeviceDescription = 1000;

	public static final int dShieldingDevicePosition = 1001;

	public static final int dSetupTechnique = 1002;

	public static final int dSetupTechniqueDescription = 1003;

	public static final int dSetupDeviceSequence = 1004;

	public static final int dSetupDeviceType = 1005;

	public static final int dSetupDeviceLabel = 1006;

	public static final int dSetupDeviceDescription = 1007;

	public static final int dSetupDeviceParameter = 1008;

	public static final int dSetupReferenceDescription = 1009;

	public static final int dTableTopVerticalSetupDisplacement = 1010;

	public static final int dTableTopLongitudinalSetupDisplacement = 1011;

	public static final int dTableTopLateralSetupDisplacement = 1012;

	public static final int dBrachyTreatmentTechnique = 1013;

	public static final int dBrachyTreatmentType = 1014;

	public static final int dTreatmentMachineSequence = 1015;

	public static final int dSourceSequence = 1016;

	public static final int dSourceNumber = 1017;

	public static final int dSourceType = 1018;

	public static final int dSourceManufacturer = 1019;

	public static final int dActiveSourceDiameter = 1020;

	public static final int dActiveSourceLength = 1021;

	public static final int dSourceEncapsulationNominalThickness = 1022;

	public static final int dSourceEncapsulationNominalTransmission = 1023;

	public static final int dSourceIsotopeName = 1024;

	public static final int dSourceIsotopeHalfLife = 1025;

	public static final int dReferenceAirKermaRate = 1026;

	public static final int dAirKermaRateReferenceDate = 1027;

	public static final int dAirKermaRateReferenceTime = 1028;

	public static final int dApplicationSetupSequence = 1029;

	public static final int dApplicationSetupType = 1030;

	public static final int dApplicationSetupNumber = 1031;

	public static final int dApplicationSetupName = 1032;

	public static final int dApplicationSetupManufacturer = 1033;

	public static final int dTemplateNumber = 1034;

	public static final int dTemplateType = 1035;

	public static final int dTemplateName = 1036;

	public static final int dTotalReferenceAirKerma = 1037;

	public static final int dBrachyAccessoryDeviceSequence = 1038;

	public static final int dBrachyAccessoryDeviceNumber = 1039;

	public static final int dBrachyAccessoryDeviceID = 1040;

	public static final int dBrachyAccessoryDeviceType = 1041;

	public static final int dBrachyAccessoryDeviceName = 1042;

	public static final int dBrachyAccessoryDeviceNominalThickness = 1043;

	public static final int dBrachyAccessoryDeviceNominalTransmission = 1044;

	public static final int dChannelSequence = 1045;

	public static final int dChannelNumber = 1046;

	public static final int dChannelLength = 1047;

	public static final int dChannelTotalTime = 1048;

	public static final int dSourceMovementType = 1049;

	public static final int dNumberOfPulses = 1050;

	public static final int dPulseRepetitionInterval = 1051;

	public static final int dSourceApplicatorNumber = 1052;

	public static final int dSourceApplicatorID = 1053;

	public static final int dSourceApplicatorType = 1054;

	public static final int dSourceApplicatorName = 1055;

	public static final int dSourceApplicatorLength = 1056;

	public static final int dSourceApplicatorManufacturer = 1057;

	public static final int dSourceApplicatorWallNominalThickness = 1058;

	public static final int dSourceApplicatorWallNominalTransmission = 1059;

	public static final int dSourceApplicatorStepSize = 1060;

	public static final int dTransferTubeNumber = 1061;

	public static final int dTransferTubeLength = 1062;

	public static final int dChannelShieldSequence = 1063;

	public static final int dChannelShieldNumber = 1064;

	public static final int dChannelShieldID = 1065;

	public static final int dChannelShieldName = 1066;

	public static final int dChannelShieldNominalThickness = 1067;

	public static final int dChannelShieldNominalTransmission = 1068;

	public static final int dFinalCumulativeTimeWeight = 1069;

	public static final int dBrachyControlPointSequence = 1070;

	public static final int dControlPointRelativePosition = 1071;

	public static final int dControlPoint3DPosition = 1072;

	public static final int dCumulativeTimeWeight = 1073;

	public static final int dReferencedRTPlanSequence = 1074;

	public static final int dReferencedBeamSequence = 1075;

	public static final int dReferencedBeamNumber = 1076;

	public static final int dReferencedReferenceImageNumber = 1077;

	public static final int dStartCumulativeMetersetWeight = 1078;

	public static final int dEndCumulativeMetersetWeight = 1079;

	public static final int dReferencedBrachyApplicationSetupSequence = 1080;

	public static final int dReferencedBrachyApplicationSetupNumber = 1081;

	public static final int dReferencedSourceNumber = 1082;

	public static final int dReferencedFractionGroupSequence = 1083;

	public static final int dReferencedFractionGroupNumber = 1084;

	public static final int dReferencedVerificationImageSequence = 1085;

	public static final int dReferencedReferenceImageSequence = 1086;

	public static final int dReferencedDoseReferenceSequence = 1087;

	public static final int dReferencedDoseReferenceNumber = 1088;

	public static final int dBrachyReferencedDoseReferenceSequence = 1089;

	public static final int dReferencedStructureSetSequence = 1090;

	public static final int dReferencedPatientSetupNumber = 1091;

	public static final int dReferencedDoseSequence = 1092;

	public static final int dReferencedToleranceTableNumber = 1093;

	public static final int dReferencedBolusSequence = 1094;

	public static final int dReferencedWedgeNumber = 1095;

	public static final int dReferencedCompensatorNumber = 1096;

	public static final int dReferencedBlockNumber = 1097;

	public static final int dReferencedControlPointIndex = 1098;

	public static final int dApprovalStatus = 1099;

	public static final int dReviewDate = 1100;

	public static final int dReviewTime = 1101;

	public static final int dReviewerName = 1102;

	public static final int dResultsGroupLength = 1103;

	public static final int dResultsID = 1104;

	public static final int dResultsIDIssuer = 1105;

	public static final int dReferencedInterpretationSequence = 1106;

	public static final int dInterpretationRecordedDate = 1107;

	public static final int dInterpretationRecordedTime = 1108;

	public static final int dInterpretationRecorder = 1109;

	public static final int dReferenceToRecordedSound = 1110;

	public static final int dInterpretationTranscriptionDate = 1111;

	public static final int dInterpretationTranscriptionTime = 1112;

	public static final int dInterpretationTranscriber = 1113;

	public static final int dInterpretationText = 1114;

	public static final int dInterpretationAuthor = 1115;

	public static final int dInterpretationApproverSequence = 1116;

	public static final int dInterpretationApprovalDate = 1117;

	public static final int dInterpretationApprovalTime = 1118;

	public static final int dPhysicianApprovingInterpretation = 1119;

	public static final int dInterpretationDiagnosisDescription = 1120;

	public static final int dDiagnosisCodeSequence = 1121;

	public static final int dResultsDistributionListSequence = 1122;

	public static final int dDistributionName = 1123;

	public static final int dDistributionAddress = 1124;

	public static final int dInterpretationID = 1125;

	public static final int dInterpretationIDIssuer = 1126;

	public static final int dInterpretationTypeID = 1127;

	public static final int dInterpretationStatusID = 1128;

	public static final int dImpressions = 1129;

	public static final int dResultsComments = 1130;

	public static final int dCurveGroupLength = 1131;

	public static final int dCurveDimensions = 1132;

	public static final int dNumberOfPoints = 1133;

	public static final int dTypeOfData = 1134;

	public static final int dCurveDescription = 1135;

	public static final int dAxisUnits = 1136;

	public static final int dAxisLabels = 1137;

	public static final int dDataValueRepresentation = 1138;

	public static final int dMinimumCoordinateValue = 1139;

	public static final int dMaximumCoordinateValue = 1140;

	public static final int dCurveRange = 1141;

	public static final int dCurveDataDescriptor = 1142;

	public static final int dCoordinateStartValue = 1143;

	public static final int dCoordinateStepValue = 1144;

	public static final int dAudioType = 1145;

	public static final int dAudioSampleFormat = 1146;

	public static final int dNumberOfChannels = 1147;

	public static final int dNumberOfSamples = 1148;

	public static final int dSampleRate = 1149;

	public static final int dTotalTime = 1150;

	public static final int dAudioSampleData = 1151;

	public static final int dAudioComments = 1152;

	public static final int dCurveLabel = 1153;

	public static final int dCurveReferencedOverlaySequence = 1154;

	public static final int dCurveReferencedOverlayGroup = 1155;

	public static final int dCurveData = 1156;

	public static final int dOverlayGroupLength = 1157;

	public static final int dOverlayRows = 1158;

	public static final int dOverlayColumns = 1159;

	public static final int dOverlayPlanes = 1160;

	public static final int dOverlayNumberOfFrames = 1161;

	public static final int dOverlayDescription = 1162;

	public static final int dOverlayType = 1163;

	public static final int dOverlaySubtype = 1164;

	public static final int dOverlayOrigin = 1165;

	public static final int dImageFrameOrigin = 1166;

	public static final int dOverlayPlaneOrigin = 1167;

	public static final int dOverlayBitsAllocated = 1168;

	public static final int dOverlayBitPosition = 1169;

	public static final int dOverlayDescriptorGray = 1170;

	public static final int dOverlayDescriptorRed = 1171;

	public static final int dOverlayDescriptorGreen = 1172;

	public static final int dOverlayDescriptorBlue = 1173;

	public static final int dOverlayGray = 1174;

	public static final int dOverlayRed = 1175;

	public static final int dOverlayGreen = 1176;

	public static final int dOverlayBlue = 1177;

	public static final int dROIArea = 1178;

	public static final int dROIMean = 1179;

	public static final int dROIStandardDeviation = 1180;

	public static final int dOverlayLabel = 1181;

	public static final int dOverlayData = 1182;

	public static final int dPixelDataGroupLength = 1183;

	public static final int dPixelData = 1184;

	public static final int dDataSetTrailingPadding = 1185;

	public static final int dItem = 1186;

	public static final int dItemDelimitationItem = 1187;

	public static final int dSequenceDelimitationItem = 1188;

	public static final int dUNDEFINED = 1189;

	public static final int dAnatomicStructureSpaceOrRegionSequence = 1190;

	public static final int dHardcopyCreationDeviceID = 1191;

	public static final int dHardcopyDeviceManufacturer = 1192;

	public static final int dHardcopyDeviceSoftwareVersion = 1193;

	public static final int dHardcopyDeviceManfuacturersModelName = 1194;

	public static final int dExposureinuAs = 1195;

	public static final int dColumnAngulation = 1196;

	public static final int dReferencedStandaloneSOPInstanceSequence = 1197;

	public static final int dPerformedStationAETitle = 1198;

	public static final int dPerformedStationName = 1199;

	public static final int dPerformedLocation = 1200;

	public static final int dPerformedProcedureStepStartDate = 1201;

	public static final int dPerformedProcedureStepStartTime = 1202;

	public static final int dPerformedProcedureStepEndDate = 1203;

	public static final int dPerformedProcedureStepEndTime = 1204;

	public static final int dPerformedProcedureStepStatus = 1205;

	public static final int dPerformedProcedureStepID = 1206;

	public static final int dPerformedProcedureStepDescription = 1207;

	public static final int dPerformedProcedureTypeDescription = 1208;

	public static final int dPerformedActionItemSequence = 1209;

	public static final int dScheduledStepAttributesSequence = 1210;

	public static final int dRequestAttributesSequence = 1211;

	public static final int dCommentsOnThePerformedProcedureSteps = 1212;

	public static final int dQuantitySequence = 1213;

	public static final int dQuantity = 1214;

	public static final int dMeasuringUnitsSequence = 1215;

	public static final int dBillingItemSequence = 1216;

	public static final int dTotalTimeOfFluoroscopy = 1217;

	public static final int dTotalNumberOfExposures = 1218;

	public static final int dEntranceDose = 1219;

	public static final int dExposedArea = 1220;

	public static final int dDistanceSourceToEntrance = 1221;

	public static final int dCommentsOnRadiationDose = 1222;

	public static final int dBillingProcedureStepSequence = 1223;

	public static final int dFilmConsumptionSequence = 1224;

	public static final int dBillingSuppliesAndDevicesSequence = 1225;

	public static final int dReferencedProcedureStepSequence = 1226;

	public static final int dPerformedSeriesSequence = 1227;

	public static final int dXRayAngioDeviceGroupLength = 1228;

	public static final int dColorImagePrintingFlag = 1229;

	public static final int dCollationFlag = 1230;

	public static final int dAnnotationFlag = 1231;

	public static final int dImageOverlayFlag = 1232;

	public static final int dPresentationLUTFlag = 1233;

	public static final int dImageBoxPresentationLUTFlag = 1234;

	public static final int dReferencedStoredPrintSequence = 1235;

	public static final int dIllumination = 1236;

	public static final int dReflectedAmbientLight = 1237;

	public static final int dPresentationLUTGroupLength = 1238;

	public static final int dPresentationLUTSequence = 1239;

	public static final int dPresentationLUTShape = 1240;

	public static final int dReferencedPresentationLUTSequence = 1241;

	public static final int dQueueGroupLength = 1242;

	public static final int dPrintContentGroupLength = 1243;

	public static final int dPrintManagementCapabilitiesSequence = 1244;

	public static final int dPrinterCharacteristicsSequence = 1245;

	public static final int dFilmBoxContentSequence = 1246;

	public static final int dImageBoxContentSequence = 1247;

	public static final int dAnnotationContentSequence = 1248;

	public static final int dImageOverlayBoxContentSequence = 1249;

	public static final int dPresentationLUTContentSequence = 1250;

	public static final int dProposedStudySequence = 1251;

	public static final int dOriginalImageSequence = 1252;

	public static final int dRTImageGroupLength = 1253;

	public static final int dRTDoseGroupLength = 1254;

	public static final int dRTStructureSetGroupLength = 1255;

	public static final int dRTPlanGroupLength = 1256;

	public static final int dRTRelationshipGroupLength = 1257;

	public static final int dRTApprovalGroupLength = 1258;

	public static final int dRETIRED_LossyImageCompression = 1259;

	public static final int dPresentationIntentType = 1260;

	public static final int dTableType = 1261;

	public static final int dRectificationType = 1262;

	public static final int dAnodeTargetMaterial = 1263;

	public static final int dBodyPartThickness = 1264;

	public static final int dCompressionForce = 1265;

	public static final int dTomoType = 1266;

	public static final int dTomoClass = 1267;

	public static final int dNumberofTomosynthesisSourceImages = 1268;

	public static final int dPositionerType = 1269;

	public static final int dProjectionEponymousNameCodeSequence = 1270;

	public static final int dDetectorConditionsNominalFlag = 1271;

	public static final int dDetectorTemperature = 1272;

	public static final int dDetectorType = 1273;

	public static final int dDetectorConfiguration = 1274;

	public static final int dDetectorDescription = 1275;

	public static final int dDetectorMode = 1276;

	public static final int dDetectorID = 1277;

	public static final int dDateofLastDetectorCalibration = 1278;

	public static final int dTimeofLastDetectorCalibration = 1279;

	public static final int dExposuresOnDetectorSinceLastCalibration = 1280;

	public static final int dExposuresOnDetectorSinceManufactured = 1281;

	public static final int dDetectorTimeSinceLastExposure = 1282;

	public static final int dDetectorActiveTime = 1283;

	public static final int dDetectorActivationOffsetFromExposure = 1284;

	public static final int dDetectorBinning = 1285;

	public static final int dDetectorElementPhysicalSize = 1286;

	public static final int dDetectorElementSpacing = 1287;

	public static final int dDetectorActiveShape = 1288;

	public static final int dDetectorActiveDimensions = 1289;

	public static final int dDetectorActiveOrigin = 1290;

	public static final int dFieldofViewOrigin = 1291;

	public static final int dFieldofViewRotation = 1292;

	public static final int dFieldofViewHorizontalFlip = 1293;

	public static final int dGridAbsorbingMaterial = 1294;

	public static final int dGridSpacingMaterial = 1295;

	public static final int dGridThickness = 1296;

	public static final int dGridPitch = 1297;

	public static final int dGridAspectRatio = 1298;

	public static final int dGridPeriod = 1299;

	public static final int dGridFocalDistance = 1300;

	public static final int dFilterMaterial = 1301;

	public static final int dFilterThicknessMinimum = 1302;

	public static final int dFilterThicknessMaximum = 1303;

	public static final int dExposureControlMode = 1304;

	public static final int dExposureControlModeDescription = 1305;

	public static final int dExposureStatus = 1306;

	public static final int dPhototimerSetting = 1307;

	public static final int dImageLaterality = 1308;

	public static final int dQualityControlImage = 1309;

	public static final int dBurnedInAnnotation = 1310;

	public static final int dPixelIntensityRelationshipSign = 1311;

	public static final int dImplantPresent = 1312;

	public static final int dLossyImageCompressionRatio = 1313;

	public static final int dDistanceSourceToSupport = 1314;

	public static final int dXRayOutput = 1315;

	public static final int dHalfValueLayer = 1316;

	public static final int dOrganDose = 1317;

	public static final int dOrganExposed = 1318;

	public static final int dAcquisitionContextSequence = 1319;

	public static final int dAcquisitionContextDescription = 1320;

	public static final int dMeasurementUnitsCodeSequence = 1321;

	public static final int dConceptNameCodeSequence = 1322;

	public static final int dDate = 1323;

	public static final int dTime = 1324;

	public static final int dPersonName = 1325;

	public static final int dReferencedFrameNumbers = 1326;

	public static final int dTextValue = 1327;

	public static final int dConceptCodeSequence = 1328;

	public static final int dNumericValue = 1329;

	public static final int dHistogramSequence = 1330;

	public static final int dHistogramNumberofBins = 1331;

	public static final int dHistogramFirstBinValue = 1332;

	public static final int dHistogramLastBinValue = 1333;

	public static final int dHistogramBinWidth = 1334;

	public static final int dHistogramExplanation = 1335;

	public static final int dHistogramData = 1336;

	public static final int dCodingSchemeVersion = 1337;

	public static final int dMappingResource = 1338;

	public static final int dContextGroupVersion = 1339;

	public static final int dContextGroupLocalVersion = 1340;

	public static final int dCodeSetExtensionFlag = 1341;

	public static final int dPrivateCodingSchemeCreatorUID = 1342;

	public static final int dCodeSetExtensionCreatorUID = 1343;

	public static final int dContextIdentifier = 1344;

	public static final int dSOPClassesSupported = 1345;

	public static final int dItemNumber = 1346;

	public static final int dPrinterConfigurationSequence = 1347;

	public static final int dMemoryBitDepth = 1348;

	public static final int dPrintingBitDepth = 1349;

	public static final int dMediaInstalledSequence = 1350;

	public static final int dOtherMediaAvailableSequence = 1351;

	public static final int dSupportedImageDisplayFormatsSequence = 1352;

	public static final int dPrinterResolutionID = 1353;

	public static final int dDefaultPrinterResolutionID = 1354;

	public static final int dDefaultMagnificationType = 1355;

	public static final int dOtherMagnificationTypesAvailable = 1356;

	public static final int dDefaultSmoothingType = 1357;

	public static final int dOtherSmoothingTypesAvailable = 1358;

	public static final int dConfigurationInformationDescription = 1359;

	public static final int dMaximumCollatedFilms = 1360;

	public static final int dPrinterPixelSpacing = 1361;

	public static final int dRequestedDecimateCropBehavior = 1362;

	public static final int dRequestedResolutionID = 1363;

	public static final int dRequestedImageSizeFlag = 1364;

	public static final int dDecimateCropResult = 1365;

	public static final int dOverlayPixelDataSequence = 1366;

	public static final int dOverlayOrImageMagnification = 1367;

	public static final int dMagnifyToNumberOfColumns = 1368;

	public static final int dOverlayBackgroundDensity = 1369;

	public static final int dSlideIdentifier = 1370;

	public static final int dImageCenterPointCoordinatesSequence = 1371;

	public static final int dXOffsetInSlideCoordinateSystem = 1372;

	public static final int dYOffsetInSlideCoordinateSystem = 1373;

	public static final int dZOffsetInSlideCoordinateSystem = 1374;

	public static final int dPixelSpacingSequence = 1375;

	public static final int dCoordinateSystemAxisCodeSequence = 1376;

	public static final int dShutterPresentationValue = 1377;

	public static final int dShutterOverlayGroup = 1378;

	public static final int dCurveActivationLayer = 1379;

	public static final int dOverlayActivationLayer = 1380;

	public static final int dGraphicAnnotationSequence = 1381;

	public static final int dGraphicLayer = 1382;

	public static final int dBoundingBoxAnnotationUnits = 1383;

	public static final int dAnchorPointAnnotationUnits = 1384;

	public static final int dGraphicAnnotationUnits = 1385;

	public static final int dUnformattedTextValue = 1386;

	public static final int dTextObjectSequence = 1387;

	public static final int dGraphicObjectSequence = 1388;

	public static final int dBoundingBoxTLHC = 1389;

	public static final int dBoundingBoxBRHC = 1390;

	public static final int dAnchorPoint = 1391;

	public static final int dAnchorPointVisibility = 1392;

	public static final int dGraphicDimensions = 1393;

	public static final int dNumberOfGraphicPoints = 1394;

	public static final int dGraphicData = 1395;

	public static final int dGraphicType = 1396;

	public static final int dGraphicFilled = 1397;

	public static final int dImageHorizontalFlip = 1398;

	public static final int dGraphicLayerSequence = 1399;

	public static final int dGraphicLayerOrder = 1400;

	public static final int dGraphicLayerRecommendedDisplayGrayScaleValue = 1401;

	public static final int dGraphicLayerDescription = 1402;

	public static final int dPresentationLabel = 1403;

	public static final int dPresentationDescription = 1404;

	public static final int dPresentationCreationDate = 1405;

	public static final int dPresentationCreationTime = 1406;

	public static final int dPresentationCreatorsName = 1407;

	public static final int dSoftcopyVOILUTSequence = 1408;

	public static final int dBoundingBoxTHJ = 1409;

	public static final int dImageRotation = 1410;

	public static final int dDisplayedAreaTLHC = 1411;

	public static final int dDisplayedAreaBRHC = 1412;

	public static final int dDisplayedAreaSelectionSequence = 1413;

	public static final int dGraphicLayerRecommendedDisplayRGBValue = 1414;

	public static final int dPresentationSizeMode = 1415;

	public static final int dPresentationPixelSpacing = 1416;

	public static final int dPresentationPixelAspectRatio = 1417;

	public static final int dPresentationPixelMagnificationRatio = 1418;

	public static final int dPlacerOrderNumber = 1419;

	public static final int dFillerOrderNumber = 1420;

	public static final int dSpecimenAccessionNumber = 1421;

	public static final int dSpecimenSequence = 1422;

	public static final int dSpecimenIdentifier = 1423;

	public static final int dSpecimenTypeCodeSequence = 1424;

	public static final int dRelationshipType = 1425;

	public static final int dVerifyingOrganization = 1426;

	public static final int dVerificationDateTime = 1427;

	public static final int dObservationDateTime = 1428;

	public static final int dValueType = 1429;

	public static final int dContinuityOfContent = 1430;

	public static final int dVerifyingObserverSequence = 1431;

	public static final int dVerifyingObserverName = 1432;

	public static final int dVerifyingObserverIdentificationCode = 1433;

	public static final int dDateTime = 1434;

	public static final int dUID = 1435;

	public static final int dTemporalRangeType = 1436;

	public static final int dReferencedSamplePositions = 1437;

	public static final int dReferencedTimeOffsets = 1438;

	public static final int dReferencedDatetime = 1439;

	public static final int dMeasuredValueSequence = 1440;

	public static final int dPredecessorDocumentsSequence = 1441;

	public static final int dReferencedRequestSequence = 1442;

	public static final int dPerformedProcedureCodeSequence = 1443;

	public static final int dCurrentRequestedProcedureEvidenceSequence = 1444;

	public static final int dPertinentOtherEvidenceSequence = 1445;

	public static final int dCompletionFlag = 1446;

	public static final int dCompletionFlagDescription = 1447;

	public static final int dVerificationFlag = 1448;

	public static final int dContentTemplateSequence = 1449;

	public static final int dIdenticalDocumentsSequence = 1450;

	public static final int dContentSequence = 1451;

	public static final int dTemplateIdentifier = 1452;

	public static final int dTemplateVersion = 1453;

	public static final int dTemplateLocalVersion = 1454;

	public static final int dTemplateExtensionFlag = 1455;

	public static final int dTemplateExtensionOrganizationUID = 1456;

	public static final int dTemplateExtensionCreatorUID = 1457;

	public static final int dReferencedContentItemIdentifier = 1458;

	public static final int dAcquisitionDatetime = 1459;

	public static final int dImageTriggerDelay = 1460;

	public static final int dMultiplexGroupTimeOffset = 1461;

	public static final int dTriggerTimeOffset = 1462;

	public static final int dSynchronizationTrigger = 1463;

	public static final int dSynchronizationChannel = 1464;

	public static final int dTriggerSamplePosition = 1465;

	public static final int dAcquisitionTimeSynchronized = 1466;

	public static final int dTimeDistributionProtocol = 1467;

	public static final int dTimeSource = 1468;

	public static final int dSynchronizationFrameofReferenceUID = 1469;

	public static final int dWaveformOriginality = 1470;

	public static final int dNumberofWaveformChannels = 1471;

	public static final int dNumberofWaveformSamples = 1472;

	public static final int dSamplingFrequency = 1473;

	public static final int dMultiplexGroupLabel = 1474;

	public static final int dChannelDefinitionSequence = 1475;

	public static final int dWaveformChannelNumber = 1476;

	public static final int dChannelLabel = 1477;

	public static final int dChannelStatus = 1478;

	public static final int dChannelSourceSequence = 1479;

	public static final int dChannelSourceModifiersSequence = 1480;

	public static final int dSourceWaveformSequence = 1481;

	public static final int dChannelDerivationDescription = 1482;

	public static final int dChannelSensitivity = 1483;

	public static final int dChannelSensitivityUnitsSequence = 1484;

	public static final int dChannelSensitivityCorrectionFactor = 1485;

	public static final int dChannelBaseline = 1486;

	public static final int dChannelTimeSkew = 1487;

	public static final int dChannelSampleSkew = 1488;

	public static final int dChannelOffset = 1489;

	public static final int dWaveformBitsStored = 1490;

	public static final int dFilterLowFrequency = 1491;

	public static final int dFilterHighFrequency = 1492;

	public static final int dNotchFilterFrequency = 1493;

	public static final int dNotchFilterBandwidth = 1494;

	public static final int dReferencedWaveformChannels = 1495;

	public static final int dAnnotationGroupNumber = 1496;

	public static final int dModifierCodeSequence = 1497;

	public static final int dAnnotationSequence = 1498;

	public static final int dWaveformSequence = 1499;

	public static final int dChannelMinimumValue = 1500;

	public static final int dChannelMaximumValue = 1501;

	public static final int dWaveformBitsAllocated = 1502;

	public static final int dWaveformSampleInterpretation = 1503;

	public static final int dWaveformPaddingValue = 1504;

	public static final int dWaveformData = 1505;

	public static final int dInstanceAvailability = 1506;

	public static final int dTimezoneOffsetFromUTC = 1507;

	public static final int dReferencedInstanceSequence = 1508;

	public static final int dReferencedWaveformSequence = 1509;

	public static final int dViewName = 1510;

	public static final int dPatientsPrimaryLanguageCodeSequence = 1511;

	public static final int dPatientsPrimaryLanguageCodeModifierSequence = 1512;

	public static final int dPageNumberVector = 1513;

	public static final int dFrameLabelVector = 1514;

	public static final int dFramePrimaryAngleVector = 1515;

	public static final int dFrameSecondaryAngleVector = 1516;

	public static final int dSliceLocationVector = 1517;

	public static final int dDisplayWindowLabelVector = 1518;

	public static final int dNominalScannedPixelSpacing = 1519;

	public static final int dDigitizingDeviceTransportDirection = 1520;

	public static final int dRotationOfScannedFilm = 1521;

	public static final int dIVUSAcquisition = 1522;

	public static final int dIVUSPullbackRate = 1523;

	public static final int dIVUSGatedRate = 1524;

	public static final int dIVUSPullbackStartFrameNumber = 1525;

	public static final int dIVUSPullbackStopFrameNumber = 1526;

	public static final int dLesionNumber = 1527;

	public static final int dExposureTimeInB5S = 1528;

	public static final int dXRayTubeCurrentInB5A = 1529;

	public static final int dPartialView = 1530;

	public static final int dPartialViewDescription = 1531;

	public static final int dStageCodeSequence = 1532;

	public static final int dExposureDoseSequence = 1533;

	public static final int dGeneralPurposeScheduledProcedureStepStatus = 1534;

	public static final int dGeneralPurposePerformedProcedureStepStatus = 1535;

	public static final int dGeneralPurposeScheduledProcedureStepPriority = 1536;

	public static final int dScheduledProcessingApplicationsCodeSequence = 1537;

	public static final int dScheduledProcedureStepStartDateAndTime = 1538;

	public static final int dMultipleCopiesFlag = 1539;

	public static final int dPerformedProcessingApplicationsCodeSequence = 1540;

	public static final int dHumanPerformerCodeSequence = 1541;

	public static final int dExpectedCompletionDateAndTime = 1542;

	public static final int dResultingGeneralPurposePerformedProcedureStepsSequence = 1543;

	public static final int dReferencedGeneralPurposeScheduledProcedureStepSequence = 1544;

	public static final int dScheduledWorkitemCodeSequence = 1545;

	public static final int dPerformedWorkitemCodeSequence = 1546;

	public static final int dInputAvailabilityFlag = 1547;

	public static final int dInputInformationSequence = 1548;

	public static final int dRelevantInformationSequence = 1549;

	public static final int dReferencedGeneralPurposeScheduledProcedureStepTransactionUID = 1550;

	public static final int dScheduledStationNameCodeSequence = 1551;

	public static final int dScheduledStationClassCodeSequence = 1552;

	public static final int dScheduledStationGeographicLocationCodeSequence = 1553;

	public static final int dPerformedStationNameCodeSequence = 1554;

	public static final int dPerformedStationClassCodeSequence = 1555;

	public static final int dPerformedStationGeographicLocationCodeSequence = 1556;

	public static final int dRequestedSubsequentWorkitemCodeSequence = 1557;

	public static final int dNonDICOMOutputCodeSequence = 1558;

	public static final int dOutputInformationSequence = 1559;

	public static final int dScheduledHumanPerformersSequence = 1560;

	public static final int dActualHumanPerformersSequence = 1561;

	public static final int dHumanPerformersOrganization = 1562;

	public static final int dHumanPerformersName = 1563;

	public static final int dEntranceDoseInMGy = 1564;

	public static final int dPurposeOfReferenceCodeSequence = 1565;

	public static final int dSOPInstanceStatus = 1566;

	public static final int dSOPAuthorizationDateAndTime = 1567;

	public static final int dSOPAuthorizationComment = 1568;

	public static final int dAuthorizationEquipmentCertificationNumber = 1569;

	public static final int dMACIDNumber = 1570;

	public static final int dMACCalculationTransferSyntaxUID = 1571;

	public static final int dMACAlgorithm = 1572;

	public static final int dDataElementsSigned = 1573;

	public static final int dDigitalSignatureUID = 1574;

	public static final int dDigitalSignatureDateTime = 1575;

	public static final int dCertificateType = 1576;

	public static final int dCertificateOfSigner = 1577;

	public static final int dSignature = 1578;

	public static final int dCertifiedTimestampType = 1579;

	public static final int dCertifiedTimestamp = 1580;

	public static final int dMaximumMemoryAllocation = 1581;

	public static final int dXRayImageReceptorTranslation = 1582;

	public static final int dDiaphragmPosition = 1583;

	public static final int dContourNumber = 1584;

	public static final int dAttachedContours = 1585;

	public static final int dMeasuredDoseReferenceSequence = 1586;

	public static final int dMeasuredDoseDescription = 1587;

	public static final int dMeasuredDoseType = 1588;

	public static final int dMeasuredDoseValue = 1589;

	public static final int dTreatmentSessionBeamSequence = 1590;

	public static final int dCurrentFractionNumber = 1591;

	public static final int dTreatmentControlPointDate = 1592;

	public static final int dTreatmentControlPointTime = 1593;

	public static final int dTreatmentTerminationStatus = 1594;

	public static final int dTreatmentTerminationCode = 1595;

	public static final int dTreatmentVerificationStatus = 1596;

	public static final int dReferencedTreatmentRecordSequence = 1597;

	public static final int dSpecifiedPrimaryMeterset = 1598;

	public static final int dSpecifiedSecondaryMeterset = 1599;

	public static final int dDeliveredPrimaryMeterset = 1600;

	public static final int dDeliveredSecondaryMeterset = 1601;

	public static final int dSpecifiedTreatmentTime = 1602;

	public static final int dDeliveredTreatmentTime = 1603;

	public static final int dControlPointDeliverySequence = 1604;

	public static final int dSpecifiedMeterset = 1605;

	public static final int dDeliveredMeterset = 1606;

	public static final int dDoseRateDelivered = 1607;

	public static final int dTreatmentSummaryCalculatedDoseReferenceSequence = 1608;

	public static final int dCumulativeDoseToDoseReference = 1609;

	public static final int dFirstTreatmentDate = 1610;

	public static final int dMostRecentTreatmentDate = 1611;

	public static final int dNumberOfFractionsDelivered = 1612;

	public static final int dOverrideSequence = 1613;

	public static final int dOverrideParameterPointer = 1614;

	public static final int dMeasuredDoseReferenceNumber = 1615;

	public static final int dOverrideReason = 1616;

	public static final int dCalculatedDoseReferenceSequence = 1617;

	public static final int dCalculatedDoseReferenceNumber = 1618;

	public static final int dCalculatedDoseReferenceDescription = 1619;

	public static final int dCalculatedDoseReferenceDoseValue = 1620;

	public static final int dStartMeterset = 1621;

	public static final int dEndMeterset = 1622;

	public static final int dReferencedMeasuredDoseReferenceSequence = 1623;

	public static final int dReferencedMeasuredDoseReferenceNumber = 1624;

	public static final int dReferencedCalculatedDoseReferenceSequence = 1625;

	public static final int dReferencedCalculatedDoseReferenceNumber = 1626;

	public static final int dBeamLimitingDeviceLeafPairsSequence = 1627;

	public static final int dRecordedWedgeSequence = 1628;

	public static final int dRecordedCompensatorSequence = 1629;

	public static final int dRecordedBlockSequence = 1630;

	public static final int dTreatmentSummaryMeasuredDoseReferenceSequence = 1631;

	public static final int dRecordedSourceSequence = 1632;

	public static final int dSourceSerialNumber = 1633;

	public static final int dTreatmentSessionApplicationSetupSequence = 1634;

	public static final int dApplicationSetupCheck = 1635;

	public static final int dRecordedBrachyAccessoryDeviceSequence = 1636;

	public static final int dReferencedBrachyAccessoryDeviceNumber = 1637;

	public static final int dRecordedChannelSequence = 1638;

	public static final int dSpecifiedChannelTotalTime = 1639;

	public static final int dDeliveredChannelTotalTime = 1640;

	public static final int dSpecifiedNumberOfPulses = 1641;

	public static final int dDeliveredNumberOfPulses = 1642;

	public static final int dSpecifiedPulseRepetitionInterval = 1643;

	public static final int dDeliveredPulseRepetitionInterval = 1644;

	public static final int dRecordedSourceApplicatorSequence = 1645;

	public static final int dReferencedSourceApplicatorNumber = 1646;

	public static final int dRecordedChannelShieldSequence = 1647;

	public static final int dReferencedChannelShieldNumber = 1648;

	public static final int dBrachyControlPointDeliveredSequence = 1649;

	public static final int dSafePositionExitDate = 1650;

	public static final int dSafePositionExitTime = 1651;

	public static final int dSafePositionReturnDate = 1652;

	public static final int dSafePositionReturnTime = 1653;

	public static final int dCurrentTreatmentStatus = 1654;

	public static final int dTreatmentStatusComment = 1655;

	public static final int dFractionGroupSummarySequence = 1656;

	public static final int dReferencedFractionNumber = 1657;

	public static final int dFractionGroupType = 1658;

	public static final int dBeamStopperPosition = 1659;

	public static final int dFractionStatusSummarySequence = 1660;

	public static final int dTreatmentDate = 1661;

	public static final int dTreatmentTime = 1662;

	public static final int dNominalBeamEnergyUnit = 1663;

	public static final int dHighDoseTechniqueType = 1664;

	public static final int dCompensatorType = 1665;

	public static final int dMACParametersSequence = 1666;

	public static final int dDigitalSignaturesSequence = 1667;

	public static final int dReferringPhysicianIdentificationSequence = 1668;

	public static final int dCodingSchemeIdentificationSequence = 1669;

	public static final int dCodingSchemeRegistry = 1670;

	public static final int dCodingSchemeExternalID = 1671;

	public static final int dCodingSchemeName = 1672;

	public static final int dResponsibleOrganization = 1673;

	public static final int dPhysicianSOfRecordIdentificationSequence = 1674;

	public static final int dPerformingPhysicianIdentificationSequence = 1675;

	public static final int dPhysicianSReadingStudyIdentificationSequence = 1676;

	public static final int dOperatorIdentificationSequence = 1677;

	public static final int dFrameType = 1678;

	public static final int dReferencedImageEvidenceSequence = 1679;

	public static final int dReferencedRawDataSequence = 1680;

	public static final int dCreatorVersionUID = 1681;

	public static final int dDerivationImageSequence = 1682;

	public static final int dSourceImageEvidenceSequence = 1683;

	public static final int dPixelPresentation = 1684;

	public static final int dVolumetricProperties = 1685;

	public static final int dVolumeBasedCalculationTechnique = 1686;

	public static final int dComplexImageComponent = 1687;

	public static final int dAcquisitionContrast = 1688;

	public static final int dDerivationCodeSequence = 1689;

	public static final int dReferencedGrayscalePresentationStateSequence = 1690;

	public static final int dClinicalTrialSponsorName = 1691;

	public static final int dClinicalTrialProtocolID = 1692;

	public static final int dClinicalTrialProtocolName = 1693;

	public static final int dClinicalTrialSiteID = 1694;

	public static final int dClinicalTrialSiteName = 1695;

	public static final int dClinicalTrialSubjectID = 1696;

	public static final int dClinicalTrialSubjectReadingID = 1697;

	public static final int dClinicalTrialTimePointID = 1698;

	public static final int dClinicalTrialTimePointDescription = 1699;

	public static final int dClinicalTrialCoordinatingCenterName = 1700;

	public static final int dDopplerSampleVolumeXPosition_2003 = 1701;

	public static final int dDopplerSampleVolumeYPosition_2003 = 1702;

	public static final int dTMLinePositionX0_2003 = 1703;

	public static final int dTMLinePositionY0_2003 = 1704;

	public static final int dTMLinePositionX1_2003 = 1705;

	public static final int dTMLinePositionY1_2003 = 1706;

	public static final int dContentQualification = 1707;

	public static final int dPulseSequenceName = 1708;

	public static final int dMRImagingModifierSequence = 1709;

	public static final int dEchoPulseSequence = 1710;

	public static final int dInversionRecovery = 1711;

	public static final int dFlowCompensation = 1712;

	public static final int dMultipleSpinEcho = 1713;

	public static final int dMultiPlanarExcitation = 1714;

	public static final int dPhaseContrast = 1715;

	public static final int dTimeOfFlightContrast = 1716;

	public static final int dSpoiling = 1717;

	public static final int dSteadyStatePulseSequence = 1718;

	public static final int dEchoPlanarPulseSequence = 1719;

	public static final int dTagAngleFirstAxis = 1720;

	public static final int dMagnetizationTransfer = 1721;

	public static final int dT2Preparation = 1722;

	public static final int dBloodSignalNulling = 1723;

	public static final int dSaturationRecovery = 1724;

	public static final int dSpectrallySelectedSuppression = 1725;

	public static final int dSpectrallySelectedExcitation = 1726;

	public static final int dSpatialPreSaturation = 1727;

	public static final int dTagging = 1728;

	public static final int dOversamplingPhase = 1729;

	public static final int dBloodSignalNulling_2003 = 1730;

	public static final int dTagSpacingFirstDimension = 1731;

	public static final int dGeometryOfKSpaceTraversal = 1732;

	public static final int dSegmentedKSpaceTraversal = 1733;

	public static final int dRectilinearPhaseEncodeReordering = 1734;

	public static final int dTagThickness = 1735;

	public static final int dPartialFourierDirection = 1736;

	public static final int dCardiacSynchronizationTechnique = 1737;

	public static final int dReceiveCoilManufacturerName = 1738;

	public static final int dMRReceiveCoilSequence = 1739;

	public static final int dReceiveCoilType = 1740;

	public static final int dQuadratureReceiveCoil = 1741;

	public static final int dMultiCoilDefinitionSequence = 1742;

	public static final int dMultiCoilConfiguration = 1743;

	public static final int dMultiCoilElementName = 1744;

	public static final int dMultiCoilElementUsed = 1745;

	public static final int dMRTransmitCoilSequence = 1746;

	public static final int dTransmitCoilManufacturerName = 1747;

	public static final int dTransmitCoilType = 1748;

	public static final int dSpectralWidth = 1749;

	public static final int dChemicalShiftReference = 1750;

	public static final int dVolumeLocalizationTechnique = 1751;

	public static final int dMRAcquisitionFrequencyEncodingSteps = 1752;

	public static final int dDeCoupling = 1753;

	public static final int dDeCoupledNucleus = 1754;

	public static final int dDeCouplingFrequency = 1755;

	public static final int dDeCouplingMethod = 1756;

	public static final int dDeCouplingChemicalShiftReference = 1757;

	public static final int dKSpaceFiltering = 1758;

	public static final int dTimeDomainFiltering = 1759;

	public static final int dNumberOfZeroFills = 1760;

	public static final int dBaselineCorrection = 1761;

	public static final int dParallelReductionFactorInPlane = 1762;

	public static final int dCardiacRRIntervalSpecified = 1763;

	public static final int dAcquisitionDuration = 1764;

	public static final int dFrameAcquisitionDatetime = 1765;

	public static final int dDiffusionDirectionality = 1766;

	public static final int dDiffusionGradientDirectionSequence = 1767;

	public static final int dParallelAcquisition = 1768;

	public static final int dParallelAcquisitionTechnique = 1769;

	public static final int dInversionTimes = 1770;

	public static final int dMetaboliteMapDescription = 1771;

	public static final int dPartialFourier = 1772;

	public static final int dEffectiveEchoTime = 1773;

	public static final int dChemicalShiftSequence = 1774;

	public static final int dCardiacSignalSource = 1775;

	public static final int dDiffusionBValue = 1776;

	public static final int dDiffusionGradientOrientation = 1777;

	public static final int dVelocityEncodingDirection = 1778;

	public static final int dVelocityEncodingMinimumValue = 1779;

	public static final int dNumberOfKSpaceTrajectories = 1780;

	public static final int dCoverageOfKSpace = 1781;

	public static final int dSpectroscopyAcquisitionPhaseRows = 1782;

	public static final int dTransmitterFrequency = 1783;

	public static final int dResonantNucleus = 1784;

	public static final int dFrequencyCorrection = 1785;

	public static final int dMRSpectroscopyFOVGeometrySequence = 1786;

	public static final int dSlabThickness = 1787;

	public static final int dSlabOrientation = 1788;

	public static final int dMidSlabPosition = 1789;

	public static final int dMRSpatialSaturationSequence = 1790;

	public static final int dMRTimingAndRelatedParametersSequence = 1791;

	public static final int dMREchoSequence = 1792;

	public static final int dMRModifierSequence = 1793;

	public static final int dMRDiffusionSequence = 1794;

	public static final int dCardiacTriggerSequence = 1795;

	public static final int dMRAveragesSequence = 1796;

	public static final int dMRFOVGeometrySequence = 1797;

	public static final int dVolumeLocalizationSequence = 1798;

	public static final int dSpectroscopyAcquisitionDataColumns = 1799;

	public static final int dDiffusionAnisotropyType = 1800;

	public static final int dFrameReferenceDatetime = 1801;

	public static final int dMRMetaboliteMapSequence = 1802;

	public static final int dParallelReductionFactorOutOfPlane = 1803;

	public static final int dSpectroscopyAcquisitionOutOfPlanePhaseSteps = 1804;

	public static final int dBulkMotionStatus = 1805;

	public static final int dParallelReductionFactorSecondInPlane = 1806;

	public static final int dCardiacBeatRejectionTechnique = 1807;

	public static final int dRespiratoryMotionCompensationTechnique = 1808;

	public static final int dRespiratorySignalSource = 1809;

	public static final int dBulkMotionCompensationTechnique = 1810;

	public static final int dBulkMotionSignalSource = 1811;

	public static final int dApplicableSafetyStandardAgency = 1812;

	public static final int dApplicableSafetyStandardDescription = 1813;

	public static final int dOperatingModeSequence = 1814;

	public static final int dOperatingModeType = 1815;

	public static final int dOperatingMode = 1816;

	public static final int dSpecificAbsorptionRateDefinition = 1817;

	public static final int dGradientOutputType = 1818;

	public static final int dSpecificAbsorptionRateValue = 1819;

	public static final int dGradientOutput = 1820;

	public static final int dFlowCompensationDirection = 1821;

	public static final int dTaggingDelay = 1822;

	public static final int dChemicalShiftsMinimumIntegrationLimit = 1823;

	public static final int dChemicalShiftsMaximumIntegrationLimit = 1824;

	public static final int dMRVelocityEncodingSequence = 1825;

	public static final int dFirstOrderPhaseCorrection = 1826;

	public static final int dWaterReferencedPhaseCorrection = 1827;

	public static final int dMRSpectroscopyAcquisitionType = 1828;

	public static final int dRespiratoryCyclePosition = 1829;

	public static final int dVelocityEncodingMaximumValue = 1830;

	public static final int dTagSpacingSecondDimension = 1831;

	public static final int dTagAngleSecondAxis = 1832;

	public static final int dFrameAcquisitionDuration = 1833;

	public static final int dMRImageFrameTypeSequence = 1834;

	public static final int dMRSpectroscopyFrameTypeSequence = 1835;

	public static final int dMRAcquisitionPhaseEncodingStepsInPlane = 1836;

	public static final int dMRAcquisitionPhaseEncodingStepsOutOfPlane = 1837;

	public static final int dSpectroscopyAcquisitionPhaseColumns = 1838;

	public static final int dCardiacCyclePosition = 1839;

	public static final int dSpecificAbsorptionRateSequence = 1840;

	public static final int dContributingEquipmentSequence = 1841;

	public static final int dContributionDateTime = 1842;

	public static final int dContributionDescription = 1843;

	public static final int dStackID = 1844;

	public static final int dInStackPositionNumber = 1845;

	public static final int dFrameAnatomySequence = 1846;

	public static final int dFrameLaterality = 1847;

	public static final int dFrameContentSequence = 1848;

	public static final int dPlanePositionSequence = 1849;

	public static final int dPlaneOrientationSequence = 1850;

	public static final int dTemporalPositionIndex = 1851;

	public static final int dTriggerDelayTime = 1852;

	public static final int dFrameAcquisitionNumber = 1853;

	public static final int dDimensionIndexValues = 1854;

	public static final int dFrameComments = 1855;

	public static final int dConcatenationUID = 1856;

	public static final int dInConcatenationNumber = 1857;

	public static final int dInConcatenationTotalNumber = 1858;

	public static final int dDimensionOrganizationUID = 1859;

	public static final int dDimensionIndexPointer = 1860;

	public static final int dFunctionalGroupPointer = 1861;

	public static final int dDimensionIndexPrivateCreator = 1862;

	public static final int dDimensionOrganizationSequence = 1863;

	public static final int dDimensionIndexSequence = 1864;

	public static final int dConcatenationFrameOffsetNumber = 1865;

	public static final int dFunctionalGroupPrivateCreator = 1866;

	public static final int dDataPointRows = 1867;

	public static final int dDataPointColumns = 1868;

	public static final int dSignalDomainColumns = 1869;

	public static final int dLargestMonochromePixelValue = 1870;

	public static final int dDataRepresentation = 1871;

	public static final int dPixelMeasuresSequence = 1872;

	public static final int dFrameVOILUTSequence = 1873;

	public static final int dPixelValueTransformationSequence = 1874;

	public static final int dSignalDomainRows = 1875;

	public static final int dRequestingPhysicianIdentificationSequence = 1876;

	public static final int dScheduledPerformingPhysicianIdentificationSequence = 1877;

	public static final int dPerformedProcedureStepDiscontinuationReasonCodeSequence = 1878;

	public static final int dIntendedRecipientsOfResultsIdentificationSequence = 1879;

	public static final int dPersonIdentificationCodeSequence = 1880;

	public static final int dPersonsAddress = 1881;

	public static final int dPersonsTelephoneNumbers = 1882;

	public static final int dRealWorldValueMappingSequence = 1883;

	public static final int dLUTLabel = 1884;

	public static final int dRealWorldValueLastValueMapped = 1885;

	public static final int dRealWorldValueLUTData = 1886;

	public static final int dRealWorldValueFirstValueMapped = 1887;

	public static final int dRealWorldValueIntercept = 1888;

	public static final int dRealWorldValueSlope = 1889;

	public static final int dNumericValueQualifierCodeSequence = 1890;

	public static final int dEncryptedAttributesSequence = 1891;

	public static final int dEncryptedContentTransferSyntaxUID = 1892;

	public static final int dEncryptedContent = 1893;

	public static final int dModifiedAttributesSequence = 1894;

	public static final int dBlockMountingPosition = 1895;

	public static final int dCompensatorDivergence = 1896;

	public static final int dCompensatorMountingPosition = 1897;

	public static final int dSourceToCompensatorDistance = 1898;

	public static final int dSharedFunctionalGroupsSequence = 1899;

	public static final int dPerFrameFunctionalGroupsSequence = 1900;

	public static final int dFirstOrderPhaseCorrectionAngle = 1901;

	public static final int dSpectroscopyData = 1902;

	private static MyVector _fldif = new MyVector();

	private static DataChunk2Holder _flddo;

	private static String a[] = { "UN", "UL", "UI", "US", "AE", "AT", "LO",
			"SH", "OB", "CS", "SQ", "DA", "TM", "ST", "PN", "IS", "DS", "AS",
			"LT", "SL", "FD", "US_SS", "OW_OB", "SS", "OW", "NONE", "FL", "UT",
			"DT", "OF" };

	public DDict() {
	}

	public static void initDDict() {
	}

	private static final void _mthif() {
		_fldif.add(new DDictEntry(0, 0, 1, "CommandGroupLength", "1"));
		_fldif.add(new DDictEntry(0, 2, 2, "AffectedSOPClassUID", "1"));
		_fldif.add(new DDictEntry(0, 3, 2, "RequestedSOPClassUID", "1"));
		_fldif.add(new DDictEntry(0, 256, 3, "CommandField", "1"));
		_fldif.add(new DDictEntry(0, 272, 3, "MessageID", "1"));
		_fldif.add(new DDictEntry(0, 288, 3, "MessageIDBeingRespondedTo", "1"));
		_fldif.add(new DDictEntry(0, 1536, 4, "MoveDestination", "1"));
		_fldif.add(new DDictEntry(0, 1792, 3, "Priority", "1"));
		_fldif.add(new DDictEntry(0, 2048, 3, "DataSetType", "1"));
		_fldif.add(new DDictEntry(0, 2304, 3, "Status", "1"));
		_fldif.add(new DDictEntry(0, 2305, 5, "OffendingElement", "1-n"));
		_fldif.add(new DDictEntry(0, 2306, 6, "ErrorComment", "1"));
		_fldif.add(new DDictEntry(0, 2307, 3, "ErrorID", "1"));
		_fldif.add(new DDictEntry(0, 4096, 2, "AffectedSOPInstanceUID", "1"));
		_fldif.add(new DDictEntry(0, 4097, 2, "RequestedSOPInstanceUID", "1"));
		_fldif.add(new DDictEntry(0, 4098, 3, "EventTypeID", "1"));
		_fldif.add(new DDictEntry(0, 4101, 5, "AttributeIdentifierList", "1-n"));
		_fldif.add(new DDictEntry(0, 4103, 5, "ModificationList", "1-n"));
		_fldif.add(new DDictEntry(0, 4104, 3, "ActionTypeID", "1"));
		_fldif.add(new DDictEntry(0, 4128, 3, "NumberOfRemainingSuboperations",
				"1"));
		_fldif.add(new DDictEntry(0, 4129, 3, "NumberOfCompletedSuboperations",
				"1"));
		_fldif
				.add(new DDictEntry(0, 4130, 3, "NumberOfFailedSuboperations",
						"1"));
		_fldif
				.add(new DDictEntry(0, 4131, 3, "NumberOfWarningSuboperations",
						"1"));
		_fldif.add(new DDictEntry(0, 4144, 4,
				"MoveOriginatorApplicationEntityTitle", "1"));
		_fldif.add(new DDictEntry(0, 4145, 3, "MoveOriginatorMessageID", "1"));
		_fldif.add(new DDictEntry(0, 20496, 7, "MessageSetID", "1"));
		_fldif.add(new DDictEntry(0, 20512, 7, "EndMessageSet", "1"));
		_fldif.add(new DDictEntry(2, 0, 1, "MetaElementGroupLength", "1"));
		_fldif.add(new DDictEntry(2, 1, 8, "FileMetaInformationVersion", "1"));
		_fldif.add(new DDictEntry(2, 2, 2, "MediaStorageSOPClassUID", "1"));
		_fldif.add(new DDictEntry(2, 3, 2, "MediaStorageSOPInstanceUID", "1"));
		_fldif.add(new DDictEntry(2, 16, 2, "TransferSyntaxUID", "1"));
		_fldif.add(new DDictEntry(2, 18, 2, "ImplementationClassUID", "1"));
		_fldif.add(new DDictEntry(2, 19, 7, "ImplementationVersionName", "1"));
		_fldif.add(new DDictEntry(2, 22, 4, "SourceApplicationEntityTitle", "1"));
		_fldif
				.add(new DDictEntry(2, 256, 2, "PrivateInformationCreatorUID",
						"1"));
		_fldif.add(new DDictEntry(2, 258, 8, "PrivateInformation", "1"));
		_fldif.add(new DDictEntry(4, 0, 1, "FileSetGroupLength", "1"));
		_fldif.add(new DDictEntry(4, 4400, 9, "FileSetID", "1"));
		_fldif.add(new DDictEntry(4, 4417, 9, "FileSetDescriptorFileID", "1-8"));
		_fldif.add(new DDictEntry(4, 4418, 9, "FileSetCharacterSet", "1"));
		_fldif.add(new DDictEntry(4, 4608, 1, "RootDirectoryFirstRecord", "1"));
		_fldif.add(new DDictEntry(4, 4610, 1, "RootDirectoryLastRecord", "1"));
		_fldif.add(new DDictEntry(4, 4626, 3, "FileSetConsistencyFlag", "1"));
		_fldif.add(new DDictEntry(4, 4640, 10, "DirectoryRecordSequence", "1"));
		_fldif.add(new DDictEntry(4, 5120, 1, "NextDirectoryRecordOffset", "1"));
		_fldif.add(new DDictEntry(4, 5136, 3, "RecordInUseFlag", "1"));
		_fldif.add(new DDictEntry(4, 5152, 1, "LowerLevelDirectoryOffset", "1"));
		_fldif.add(new DDictEntry(4, 5168, 9, "DirectoryRecordType", "1"));
		_fldif.add(new DDictEntry(4, 5170, 2, "PrivateRecordUID", "1"));
		_fldif.add(new DDictEntry(4, 5376, 9, "ReferencedFileID", "1-8"));
		_fldif.add(new DDictEntry(4, 5380, 1, "DirectoryRecordOffset", "1"));
		_fldif
				.add(new DDictEntry(4, 5392, 2, "ReferencedSOPClassUIDInFile",
						"1"));
		_fldif.add(new DDictEntry(4, 5393, 2, "ReferencedSOPInstanceUIDInFile",
				"1"));
		_fldif.add(new DDictEntry(4, 5394, 2,
				"ReferencedTransferSyntaxUIDInFile", "1"));
		_fldif.add(new DDictEntry(4, 5632, 1, "NumberOfReferences", "1"));
		_fldif.add(new DDictEntry(8, 0, 1, "IdentifyingGroupLength", "1"));
		_fldif.add(new DDictEntry(8, 5, 9, "SpecificCharacterSet", "1-n"));
		_fldif.add(new DDictEntry(8, 8, 9, "ImageType", "1-n"));
		_fldif.add(new DDictEntry(8, 18, 11, "InstanceCreationDate", "1"));
		_fldif.add(new DDictEntry(8, 19, 12, "InstanceCreationTime", "1"));
		_fldif.add(new DDictEntry(8, 20, 2, "InstanceCreatorUID", "1"));
		_fldif.add(new DDictEntry(8, 22, 2, "SOPClassUID", "1"));
		_fldif.add(new DDictEntry(8, 24, 2, "SOPInstanceUID", "1"));
		_fldif.add(new DDictEntry(8, 32, 11, "StudyDate", "1"));
		_fldif.add(new DDictEntry(8, 33, 11, "SeriesDate", "1"));
		_fldif.add(new DDictEntry(8, 34, 11, "AcquisitionDate", "1"));
		_fldif.add(new DDictEntry(8, 35, 11, "ContentDate", "1"));
		_fldif.add(new DDictEntry(8, 36, 11, "OverlayDate", "1"));
		_fldif.add(new DDictEntry(8, 37, 11, "CurveDate", "1"));
		_fldif.add(new DDictEntry(8, 48, 12, "StudyTime", "1"));
		_fldif.add(new DDictEntry(8, 49, 12, "SeriesTime", "1"));
		_fldif.add(new DDictEntry(8, 50, 12, "AcquisitionTime", "1"));
		_fldif.add(new DDictEntry(8, 51, 12, "ContentTime", "1"));
		_fldif.add(new DDictEntry(8, 52, 12, "OverlayTime", "1"));
		_fldif.add(new DDictEntry(8, 53, 12, "CurveTime", "1"));
		_fldif.add(new DDictEntry(8, 66, 9, "NuclearMedicineSeriesType", "1"));
		_fldif.add(new DDictEntry(8, 80, 7, "AccessionNumber", "1"));
		_fldif.add(new DDictEntry(8, 82, 9, "QueryRetrieveLevel", "1"));
		_fldif.add(new DDictEntry(8, 84, 4, "RetrieveAETitle", "1-n"));
		_fldif.add(new DDictEntry(8, 88, 2, "FailedSOPInstanceUIDList", "1-n"));
		_fldif.add(new DDictEntry(8, 96, 9, "Modality", "1"));
		_fldif.add(new DDictEntry(8, 97, 9, "ModalitiesInStudy", "1-n"));
		_fldif.add(new DDictEntry(8, 100, 9, "ConversionType", "1"));
		_fldif.add(new DDictEntry(8, 112, 6, "Manufacturer", "1"));
		_fldif.add(new DDictEntry(8, 128, 6, "InstitutionName", "1"));
		_fldif.add(new DDictEntry(8, 129, 13, "InstitutionAddress", "1"));
		_fldif.add(new DDictEntry(8, 130, 10, "InstitutionCodeSequence", "1"));
		_fldif.add(new DDictEntry(8, 144, 14, "ReferringPhysiciansName", "1"));
		_fldif.add(new DDictEntry(8, 146, 13, "ReferringPhysiciansAddress", "1"));
		_fldif.add(new DDictEntry(8, 148, 7,
				"ReferringPhysiciansTelephoneNumber", "1-n"));
		_fldif.add(new DDictEntry(8, 256, 7, "CodeValue", "1"));
		_fldif.add(new DDictEntry(8, 258, 7, "CodingSchemeDesignator", "1"));
		_fldif.add(new DDictEntry(8, 260, 6, "CodeMeaning", "1"));
		_fldif.add(new DDictEntry(8, 4112, 7, "StationName", "1"));
		_fldif.add(new DDictEntry(8, 4144, 6, "StudyDescription", "1"));
		_fldif.add(new DDictEntry(8, 4146, 10, "ProcedureCodeSequence", "1"));
		_fldif.add(new DDictEntry(8, 4158, 6, "SeriesDescription", "1"));
		_fldif
				.add(new DDictEntry(8, 4160, 6, "InstitutionalDepartmentName",
						"1"));
		_fldif.add(new DDictEntry(8, 4168, 14, "PhysiciansOfRecord", "1-n"));
		_fldif
				.add(new DDictEntry(8, 4176, 14, "PerformingPhysiciansName",
						"1-n"));
		_fldif.add(new DDictEntry(8, 4192, 14, "NameOfPhysiciansReadingStudy",
				"1-n"));
		_fldif.add(new DDictEntry(8, 4208, 14, "OperatorsName", "1-n"));
		_fldif.add(new DDictEntry(8, 4224, 6, "AdmittingDiagnosisDescription",
				"1-n"));
		_fldif.add(new DDictEntry(8, 4228, 10, "AdmittingDiagnosisCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(8, 4240, 6, "ManufacturerModelName", "1"));
		_fldif.add(new DDictEntry(8, 4352, 10, "ReferencedResultsSequence", "1"));
		_fldif.add(new DDictEntry(8, 4368, 10, "ReferencedStudySequence", "1"));
		_fldif.add(new DDictEntry(8, 4369, 10,
				"ReferencedStudyComponentSequence", "1"));
		_fldif.add(new DDictEntry(8, 4373, 10, "ReferencedSeriesSequence", "1"));
		_fldif.add(new DDictEntry(8, 4384, 10, "ReferencedPatientSequence", "1"));
		_fldif.add(new DDictEntry(8, 4389, 10, "ReferencedVisitSequence", "1"));
		_fldif.add(new DDictEntry(8, 4400, 10, "ReferencedOverlaySequence", "1"));
		_fldif.add(new DDictEntry(8, 4416, 10, "ReferencedImageSequence", "1"));
		_fldif.add(new DDictEntry(8, 4421, 10, "ReferencedCurveSequence", "1"));
		_fldif.add(new DDictEntry(8, 4432, 2, "ReferencedSOPClassUID", "1"));
		_fldif.add(new DDictEntry(8, 4437, 2, "ReferencedSOPInstanceUID", "1"));
		_fldif.add(new DDictEntry(8, 4448, 15, "ReferencedFrameNumber", "1"));
		_fldif.add(new DDictEntry(8, 4501, 2, "TransactionUID", "1"));
		_fldif.add(new DDictEntry(8, 4503, 3, "FailureReason", "1"));
		_fldif.add(new DDictEntry(8, 4504, 10, "FailedSOPSequence", "1"));
		_fldif.add(new DDictEntry(8, 4505, 10, "ReferencedSOPSequence", "1"));
		_fldif.add(new DDictEntry(8, 8465, 13, "DerivationDescription", "1"));
		_fldif.add(new DDictEntry(8, 8466, 10, "SourceImageSequence", "1"));
		_fldif.add(new DDictEntry(8, 8480, 7, "StageName", "1"));
		_fldif.add(new DDictEntry(8, 8482, 15, "StageNumber", "1"));
		_fldif.add(new DDictEntry(8, 8484, 15, "NumberOfStages", "1"));
		_fldif.add(new DDictEntry(8, 8488, 15, "ViewNumber", "1"));
		_fldif.add(new DDictEntry(8, 8489, 15, "NumberOfEventTimers", "1"));
		_fldif.add(new DDictEntry(8, 8490, 15, "NumberOfViewsInStage", "1"));
		_fldif.add(new DDictEntry(8, 8496, 16, "EventElapsedTime", "1-n"));
		_fldif.add(new DDictEntry(8, 8498, 6, "EventTimerName", "1-n"));
		_fldif.add(new DDictEntry(8, 8514, 15, "StartTrim", "1"));
		_fldif.add(new DDictEntry(8, 8515, 15, "StopTrim", "1"));
		_fldif
				.add(new DDictEntry(8, 8516, 15, "RecommendedDisplayFrameRate",
						"1"));
		_fldif.add(new DDictEntry(8, 8704, 9, "TransducerPosition", "1"));
		_fldif.add(new DDictEntry(8, 8708, 9, "TransducerOrientation", "1"));
		_fldif.add(new DDictEntry(8, 8712, 9, "AnatomicStructure", "1"));
		_fldif.add(new DDictEntry(8, 8728, 10, "AnatomicRegionSequence", "1"));
		_fldif.add(new DDictEntry(8, 8736, 10, "AnatomicRegionModifierSequence",
				"1"));
		_fldif.add(new DDictEntry(8, 8744, 10,
				"PrimaryAnatomicStructureSequence", "1"));
		_fldif.add(new DDictEntry(8, 8752, 10,
				"PrimaryAnatomicStructureModifierSequence", "1"));
		_fldif
				.add(new DDictEntry(8, 8768, 10, "TransducerPositionSequence",
						"1"));
		_fldif.add(new DDictEntry(8, 8770, 10,
				"TransducerPositionModifierSequence", "1"));
		_fldif.add(new DDictEntry(8, 8772, 10, "TransducerOrientationSequence",
				"1"));
		_fldif.add(new DDictEntry(8, 8774, 10,
				"TransducerOrientationModifierSequence", "1"));
		_fldif.add(new DDictEntry(16, 0, 1, "PatientGroupLength", "1"));
		_fldif.add(new DDictEntry(16, 16, 14, "PatientName", "1"));
		_fldif.add(new DDictEntry(16, 32, 6, "PatientID", "1"));
		_fldif.add(new DDictEntry(16, 33, 6, "IssuerOfPatientID", "1"));
		_fldif.add(new DDictEntry(16, 48, 11, "PatientBirthDate", "1"));
		_fldif.add(new DDictEntry(16, 50, 12, "PatientBirthTime", "1"));
		_fldif.add(new DDictEntry(16, 64, 9, "PatientSex", "1"));
		_fldif.add(new DDictEntry(16, 80, 10, "PatientInsurancePlanCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(16, 4096, 6, "OtherPatientID", "1-n"));
		_fldif.add(new DDictEntry(16, 4097, 14, "OtherPatientNames", "1-n"));
		_fldif.add(new DDictEntry(16, 4101, 14, "PatientBirthName", "1"));
		_fldif.add(new DDictEntry(16, 4112, 17, "PatientAge", "1"));
		_fldif.add(new DDictEntry(16, 4128, 16, "PatientSize", "1"));
		_fldif.add(new DDictEntry(16, 4144, 16, "PatientWeight", "1"));
		_fldif.add(new DDictEntry(16, 4160, 6, "PatientAddress", "1"));
		_fldif.add(new DDictEntry(16, 4192, 14, "PatientMotherBirthName", "1"));
		_fldif.add(new DDictEntry(16, 4224, 6, "MilitaryRank", "1"));
		_fldif.add(new DDictEntry(16, 4225, 6, "BranchOfService", "1"));
		_fldif.add(new DDictEntry(16, 4240, 6, "MedicalRecordLocator", "1"));
		_fldif.add(new DDictEntry(16, 8192, 6, "MedicalAlerts", "1-n"));
		_fldif.add(new DDictEntry(16, 8464, 6, "ContrastAllergies", "1-n"));
		_fldif.add(new DDictEntry(16, 8528, 6, "CountryOfResidence", "1"));
		_fldif.add(new DDictEntry(16, 8530, 6, "RegionOfResidence", "1"));
		_fldif.add(new DDictEntry(16, 8532, 7, "PatientTelephoneNumber", "1-n"));
		_fldif.add(new DDictEntry(16, 8544, 7, "EthnicGroup", "1"));
		_fldif.add(new DDictEntry(16, 8576, 7, "Occupation", "1"));
		_fldif.add(new DDictEntry(16, 8608, 9, "SmokingStatus", "1"));
		_fldif.add(new DDictEntry(16, 8624, 18, "AdditionalPatientHistory", "1"));
		_fldif.add(new DDictEntry(16, 8640, 3, "PregnancyStatus", "1"));
		_fldif.add(new DDictEntry(16, 8656, 11, "LastMenstrualDate", "1"));
		_fldif
				.add(new DDictEntry(16, 8688, 6, "PatientReligiousPreference",
						"1"));
		_fldif.add(new DDictEntry(16, 16384, 18, "PatientComments", "1"));
		_fldif.add(new DDictEntry(24, 0, 1, "AcquisitionGroupLength", "1"));
		_fldif.add(new DDictEntry(24, 16, 6, "ContrastBolusAgent", "1"));
		_fldif.add(new DDictEntry(24, 18, 10, "ContrastBolusAgentSequence", "1"));
		_fldif.add(new DDictEntry(24, 20, 10,
				"ContrastBolusAdministrationRouteSequence", "1"));
		_fldif.add(new DDictEntry(24, 21, 9, "BodyPartExamined", "1"));
		_fldif.add(new DDictEntry(24, 32, 9, "ScanningSequence", "1-n"));
		_fldif.add(new DDictEntry(24, 33, 9, "SequenceVariant", "1-n"));
		_fldif.add(new DDictEntry(24, 34, 9, "ScanOptions", "1-n"));
		_fldif.add(new DDictEntry(24, 35, 9, "MRAcquisitionType", "1"));
		_fldif.add(new DDictEntry(24, 36, 7, "SequenceName", "1"));
		_fldif.add(new DDictEntry(24, 37, 9, "AngioFlag", "1"));
		_fldif.add(new DDictEntry(24, 38, 10,
				"InterventionDrugInformationSequence", "1"));
		_fldif.add(new DDictEntry(24, 39, 12, "InterventionDrugStopTime", "1"));
		_fldif.add(new DDictEntry(24, 40, 16, "InterventionDrugDose", "1"));
		_fldif
				.add(new DDictEntry(24, 41, 10, "InterventionDrugCodeSequence",
						"1"));
		_fldif.add(new DDictEntry(24, 42, 10, "AdditionalDrugSequence", "1"));
		_fldif.add(new DDictEntry(24, 48, 6, "Radionuclide", "1-n"));
		_fldif.add(new DDictEntry(24, 49, 6, "Radiopharmaceutical", "1-n"));
		_fldif.add(new DDictEntry(24, 50, 16, "EnergyWindowCenterline", "1"));
		_fldif.add(new DDictEntry(24, 51, 16, "EnergyWindowTotalWidth", "1-n"));
		_fldif.add(new DDictEntry(24, 52, 6, "InterventionDrugName", "1"));
		_fldif.add(new DDictEntry(24, 53, 12, "InterventionDrugStartTime", "1"));
		_fldif.add(new DDictEntry(24, 54, 10, "InterventionalTherapySequence",
				"1"));
		_fldif.add(new DDictEntry(24, 55, 9, "TherapyType", "1"));
		_fldif.add(new DDictEntry(24, 56, 9, "InterventionalStatus", "1"));
		_fldif.add(new DDictEntry(24, 57, 9, "TherapyDescription", "1"));
		_fldif.add(new DDictEntry(24, 64, 15, "CineRate", "1"));
		_fldif.add(new DDictEntry(24, 80, 16, "SliceThickness", "1"));
		_fldif.add(new DDictEntry(24, 96, 16, "KVP", "1"));
		_fldif.add(new DDictEntry(24, 112, 15, "CountsAccumulated", "1"));
		_fldif.add(new DDictEntry(24, 113, 9, "AcquisitionTerminationCondition",
				"1"));
		_fldif.add(new DDictEntry(24, 114, 16, "EffectiveDuration", "1"));
		_fldif.add(new DDictEntry(24, 115, 9, "AcquisitionStartCondition", "1"));
		_fldif.add(new DDictEntry(24, 116, 15, "AcquisitionStartConditionData",
				"1"));
		_fldif.add(new DDictEntry(24, 117, 15,
				"AcquisitionTerminationConditionData", "1"));
		_fldif.add(new DDictEntry(24, 128, 16, "RepetitionTime", "1"));
		_fldif.add(new DDictEntry(24, 129, 16, "EchoTime", "1"));
		_fldif.add(new DDictEntry(24, 130, 16, "InversionTime", "1"));
		_fldif.add(new DDictEntry(24, 131, 16, "NumberOfAverages", "1"));
		_fldif.add(new DDictEntry(24, 132, 16, "ImagingFrequency", "1"));
		_fldif.add(new DDictEntry(24, 133, 7, "ImagedNucleus", "1"));
		_fldif.add(new DDictEntry(24, 134, 15, "EchoNumber", "1-n"));
		_fldif.add(new DDictEntry(24, 135, 16, "MagneticFieldStrength", "1"));
		_fldif.add(new DDictEntry(24, 136, 16, "SpacingBetweenSlices", "1"));
		_fldif
				.add(new DDictEntry(24, 137, 15, "NumberOfPhaseEncodingSteps",
						"1"));
		_fldif.add(new DDictEntry(24, 144, 16, "DataCollectionDiameter", "1"));
		_fldif.add(new DDictEntry(24, 145, 15, "EchoTrainLength", "1"));
		_fldif.add(new DDictEntry(24, 147, 16, "PercentSampling", "1"));
		_fldif.add(new DDictEntry(24, 148, 16, "PercentPhaseFieldOfView", "1"));
		_fldif.add(new DDictEntry(24, 149, 16, "PixelBandwidth", "1"));
		_fldif.add(new DDictEntry(24, 4096, 6, "DeviceSerialNumber", "1"));
		_fldif.add(new DDictEntry(24, 4100, 6, "PlateID", "1"));
		_fldif.add(new DDictEntry(24, 4112, 6, "SecondaryCaptureDeviceID", "1"));
		_fldif.add(new DDictEntry(24, 4114, 11, "DateOfSecondaryCapture", "1"));
		_fldif.add(new DDictEntry(24, 4116, 12, "TimeOfSecondaryCapture", "1"));
		_fldif.add(new DDictEntry(24, 4118, 6,
				"SecondaryCaptureDeviceManufacturer", "1"));
		_fldif.add(new DDictEntry(24, 4120, 6,
				"SecondaryCaptureDeviceManufacturerModelName", "1"));
		_fldif.add(new DDictEntry(24, 4121, 6,
				"SecondaryCaptureDeviceSoftwareVersion", "1-n"));
		_fldif.add(new DDictEntry(24, 4128, 6, "SoftwareVersion", "1-n"));
		_fldif.add(new DDictEntry(24, 4130, 7, "VideoImageFormatAcquired", "1"));
		_fldif
				.add(new DDictEntry(24, 4131, 6, "DigitalImageFormatAcquired",
						"1"));
		_fldif.add(new DDictEntry(24, 4144, 6, "ProtocolName", "1"));
		_fldif.add(new DDictEntry(24, 4160, 6, "ContrastBolusRoute", "1"));
		_fldif.add(new DDictEntry(24, 4161, 16, "ContrastBolusVolume", "1"));
		_fldif.add(new DDictEntry(24, 4162, 12, "ContrastBolusStartTime", "1"));
		_fldif.add(new DDictEntry(24, 4163, 12, "ContrastBolusStopTime", "1"));
		_fldif.add(new DDictEntry(24, 4164, 16, "ContrastBolusTotalDose", "1"));
		_fldif.add(new DDictEntry(24, 4165, 15, "SyringeCounts", "1"));
		_fldif.add(new DDictEntry(24, 4166, 16, "ContrastFlowRates", "1-n"));
		_fldif.add(new DDictEntry(24, 4167, 16, "ContrastFlowDurations", "1-n"));
		_fldif.add(new DDictEntry(24, 4168, 9, "ContrastBolusIngredient", "1"));
		_fldif.add(new DDictEntry(24, 4169, 16,
				"ContrastBolusIngredientConcentration", "1"));
		_fldif.add(new DDictEntry(24, 4176, 16, "SpatialResolution", "1"));
		_fldif.add(new DDictEntry(24, 4192, 16, "TriggerTime", "1"));
		_fldif.add(new DDictEntry(24, 4193, 6, "TriggerSourceOrType", "1"));
		_fldif.add(new DDictEntry(24, 4194, 15, "NominalInterval", "1"));
		_fldif.add(new DDictEntry(24, 4195, 16, "FrameTime", "1"));
		_fldif.add(new DDictEntry(24, 4196, 6, "FramingType", "1"));
		_fldif.add(new DDictEntry(24, 4197, 16, "FrameTimeVector", "1-n"));
		_fldif.add(new DDictEntry(24, 4198, 16, "FrameDelay", "1"));
		_fldif.add(new DDictEntry(24, 4208, 6, "RadionuclideRoute", "1-n"));
		_fldif.add(new DDictEntry(24, 4209, 16, "RadionuclideVolume", "1-n"));
		_fldif.add(new DDictEntry(24, 4210, 12, "RadiopharmaceuticalStartTime",
				"1"));
		_fldif.add(new DDictEntry(24, 4211, 12, "RadiopharmaceuticalStopTime",
				"1"));
		_fldif.add(new DDictEntry(24, 4212, 16, "RadionuclideTotalDose", "1"));
		_fldif.add(new DDictEntry(24, 4213, 16, "RadionuclideHalfLife", "1"));
		_fldif.add(new DDictEntry(24, 4214, 16, "RadionuclidePositronFraction",
				"1"));
		_fldif.add(new DDictEntry(24, 4215, 16,
				"RadiopharmaceuticalSpecificActivity", "1"));
		_fldif.add(new DDictEntry(24, 4224, 9, "BeatRejectionFlag", "1"));
		_fldif.add(new DDictEntry(24, 4225, 15, "LowRRValue", "1"));
		_fldif.add(new DDictEntry(24, 4226, 15, "HighRRValue", "1"));
		_fldif.add(new DDictEntry(24, 4227, 15, "IntervalsAcquired", "1"));
		_fldif.add(new DDictEntry(24, 4228, 15, "IntervalsRejected", "1"));
		_fldif.add(new DDictEntry(24, 4229, 6, "PVCRejection", "1"));
		_fldif.add(new DDictEntry(24, 4230, 15, "SkipBeats", "1"));
		_fldif.add(new DDictEntry(24, 4232, 15, "HeartRate", "1"));
		_fldif.add(new DDictEntry(24, 4240, 15, "CardiacNumberOfImages", "1"));
		_fldif.add(new DDictEntry(24, 4244, 15, "TriggerWindow", "1"));
		_fldif.add(new DDictEntry(24, 4352, 16, "ReconstructionDiameter", "1"));
		_fldif.add(new DDictEntry(24, 4368, 16, "DistanceSourceToDetector", "1"));
		_fldif.add(new DDictEntry(24, 4369, 16, "DistanceSourceToPatient", "1"));
		_fldif.add(new DDictEntry(24, 4372, 16,
				"EstimatedRadiographicMagnificationFactor", "1"));
		_fldif.add(new DDictEntry(24, 4384, 16, "GantryDetectorTilt", "1"));
		_fldif.add(new DDictEntry(24, 4385, 16, "GantryDetectorSlew", "1"));
		_fldif.add(new DDictEntry(24, 4400, 16, "TableHeight", "1"));
		_fldif.add(new DDictEntry(24, 4401, 16, "TableTraverse", "1"));
		_fldif.add(new DDictEntry(24, 4404, 9, "TableMotion", "1"));
		_fldif.add(new DDictEntry(24, 4405, 16, "TableVerticalIncrement", "1-n"));
		_fldif.add(new DDictEntry(24, 4406, 16, "TableLateralIncrement", "1-n"));
		_fldif.add(new DDictEntry(24, 4407, 16, "TableLongitudinalIncrement",
				"1-n"));
		_fldif.add(new DDictEntry(24, 4408, 16, "TableAngle", "1"));
		_fldif.add(new DDictEntry(24, 4416, 9, "RotationDirection", "1"));
		_fldif.add(new DDictEntry(24, 4417, 16, "AngularPosition", "1"));
		_fldif.add(new DDictEntry(24, 4418, 16, "RadialPosition", "1-n"));
		_fldif.add(new DDictEntry(24, 4419, 16, "ScanArc", "1"));
		_fldif.add(new DDictEntry(24, 4420, 16, "AngularStep", "1"));
		_fldif.add(new DDictEntry(24, 4421, 16, "CenterOfRotationOffset", "1"));
		_fldif.add(new DDictEntry(24, 4422, 16, "RotationOffset", "1-n"));
		_fldif.add(new DDictEntry(24, 4423, 9, "FieldOfViewShape", "1"));
		_fldif.add(new DDictEntry(24, 4425, 15, "FieldOfViewDimension", "1-2"));
		_fldif.add(new DDictEntry(24, 4432, 15, "ExposureTime", "1"));
		_fldif.add(new DDictEntry(24, 4433, 15, "XRayTubeCurrent", "1"));
		_fldif.add(new DDictEntry(24, 4434, 15, "Exposure", "1"));
		_fldif.add(new DDictEntry(24, 4436, 16, "AveragePulseWidth", "1"));
		_fldif.add(new DDictEntry(24, 4437, 9, "RadiationSetting", "1"));
		_fldif.add(new DDictEntry(24, 4442, 9, "RadiationMode", "1"));
		_fldif.add(new DDictEntry(24, 4446, 16, "ImageAreaDoseProduct", "1"));
		_fldif.add(new DDictEntry(24, 4448, 7, "FilterType", "1"));
		_fldif.add(new DDictEntry(24, 4449, 6, "TypeOfFilters", "1-n"));
		_fldif.add(new DDictEntry(24, 4450, 16, "IntensifierSize", "1"));
		_fldif.add(new DDictEntry(24, 4452, 16, "ImagerPixelSpacing", "2"));
		_fldif.add(new DDictEntry(24, 4454, 9, "Grid", "1"));
		_fldif.add(new DDictEntry(24, 4464, 15, "GeneratorPower", "1"));
		_fldif.add(new DDictEntry(24, 4480, 7, "CollimatorGridName", "1"));
		_fldif.add(new DDictEntry(24, 4481, 9, "CollimatorType", "1"));
		_fldif.add(new DDictEntry(24, 4482, 15, "FocalDistance", "1"));
		_fldif.add(new DDictEntry(24, 4483, 16, "XFocusCenter", "1"));
		_fldif.add(new DDictEntry(24, 4484, 16, "YFocusCenter", "1"));
		_fldif.add(new DDictEntry(24, 4496, 16, "FocalSpot", "1-n"));
		_fldif.add(new DDictEntry(24, 4608, 11, "DateOfLastCalibration", "1-n"));
		_fldif.add(new DDictEntry(24, 4609, 12, "TimeOfLastCalibration", "1-n"));
		_fldif.add(new DDictEntry(24, 4624, 7, "ConvolutionKernel", "1-n"));
		_fldif.add(new DDictEntry(24, 4674, 15, "ActualFrameDuration", "1"));
		_fldif.add(new DDictEntry(24, 4675, 15, "CountRate", "1"));
		_fldif
				.add(new DDictEntry(24, 4676, 3, "PreferredPlaybackSequencing",
						"1"));
		_fldif.add(new DDictEntry(24, 4688, 7, "ReceivingCoil", "1"));
		_fldif.add(new DDictEntry(24, 4689, 7, "TransmittingCoil", "1"));
		_fldif.add(new DDictEntry(24, 4704, 7, "PlateType", "1"));
		_fldif.add(new DDictEntry(24, 4705, 6, "PhosphorType", "1"));
		_fldif.add(new DDictEntry(24, 4864, 16, "ScanVelocity", "1"));
		_fldif.add(new DDictEntry(24, 4865, 9, "WholeBodyTechnique", "1-n"));
		_fldif.add(new DDictEntry(24, 4866, 15, "ScanLength", "1"));
		_fldif.add(new DDictEntry(24, 4880, 3, "AcquisitionMatrix", "4"));
		_fldif.add(new DDictEntry(24, 4882, 9, "PhaseEncodingDirection", "1"));
		_fldif.add(new DDictEntry(24, 4884, 16, "FlipAngle", "1"));
		_fldif.add(new DDictEntry(24, 4885, 9, "VariableFlipAngleFlag", "1"));
		_fldif.add(new DDictEntry(24, 4886, 16, "SAR", "1"));
		_fldif.add(new DDictEntry(24, 4888, 16, "dBdt", "1"));
		_fldif.add(new DDictEntry(24, 5120, 6,
				"AcquisitionDeviceProcessingDescription", "1"));
		_fldif.add(new DDictEntry(24, 5121, 6, "AcquisitionDeviceProcessingCode",
				"1"));
		_fldif.add(new DDictEntry(24, 5122, 9, "CassetteOrientation", "1"));
		_fldif.add(new DDictEntry(24, 5123, 9, "CassetteSize", "1"));
		_fldif.add(new DDictEntry(24, 5124, 3, "ExposuresOnPlate", "1"));
		_fldif.add(new DDictEntry(24, 5125, 15, "RelativeXrayExposure", "1"));
		_fldif.add(new DDictEntry(24, 5216, 16, "TomoLayerHeight", "1"));
		_fldif.add(new DDictEntry(24, 5232, 16, "TomoAngle", "1"));
		_fldif.add(new DDictEntry(24, 5248, 16, "TomoTime", "1"));
		_fldif.add(new DDictEntry(24, 5376, 9, "PositionerMotion", "1"));
		_fldif.add(new DDictEntry(24, 5392, 16, "PositionerPrimaryAngle", "1"));
		_fldif.add(new DDictEntry(24, 5393, 16, "PositionerSecondaryAngle", "1"));
		_fldif.add(new DDictEntry(24, 5408, 16,
				"PositionerPrimaryAngleIncrement", "1-n"));
		_fldif.add(new DDictEntry(24, 5409, 16,
				"PositionerSecondaryAngleIncrement", "1-n"));
		_fldif.add(new DDictEntry(24, 5424, 16, "DetectorPrimaryAngle", "1"));
		_fldif.add(new DDictEntry(24, 5425, 16, "DetectorSecondaryAngle", "1"));
		_fldif.add(new DDictEntry(24, 5632, 9, "ShutterShape", "1-3"));
		_fldif.add(new DDictEntry(24, 5634, 15, "ShutterLeftVerticalEdge", "1"));
		_fldif.add(new DDictEntry(24, 5636, 15, "ShutterRightVerticalEdge", "1"));
		_fldif
				.add(new DDictEntry(24, 5638, 15, "ShutterUpperHorizontalEdge",
						"1"));
		_fldif
				.add(new DDictEntry(24, 5640, 15, "ShutterLowerHorizontalEdge",
						"1"));
		_fldif.add(new DDictEntry(24, 5648, 15, "CenterOfCircularShutter", "2"));
		_fldif.add(new DDictEntry(24, 5650, 15, "RadiusOfCircularShutter", "1"));
		_fldif.add(new DDictEntry(24, 5664, 15, "VerticesOfThePolygonalShutter",
				"2-2n"));
		_fldif.add(new DDictEntry(24, 5888, 9, "CollimatorShape", "1-3"));
		_fldif
				.add(new DDictEntry(24, 5890, 15, "CollimatorLeftVerticalEdge",
						"1"));
		_fldif.add(new DDictEntry(24, 5892, 15, "CollimatorRightVerticalEdge",
				"1"));
		_fldif.add(new DDictEntry(24, 5894, 15, "CollimatorUpperHorizontalEdge",
				"1"));
		_fldif.add(new DDictEntry(24, 5896, 15, "CollimatorLowerHorizontalEdge",
				"1"));
		_fldif
				.add(new DDictEntry(24, 5904, 15, "CenterOfCircularCollimator",
						"2"));
		_fldif
				.add(new DDictEntry(24, 5906, 15, "RadiusOfCircularCollimator",
						"1"));
		_fldif.add(new DDictEntry(24, 5920, 15,
				"VerticesOfThePolygonalCollimator", "2-2n"));
		_fldif.add(new DDictEntry(24, 20480, 7, "OutputPower", "1-n"));
		_fldif.add(new DDictEntry(24, 20496, 6, "TransducerData", "3"));
		_fldif.add(new DDictEntry(24, 20498, 16, "FocusDepth", "1"));
		_fldif.add(new DDictEntry(24, 20512, 6, "PreprocessingFunction", "1"));
		_fldif.add(new DDictEntry(24, 20513, 6, "PostprocessingFunction", "1"));
		_fldif.add(new DDictEntry(24, 20514, 16, "MechanicalIndex", "1"));
		_fldif.add(new DDictEntry(24, 20516, 16, "BoneThermalIndex", "1"));
		_fldif.add(new DDictEntry(24, 20518, 16, "CranialThermalIndex", "1"));
		_fldif.add(new DDictEntry(24, 20519, 16, "SoftTissueThermalIndex", "1"));
		_fldif.add(new DDictEntry(24, 20520, 16, "SoftTissueFocusThermalIndex",
				"1"));
		_fldif.add(new DDictEntry(24, 20521, 16, "SoftTissueSurfaceThermalIndex",
				"1"));
		_fldif.add(new DDictEntry(24, 20560, 15, "DepthOfScanField", "1"));
		_fldif.add(new DDictEntry(24, 20736, 9, "PatientPosition", "1"));
		_fldif.add(new DDictEntry(24, 20737, 9, "ViewPosition", "1"));
		_fldif
				.add(new DDictEntry(24, 21008, 16, "ImageTransformationMatrix",
						"6"));
		_fldif.add(new DDictEntry(24, 21010, 16, "ImageTranslationVector", "3"));
		_fldif.add(new DDictEntry(24, 24576, 16, "Sensitivity", "1"));
		_fldif.add(new DDictEntry(24, 24593, 10, "SequenceOfUltrasoundRegions",
				"1"));
		_fldif.add(new DDictEntry(24, 24594, 3, "RegionSpatialFormat", "1"));
		_fldif.add(new DDictEntry(24, 24596, 3, "RegionDataType", "1"));
		_fldif.add(new DDictEntry(24, 24598, 1, "RegionFlags", "1"));
		_fldif.add(new DDictEntry(24, 24600, 1, "RegionLocationMinX0", "1"));
		_fldif.add(new DDictEntry(24, 24602, 1, "RegionLocationMinY0", "1"));
		_fldif.add(new DDictEntry(24, 24604, 1, "RegionLocationMaxX1", "1"));
		_fldif.add(new DDictEntry(24, 24606, 1, "RegionLocationMaxY1", "1"));
		_fldif.add(new DDictEntry(24, 24608, 19, "ReferencePixelX0", "1"));
		_fldif.add(new DDictEntry(24, 24610, 19, "ReferencePixelY0", "1"));
		_fldif.add(new DDictEntry(24, 24612, 3, "PhysicalUnitsXDirection", "1"));
		_fldif.add(new DDictEntry(24, 24614, 3, "PhysicalUnitsYDirection", "1"));
		_fldif.add(new DDictEntry(24, 24616, 20, "ReferencePixelPhysicalValueX",
				"1"));
		_fldif.add(new DDictEntry(24, 24618, 20, "ReferencePixelPhysicalValueY",
				"1"));
		_fldif.add(new DDictEntry(24, 24620, 20, "PhysicalDeltaX", "1"));
		_fldif.add(new DDictEntry(24, 24622, 20, "PhysicalDeltaY", "1"));
		_fldif.add(new DDictEntry(24, 24624, 1, "TransducerFrequency", "1"));
		_fldif.add(new DDictEntry(24, 24625, 9, "TransducerType", "1"));
		_fldif.add(new DDictEntry(24, 24626, 1, "PulseRepetitionFrequency", "1"));
		_fldif.add(new DDictEntry(24, 24628, 20, "DopplerCorrectionAngle", "1"));
		_fldif.add(new DDictEntry(24, 24630, 20, "SteeringAngle", "1"));
		_fldif.add(new DDictEntry(24, 24632, 1, "DopplerSampleVolumeXPosition",
				"1"));
		_fldif.add(new DDictEntry(24, 24634, 1, "DopplerSampleVolumeYPosition",
				"1"));
		_fldif.add(new DDictEntry(24, 24636, 1, "TMLinePositionX0", "1"));
		_fldif.add(new DDictEntry(24, 24638, 1, "TMLinePositionY0", "1"));
		_fldif.add(new DDictEntry(24, 24640, 1, "TMLinePositionX1", "1"));
		_fldif.add(new DDictEntry(24, 24642, 1, "TMLinePositionY1", "1"));
		_fldif
				.add(new DDictEntry(24, 24644, 3, "PixelComponentOrganization",
						"1"));
		_fldif.add(new DDictEntry(24, 24646, 1, "PixelComponentMask", "1"));
		_fldif.add(new DDictEntry(24, 24648, 1, "PixelComponentRangeStart", "1"));
		_fldif.add(new DDictEntry(24, 24650, 1, "PixelComponentRangeStop", "1"));
		_fldif.add(new DDictEntry(24, 24652, 3, "PixelComponentPhysicalUnits",
				"1"));
		_fldif.add(new DDictEntry(24, 24654, 3, "PixelComponentDataType", "1"));
		_fldif.add(new DDictEntry(24, 24656, 1, "NumberOfTableBreakPoints", "1"));
		_fldif.add(new DDictEntry(24, 24658, 1, "TableOfXBreakPoints", "1-n"));
		_fldif.add(new DDictEntry(24, 24660, 20, "TableOfYBreakPoints", "1-n"));
		_fldif.add(new DDictEntry(24, 24662, 1, "NumberOfTableEntries", "1"));
		_fldif.add(new DDictEntry(24, 24664, 1, "TableOfPixelValues", "1-n"));
		_fldif
				.add(new DDictEntry(24, 24666, 26, "TableOfParameterValues",
						"1-n"));
		_fldif.add(new DDictEntry(32, 0, 1, "ImageGroupLength", "1"));
		_fldif.add(new DDictEntry(32, 13, 2, "StudyInstanceUID", "1"));
		_fldif.add(new DDictEntry(32, 14, 2, "SeriesInstanceUID", "1"));
		_fldif.add(new DDictEntry(32, 16, 7, "StudyID", "1"));
		_fldif.add(new DDictEntry(32, 17, 15, "SeriesNumber", "1"));
		_fldif.add(new DDictEntry(32, 18, 15, "AcquisitionNumber", "1"));
		_fldif.add(new DDictEntry(32, 19, 15, "InstanceNumber", "1"));
		_fldif.add(new DDictEntry(32, 20, 15, "IsotopeNumber", "1"));
		_fldif.add(new DDictEntry(32, 21, 15, "PhaseNumber", "1"));
		_fldif.add(new DDictEntry(32, 22, 15, "IntervalNumber", "1"));
		_fldif.add(new DDictEntry(32, 23, 15, "TimeSlotNumber", "1"));
		_fldif.add(new DDictEntry(32, 24, 15, "AngleNumber", "1"));
		_fldif.add(new DDictEntry(32, 32, 9, "PatientOrientation", "2"));
		_fldif.add(new DDictEntry(32, 34, 15, "OverlayNumber", "1"));
		_fldif.add(new DDictEntry(32, 36, 15, "CurveNumber", "1"));
		_fldif.add(new DDictEntry(32, 38, 15, "LUTNumber", "1"));
		_fldif.add(new DDictEntry(32, 50, 16, "ImagePositionPatient", "3"));
		_fldif.add(new DDictEntry(32, 55, 16, "ImageOrientationPatient", "6"));
		_fldif.add(new DDictEntry(32, 82, 2, "FrameOfReferenceUID", "1"));
		_fldif.add(new DDictEntry(32, 96, 9, "Laterality", "1"));
		_fldif
				.add(new DDictEntry(32, 256, 15, "TemporalPositionIdentifier",
						"1"));
		_fldif.add(new DDictEntry(32, 261, 15, "NumberOfTemporalPositions", "1"));
		_fldif.add(new DDictEntry(32, 272, 16, "TemporalResolution", "1"));
		_fldif.add(new DDictEntry(32, 4096, 15, "SeriesInStudy", "1"));
		_fldif.add(new DDictEntry(32, 4098, 15, "ImagesInAcquisition", "1"));
		_fldif.add(new DDictEntry(32, 4100, 15, "AcquisitionsInStudy", "1"));
		_fldif
				.add(new DDictEntry(32, 4160, 6, "PositionReferenceIndicator",
						"1"));
		_fldif.add(new DDictEntry(32, 4161, 16, "SliceLocation", "1"));
		_fldif.add(new DDictEntry(32, 4208, 15, "OtherStudyNumbers", "1-n"));
		_fldif.add(new DDictEntry(32, 4608, 15, "NumberOfPatientRelatedStudies",
				"1"));
		_fldif.add(new DDictEntry(32, 4610, 15, "NumberOfPatientRelatedSeries",
				"1"));
		_fldif.add(new DDictEntry(32, 4612, 15, "NumberOfPatientRelatedImages",
				"1"));
		_fldif
				.add(new DDictEntry(32, 4614, 15, "NumberOfStudyRelatedSeries",
						"1"));
		_fldif
				.add(new DDictEntry(32, 4616, 15, "NumberOfStudyRelatedImages",
						"1"));
		_fldif.add(new DDictEntry(32, 4617, 15, "NumberOfSeriesRelatedImages",
				"1"));
		_fldif.add(new DDictEntry(32, 16384, 18, "ImageComments", "1"));
		_fldif.add(new DDictEntry(40, 0, 1, "ImagePresentationGroupLength", "1"));
		_fldif.add(new DDictEntry(40, 2, 3, "SamplesPerPixel", "1"));
		_fldif.add(new DDictEntry(40, 4, 9, "PhotometricInterpretation", "1"));
		_fldif.add(new DDictEntry(40, 6, 3, "PlanarConfiguration", "1"));
		_fldif.add(new DDictEntry(40, 8, 15, "NumberOfFrames", "1"));
		_fldif.add(new DDictEntry(40, 9, 5, "FrameIncrementPointer", "1-n"));
		_fldif.add(new DDictEntry(40, 16, 3, "Rows", "1"));
		_fldif.add(new DDictEntry(40, 17, 3, "Columns", "1"));
		_fldif.add(new DDictEntry(40, 18, 3, "Planes", "1"));
		_fldif.add(new DDictEntry(40, 20, 3, "UltrasoundColorDataPresent", "1"));
		_fldif.add(new DDictEntry(40, 48, 16, "PixelSpacing", "2"));
		_fldif.add(new DDictEntry(40, 49, 16, "ZoomFactor", "2"));
		_fldif.add(new DDictEntry(40, 50, 16, "ZoomCenter", "2"));
		_fldif.add(new DDictEntry(40, 52, 15, "PixelAspectRatio", "2"));
		_fldif.add(new DDictEntry(40, 81, 9, "CorrectedImage", "1"));
		_fldif.add(new DDictEntry(40, 256, 3, "BitsAllocated", "1"));
		_fldif.add(new DDictEntry(40, 257, 3, "BitsStored", "1"));
		_fldif.add(new DDictEntry(40, 258, 3, "HighBit", "1"));
		_fldif.add(new DDictEntry(40, 259, 3, "PixelRepresentation", "1"));
		_fldif.add(new DDictEntry(40, 262, 21, "SmallestImagePixelValue", "1"));
		_fldif.add(new DDictEntry(40, 263, 21, "LargestImagePixelValue", "1"));
		_fldif
				.add(new DDictEntry(40, 264, 21, "SmallestPixelValueInSeries",
						"1"));
		_fldif.add(new DDictEntry(40, 265, 21, "LargestPixelValueInSeries", "1"));
		_fldif.add(new DDictEntry(40, 272, 21, "SmallestImagePixelValueInPlane",
				"1"));
		_fldif.add(new DDictEntry(40, 273, 21, "LargestImagePixelValueInPlane",
				"1"));
		_fldif.add(new DDictEntry(40, 288, 21, "PixelPaddingValue", "1"));
		_fldif
				.add(new DDictEntry(40, 4160, 9, "PixelIntensityRelationship",
						"1"));
		_fldif.add(new DDictEntry(40, 4176, 16, "WindowCenter", "1-n"));
		_fldif.add(new DDictEntry(40, 4177, 16, "WindowWidth", "1-n"));
		_fldif.add(new DDictEntry(40, 4178, 16, "RescaleIntercept", "1"));
		_fldif.add(new DDictEntry(40, 4179, 16, "RescaleSlope", "1"));
		_fldif.add(new DDictEntry(40, 4180, 6, "RescaleType", "1"));
		_fldif.add(new DDictEntry(40, 4181, 6, "WindowCenterWidthExplanation",
				"1-n"));
		_fldif.add(new DDictEntry(40, 4240, 9, "RecommendedViewingMode", "1"));
		_fldif.add(new DDictEntry(40, 4353, 21,
				"RedPaletteColorLookupTableDescriptor", "3"));
		_fldif.add(new DDictEntry(40, 4354, 21,
				"GreenPaletteColorLookupTableDescriptor", "3"));
		_fldif.add(new DDictEntry(40, 4355, 21,
				"BluePaletteColorLookupTableDescriptor", "3"));
		_fldif
				.add(new DDictEntry(40, 4505, 2, "PaletteColorLookupTableUID",
						"1"));
		_fldif.add(new DDictEntry(40, 4609, 24, "RedPaletteColorLookupTableData",
				"1"));
		_fldif.add(new DDictEntry(40, 4610, 24,
				"GreenPaletteColorLookupTableData", "1"));
		_fldif.add(new DDictEntry(40, 4611, 24,
				"BluePaletteColorLookupTableData", "1"));
		_fldif.add(new DDictEntry(40, 4641, 24,
				"SegmentedRedPaletteColorLookupTableData", "1"));
		_fldif.add(new DDictEntry(40, 4642, 24,
				"SegmentedGreenPaletteColorLookupTableData", "1"));
		_fldif.add(new DDictEntry(40, 4643, 24,
				"SegmentedBluePaletteColorLookupTableData", "1"));
		_fldif.add(new DDictEntry(40, 8464, 9, "LossyImageCompression", "1"));
		_fldif.add(new DDictEntry(40, 12288, 10, "ModalityLUTSequence", "1"));
		_fldif.add(new DDictEntry(40, 12290, 21, "LUTDescriptor", "3"));
		_fldif.add(new DDictEntry(40, 12291, 6, "LUTExplanation", "1"));
		_fldif.add(new DDictEntry(40, 12292, 6, "ModalityLUTType", "1"));
		_fldif.add(new DDictEntry(40, 12294, 21, "LUTData", "1-n"));
		_fldif.add(new DDictEntry(40, 12304, 10, "VOILUTSequence", "1"));
		_fldif.add(new DDictEntry(40, 20480, 10, "BiPlaneAcquisitionSequence",
				"1"));
		_fldif
				.add(new DDictEntry(40, 24592, 3, "RepresentativeFrameNumber",
						"1"));
		_fldif.add(new DDictEntry(40, 24608, 3, "FrameNumbersOfInterestFOI",
				"1-n"));
		_fldif.add(new DDictEntry(40, 24610, 6, "FrameOfInterestDescription",
				"1-n"));
		_fldif.add(new DDictEntry(40, 24624, 3, "MaskPointers", "1-n"));
		_fldif.add(new DDictEntry(40, 24640, 3, "RWavePointer", "1-n"));
		_fldif.add(new DDictEntry(40, 24832, 10, "MaskSubtractionSequence", "1"));
		_fldif.add(new DDictEntry(40, 24833, 9, "MaskOperation", "1"));
		_fldif.add(new DDictEntry(40, 24834, 3, "ApplicableFrameRange", "2-2n"));
		_fldif.add(new DDictEntry(40, 24848, 3, "MaskFrameNumbers", "1-n"));
		_fldif.add(new DDictEntry(40, 24850, 3, "ContrastFrameAveraging", "1"));
		_fldif.add(new DDictEntry(40, 24852, 26, "MaskSubPixelShift", "2"));
		_fldif.add(new DDictEntry(40, 24864, 23, "TIDOffset", "1"));
		_fldif
				.add(new DDictEntry(40, 24976, 13, "MaskOperationExplanation",
						"1"));
		_fldif.add(new DDictEntry(50, 0, 1, "StudyGroupLength", "1"));
		_fldif.add(new DDictEntry(50, 10, 9, "StudyStatusID", "1"));
		_fldif.add(new DDictEntry(50, 12, 9, "StudyPriorityID", "1"));
		_fldif.add(new DDictEntry(50, 18, 6, "StudyIDIssuer", "1"));
		_fldif.add(new DDictEntry(50, 50, 11, "StudyVerifiedDate", "1"));
		_fldif.add(new DDictEntry(50, 51, 12, "StudyVerifiedTime", "1"));
		_fldif.add(new DDictEntry(50, 52, 11, "StudyReadDate", "1"));
		_fldif.add(new DDictEntry(50, 53, 12, "StudyReadTime", "1"));
		_fldif.add(new DDictEntry(50, 4096, 11, "ScheduledStudyStartDate", "1"));
		_fldif.add(new DDictEntry(50, 4097, 12, "ScheduledStudyStartTime", "1"));
		_fldif.add(new DDictEntry(50, 4112, 11, "ScheduledStudyStopDate", "1"));
		_fldif.add(new DDictEntry(50, 4113, 12, "ScheduledStudyStopTime", "1"));
		_fldif.add(new DDictEntry(50, 4128, 6, "ScheduledStudyLocation", "1"));
		_fldif.add(new DDictEntry(50, 4129, 4, "ScheduledStudyLocationAETitle",
				"1-n"));
		_fldif.add(new DDictEntry(50, 4144, 6, "ReasonForStudy", "1"));
		_fldif.add(new DDictEntry(50, 4146, 14, "RequestingPhysician", "1"));
		_fldif.add(new DDictEntry(50, 4147, 6, "RequestingService", "1"));
		_fldif.add(new DDictEntry(50, 4160, 11, "StudyArrivalDate", "1"));
		_fldif.add(new DDictEntry(50, 4161, 12, "StudyArrivalTime", "1"));
		_fldif.add(new DDictEntry(50, 4176, 11, "StudyCompletionDate", "1"));
		_fldif.add(new DDictEntry(50, 4177, 12, "StudyCompletionTime", "1"));
		_fldif.add(new DDictEntry(50, 4181, 9, "StudyComponentStatusID", "1"));
		_fldif.add(new DDictEntry(50, 4192, 6, "RequestedProcedureDescription",
				"1"));
		_fldif.add(new DDictEntry(50, 4196, 10, "RequestedProcedureCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(50, 4208, 6, "RequestedContrastAgent", "1"));
		_fldif.add(new DDictEntry(50, 16384, 18, "StudyComments", "1"));
		_fldif.add(new DDictEntry(56, 0, 1, "VisitGroupLength", "1"));
		_fldif.add(new DDictEntry(56, 4, 10, "ReferencedPatientAliasSequence",
				"1"));
		_fldif.add(new DDictEntry(56, 8, 9, "VisitStatusID", "1"));
		_fldif.add(new DDictEntry(56, 16, 6, "AdmissionID", "1"));
		_fldif.add(new DDictEntry(56, 17, 6, "IssuerOfAdmissionID", "1"));
		_fldif.add(new DDictEntry(56, 22, 6, "RouteOfAdmissions", "1"));
		_fldif.add(new DDictEntry(56, 26, 11, "ScheduledAdmissionDate", "1"));
		_fldif.add(new DDictEntry(56, 27, 12, "ScheduledAdmissionTime", "1"));
		_fldif.add(new DDictEntry(56, 28, 11, "ScheduledDischargeDate", "1"));
		_fldif.add(new DDictEntry(56, 29, 12, "ScheduledDischargeTime", "1"));
		_fldif.add(new DDictEntry(56, 30, 6,
				"ScheduledPatientInstitutionResidence", "1"));
		_fldif.add(new DDictEntry(56, 32, 11, "AdmittingDate", "1"));
		_fldif.add(new DDictEntry(56, 33, 12, "AdmittingTime", "1"));
		_fldif.add(new DDictEntry(56, 48, 11, "DischargeDate", "1"));
		_fldif.add(new DDictEntry(56, 50, 12, "DischargeTime", "1"));
		_fldif
				.add(new DDictEntry(56, 64, 6, "DischargeDiagnosisDescription",
						"1"));
		_fldif.add(new DDictEntry(56, 68, 10, "DischargeDiagnosisCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(56, 80, 6, "SpecialNeeds", "1"));
		_fldif.add(new DDictEntry(56, 768, 6, "CurrentPatientLocation", "1"));
		_fldif
				.add(new DDictEntry(56, 1024, 6, "PatientInstitutionResidence",
						"1"));
		_fldif.add(new DDictEntry(56, 1280, 6, "PatientState", "1"));
		_fldif.add(new DDictEntry(56, 16384, 18, "VisitComments", "1"));
		_fldif.add(new DDictEntry(64, 0, 1, "ModalityWorklistGroupLength", "1"));
		_fldif.add(new DDictEntry(64, 1, 4, "ScheduledStationAETitle", "1-n"));
		_fldif.add(new DDictEntry(64, 2, 11, "ScheduledProcedureStepStartDate",
				"1"));
		_fldif.add(new DDictEntry(64, 3, 12, "ScheduledProcedureStepStartTime",
				"1"));
		_fldif
				.add(new DDictEntry(64, 4, 11, "ScheduledProcedureStepEndDate",
						"1"));
		_fldif
				.add(new DDictEntry(64, 5, 12, "ScheduledProcedureStepEndTime",
						"1"));
		_fldif.add(new DDictEntry(64, 6, 14, "ScheduledPerformingPhysiciansName",
				"1"));
		_fldif.add(new DDictEntry(64, 7, 6, "ScheduledProcedureStepDescription",
				"1"));
		_fldif.add(new DDictEntry(64, 8, 10, "ScheduledActionItemCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 9, 7, "ScheduledProcedureStepID", "1"));
		_fldif.add(new DDictEntry(64, 16, 7, "ScheduledStationName", "1-n"));
		_fldif.add(new DDictEntry(64, 17, 7, "ScheduledProcedureStepLocation",
				"1"));
		_fldif.add(new DDictEntry(64, 18, 6, "PreMedication", "1"));
		_fldif
				.add(new DDictEntry(64, 32, 9, "ScheduledProcedureStepStatus",
						"1"));
		_fldif.add(new DDictEntry(64, 256, 10, "ScheduledProcedureStepSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 1024, 18,
				"CommentsOnTheScheduledProcedureStep", "1"));
		_fldif.add(new DDictEntry(64, 4097, 7, "RequestedProcedureID", "1"));
		_fldif.add(new DDictEntry(64, 4098, 6, "ReasonForTheRequestedProcedure",
				"1"));
		_fldif
				.add(new DDictEntry(64, 4099, 7, "RequestedProcedurePriority",
						"1"));
		_fldif.add(new DDictEntry(64, 4100, 6, "PatientTransportArrangements",
				"1"));
		_fldif
				.add(new DDictEntry(64, 4101, 6, "RequestedProcedureLocation",
						"1"));
		_fldif
				.add(new DDictEntry(64, 4102, 7, "PlacerOrderNumberProcedure",
						"1"));
		_fldif
				.add(new DDictEntry(64, 4103, 7, "FillerOrderNumberProcedure",
						"1"));
		_fldif.add(new DDictEntry(64, 4104, 6, "ConfidentialityCode", "1"));
		_fldif.add(new DDictEntry(64, 4105, 7, "ReportingPriority", "1"));
		_fldif.add(new DDictEntry(64, 4112, 14,
				"NamesOfIntendedRecipientsOfResults", "1-n"));
		_fldif
				.add(new DDictEntry(64, 5120, 18, "RequestedProcedureComments",
						"1"));
		_fldif.add(new DDictEntry(64, 8193, 6,
				"ReasonForTheImagingServiceRequest", "1"));
		_fldif.add(new DDictEntry(64, 8196, 11,
				"IssueDateOfImagingServiceRequest", "1"));
		_fldif.add(new DDictEntry(64, 8197, 12,
				"IssueTimeOfImagingServiceRequest", "1"));
		_fldif.add(new DDictEntry(64, 8198, 7,
				"PlacerOrderNumberImagingServiceRequest", "1"));
		_fldif.add(new DDictEntry(64, 8199, 7,
				"FillerOrderNumberImagingServiceRequest", "1"));
		_fldif.add(new DDictEntry(64, 8200, 14, "OrderEnteredBy", "1"));
		_fldif.add(new DDictEntry(64, 8201, 7, "OrderEnterersLocation", "1"));
		_fldif.add(new DDictEntry(64, 8208, 7, "OrderCallbackPhoneNumber", "1"));
		_fldif.add(new DDictEntry(64, 9216, 18, "ImagingServiceRequestComments",
				"1"));
		_fldif.add(new DDictEntry(64, 12289, 6,
				"ConfidentialityConstraintOnPatientData", "1"));
		_fldif.add(new DDictEntry(80, 4, 9, "CalibrationObject", "1"));
		_fldif.add(new DDictEntry(80, 16, 10, "DeviceSequence", "1"));
		_fldif.add(new DDictEntry(80, 20, 16, "DeviceLength", "1"));
		_fldif.add(new DDictEntry(80, 22, 16, "DeviceDiameter", "1"));
		_fldif.add(new DDictEntry(80, 23, 9, "DeviceDiameterUnits", "1"));
		_fldif.add(new DDictEntry(80, 24, 16, "DeviceVolume", "1"));
		_fldif.add(new DDictEntry(80, 25, 16, "InterMarkerDistance", "1"));
		_fldif.add(new DDictEntry(80, 32, 6, "DeviceDescription", "1"));
		_fldif.add(new DDictEntry(84, 0, 1, "GroupLength", "1"));
		_fldif.add(new DDictEntry(84, 16, 3, "EnergyWindowVector", "1-n"));
		_fldif.add(new DDictEntry(84, 17, 3, "NumberOfEnergyWindows", "1"));
		_fldif.add(new DDictEntry(84, 18, 10, "EnergyWindowInformationSequence",
				"1"));
		_fldif.add(new DDictEntry(84, 19, 10, "EnergyWindowRangeSequence", "1"));
		_fldif.add(new DDictEntry(84, 20, 16, "EnergyWindowLowerLimit", "1"));
		_fldif.add(new DDictEntry(84, 21, 16, "EnergyWindowUpperLimit", "1"));
		_fldif.add(new DDictEntry(84, 22, 10,
				"RadiopharmaceuticalInformationSequence", "1"));
		_fldif.add(new DDictEntry(84, 23, 15, "ResidualSyringeCounts", "1"));
		_fldif.add(new DDictEntry(84, 24, 7, "EnergyWindowName", "1"));
		_fldif.add(new DDictEntry(84, 32, 3, "DetectorVector", "1-n"));
		_fldif.add(new DDictEntry(84, 33, 3, "NumberOfDetectors", "1"));
		_fldif
				.add(new DDictEntry(84, 34, 10, "DetectorInformationSequence",
						"1"));
		_fldif.add(new DDictEntry(84, 48, 3, "PhaseVector", "1-n"));
		_fldif.add(new DDictEntry(84, 49, 3, "NumberOfPhases", "1"));
		_fldif.add(new DDictEntry(84, 50, 10, "PhaseInformationSequence", "1"));
		_fldif.add(new DDictEntry(84, 51, 3, "NumberOfFramesInPhase", "1"));
		_fldif.add(new DDictEntry(84, 54, 15, "PhaseDelay", "1"));
		_fldif.add(new DDictEntry(84, 56, 15, "PauseBetweenFrames", "1"));
		_fldif.add(new DDictEntry(84, 80, 3, "RotationVector", "1-n"));
		_fldif.add(new DDictEntry(84, 81, 3, "NumberOfRotations", "1"));
		_fldif
				.add(new DDictEntry(84, 82, 10, "RotationInformationSequence",
						"1"));
		_fldif.add(new DDictEntry(84, 83, 3, "NumberOfFramesInRotation", "1"));
		_fldif.add(new DDictEntry(84, 96, 3, "RRIntervalVector", "1-n"));
		_fldif.add(new DDictEntry(84, 97, 3, "NumberOfRRIntervals", "1"));
		_fldif.add(new DDictEntry(84, 98, 10, "GatedInformationSequence", "1"));
		_fldif.add(new DDictEntry(84, 99, 10, "DataInformationSequence", "1"));
		_fldif.add(new DDictEntry(84, 112, 3, "TimeSlotVector", "1-n"));
		_fldif.add(new DDictEntry(84, 113, 3, "NumberOfTimeSlots", "1"));
		_fldif
				.add(new DDictEntry(84, 114, 10, "TimeSlotInformationSequence",
						"1"));
		_fldif.add(new DDictEntry(84, 115, 16, "TimeSlotTime", "1"));
		_fldif.add(new DDictEntry(84, 128, 3, "SliceVector", "1-n"));
		_fldif.add(new DDictEntry(84, 129, 3, "NumberOfSlices", "1"));
		_fldif.add(new DDictEntry(84, 144, 3, "AngularViewVector", "1-n"));
		_fldif.add(new DDictEntry(84, 256, 3, "TimeSliceVector", "1-n"));
		_fldif.add(new DDictEntry(84, 257, 3, "NumberOfTimeSlices", "1"));
		_fldif.add(new DDictEntry(84, 512, 16, "StartAngle", "1"));
		_fldif.add(new DDictEntry(84, 514, 9, "TypeOfDetectorMotion", "1"));
		_fldif.add(new DDictEntry(84, 528, 15, "TriggerVector", "1-n"));
		_fldif.add(new DDictEntry(84, 529, 3, "NumberOfTriggersInPhase", "1"));
		_fldif.add(new DDictEntry(84, 544, 10, "ViewCodeSequence", "1"));
		_fldif.add(new DDictEntry(84, 546, 10,
				"ViewAngulationModifierCodeSequence", "1"));
		_fldif.add(new DDictEntry(84, 768, 10, "RadionuclideCodeSequence", "1"));
		_fldif.add(new DDictEntry(84, 770, 10, "AdministrationRouteCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(84, 772, 10, "RadiopharmaceuticalCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(84, 774, 10, "CalibrationDataSequence", "1"));
		_fldif.add(new DDictEntry(84, 776, 3, "EnergyWindowNumber", "1"));
		_fldif.add(new DDictEntry(84, 1024, 7, "ImageID", "1"));
		_fldif.add(new DDictEntry(84, 1040, 10, "PatientOrientationCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(84, 1042, 10,
				"PatientOrientationModifierCodeSequence", "1"));
		_fldif.add(new DDictEntry(84, 1044, 10,
				"PatientGantryRelationshipCodeSequence", "1"));
		_fldif.add(new DDictEntry(84, 4096, 9, "SeriesType", "2"));
		_fldif.add(new DDictEntry(84, 4097, 9, "Units", "1"));
		_fldif.add(new DDictEntry(84, 4098, 9, "CountsSource", "1"));
		_fldif.add(new DDictEntry(84, 4100, 9, "ReprojectionMethod", "1"));
		_fldif.add(new DDictEntry(84, 4352, 9, "RandomsCorrectionMethod", "1"));
		_fldif
				.add(new DDictEntry(84, 4353, 6, "AttenuationCorrectionMethod",
						"1"));
		_fldif.add(new DDictEntry(84, 4354, 9, "DecayCorrection", "1"));
		_fldif.add(new DDictEntry(84, 4355, 6, "ReconstructionMethod", "1"));
		_fldif
				.add(new DDictEntry(84, 4356, 6, "DetectorLinesOfResponseUsed",
						"1"));
		_fldif.add(new DDictEntry(84, 4357, 6, "ScatterCorrectionMethod", "1"));
		_fldif.add(new DDictEntry(84, 4608, 16, "AxialAcceptance", "1"));
		_fldif.add(new DDictEntry(84, 4609, 15, "AxialMash", "2"));
		_fldif.add(new DDictEntry(84, 4610, 15, "TransverseMash", "1"));
		_fldif.add(new DDictEntry(84, 4611, 16, "DetectorElementSize", "2"));
		_fldif.add(new DDictEntry(84, 4624, 16, "CoincidenceWindowWidth", "1"));
		_fldif.add(new DDictEntry(84, 4640, 9, "SecondaryCountsType", "1-n"));
		_fldif.add(new DDictEntry(84, 4864, 16, "FrameReferenceTime", "1"));
		_fldif.add(new DDictEntry(84, 4880, 15,
				"PrimaryPromptsCountsAccumulated", "1"));
		_fldif.add(new DDictEntry(84, 4881, 15, "SecondaryCountsAccumulated",
				"1-n"));
		_fldif.add(new DDictEntry(84, 4896, 16, "SliceSensitivityFactor", "1"));
		_fldif.add(new DDictEntry(84, 4897, 16, "DecayFactor", "1"));
		_fldif.add(new DDictEntry(84, 4898, 16, "DoseCalibrationFactor", "1"));
		_fldif.add(new DDictEntry(84, 4899, 16, "ScatterFractionFactor", "1"));
		_fldif.add(new DDictEntry(84, 4900, 16, "DeadTimeFactor", "1"));
		_fldif.add(new DDictEntry(84, 4912, 3, "ImageIndex", "1"));
		_fldif.add(new DDictEntry(84, 5120, 9, "CountsIncluded", "1-n"));
		_fldif.add(new DDictEntry(84, 5121, 9, "DeadTimeCorrectionFlag", "1"));
		_fldif.add(new DDictEntry(136, 0, 1, "StorageGroupLength", "1"));
		_fldif.add(new DDictEntry(136, 304, 7, "StorageMediaFilesetID", "1"));
		_fldif.add(new DDictEntry(136, 320, 2, "StorageMediaFilesetUID", "1"));
		_fldif.add(new DDictEntry(136, 512, 10, "IconImage", "1"));
		_fldif.add(new DDictEntry(136, 2308, 6, "TopicTitle", "1"));
		_fldif.add(new DDictEntry(136, 2310, 13, "TopicSubject", "1"));
		_fldif.add(new DDictEntry(136, 2320, 6, "TopicAuthor", "1"));
		_fldif.add(new DDictEntry(136, 2322, 6, "TopicKeyWords", "1-32"));
	}

	private static final void a() {
		_fldif.add(new DDictEntry(8192, 0, 1, "FilmSessionGroupLength", "1"));
		_fldif.add(new DDictEntry(8192, 16, 15, "NumberOfCopies", "1"));
		_fldif.add(new DDictEntry(8192, 32, 9, "PrintPriority", "1"));
		_fldif.add(new DDictEntry(8192, 48, 9, "MediumType", "1"));
		_fldif.add(new DDictEntry(8192, 64, 9, "FilmDestination", "1"));
		_fldif.add(new DDictEntry(8192, 80, 6, "FilmSessionLabel", "1"));
		_fldif.add(new DDictEntry(8192, 96, 15, "MemoryAllocation", "1"));
		_fldif.add(new DDictEntry(8192, 1280, 10, "ReferencedFilmBoxSequence",
				"1"));
		_fldif.add(new DDictEntry(8208, 0, 1, "FilmBoxGroupLength", "1"));
		_fldif.add(new DDictEntry(8208, 16, 13, "ImageDisplayFormat", "1"));
		_fldif.add(new DDictEntry(8208, 48, 9, "AnnotationDisplayFormatID", "1"));
		_fldif.add(new DDictEntry(8208, 64, 9, "FilmOrientation", "1"));
		_fldif.add(new DDictEntry(8208, 80, 9, "FilmSizeID", "1"));
		_fldif.add(new DDictEntry(8208, 96, 9, "MagnificationType", "1"));
		_fldif.add(new DDictEntry(8208, 128, 9, "SmoothingType", "1"));
		_fldif.add(new DDictEntry(8208, 256, 9, "BorderDensity", "1"));
		_fldif.add(new DDictEntry(8208, 272, 9, "EmptyImageDensity", "1"));
		_fldif.add(new DDictEntry(8208, 288, 3, "MinDensity", "1"));
		_fldif.add(new DDictEntry(8208, 304, 3, "MaxDensity", "1"));
		_fldif.add(new DDictEntry(8208, 320, 9, "Trim", "1"));
		_fldif
				.add(new DDictEntry(8208, 336, 13, "ConfigurationInformation",
						"1"));
		_fldif.add(new DDictEntry(8208, 1280, 10,
				"ReferencedFilmSessionSequence", "1"));
		_fldif.add(new DDictEntry(8208, 1296, 10, "ReferencedImageBoxSequence",
				"1"));
		_fldif.add(new DDictEntry(8208, 1312, 10,
				"ReferencedBasicAnnotationBoxSequence", "1"));
		_fldif.add(new DDictEntry(8224, 0, 1, "ImageBoxGroupLength", "1"));
		_fldif.add(new DDictEntry(8224, 16, 3, "ImageBoxPosition", "1"));
		_fldif.add(new DDictEntry(8224, 32, 9, "Polarity", "1"));
		_fldif.add(new DDictEntry(8224, 48, 16, "RequestedImageSize", "1"));
		_fldif.add(new DDictEntry(8224, 272, 10,
				"PreformattedGrayscaleImageSequence", "1"));
		_fldif.add(new DDictEntry(8224, 273, 10,
				"PreformattedColorImageSequence", "1"));
		_fldif.add(new DDictEntry(8224, 304, 10,
				"ReferencedImageOverlayBoxSequence", "1"));
		_fldif.add(new DDictEntry(8224, 320, 10, "ReferencedVOILUTBoxSequence",
				"1"));
		_fldif.add(new DDictEntry(8240, 0, 1, "AnnotationGroupLength", "1"));
		_fldif.add(new DDictEntry(8240, 16, 3, "AnnotationPosition", "1"));
		_fldif.add(new DDictEntry(8240, 32, 6, "TextString", "1"));
		_fldif.add(new DDictEntry(8256, 0, 1, "OverlayBoxGroupLength", "1"));
		_fldif.add(new DDictEntry(8256, 16, 10, "ReferencedOverlayPlaneSequence",
				"1"));
		_fldif.add(new DDictEntry(8256, 17, 3, "ReferencedOverlayPlaneGroups",
				"1-99"));
		_fldif.add(new DDictEntry(8256, 96, 9, "OverlayMagnificationType", "1"));
		_fldif.add(new DDictEntry(8256, 112, 9, "OverlaySmoothingType", "1"));
		_fldif.add(new DDictEntry(8256, 128, 9, "OverlayForegroundDensity", "1"));
		_fldif.add(new DDictEntry(8256, 144, 9, "OverlayMode", "1"));
		_fldif.add(new DDictEntry(8256, 256, 9, "ThresholdDensity", "1"));
		_fldif.add(new DDictEntry(8256, 1280, 10,
				"RETIRED_ReferencedImageBoxSequence", "1"));
		_fldif.add(new DDictEntry(8448, 0, 1, "PrintJobGroupLength", "1"));
		_fldif.add(new DDictEntry(8448, 16, 7, "PrintJobID", "1"));
		_fldif.add(new DDictEntry(8448, 32, 9, "ExecutionStatus", "1"));
		_fldif.add(new DDictEntry(8448, 48, 9, "ExecutionStatusInfo", "1"));
		_fldif.add(new DDictEntry(8448, 64, 11, "CreationDate", "1"));
		_fldif.add(new DDictEntry(8448, 80, 12, "CreationTime", "1"));
		_fldif.add(new DDictEntry(8448, 112, 4, "Originator", "1"));
		_fldif.add(new DDictEntry(8448, 320, 4, "DestinationAE", "1"));
		_fldif.add(new DDictEntry(8448, 352, 7, "OwnerID", "1"));
		_fldif.add(new DDictEntry(8448, 368, 15, "NumberOfFilms", "1"));
		_fldif.add(new DDictEntry(8448, 1280, 10, "ReferencedPrintJobSequence",
				"1"));
		_fldif.add(new DDictEntry(8464, 0, 1, "PrinterGroupLength", "1"));
		_fldif.add(new DDictEntry(8464, 16, 9, "PrinterStatus", "1"));
		_fldif.add(new DDictEntry(8464, 32, 9, "PrinterStatusInfo", "1"));
		_fldif.add(new DDictEntry(8464, 48, 6, "PrinterName", "1"));
		_fldif.add(new DDictEntry(8464, 153, 7, "PrintQueueID", "1"));
		_fldif.add(new DDictEntry(8480, 16, 9, "QueueStatus", "1"));
		_fldif.add(new DDictEntry(8480, 80, 10, "PrintJobDescriptionSequence",
				"1"));
		_fldif.add(new DDictEntry(8480, 112, 10,
				"QueueReferencedPrintJobSequence", "1"));
		_fldif.add(new DDictEntry(12290, 2, 7, "RTImageLabel", "1"));
		_fldif.add(new DDictEntry(12290, 3, 6, "RTImageName", "1"));
		_fldif.add(new DDictEntry(12290, 4, 13, "RTImageDescription", "1"));
		_fldif.add(new DDictEntry(12290, 10, 9, "ReportedValuesOrigin", "1"));
		_fldif.add(new DDictEntry(12290, 12, 9, "RTImagePlane", "1"));
		_fldif.add(new DDictEntry(12290, 14, 16, "XRayImageReceptorAngle", "1"));
		_fldif.add(new DDictEntry(12290, 16, 16, "RTImageOrientation", "6"));
		_fldif.add(new DDictEntry(12290, 17, 16, "ImagePlanePixelSpacing", "2"));
		_fldif.add(new DDictEntry(12290, 18, 16, "RTImagePosition", "2"));
		_fldif.add(new DDictEntry(12290, 32, 7, "RadiationMachineName", "1"));
		_fldif.add(new DDictEntry(12290, 34, 16, "RadiationMachineSAD", "1"));
		_fldif.add(new DDictEntry(12290, 36, 16, "RadiationMachineSSD", "1"));
		_fldif.add(new DDictEntry(12290, 38, 16, "RTImageSID", "1"));
		_fldif.add(new DDictEntry(12290, 40, 16,
				"SourceToReferenceObjectDistance", "1"));
		_fldif.add(new DDictEntry(12290, 41, 15, "FractionNumber", "1"));
		_fldif.add(new DDictEntry(12290, 48, 10, "ExposureSequence", "1"));
		_fldif.add(new DDictEntry(12290, 50, 16, "MetersetExposure", "1"));
		_fldif.add(new DDictEntry(12292, 1, 9, "DVHType", "1"));
		_fldif.add(new DDictEntry(12292, 2, 9, "DoseUnits", "1"));
		_fldif.add(new DDictEntry(12292, 4, 9, "DoseType", "1"));
		_fldif.add(new DDictEntry(12292, 6, 6, "DoseComment", "1"));
		_fldif.add(new DDictEntry(12292, 8, 16, "NormalizationPoint", "3"));
		_fldif.add(new DDictEntry(12292, 10, 9, "DoseSummationType", "1"));
		_fldif.add(new DDictEntry(12292, 12, 16, "GridFrameOffsetVector", "2-n"));
		_fldif.add(new DDictEntry(12292, 14, 16, "DoseGridScaling", "1"));
		_fldif.add(new DDictEntry(12292, 16, 10, "RTDoseROISequence", "1"));
		_fldif.add(new DDictEntry(12292, 18, 16, "DoseValue", "1"));
		_fldif.add(new DDictEntry(12292, 64, 16, "DVHNormalizationPoint", "3"));
		_fldif
				.add(new DDictEntry(12292, 66, 16, "DVHNormalizationDoseValue",
						"1"));
		_fldif.add(new DDictEntry(12292, 80, 10, "DVHSequence", "1"));
		_fldif.add(new DDictEntry(12292, 82, 16, "DVHDoseScaling", "1"));
		_fldif.add(new DDictEntry(12292, 84, 9, "DVHVolumeUnits", "1"));
		_fldif.add(new DDictEntry(12292, 86, 15, "DVHNumberOfBins", "1"));
		_fldif.add(new DDictEntry(12292, 88, 16, "DVHData", "2-2n"));
		_fldif
				.add(new DDictEntry(12292, 96, 10, "DVHReferencedROISequence",
						"1"));
		_fldif.add(new DDictEntry(12292, 98, 9, "DVHROIContributionType", "1"));
		_fldif.add(new DDictEntry(12292, 112, 16, "DVHMinimumDose", "1"));
		_fldif.add(new DDictEntry(12292, 114, 16, "DVHMaximumDose", "1"));
		_fldif.add(new DDictEntry(12292, 116, 16, "DVHMeanDose", "1"));
		_fldif.add(new DDictEntry(12294, 2, 7, "StructureSetLabel", "1"));
		_fldif.add(new DDictEntry(12294, 4, 6, "StructureSetName", "1"));
		_fldif.add(new DDictEntry(12294, 6, 13, "StructureSetDescription", "1"));
		_fldif.add(new DDictEntry(12294, 8, 11, "StructureSetDate", "1"));
		_fldif.add(new DDictEntry(12294, 9, 12, "StructureSetTime", "1"));
		_fldif.add(new DDictEntry(12294, 16, 10,
				"ReferencedFrameOfReferenceSequence", "1"));
		_fldif
				.add(new DDictEntry(12294, 18, 10, "RTReferencedStudySequence",
						"1"));
		_fldif.add(new DDictEntry(12294, 20, 10, "RTReferencedSeriesSequence",
				"1"));
		_fldif.add(new DDictEntry(12294, 22, 10, "ContourImageSequence", "1"));
		_fldif.add(new DDictEntry(12294, 32, 10, "StructureSetROISequence", "1"));
		_fldif.add(new DDictEntry(12294, 34, 15, "ROINumber", "1"));
		_fldif.add(new DDictEntry(12294, 36, 2, "ReferencedFrameOfReferenceUID",
				"1"));
		_fldif.add(new DDictEntry(12294, 38, 6, "ROIName", "1"));
		_fldif.add(new DDictEntry(12294, 40, 13, "ROIDescription", "1"));
		_fldif.add(new DDictEntry(12294, 42, 15, "ROIDisplayColor", "3"));
		_fldif.add(new DDictEntry(12294, 44, 16, "ROIVolume", "1"));
		_fldif.add(new DDictEntry(12294, 48, 10, "RTRelatedROISequence", "1"));
		_fldif.add(new DDictEntry(12294, 51, 9, "RTROIRelationship", "1"));
		_fldif.add(new DDictEntry(12294, 54, 9, "ROIGenerationAlgorithm", "1"));
		_fldif.add(new DDictEntry(12294, 56, 6, "ROIGenerationDescription", "1"));
		_fldif.add(new DDictEntry(12294, 57, 10, "ROIContourSequence", "1"));
		_fldif.add(new DDictEntry(12294, 64, 10, "ContourSequence", "1"));
		_fldif.add(new DDictEntry(12294, 66, 9, "ContourGeometricType", "1"));
		_fldif.add(new DDictEntry(12294, 68, 16, "ContourSlabThickness", "1"));
		_fldif.add(new DDictEntry(12294, 69, 16, "ContourOffsetVector", "3"));
		_fldif.add(new DDictEntry(12294, 70, 15, "NumberOfContourPoints", "1"));
		_fldif.add(new DDictEntry(12294, 80, 16, "ContourData", "3-3n"));
		_fldif.add(new DDictEntry(12294, 128, 10, "RTROIObservationsSequence",
				"1"));
		_fldif.add(new DDictEntry(12294, 130, 15, "ObservationNumber", "1"));
		_fldif.add(new DDictEntry(12294, 132, 15, "ReferencedROINumber", "1"));
		_fldif.add(new DDictEntry(12294, 133, 7, "ROIObservationLabel", "1"));
		_fldif.add(new DDictEntry(12294, 134, 10,
				"RTROIIdentificationCodeSequence", "1"));
		_fldif.add(new DDictEntry(12294, 136, 13, "ROIObservationDescription",
				"1"));
		_fldif.add(new DDictEntry(12294, 160, 10,
				"RelatedRTROIObservationsSequence", "1"));
		_fldif.add(new DDictEntry(12294, 164, 9, "RTROIInterpretedType", "1"));
		_fldif.add(new DDictEntry(12294, 166, 14, "ROIInterpreter", "1"));
		_fldif.add(new DDictEntry(12294, 176, 10,
				"ROIPhysicalPropertiesSequence", "1"));
		_fldif.add(new DDictEntry(12294, 178, 9, "ROIPhysicalProperty", "1"));
		_fldif
				.add(new DDictEntry(12294, 180, 16, "ROIPhysicalPropertyValue",
						"1"));
		_fldif.add(new DDictEntry(12294, 192, 10,
				"FrameOfReferenceRelationshipSequence", "1"));
		_fldif.add(new DDictEntry(12294, 194, 2, "RelatedFrameOfReferenceUID",
				"1"));
		_fldif.add(new DDictEntry(12294, 196, 9,
				"FrameOfReferenceTransformationType", "1"));
		_fldif.add(new DDictEntry(12294, 198, 16,
				"FrameOfReferenceTransformationMatrix", "16"));
		_fldif.add(new DDictEntry(12294, 200, 6,
				"FrameOfReferenceTransformationComment", "1"));
		_fldif.add(new DDictEntry(12298, 2, 7, "RTPlanLabel", "1"));
		_fldif.add(new DDictEntry(12298, 3, 6, "RTPlanName", "1"));
		_fldif.add(new DDictEntry(12298, 4, 13, "RTPlanDescription", "1"));
		_fldif.add(new DDictEntry(12298, 6, 11, "RTPlanDate", "1"));
		_fldif.add(new DDictEntry(12298, 7, 12, "RTPlanTime", "1"));
		_fldif.add(new DDictEntry(12298, 9, 6, "TreatmentProtocols", "1-n"));
		_fldif.add(new DDictEntry(12298, 10, 9, "TreatmentIntent", "1"));
		_fldif.add(new DDictEntry(12298, 11, 6, "TreatmentSites", "1-n"));
		_fldif.add(new DDictEntry(12298, 12, 9, "RTPlanGeometry", "1"));
		_fldif.add(new DDictEntry(12298, 14, 13, "PrescriptionDescription", "1"));
		_fldif.add(new DDictEntry(12298, 16, 10, "DoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12298, 18, 15, "DoseReferenceNumber", "1"));
		_fldif
				.add(new DDictEntry(12298, 20, 9, "DoseReferenceStructureType",
						"1"));
		_fldif.add(new DDictEntry(12298, 22, 6, "DoseReferenceDescription", "1"));
		_fldif.add(new DDictEntry(12298, 24, 16, "DoseReferencePointCoordinates",
				"3"));
		_fldif.add(new DDictEntry(12298, 26, 16, "NominalPriorDose", "1"));
		_fldif.add(new DDictEntry(12298, 32, 9, "DoseReferenceType", "1"));
		_fldif.add(new DDictEntry(12298, 33, 16, "ConstraintWeight", "1"));
		_fldif.add(new DDictEntry(12298, 34, 16, "DeliveryWarningDose", "1"));
		_fldif.add(new DDictEntry(12298, 35, 16, "DeliveryMaximumDose", "1"));
		_fldif.add(new DDictEntry(12298, 37, 16, "TargetMinimumDose", "1"));
		_fldif.add(new DDictEntry(12298, 38, 16, "TargetPrescriptionDose", "1"));
		_fldif.add(new DDictEntry(12298, 39, 16, "TargetMaximumDose", "1"));
		_fldif.add(new DDictEntry(12298, 40, 16, "TargetUnderdoseVolumeFraction",
				"1"));
		_fldif
				.add(new DDictEntry(12298, 42, 16, "OrganAtRiskFullVolumeDose",
						"1"));
		_fldif.add(new DDictEntry(12298, 43, 16, "OrganAtRiskLimitDose", "1"));
		_fldif.add(new DDictEntry(12298, 44, 16, "OrganAtRiskMaximumDose", "1"));
		_fldif.add(new DDictEntry(12298, 45, 16,
				"OrganAtRiskOverdoseVolumeFraction", "1"));
		_fldif.add(new DDictEntry(12298, 64, 10, "ToleranceTableSequence", "1"));
		_fldif.add(new DDictEntry(12298, 66, 15, "ToleranceTableNumber", "1"));
		_fldif.add(new DDictEntry(12298, 67, 7, "ToleranceTableLabel", "1"));
		_fldif.add(new DDictEntry(12298, 68, 16, "GantryAngleTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 70, 16,
				"BeamLimitingDeviceAngleTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 72, 10,
				"BeamLimitingDeviceToleranceSequence", "1"));
		_fldif.add(new DDictEntry(12298, 74, 16,
				"BeamLimitingDevicePositionTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 76, 16, "PatientSupportAngleTolerance",
				"1"));
		_fldif.add(new DDictEntry(12298, 78, 16,
				"TableTopEccentricAngleTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 81, 16,
				"TableTopVerticalPositionTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 82, 16,
				"TableTopLongitudinalPositionTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 83, 16,
				"TableTopLateralPositionTolerance", "1"));
		_fldif.add(new DDictEntry(12298, 85, 9, "RTPlanRelationship", "1"));
		_fldif.add(new DDictEntry(12298, 112, 10, "FractionGroupSequence", "1"));
		_fldif.add(new DDictEntry(12298, 113, 15, "FractionGroupNumber", "1"));
		_fldif
				.add(new DDictEntry(12298, 120, 15, "NumberOfFractionsPlanned",
						"1"));
		_fldif
				.add(new DDictEntry(12298, 121, 15, "NumberOfFractionsPerDay",
						"1"));
		_fldif.add(new DDictEntry(12298, 122, 15, "RepeatFractionCycleLength",
				"1"));
		_fldif.add(new DDictEntry(12298, 123, 18, "FractionPattern", "1"));
		_fldif.add(new DDictEntry(12298, 128, 15, "NumberOfBeams", "1"));
		_fldif.add(new DDictEntry(12298, 130, 16, "BeamDoseSpecificationPoint",
				"3"));
		_fldif.add(new DDictEntry(12298, 132, 16, "BeamDose", "1"));
		_fldif.add(new DDictEntry(12298, 134, 16, "BeamMeterset", "1"));
		_fldif.add(new DDictEntry(12298, 160, 15,
				"NumberOfBrachyApplicationSetups", "1"));
		_fldif.add(new DDictEntry(12298, 162, 16,
				"BrachyApplicationSetupDoseSpecificationPoint", "3"));
		_fldif.add(new DDictEntry(12298, 164, 16, "BrachyApplicationSetupDose",
				"1"));
		_fldif.add(new DDictEntry(12298, 176, 10, "BeamSequence", "1"));
		_fldif.add(new DDictEntry(12298, 178, 7, "TreatmentMachineName", "1"));
		_fldif.add(new DDictEntry(12298, 179, 9, "PrimaryDosimeterUnit", "1"));
		_fldif.add(new DDictEntry(12298, 180, 16, "Source-AxisDistance", "1"));
		_fldif.add(new DDictEntry(12298, 182, 10, "BeamLimitingDeviceSequence",
				"1"));
		_fldif
				.add(new DDictEntry(12298, 184, 9, "RTBeamLimitingDeviceType",
						"1"));
		_fldif.add(new DDictEntry(12298, 186, 16,
				"SourceToBeamLimitingDeviceDistance", "1"));
		_fldif.add(new DDictEntry(12298, 188, 15, "NumberOfLeafJawPairs", "1"));
		_fldif
				.add(new DDictEntry(12298, 190, 16, "LeafPositionBoundaries",
						"3-n"));
		_fldif.add(new DDictEntry(12298, 192, 15, "BeamNumber", "1"));
		_fldif.add(new DDictEntry(12298, 194, 6, "BeamName", "1"));
		_fldif.add(new DDictEntry(12298, 195, 13, "BeamDescription", "1"));
		_fldif.add(new DDictEntry(12298, 196, 9, "BeamType", "1"));
		_fldif.add(new DDictEntry(12298, 198, 9, "RadiationType", "1"));
		_fldif.add(new DDictEntry(12298, 200, 15, "ReferenceImageNumber", "1"));
		_fldif.add(new DDictEntry(12298, 202, 10,
				"PlannedVerificationImageSequence", "1"));
		_fldif.add(new DDictEntry(12298, 204, 6,
				"ImagingDeviceSpecificAcquisitionParameters", "1-n"));
		_fldif.add(new DDictEntry(12298, 206, 9, "TreatmentDeliveryType", "1"));
		_fldif.add(new DDictEntry(12298, 208, 15, "NumberOfWedges", "1"));
		_fldif.add(new DDictEntry(12298, 209, 10, "WedgeSequence", "1"));
		_fldif.add(new DDictEntry(12298, 210, 15, "WedgeNumber", "1"));
		_fldif.add(new DDictEntry(12298, 211, 9, "WedgeType", "1"));
		_fldif.add(new DDictEntry(12298, 212, 7, "WedgeID", "1"));
		_fldif.add(new DDictEntry(12298, 213, 15, "WedgeAngle", "1"));
		_fldif.add(new DDictEntry(12298, 214, 16, "WedgeFactor", "1"));
		_fldif.add(new DDictEntry(12298, 216, 16, "WedgeOrientation", "1"));
		_fldif.add(new DDictEntry(12298, 218, 16, "SourceToWedgeTrayDistance",
				"1"));
		_fldif.add(new DDictEntry(12298, 224, 15, "NumberOfCompensators", "1"));
		_fldif.add(new DDictEntry(12298, 225, 7, "MaterialID", "1"));
		_fldif.add(new DDictEntry(12298, 226, 16, "TotalCompensatorTrayFactor",
				"1"));
		_fldif.add(new DDictEntry(12298, 227, 10, "CompensatorSequence", "1"));
		_fldif.add(new DDictEntry(12298, 228, 15, "CompensatorNumber", "1"));
		_fldif.add(new DDictEntry(12298, 229, 7, "CompensatorID", "1"));
		_fldif.add(new DDictEntry(12298, 230, 16,
				"SourceToCompensatorTrayDistance", "1"));
		_fldif.add(new DDictEntry(12298, 231, 15, "CompensatorRows", "1"));
		_fldif.add(new DDictEntry(12298, 232, 15, "CompensatorColumns", "1"));
		_fldif
				.add(new DDictEntry(12298, 233, 16, "CompensatorPixelSpacing",
						"2"));
		_fldif.add(new DDictEntry(12298, 234, 16, "CompensatorPosition", "2"));
		_fldif.add(new DDictEntry(12298, 235, 16, "CompensatorTransmissionData",
				"1-n"));
		_fldif.add(new DDictEntry(12298, 236, 16, "CompensatorThicknessData",
				"1-n"));
		_fldif.add(new DDictEntry(12298, 237, 15, "NumberOfBoli", "1"));
		_fldif.add(new DDictEntry(12298, 240, 15, "NumberOfBlocks", "1"));
		_fldif.add(new DDictEntry(12298, 242, 16, "TotalBlockTrayFactor", "1"));
		_fldif.add(new DDictEntry(12298, 244, 10, "BlockSequence", "1"));
		_fldif.add(new DDictEntry(12298, 245, 7, "BlockTrayID", "1"));
		_fldif.add(new DDictEntry(12298, 246, 16, "SourceToBlockTrayDistance",
				"1"));
		_fldif.add(new DDictEntry(12298, 248, 9, "BlockType", "1"));
		_fldif.add(new DDictEntry(12298, 250, 9, "BlockDivergence", "1"));
		_fldif.add(new DDictEntry(12298, 252, 15, "BlockNumber", "1"));
		_fldif.add(new DDictEntry(12298, 254, 6, "BlockName", "1"));
		_fldif.add(new DDictEntry(12298, 256, 16, "BlockThickness", "1"));
		_fldif.add(new DDictEntry(12298, 258, 16, "BlockTransmission", "1"));
		_fldif.add(new DDictEntry(12298, 260, 15, "BlockNumberOfPoints", "1"));
		_fldif.add(new DDictEntry(12298, 262, 16, "BlockData", "2-2n"));
		_fldif.add(new DDictEntry(12298, 263, 10, "ApplicatorSequence", "1"));
		_fldif.add(new DDictEntry(12298, 264, 7, "ApplicatorID", "1"));
		_fldif.add(new DDictEntry(12298, 265, 9, "ApplicatorType", "1"));
		_fldif.add(new DDictEntry(12298, 266, 6, "ApplicatorDescription", "1"));
		_fldif.add(new DDictEntry(12298, 268, 16,
				"CumulativeDoseReferenceCoefficient", "1"));
		_fldif.add(new DDictEntry(12298, 270, 16,
				"FinalCumulativeMetersetWeight", "1"));
		_fldif.add(new DDictEntry(12298, 272, 15, "NumberOfControlPoints", "1"));
		_fldif.add(new DDictEntry(12298, 273, 10, "ControlPointSequence", "1"));
		_fldif.add(new DDictEntry(12298, 274, 15, "ControlPointIndex", "1"));
		_fldif.add(new DDictEntry(12298, 276, 16, "NominalBeamEnergy", "1"));
		_fldif.add(new DDictEntry(12298, 277, 16, "DoseRateSet", "1"));
		_fldif.add(new DDictEntry(12298, 278, 10, "WedgePositionSequence", "1"));
		_fldif.add(new DDictEntry(12298, 280, 9, "WedgePosition", "1"));
		_fldif.add(new DDictEntry(12298, 282, 10,
				"BeamLimitingDevicePositionSequence", "1"));
		_fldif.add(new DDictEntry(12298, 284, 16, "LeafJawPositions", "2-2n"));
		_fldif.add(new DDictEntry(12298, 286, 16, "GantryAngle", "1"));
		_fldif.add(new DDictEntry(12298, 287, 9, "GantryRotationDirection", "1"));
		_fldif
				.add(new DDictEntry(12298, 288, 16, "BeamLimitingDeviceAngle",
						"1"));
		_fldif.add(new DDictEntry(12298, 289, 9,
				"BeamLimitingDeviceRotationDirection", "1"));
		_fldif.add(new DDictEntry(12298, 290, 16, "PatientSupportAngle", "1"));
		_fldif.add(new DDictEntry(12298, 291, 9,
				"PatientSupportRotationDirection", "1"));
		_fldif.add(new DDictEntry(12298, 292, 16,
				"TableTopEccentricAxisDistance", "1"));
		_fldif.add(new DDictEntry(12298, 293, 16, "TableTopEccentricAngle", "1"));
		_fldif.add(new DDictEntry(12298, 294, 9,
				"TableTopEccentricRotationDirection", "1"));
		_fldif
				.add(new DDictEntry(12298, 296, 16, "TableTopVerticalPosition",
						"1"));
		_fldif.add(new DDictEntry(12298, 297, 16, "TableTopLongitudinalPosition",
				"1"));
		_fldif
				.add(new DDictEntry(12298, 298, 16, "TableTopLateralPosition",
						"1"));
		_fldif.add(new DDictEntry(12298, 300, 16, "IsocenterPosition", "3"));
		_fldif.add(new DDictEntry(12298, 302, 16, "SurfaceEntryPoint", "3"));
		_fldif
				.add(new DDictEntry(12298, 304, 16, "SourceToSurfaceDistance",
						"1"));
		_fldif
				.add(new DDictEntry(12298, 308, 16, "CumulativeMetersetWeight",
						"1"));
		_fldif.add(new DDictEntry(12298, 384, 10, "PatientSetupSequence", "1"));
		_fldif.add(new DDictEntry(12298, 386, 15, "PatientSetupNumber", "1"));
		_fldif
				.add(new DDictEntry(12298, 388, 6, "PatientAdditionalPosition",
						"1"));
		_fldif.add(new DDictEntry(12298, 400, 10, "FixationDeviceSequence", "1"));
		_fldif.add(new DDictEntry(12298, 402, 9, "FixationDeviceType", "1"));
		_fldif.add(new DDictEntry(12298, 404, 7, "FixationDeviceLabel", "1"));
		_fldif.add(new DDictEntry(12298, 406, 13, "FixationDeviceDescription",
				"1"));
		_fldif.add(new DDictEntry(12298, 408, 7, "FixationDevicePosition", "1"));
		_fldif
				.add(new DDictEntry(12298, 416, 10, "ShieldingDeviceSequence",
						"1"));
		_fldif.add(new DDictEntry(12298, 418, 9, "ShieldingDeviceType", "1"));
		_fldif.add(new DDictEntry(12298, 420, 7, "ShieldingDeviceLabel", "1"));
		_fldif.add(new DDictEntry(12298, 422, 13, "ShieldingDeviceDescription",
				"1"));
		_fldif.add(new DDictEntry(12298, 424, 7, "ShieldingDevicePosition", "1"));
		_fldif.add(new DDictEntry(12298, 432, 9, "SetupTechnique", "1"));
		_fldif.add(new DDictEntry(12298, 434, 13, "SetupTechniqueDescription",
				"1"));
		_fldif.add(new DDictEntry(12298, 436, 10, "SetupDeviceSequence", "1"));
		_fldif.add(new DDictEntry(12298, 438, 9, "SetupDeviceType", "1"));
		_fldif.add(new DDictEntry(12298, 440, 7, "SetupDeviceLabel", "1"));
		_fldif.add(new DDictEntry(12298, 442, 13, "SetupDeviceDescription", "1"));
		_fldif.add(new DDictEntry(12298, 444, 16, "SetupDeviceParameter", "1"));
		_fldif.add(new DDictEntry(12298, 464, 13, "SetupReferenceDescription",
				"1"));
		_fldif.add(new DDictEntry(12298, 466, 16,
				"TableTopVerticalSetupDisplacement", "1"));
		_fldif.add(new DDictEntry(12298, 468, 16,
				"TableTopLongitudinalSetupDisplacement", "1"));
		_fldif.add(new DDictEntry(12298, 470, 16,
				"TableTopLateralSetupDisplacement", "1"));
		_fldif
				.add(new DDictEntry(12298, 512, 9, "BrachyTreatmentTechnique",
						"1"));
		_fldif.add(new DDictEntry(12298, 514, 9, "BrachyTreatmentType", "1"));
		_fldif
				.add(new DDictEntry(12298, 518, 10, "TreatmentMachineSequence",
						"1"));
		_fldif.add(new DDictEntry(12298, 528, 10, "SourceSequence", "1"));
		_fldif.add(new DDictEntry(12298, 530, 15, "SourceNumber", "1"));
		_fldif.add(new DDictEntry(12298, 532, 9, "SourceType", "1"));
		_fldif.add(new DDictEntry(12298, 534, 6, "SourceManufacturer", "1"));
		_fldif.add(new DDictEntry(12298, 536, 16, "ActiveSourceDiameter", "1"));
		_fldif.add(new DDictEntry(12298, 538, 16, "ActiveSourceLength", "1"));
		_fldif.add(new DDictEntry(12298, 546, 16,
				"SourceEncapsulationNominalThickness", "1"));
		_fldif.add(new DDictEntry(12298, 548, 16,
				"SourceEncapsulationNominalTransmission", "1"));
		_fldif.add(new DDictEntry(12298, 550, 6, "SourceIsotopeName", "1"));
		_fldif.add(new DDictEntry(12298, 552, 16, "SourceIsotopeHalfLife", "1"));
		_fldif.add(new DDictEntry(12298, 554, 16, "ReferenceAirKermaRate", "1"));
		_fldif.add(new DDictEntry(12298, 556, 11, "AirKermaRateReferenceDate",
				"1"));
		_fldif.add(new DDictEntry(12298, 558, 12, "AirKermaRateReferenceTime",
				"1"));
		_fldif
				.add(new DDictEntry(12298, 560, 10, "ApplicationSetupSequence",
						"1"));
		_fldif.add(new DDictEntry(12298, 562, 9, "ApplicationSetupType", "1"));
		_fldif.add(new DDictEntry(12298, 564, 15, "ApplicationSetupNumber", "1"));
		_fldif.add(new DDictEntry(12298, 566, 6, "ApplicationSetupName", "1"));
		_fldif.add(new DDictEntry(12298, 568, 6, "ApplicationSetupManufacturer",
				"1"));
		_fldif.add(new DDictEntry(12298, 576, 15, "TemplateNumber", "1"));
		_fldif.add(new DDictEntry(12298, 578, 7, "TemplateType", "1"));
		_fldif.add(new DDictEntry(12298, 580, 6, "TemplateName", "1"));
		_fldif.add(new DDictEntry(12298, 592, 16, "TotalReferenceAirKerma", "1"));
		_fldif.add(new DDictEntry(12298, 608, 10,
				"BrachyAccessoryDeviceSequence", "1"));
		_fldif.add(new DDictEntry(12298, 610, 15, "BrachyAccessoryDeviceNumber",
				"1"));
		_fldif.add(new DDictEntry(12298, 611, 7, "BrachyAccessoryDeviceID", "1"));
		_fldif
				.add(new DDictEntry(12298, 612, 9, "BrachyAccessoryDeviceType",
						"1"));
		_fldif
				.add(new DDictEntry(12298, 614, 6, "BrachyAccessoryDeviceName",
						"1"));
		_fldif.add(new DDictEntry(12298, 618, 16,
				"BrachyAccessoryDeviceNominalThickness", "1"));
		_fldif.add(new DDictEntry(12298, 620, 16,
				"BrachyAccessoryDeviceNominalTransmission", "1"));
		_fldif.add(new DDictEntry(12298, 640, 10, "ChannelSequence", "1"));
		_fldif.add(new DDictEntry(12298, 642, 15, "ChannelNumber", "1"));
		_fldif.add(new DDictEntry(12298, 644, 16, "ChannelLength", "1"));
		_fldif.add(new DDictEntry(12298, 646, 16, "ChannelTotalTime", "1"));
		_fldif.add(new DDictEntry(12298, 648, 9, "SourceMovementType", "1"));
		_fldif.add(new DDictEntry(12298, 650, 15, "NumberOfPulses", "1"));
		_fldif
				.add(new DDictEntry(12298, 652, 16, "PulseRepetitionInterval",
						"1"));
		_fldif.add(new DDictEntry(12298, 656, 15, "SourceApplicatorNumber", "1"));
		_fldif.add(new DDictEntry(12298, 657, 7, "SourceApplicatorID", "1"));
		_fldif.add(new DDictEntry(12298, 658, 9, "SourceApplicatorType", "1"));
		_fldif.add(new DDictEntry(12298, 660, 6, "SourceApplicatorName", "1"));
		_fldif.add(new DDictEntry(12298, 662, 16, "SourceApplicatorLength", "1"));
		_fldif.add(new DDictEntry(12298, 664, 6, "SourceApplicatorManufacturer",
				"1"));
		_fldif.add(new DDictEntry(12298, 668, 16,
				"SourceApplicatorWallNominalThickness", "1"));
		_fldif.add(new DDictEntry(12298, 670, 16,
				"SourceApplicatorWallNominalTransmission", "1"));
		_fldif
				.add(new DDictEntry(12298, 672, 16, "SourceApplicatorStepSize",
						"1"));
		_fldif.add(new DDictEntry(12298, 674, 15, "TransferTubeNumber", "1"));
		_fldif.add(new DDictEntry(12298, 676, 16, "TransferTubeLength", "1"));
		_fldif.add(new DDictEntry(12298, 688, 10, "ChannelShieldSequence", "1"));
		_fldif.add(new DDictEntry(12298, 690, 15, "ChannelShieldNumber", "1"));
		_fldif.add(new DDictEntry(12298, 691, 7, "ChannelShieldID", "1"));
		_fldif.add(new DDictEntry(12298, 692, 6, "ChannelShieldName", "1"));
		_fldif.add(new DDictEntry(12298, 696, 16,
				"ChannelShieldNominalThickness", "1"));
		_fldif.add(new DDictEntry(12298, 698, 16,
				"ChannelShieldNominalTransmission", "1"));
		_fldif.add(new DDictEntry(12298, 712, 16, "FinalCumulativeTimeWeight",
				"1"));
		_fldif.add(new DDictEntry(12298, 720, 10, "BrachyControlPointSequence",
				"1"));
		_fldif.add(new DDictEntry(12298, 722, 16, "ControlPointRelativePosition",
				"1"));
		_fldif.add(new DDictEntry(12298, 724, 16, "ControlPoint3DPosition", "3"));
		_fldif.add(new DDictEntry(12298, 726, 16, "CumulativeTimeWeight", "1"));
		_fldif.add(new DDictEntry(12300, 2, 10, "ReferencedRTPlanSequence", "1"));
		_fldif.add(new DDictEntry(12300, 4, 10, "ReferencedBeamSequence", "1"));
		_fldif.add(new DDictEntry(12300, 6, 15, "ReferencedBeamNumber", "1"));
		_fldif.add(new DDictEntry(12300, 7, 15, "ReferencedReferenceImageNumber",
				"1"));
		_fldif.add(new DDictEntry(12300, 8, 16, "StartCumulativeMetersetWeight",
				"1"));
		_fldif.add(new DDictEntry(12300, 9, 16, "EndCumulativeMetersetWeight",
				"1"));
		_fldif.add(new DDictEntry(12300, 10, 10,
				"ReferencedBrachyApplicationSetupSequence", "1"));
		_fldif.add(new DDictEntry(12300, 12, 15,
				"ReferencedBrachyApplicationSetupNumber", "1"));
		_fldif.add(new DDictEntry(12300, 14, 15, "ReferencedSourceNumber", "1"));
		_fldif.add(new DDictEntry(12300, 32, 10,
				"ReferencedFractionGroupSequence", "1"));
		_fldif.add(new DDictEntry(12300, 34, 15, "ReferencedFractionGroupNumber",
				"1"));
		_fldif.add(new DDictEntry(12300, 64, 10,
				"ReferencedVerificationImageSequence", "1"));
		_fldif.add(new DDictEntry(12300, 66, 10,
				"ReferencedReferenceImageSequence", "1"));
		_fldif.add(new DDictEntry(12300, 80, 10,
				"ReferencedDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12300, 81, 15, "ReferencedDoseReferenceNumber",
				"1"));
		_fldif.add(new DDictEntry(12300, 85, 10,
				"BrachyReferencedDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12300, 96, 10,
				"ReferencedStructureSetSequence", "1"));
		_fldif.add(new DDictEntry(12300, 106, 15, "ReferencedPatientSetupNumber",
				"1"));
		_fldif.add(new DDictEntry(12300, 128, 10, "ReferencedDoseSequence", "1"));
		_fldif.add(new DDictEntry(12300, 160, 15,
				"ReferencedToleranceTableNumber", "1"));
		_fldif
				.add(new DDictEntry(12300, 176, 10, "ReferencedBolusSequence",
						"1"));
		_fldif.add(new DDictEntry(12300, 192, 15, "ReferencedWedgeNumber", "1"));
		_fldif.add(new DDictEntry(12300, 208, 15, "ReferencedCompensatorNumber",
				"1"));
		_fldif.add(new DDictEntry(12300, 224, 15, "ReferencedBlockNumber", "1"));
		_fldif.add(new DDictEntry(12300, 240, 15, "ReferencedControlPointIndex",
				"1"));
		_fldif.add(new DDictEntry(12302, 2, 9, "ApprovalStatus", "1"));
		_fldif.add(new DDictEntry(12302, 4, 11, "ReviewDate", "1"));
		_fldif.add(new DDictEntry(12302, 5, 12, "ReviewTime", "1"));
		_fldif.add(new DDictEntry(12302, 8, 14, "ReviewerName", "1"));
		_fldif.add(new DDictEntry(16392, 0, 1, "ResultsGroupLength", "1"));
		_fldif.add(new DDictEntry(16392, 64, 7, "ResultsID", "1"));
		_fldif.add(new DDictEntry(16392, 66, 6, "ResultsIDIssuer", "1"));
		_fldif.add(new DDictEntry(16392, 80, 10,
				"ReferencedInterpretationSequence", "1"));
		_fldif.add(new DDictEntry(16392, 256, 11, "InterpretationRecordedDate",
				"1"));
		_fldif.add(new DDictEntry(16392, 257, 12, "InterpretationRecordedTime",
				"1"));
		_fldif.add(new DDictEntry(16392, 258, 14, "InterpretationRecorder", "1"));
		_fldif
				.add(new DDictEntry(16392, 259, 6, "ReferenceToRecordedSound",
						"1"));
		_fldif.add(new DDictEntry(16392, 264, 11,
				"InterpretationTranscriptionDate", "1"));
		_fldif.add(new DDictEntry(16392, 265, 12,
				"InterpretationTranscriptionTime", "1"));
		_fldif.add(new DDictEntry(16392, 266, 14, "InterpretationTranscriber",
				"1"));
		_fldif.add(new DDictEntry(16392, 267, 13, "InterpretationText", "1"));
		_fldif.add(new DDictEntry(16392, 268, 14, "InterpretationAuthor", "1"));
		_fldif.add(new DDictEntry(16392, 273, 10,
				"InterpretationApproverSequence", "1"));
		_fldif.add(new DDictEntry(16392, 274, 11, "InterpretationApprovalDate",
				"1"));
		_fldif.add(new DDictEntry(16392, 275, 12, "InterpretationApprovalTime",
				"1"));
		_fldif.add(new DDictEntry(16392, 276, 14,
				"PhysicianApprovingInterpretation", "1"));
		_fldif.add(new DDictEntry(16392, 277, 18,
				"InterpretationDiagnosisDescription", "1"));
		_fldif.add(new DDictEntry(16392, 279, 10, "DiagnosisCodeSequence", "1"));
		_fldif.add(new DDictEntry(16392, 280, 10,
				"ResultsDistributionListSequence", "1"));
		_fldif.add(new DDictEntry(16392, 281, 14, "DistributionName", "1"));
		_fldif.add(new DDictEntry(16392, 282, 6, "DistributionAddress", "1"));
		_fldif.add(new DDictEntry(16392, 512, 7, "InterpretationID", "1"));
		_fldif.add(new DDictEntry(16392, 514, 6, "InterpretationIDIssuer", "1"));
		_fldif.add(new DDictEntry(16392, 528, 9, "InterpretationTypeID", "1"));
		_fldif.add(new DDictEntry(16392, 530, 9, "InterpretationStatusID", "1"));
		_fldif.add(new DDictEntry(16392, 768, 13, "Impressions", "1"));
		_fldif.add(new DDictEntry(16392, 16384, 13, "ResultsComments", "1"));
		_fldif.add(new DDictEntry(20480, 0, 1, "CurveGroupLength", "1"));
		_fldif.add(new DDictEntry(20480, 5, 3, "CurveDimensions", "1"));
		_fldif.add(new DDictEntry(20480, 16, 3, "NumberOfPoints", "1"));
		_fldif.add(new DDictEntry(20480, 32, 9, "TypeOfData", "1"));
		_fldif.add(new DDictEntry(20480, 34, 6, "CurveDescription", "1"));
		_fldif.add(new DDictEntry(20480, 48, 7, "AxisUnits", "1-n"));
		_fldif.add(new DDictEntry(20480, 64, 7, "AxisLabels", "1-n"));
		_fldif.add(new DDictEntry(20480, 259, 3, "DataValueRepresentation", "1"));
		_fldif
				.add(new DDictEntry(20480, 260, 3, "MinimumCoordinateValue",
						"1-n"));
		_fldif
				.add(new DDictEntry(20480, 261, 3, "MaximumCoordinateValue",
						"1-n"));
		_fldif.add(new DDictEntry(20480, 262, 7, "CurveRange", "1-n"));
		_fldif.add(new DDictEntry(20480, 272, 3, "CurveDataDescriptor", "1"));
		_fldif.add(new DDictEntry(20480, 274, 3, "CoordinateStartValue", "1"));
		_fldif.add(new DDictEntry(20480, 276, 3, "CoordinateStepValue", "1"));
		_fldif.add(new DDictEntry(20480, 8192, 3, "AudioType", "1"));
		_fldif.add(new DDictEntry(20480, 8194, 3, "AudioSampleFormat", "1"));
		_fldif.add(new DDictEntry(20480, 8196, 3, "NumberOfChannels", "1"));
		_fldif.add(new DDictEntry(20480, 8198, 1, "NumberOfSamples", "1"));
		_fldif.add(new DDictEntry(20480, 8200, 1, "SampleRate", "1"));
		_fldif.add(new DDictEntry(20480, 8202, 1, "TotalTime", "1"));
		_fldif.add(new DDictEntry(20480, 8204, 22, "AudioSampleData", "1"));
		_fldif.add(new DDictEntry(20480, 8206, 18, "AudioComments", "1"));
		_fldif.add(new DDictEntry(20480, 9472, 6, "CurveLabel", "1"));
		_fldif.add(new DDictEntry(20480, 9728, 10,
				"CurveReferencedOverlaySequence", "1"));
		_fldif.add(new DDictEntry(20480, 9744, 3, "CurveReferencedOverlayGroup",
				"1"));
		_fldif.add(new DDictEntry(20480, 12288, 22, "CurveData", "1"));
		_fldif.add(new DDictEntry(24576, 0, 1, "OverlayGroupLength", "1"));
		_fldif.add(new DDictEntry(24576, 16, 3, "OverlayRows", "1"));
		_fldif.add(new DDictEntry(24576, 17, 3, "OverlayColumns", "1"));
		_fldif.add(new DDictEntry(24576, 18, 3, "OverlayPlanes", "1"));
		_fldif.add(new DDictEntry(24576, 21, 15, "OverlayNumberOfFrames", "1"));
		_fldif.add(new DDictEntry(24576, 34, 6, "OverlayDescription", "1"));
		_fldif.add(new DDictEntry(24576, 64, 9, "OverlayType", "1"));
		_fldif.add(new DDictEntry(24576, 69, 6, "OverlaySubtype", "1"));
		_fldif.add(new DDictEntry(24576, 80, 23, "OverlayOrigin", "2"));
		_fldif.add(new DDictEntry(24576, 81, 3, "ImageFrameOrigin", "1"));
		_fldif.add(new DDictEntry(24576, 82, 3, "OverlayPlaneOrigin", "1"));
		_fldif.add(new DDictEntry(24576, 256, 3, "OverlayBitsAllocated", "1"));
		_fldif.add(new DDictEntry(24576, 258, 3, "OverlayBitPosition", "1"));
		_fldif.add(new DDictEntry(24576, 4352, 3, "OverlayDescriptorGray", "1"));
		_fldif.add(new DDictEntry(24576, 4353, 3, "OverlayDescriptorRed", "1"));
		_fldif.add(new DDictEntry(24576, 4354, 3, "OverlayDescriptorGreen", "1"));
		_fldif.add(new DDictEntry(24576, 4355, 3, "OverlayDescriptorBlue", "1"));
		_fldif.add(new DDictEntry(24576, 4608, 3, "OverlayGray", "1-n"));
		_fldif.add(new DDictEntry(24576, 4609, 3, "OverlayRed", "1-n"));
		_fldif.add(new DDictEntry(24576, 4610, 3, "OverlayGreen", "1-n"));
		_fldif.add(new DDictEntry(24576, 4611, 3, "OverlayBlue", "1-n"));
		_fldif.add(new DDictEntry(24576, 4865, 15, "ROIArea", "1"));
		_fldif.add(new DDictEntry(24576, 4866, 16, "ROIMean", "1"));
		_fldif.add(new DDictEntry(24576, 4867, 16, "ROIStandardDeviation", "1"));
		_fldif.add(new DDictEntry(24576, 5376, 6, "OverlayLabel", "1"));
		_fldif.add(new DDictEntry(24576, 12288, 24, "OverlayData", "1"));
		_fldif.add(new DDictEntry(32736, 0, 1, "PixelDataGroupLength", "1"));
		_fldif.add(new DDictEntry(32736, 16, 22, "PixelData", "1"));
		_fldif
				.add(new DDictEntry(65532, 65532, 8, "DataSetTrailingPadding",
						"1"));
		_fldif.add(new DDictEntry(65534, 57344, 25, "Item", "1"));
		_fldif.add(new DDictEntry(65534, 57357, 25, "ItemDelimitationItem", "1"));
		_fldif.add(new DDictEntry(65534, 57565, 25, "SequenceDelimitationItem",
				"1"));
		_fldif.add(new DDictEntry(0x7fffffff, 0x7fffffff, 25, "Undefined", "1"));
		_fldif.add(new DDictEntry(8, 8745, 10,
				"AnatomicStructureSpaceOrRegionSequence", "1"));
		_fldif.add(new DDictEntry(24, 4113, 6, "HardcopyCreationDeviceID", "1"));
		_fldif
				.add(new DDictEntry(24, 4119, 6, "HardcopyDeviceManufacturer",
						"1"));
		_fldif.add(new DDictEntry(24, 4122, 6, "HardcopyDeviceSoftwareVersion",
				"1-n"));
		_fldif.add(new DDictEntry(24, 4123, 6,
				"HardcopyDeviceManfuacturersModelName", "1"));
		_fldif.add(new DDictEntry(24, 4435, 15, "ExposureinuAs", "1"));
		_fldif.add(new DDictEntry(24, 5200, 16, "ColumnAngulation", "1"));
		_fldif.add(new DDictEntry(64, 544, 10,
				"ReferencedStandaloneSOPInstanceSequence", "1"));
		_fldif.add(new DDictEntry(64, 577, 4, "PerformedStationAETitle", "1"));
		_fldif.add(new DDictEntry(64, 578, 7, "PerformedStationName", "1"));
		_fldif.add(new DDictEntry(64, 579, 7, "PerformedLocation", "1"));
		_fldif.add(new DDictEntry(64, 580, 11, "PerformedProcedureStepStartDate",
				"1"));
		_fldif.add(new DDictEntry(64, 581, 12, "PerformedProcedureStepStartTime",
				"1"));
		_fldif.add(new DDictEntry(64, 592, 11, "PerformedProcedureStepEndDate",
				"1"));
		_fldif.add(new DDictEntry(64, 593, 12, "PerformedProcedureStepEndTime",
				"1"));
		_fldif
				.add(new DDictEntry(64, 594, 9, "PerformedProcedureStepStatus",
						"1"));
		_fldif.add(new DDictEntry(64, 595, 7, "PerformedProcedureStepID", "1"));
		_fldif.add(new DDictEntry(64, 596, 6,
				"PerformedProcedureStepDescription", "1"));
		_fldif.add(new DDictEntry(64, 597, 6,
				"PerformedProcedureTypeDescription", "1"));
		_fldif
				.add(new DDictEntry(64, 608, 10, "PerformedActionItemSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 624, 10, "ScheduledStepAttributesSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 629, 10, "RequestAttributesSequence", "1"));
		_fldif.add(new DDictEntry(64, 640, 13,
				"CommentsOnThePerformedProcedureSteps", "1"));
		_fldif.add(new DDictEntry(64, 659, 10, "QuantitySequence", "1"));
		_fldif.add(new DDictEntry(64, 660, 16, "Quantity", "1"));
		_fldif.add(new DDictEntry(64, 661, 10, "MeasuringUnitsSequence", "1"));
		_fldif.add(new DDictEntry(64, 662, 10, "BillingItemSequence", "1"));
		_fldif.add(new DDictEntry(64, 768, 3, "TotalTimeOfFluoroscopy", "1"));
		_fldif.add(new DDictEntry(64, 769, 3, "TotalNumberOfExposures", "1"));
		_fldif.add(new DDictEntry(64, 770, 3, "EntranceDose", "1"));
		_fldif.add(new DDictEntry(64, 771, 3, "ExposedArea", "1-2"));
		_fldif.add(new DDictEntry(64, 774, 16, "DistanceSourceToEntrance", "1"));
		_fldif.add(new DDictEntry(64, 784, 13, "CommentsOnRadiationDose", "1"));
		_fldif.add(new DDictEntry(64, 800, 10, "BillingProcedureStepSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 801, 10, "FilmConsumptionSequence", "1"));
		_fldif.add(new DDictEntry(64, 804, 10,
				"BillingSuppliesAndDevicesSequence", "1"));
		_fldif.add(new DDictEntry(64, 816, 10, "ReferencedProcedureStepSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 832, 10, "PerformedSeriesSequence", "1"));
		_fldif.add(new DDictEntry(80, 0, 1, "XRayAngioDeviceGroupLength", "1"));
		_fldif.add(new DDictEntry(8192, 98, 9, "ColorImagePrintingFlag", "1"));
		_fldif.add(new DDictEntry(8192, 99, 9, "CollationFlag", "1"));
		_fldif.add(new DDictEntry(8192, 101, 9, "AnnotationFlag", "1"));
		_fldif.add(new DDictEntry(8192, 103, 9, "ImageOverlayFlag", "1"));
		_fldif.add(new DDictEntry(8192, 105, 9, "PresentationLUTFlag", "1"));
		_fldif.add(new DDictEntry(8192, 106, 9, "ImageBoxPresentationLUTFlag",
				"1"));
		_fldif.add(new DDictEntry(8192, 1296, 10,
				"ReferencedStoredPrintSequence", "1"));
		_fldif.add(new DDictEntry(8208, 350, 3, "Illumination", "1"));
		_fldif.add(new DDictEntry(8208, 352, 3, "ReflectedAmbientLight", "1"));
		_fldif.add(new DDictEntry(8272, 0, 1, "PresentationLUTGroupLength", "1"));
		_fldif.add(new DDictEntry(8272, 16, 10, "PresentationLUTSequence", "1"));
		_fldif.add(new DDictEntry(8272, 32, 9, "PresentationLUTShape", "1"));
		_fldif.add(new DDictEntry(8272, 1280, 10,
				"ReferencedPresentationLUTSequence", "1"));
		_fldif.add(new DDictEntry(8480, 0, 1, "QueueGroupLength", "1"));
		_fldif.add(new DDictEntry(8496, 0, 1, "PrintContentGroupLength", "1"));
		_fldif.add(new DDictEntry(8496, 16, 10,
				"PrintManagementCapabilitiesSequence", "1"));
		_fldif.add(new DDictEntry(8496, 21, 10, "PrinterCharacteristicsSequence",
				"1"));
		_fldif.add(new DDictEntry(8496, 48, 10, "FilmBoxContentSequence", "1"));
		_fldif.add(new DDictEntry(8496, 64, 10, "ImageBoxContentSequence", "1"));
		_fldif
				.add(new DDictEntry(8496, 80, 10, "AnnotationContentSequence",
						"1"));
		_fldif.add(new DDictEntry(8496, 96, 10, "ImageOverlayBoxContentSequence",
				"1"));
		_fldif.add(new DDictEntry(8496, 128, 10,
				"PresentationLUTContentSequence", "1"));
		_fldif.add(new DDictEntry(8496, 160, 10, "ProposedStudySequence", "1"));
		_fldif.add(new DDictEntry(8496, 192, 10, "OriginalImageSequence", "1"));
		_fldif.add(new DDictEntry(12290, 0, 1, "RTImageGroupLength", "1"));
		_fldif.add(new DDictEntry(12292, 0, 1, "RTDoseGroupLength", "1"));
		_fldif.add(new DDictEntry(12294, 0, 1, "RTStructureSetGroupLength", "1"));
		_fldif.add(new DDictEntry(12298, 0, 1, "RTPlanGroupLength", "1"));
		_fldif.add(new DDictEntry(12300, 0, 1, "RTRelationshipGroupLength", "1"));
		_fldif.add(new DDictEntry(12302, 0, 1, "RTApprovalGroupLength", "1"));
		_fldif.add(new DDictEntry(8, 8464, 9, "RETIRED_LossyImageCompression",
				"1"));
		_fldif.add(new DDictEntry(8, 104, 9, "PresentationIntentType", "1"));
		_fldif.add(new DDictEntry(24, 4410, 9, "TableType", "1"));
		_fldif.add(new DDictEntry(24, 4438, 9, "RectificationType", "1"));
		_fldif.add(new DDictEntry(24, 4497, 9, "AnodeTargetMaterial", "1"));
		_fldif.add(new DDictEntry(24, 4512, 16, "BodyPartThickness", "1"));
		_fldif.add(new DDictEntry(24, 4514, 16, "CompressionForce", "1"));
		_fldif.add(new DDictEntry(24, 5264, 9, "TomoType", "1"));
		_fldif.add(new DDictEntry(24, 5265, 9, "TomoClass", "1"));
		_fldif.add(new DDictEntry(24, 5269, 15,
				"NumberofTomosynthesisSourceImages", "1"));
		_fldif.add(new DDictEntry(24, 5384, 9, "PositionerType", "1"));
		_fldif.add(new DDictEntry(24, 20740, 10,
				"ProjectionEponymousNameCodeSequence", "1"));
		_fldif.add(new DDictEntry(24, 28672, 9, "DetectorConditionsNominalFlag",
				"1"));
		_fldif.add(new DDictEntry(24, 28673, 16, "DetectorTemperature", "1"));
		_fldif.add(new DDictEntry(24, 28676, 9, "DetectorType", "1"));
		_fldif.add(new DDictEntry(24, 28677, 9, "DetectorConfiguration", "1"));
		_fldif.add(new DDictEntry(24, 28678, 18, "DetectorDescription", "1"));
		_fldif.add(new DDictEntry(24, 28680, 18, "DetectorMode", "1"));
		_fldif.add(new DDictEntry(24, 28682, 7, "DetectorID", "1"));
		_fldif.add(new DDictEntry(24, 28684, 11, "DateofLastDetectorCalibration",
				"1"));
		_fldif.add(new DDictEntry(24, 28686, 12, "TimeofLastDetectorCalibration",
				"1"));
		_fldif.add(new DDictEntry(24, 28688, 15,
				"ExposuresOnDetectorSinceLastCalibration", "1"));
		_fldif.add(new DDictEntry(24, 28689, 15,
				"ExposuresOnDetectorSinceManufactured", "1"));
		_fldif.add(new DDictEntry(24, 28690, 16, "DetectorTimeSinceLastExposure",
				"1"));
		_fldif.add(new DDictEntry(24, 28692, 16, "DetectorActiveTime", "1"));
		_fldif.add(new DDictEntry(24, 28694, 16,
				"DetectorActivationOffsetFromExposure", "1"));
		_fldif.add(new DDictEntry(24, 28698, 16, "DetectorBinning", "2"));
		_fldif.add(new DDictEntry(24, 28704, 16, "DetectorElementPhysicalSize",
				"2"));
		_fldif.add(new DDictEntry(24, 28706, 16, "DetectorElementSpacing", "2"));
		_fldif.add(new DDictEntry(24, 28708, 9, "DetectorActiveShape", "1"));
		_fldif.add(new DDictEntry(24, 28710, 16, "DetectorActiveDimensions",
				"1-2"));
		_fldif.add(new DDictEntry(24, 28712, 16, "DetectorActiveOrigin", "2"));
		_fldif.add(new DDictEntry(24, 28720, 16, "FieldofViewOrigin", "2"));
		_fldif.add(new DDictEntry(24, 28722, 16, "FieldofViewRotation", "1"));
		_fldif
				.add(new DDictEntry(24, 28724, 9, "FieldofViewHorizontalFlip",
						"1"));
		_fldif.add(new DDictEntry(24, 28736, 18, "GridAbsorbingMaterial", "1"));
		_fldif.add(new DDictEntry(24, 28737, 18, "GridSpacingMaterial", "1"));
		_fldif.add(new DDictEntry(24, 28738, 16, "GridThickness", "1"));
		_fldif.add(new DDictEntry(24, 28740, 16, "GridPitch", "1"));
		_fldif.add(new DDictEntry(24, 28742, 15, "GridAspectRatio", "2"));
		_fldif.add(new DDictEntry(24, 28744, 16, "GridPeriod", "1"));
		_fldif.add(new DDictEntry(24, 28748, 16, "GridFocalDistance", "1"));
		_fldif.add(new DDictEntry(24, 28752, 9, "FilterMaterial", "1-n"));
		_fldif
				.add(new DDictEntry(24, 28754, 16, "FilterThicknessMinimum",
						"1-n"));
		_fldif
				.add(new DDictEntry(24, 28756, 16, "FilterThicknessMaximum",
						"1-n"));
		_fldif.add(new DDictEntry(24, 28768, 9, "ExposureControlMode", "1"));
		_fldif.add(new DDictEntry(24, 28770, 18,
				"ExposureControlModeDescription", "1"));
		_fldif.add(new DDictEntry(24, 28772, 9, "ExposureStatus", "1"));
		_fldif.add(new DDictEntry(24, 28773, 16, "PhototimerSetting", "1"));
		_fldif.add(new DDictEntry(32, 98, 9, "ImageLaterality", "1"));
		_fldif.add(new DDictEntry(40, 768, 9, "QualityControlImage", "1"));
		_fldif.add(new DDictEntry(40, 769, 9, "BurnedInAnnotation", "1"));
		_fldif.add(new DDictEntry(40, 4161, 23, "PixelIntensityRelationshipSign",
				"1"));
		_fldif.add(new DDictEntry(40, 4864, 9, "ImplantPresent", "1"));
		_fldif.add(new DDictEntry(40, 8466, 16, "LossyImageCompressionRatio",
				"1-n"));
		_fldif.add(new DDictEntry(64, 775, 16, "DistanceSourceToSupport", "1"));
		_fldif.add(new DDictEntry(64, 786, 16, "XRayOutput", "1"));
		_fldif.add(new DDictEntry(64, 788, 16, "HalfValueLayer", "1"));
		_fldif.add(new DDictEntry(64, 790, 16, "OrganDose", "1"));
		_fldif.add(new DDictEntry(64, 792, 9, "OrganExposed", "1"));
		_fldif
				.add(new DDictEntry(64, 1365, 10, "AcquisitionContextSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 1366, 13, "AcquisitionContextDescription",
				"1"));
		_fldif.add(new DDictEntry(64, 2282, 10, "MeasurementUnitsCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 41027, 10, "ConceptNameCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 41249, 11, "Date", "1"));
		_fldif.add(new DDictEntry(64, 41250, 12, "Time", "1"));
		_fldif.add(new DDictEntry(64, 41251, 14, "PersonName", "1"));
		_fldif.add(new DDictEntry(64, 41270, 3, "ReferencedFrameNumbers", "1-n"));
		_fldif.add(new DDictEntry(64, 41312, 27, "TextValue", "1"));
		_fldif.add(new DDictEntry(64, 41320, 10, "ConceptCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 41738, 16, "NumericValue", "1-n"));
		_fldif.add(new DDictEntry(96, 12288, 10, "HistogramSequence", "1"));
		_fldif.add(new DDictEntry(96, 12290, 3, "HistogramNumberofBins", "1"));
		_fldif.add(new DDictEntry(96, 12292, 21, "HistogramFirstBinValue", "1"));
		_fldif.add(new DDictEntry(96, 12294, 21, "HistogramLastBinValue", "1"));
		_fldif.add(new DDictEntry(96, 12296, 3, "HistogramBinWidth", "1"));
		_fldif.add(new DDictEntry(96, 12304, 6, "HistogramExplanation", "1"));
		_fldif.add(new DDictEntry(96, 12320, 1, "HistogramData", "1-n"));
		_fldif.add(new DDictEntry(8, 259, 7, "CodingSchemeVersion", "1"));
		_fldif.add(new DDictEntry(8, 261, 9, "MappingResource", "1"));
		_fldif.add(new DDictEntry(8, 262, 28, "ContextGroupVersion", "1"));
		_fldif.add(new DDictEntry(8, 263, 28, "ContextGroupLocalVersion", "1"));
		_fldif.add(new DDictEntry(8, 267, 9, "CodeSetExtensionFlag", "1"));
		_fldif
				.add(new DDictEntry(8, 268, 2, "PrivateCodingSchemeCreatorUID",
						"1"));
		_fldif.add(new DDictEntry(8, 269, 2, "CodeSetExtensionCreatorUID", "1"));
		_fldif.add(new DDictEntry(8, 271, 9, "ContextIdentifier", "1"));
		_fldif.add(new DDictEntry(8, 4442, 2, "SOPClassesSupported", "1-n"));
		_fldif.add(new DDictEntry(32, 25, 15, "ItemNumber", "1"));
		_fldif.add(new DDictEntry(8192, 30, 10, "PrinterConfigurationSequence",
				"1"));
		_fldif.add(new DDictEntry(8192, 160, 3, "MemoryBitDepth", "1"));
		_fldif.add(new DDictEntry(8192, 161, 3, "PrintingBitDepth", "1"));
		_fldif.add(new DDictEntry(8192, 162, 10, "MediaInstalledSequence", "1"));
		_fldif.add(new DDictEntry(8192, 164, 10, "OtherMediaAvailableSequence",
				"1"));
		_fldif.add(new DDictEntry(8192, 168, 10,
				"SupportedImageDisplayFormatsSequence", "1"));
		_fldif.add(new DDictEntry(8208, 82, 9, "PrinterResolutionID", "1"));
		_fldif
				.add(new DDictEntry(8208, 84, 9, "DefaultPrinterResolutionID",
						"1"));
		_fldif.add(new DDictEntry(8208, 166, 9, "DefaultMagnificationType", "1"));
		_fldif.add(new DDictEntry(8208, 167, 9,
				"OtherMagnificationTypesAvailable", "1-n"));
		_fldif.add(new DDictEntry(8208, 168, 9, "DefaultSmoothingType", "1"));
		_fldif.add(new DDictEntry(8208, 169, 9, "OtherSmoothingTypesAvailable",
				"1-n"));
		_fldif.add(new DDictEntry(8208, 338, 18,
				"ConfigurationInformationDescription", "1"));
		_fldif.add(new DDictEntry(8208, 340, 15, "MaximumCollatedFilms", "1"));
		_fldif.add(new DDictEntry(8208, 886, 16, "PrinterPixelSpacing", "2"));
		_fldif.add(new DDictEntry(8224, 64, 9, "RequestedDecimateCropBehavior",
				"1"));
		_fldif.add(new DDictEntry(8224, 80, 9, "RequestedResolutionID", "1"));
		_fldif.add(new DDictEntry(8224, 160, 9, "RequestedImageSizeFlag", "1"));
		_fldif.add(new DDictEntry(8224, 162, 9, "DecimateCropResult", "1"));
		_fldif.add(new DDictEntry(8256, 32, 10, "OverlayPixelDataSequence", "1"));
		_fldif.add(new DDictEntry(8256, 114, 9, "OverlayOrImageMagnification",
				"1"));
		_fldif.add(new DDictEntry(8256, 116, 3, "MagnifyToNumberOfColumns", "1"));
		_fldif.add(new DDictEntry(8256, 130, 9, "OverlayBackgroundDensity", "1"));
		_fldif.add(new DDictEntry(64, 1786, 6, "SlideIdentifier", "1"));
		_fldif.add(new DDictEntry(64, 1818, 10,
				"ImageCenterPointCoordinatesSequence", "1"));
		_fldif.add(new DDictEntry(64, 1834, 16, "XOffsetInSlideCoordinateSystem",
				"1"));
		_fldif.add(new DDictEntry(64, 1850, 16, "YOffsetInSlideCoordinateSystem",
				"1"));
		_fldif.add(new DDictEntry(64, 1866, 16, "ZOffsetInSlideCoordinateSystem",
				"1"));
		_fldif.add(new DDictEntry(64, 2264, 10, "PixelSpacingSequence", "1"));
		_fldif.add(new DDictEntry(64, 2266, 10,
				"CoordinateSystemAxisCodeSequence", "1"));
		_fldif.add(new DDictEntry(24, 5666, 3, "ShutterPresentationValue", "1"));
		_fldif.add(new DDictEntry(24, 5667, 3, "ShutterOverlayGroup", "1"));
		_fldif.add(new DDictEntry(20480, 4097, 9, "CurveActivationLayer", "1"));
		_fldif.add(new DDictEntry(24576, 4097, 9, "OverlayActivationLayer", "1"));
		_fldif.add(new DDictEntry(112, 1, 10, "GraphicAnnotationSequence", "1"));
		_fldif.add(new DDictEntry(112, 2, 9, "GraphicLayer", "1"));
		_fldif.add(new DDictEntry(112, 3, 9, "BoundingBoxAnnotationUnits", "1"));
		_fldif.add(new DDictEntry(112, 4, 9, "AnchorPointAnnotationUnits", "1"));
		_fldif.add(new DDictEntry(112, 5, 9, "GraphicAnnotationUnits", "1"));
		_fldif.add(new DDictEntry(112, 6, 13, "UnformattedTextValue", "1"));
		_fldif.add(new DDictEntry(112, 8, 10, "TextObjectSequence", "1"));
		_fldif.add(new DDictEntry(112, 9, 10, "GraphicObjectSequence", "1"));
		_fldif.add(new DDictEntry(112, 16, 26, "BoundingBoxTLHC", "2"));
		_fldif.add(new DDictEntry(112, 17, 26, "BoundingBoxBRHC", "2"));
		_fldif.add(new DDictEntry(112, 20, 26, "AnchorPoint", "2"));
		_fldif.add(new DDictEntry(112, 21, 9, "AnchorPointVisibility", "1"));
		_fldif.add(new DDictEntry(112, 32, 3, "GraphicDimensions", "1"));
		_fldif.add(new DDictEntry(112, 33, 3, "NumberOfGraphicPoints", "1"));
		_fldif.add(new DDictEntry(112, 34, 26, "GraphicData", "2-n"));
		_fldif.add(new DDictEntry(112, 35, 9, "GraphicType", "1"));
		_fldif.add(new DDictEntry(112, 36, 9, "GraphicFilled", "1"));
		_fldif.add(new DDictEntry(112, 65, 9, "ImageHorizontalFlip", "1"));
		_fldif.add(new DDictEntry(112, 96, 10, "GraphicLayerSequence", "1"));
		_fldif.add(new DDictEntry(112, 98, 15, "GraphicLayerOrder", "1"));
		_fldif.add(new DDictEntry(112, 102, 3,
				"GraphicLayerRecommendedDisplayGrayScaleValue", "1"));
		_fldif.add(new DDictEntry(112, 104, 6, "GraphicLayerDescription", "1"));
		_fldif.add(new DDictEntry(112, 128, 9, "PresentationLabel", "1"));
		_fldif.add(new DDictEntry(112, 129, 6, "PresentationDescription", "1"));
		_fldif.add(new DDictEntry(112, 130, 11, "PresentationCreationDate", "1"));
		_fldif.add(new DDictEntry(112, 131, 12, "PresentationCreationTime", "1"));
		_fldif.add(new DDictEntry(112, 132, 14, "PresentationCreatorsName", "1"));
		_fldif.add(new DDictEntry(40, 12560, 10, "SoftcopyVOILUTSequence", "1"));
		_fldif.add(new DDictEntry(112, 18, 9, "BoundingBoxTHJ", "1"));
		_fldif.add(new DDictEntry(112, 66, 3, "ImageRotation", "1"));
		_fldif.add(new DDictEntry(112, 82, 19, "DisplayedAreaTLHC", "2"));
		_fldif.add(new DDictEntry(112, 83, 19, "DisplayedAreaBRHC", "2"));
		_fldif.add(new DDictEntry(112, 90, 10, "DisplayedAreaSelectionSequence",
				"1"));
		_fldif.add(new DDictEntry(112, 103, 3,
				"GraphicLayerRecommendedDisplayRGBValue", "3"));
		_fldif.add(new DDictEntry(112, 256, 9, "PresentationSizeMode", "1"));
		_fldif.add(new DDictEntry(112, 257, 16, "PresentationPixelSpacing", "2"));
		_fldif.add(new DDictEntry(112, 258, 15, "PresentationPixelAspectRatio",
				"2"));
		_fldif.add(new DDictEntry(112, 259, 26,
				"PresentationPixelMagnificationRatio", "1"));
		_fldif.add(new DDictEntry(64, 8214, 6, "PlacerOrderNumber", "1"));
		_fldif.add(new DDictEntry(64, 8215, 6, "FillerOrderNumber", "1"));
		_fldif.add(new DDictEntry(64, 1290, 6, "SpecimenAccessionNumber", "1"));
		_fldif.add(new DDictEntry(64, 1360, 10, "SpecimenSequence", "1"));
		_fldif.add(new DDictEntry(64, 1361, 6, "SpecimenIdentifier", "1"));
		_fldif.add(new DDictEntry(64, 1434, 10, "SpecimenTypeCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 40976, 9, "RelationshipType", "1"));
		_fldif.add(new DDictEntry(64, 40999, 6, "VerifyingOrganization", "1"));
		_fldif.add(new DDictEntry(64, 41008, 28, "VerificationDateTime", "1"));
		_fldif.add(new DDictEntry(64, 41010, 28, "ObservationDateTime", "1"));
		_fldif.add(new DDictEntry(64, 41024, 9, "ValueType", "1"));
		_fldif.add(new DDictEntry(64, 41040, 9, "ContinuityOfContent", "1"));
		_fldif
				.add(new DDictEntry(64, 41075, 10, "VerifyingObserverSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 41077, 14, "VerifyingObserverName", "1"));
		_fldif.add(new DDictEntry(64, 41096, 10,
				"VerifyingObserverIdentificationCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 41248, 28, "DateTime", "1"));
		_fldif.add(new DDictEntry(64, 41252, 2, "UID", "1"));
		_fldif.add(new DDictEntry(64, 41264, 9, "TemporalRangeType", "1"));
		_fldif.add(new DDictEntry(64, 41266, 1, "ReferencedSamplePositions",
				"1-n"));
		_fldif.add(new DDictEntry(64, 41272, 16, "ReferencedTimeOffsets", "1-n"));
		_fldif.add(new DDictEntry(64, 41274, 28, "ReferencedDatetime", "1-n"));
		_fldif.add(new DDictEntry(64, 41728, 10, "MeasuredValueSequence", "1"));
		_fldif.add(new DDictEntry(64, 41824, 10, "PredecessorDocumentsSequence",
				"1"));
		_fldif
				.add(new DDictEntry(64, 41840, 10, "ReferencedRequestSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 41842, 10,
				"PerformedProcedureCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 41845, 10,
				"CurrentRequestedProcedureEvidenceSequence", "1"));
		_fldif.add(new DDictEntry(64, 41861, 10,
				"PertinentOtherEvidenceSequence", "1"));
		_fldif.add(new DDictEntry(64, 42129, 9, "CompletionFlag", "1"));
		_fldif
				.add(new DDictEntry(64, 42130, 6, "CompletionFlagDescription",
						"1"));
		_fldif.add(new DDictEntry(64, 42131, 9, "VerificationFlag", "1"));
		_fldif.add(new DDictEntry(64, 42244, 10, "ContentTemplateSequence", "1"));
		_fldif.add(new DDictEntry(64, 42277, 10, "IdenticalDocumentsSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 42800, 10, "ContentSequence", "1"));
		_fldif.add(new DDictEntry(64, 56064, 9, "TemplateIdentifier", "1"));
		_fldif.add(new DDictEntry(64, 56070, 28, "TemplateVersion", "1"));
		_fldif.add(new DDictEntry(64, 56071, 28, "TemplateLocalVersion", "1"));
		_fldif.add(new DDictEntry(64, 56075, 9, "TemplateExtensionFlag", "1"));
		_fldif.add(new DDictEntry(64, 56076, 2,
				"TemplateExtensionOrganizationUID", "1"));
		_fldif.add(new DDictEntry(64, 56077, 2, "TemplateExtensionCreatorUID",
				"1"));
		_fldif.add(new DDictEntry(64, 56179, 1,
				"ReferencedContentItemIdentifier", "1-n"));
		_fldif.add(new DDictEntry(8, 42, 28, "AcquisitionDatetime", "1"));
		_fldif.add(new DDictEntry(24, 4199, 16, "ImageTriggerDelay", "1"));
		_fldif.add(new DDictEntry(24, 4200, 16, "MultiplexGroupTimeOffset", "1"));
		_fldif.add(new DDictEntry(24, 4201, 16, "TriggerTimeOffset", "1"));
		_fldif.add(new DDictEntry(24, 4202, 9, "SynchronizationTrigger", "1"));
		_fldif.add(new DDictEntry(24, 4204, 3, "SynchronizationChannel", "2"));
		_fldif.add(new DDictEntry(24, 4206, 1, "TriggerSamplePosition", "1"));
		_fldif
				.add(new DDictEntry(24, 6144, 9, "AcquisitionTimeSynchronized",
						"1"));
		_fldif.add(new DDictEntry(24, 6146, 9, "TimeDistributionProtocol", "1"));
		_fldif.add(new DDictEntry(24, 6145, 7, "TimeSource", "1"));
		_fldif.add(new DDictEntry(32, 512, 2,
				"SynchronizationFrameofReferenceUID", "1"));
		_fldif.add(new DDictEntry(58, 4, 9, "WaveformOriginality", "1"));
		_fldif.add(new DDictEntry(58, 5, 3, "NumberofWaveformChannels", "1"));
		_fldif.add(new DDictEntry(58, 16, 1, "NumberofWaveformSamples", "1"));
		_fldif.add(new DDictEntry(58, 26, 16, "SamplingFrequency", "1"));
		_fldif.add(new DDictEntry(58, 32, 7, "MultiplexGroupLabel", "1"));
		_fldif.add(new DDictEntry(58, 512, 10, "ChannelDefinitionSequence", "1"));
		_fldif.add(new DDictEntry(58, 514, 15, "WaveformChannelNumber", "1"));
		_fldif.add(new DDictEntry(58, 515, 7, "ChannelLabel", "1"));
		_fldif.add(new DDictEntry(58, 517, 9, "ChannelStatus", "1-n"));
		_fldif.add(new DDictEntry(58, 520, 10, "ChannelSourceSequence", "1"));
		_fldif.add(new DDictEntry(58, 521, 10, "ChannelSourceModifiersSequence",
				"1"));
		_fldif.add(new DDictEntry(58, 522, 10, "SourceWaveformSequence", "1"));
		_fldif
				.add(new DDictEntry(58, 524, 6, "ChannelDerivationDescription",
						"1"));
		_fldif.add(new DDictEntry(58, 528, 16, "ChannelSensitivity", "1"));
		_fldif.add(new DDictEntry(58, 529, 10, "ChannelSensitivityUnitsSequence",
				"1"));
		_fldif.add(new DDictEntry(58, 530, 16,
				"ChannelSensitivityCorrectionFactor", "1"));
		_fldif.add(new DDictEntry(58, 531, 16, "ChannelBaseline", "1"));
		_fldif.add(new DDictEntry(58, 532, 16, "ChannelTimeSkew", "1"));
		_fldif.add(new DDictEntry(58, 533, 16, "ChannelSampleSkew", "1"));
		_fldif.add(new DDictEntry(58, 536, 16, "ChannelOffset", "1"));
		_fldif.add(new DDictEntry(58, 538, 3, "WaveformBitsStored", "1"));
		_fldif.add(new DDictEntry(58, 544, 16, "FilterLowFrequency", "1"));
		_fldif.add(new DDictEntry(58, 545, 16, "FilterHighFrequency", "1"));
		_fldif.add(new DDictEntry(58, 546, 16, "NotchFilterFrequency", "1"));
		_fldif.add(new DDictEntry(58, 547, 16, "NotchFilterBandwidth", "1"));
		_fldif.add(new DDictEntry(64, 41136, 3, "ReferencedWaveformChannels",
				"2-2n"));
		_fldif.add(new DDictEntry(64, 41344, 3, "AnnotationGroupNumber", "1"));
		_fldif.add(new DDictEntry(64, 41365, 10, "ModifierCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 45088, 10, "AnnotationSequence", "1"));
		_fldif.add(new DDictEntry(21504, 256, 10, "WaveformSequence", "1"));
		_fldif.add(new DDictEntry(21504, 272, 22, "ChannelMinimumValue", "1"));
		_fldif.add(new DDictEntry(21504, 274, 22, "ChannelMaximumValue", "1"));
		_fldif.add(new DDictEntry(21504, 4100, 3, "WaveformBitsAllocated", "1"));
		_fldif.add(new DDictEntry(21504, 4102, 9, "WaveformSampleInterpretation",
				"1"));
		_fldif.add(new DDictEntry(21504, 4106, 22, "WaveformPaddingValue", "1"));
		_fldif.add(new DDictEntry(21504, 4112, 22, "WaveformData", "1"));
		_fldif.add(new DDictEntry(8, 86, 9, "InstanceAvailability", "1"));
		_fldif.add(new DDictEntry(8, 513, 7, "TimezoneOffsetFromUTC", "1"));
		_fldif
				.add(new DDictEntry(8, 4426, 10, "ReferencedInstanceSequence",
						"1"));
		_fldif
				.add(new DDictEntry(8, 4410, 10, "ReferencedWaveformSequence",
						"1"));
		_fldif.add(new DDictEntry(8, 8487, 7, "ViewName", "1"));
		_fldif.add(new DDictEntry(16, 257, 10,
				"PatientsPrimaryLanguageCodeSequence", "1"));
		_fldif.add(new DDictEntry(16, 258, 10,
				"PatientsPrimaryLanguageCodeModifierSequence", "1"));
		_fldif.add(new DDictEntry(24, 8193, 15, "PageNumberVector", "1-n"));
		_fldif.add(new DDictEntry(24, 8194, 7, "FrameLabelVector", "1-n"));
		_fldif
				.add(new DDictEntry(24, 8195, 16, "FramePrimaryAngleVector",
						"1-n"));
		_fldif.add(new DDictEntry(24, 8196, 16, "FrameSecondaryAngleVector",
				"1-n"));
		_fldif.add(new DDictEntry(24, 8197, 16, "SliceLocationVector", "1-n"));
		_fldif
				.add(new DDictEntry(24, 8198, 7, "DisplayWindowLabelVector",
						"1-n"));
		_fldif
				.add(new DDictEntry(24, 8208, 16, "NominalScannedPixelSpacing",
						"2"));
		_fldif.add(new DDictEntry(24, 8224, 9,
				"DigitizingDeviceTransportDirection", "1"));
		_fldif.add(new DDictEntry(24, 8240, 16, "RotationOfScannedFilm", "1"));
		_fldif.add(new DDictEntry(24, 12544, 9, "IVUSAcquisition", "1"));
		_fldif.add(new DDictEntry(24, 12545, 16, "IVUSPullbackRate", "1"));
		_fldif.add(new DDictEntry(24, 12546, 16, "IVUSGatedRate", "1"));
		_fldif.add(new DDictEntry(24, 12547, 15, "IVUSPullbackStartFrameNumber",
				"1"));
		_fldif.add(new DDictEntry(24, 12548, 15, "IVUSPullbackStopFrameNumber",
				"1"));
		_fldif.add(new DDictEntry(24, 12549, 15, "LesionNumber", "1-n"));
		_fldif.add(new DDictEntry(24, 33104, 16, "ExposureTimeIn\265S", "1"));
		_fldif.add(new DDictEntry(24, 33105, 16, "XRayTubeCurrentIn\265A", "1"));
		_fldif.add(new DDictEntry(40, 4944, 9, "PartialView", "1"));
		_fldif.add(new DDictEntry(40, 4945, 13, "PartialViewDescription", "1"));
		_fldif.add(new DDictEntry(64, 10, 10, "StageCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 782, 10, "ExposureDoseSequence", "1"));
		_fldif.add(new DDictEntry(64, 16385, 9,
				"GeneralPurposeScheduledProcedureStepStatus", "1"));
		_fldif.add(new DDictEntry(64, 16386, 9,
				"GeneralPurposePerformedProcedureStepStatus", "1"));
		_fldif.add(new DDictEntry(64, 16387, 9,
				"GeneralPurposeScheduledProcedureStepPriority", "1"));
		_fldif.add(new DDictEntry(64, 16388, 10,
				"ScheduledProcessingApplicationsCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16389, 28,
				"ScheduledProcedureStepStartDateAndTime", "1"));
		_fldif.add(new DDictEntry(64, 16390, 9, "MultipleCopiesFlag", "1"));
		_fldif.add(new DDictEntry(64, 16391, 10,
				"PerformedProcessingApplicationsCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16393, 10, "HumanPerformerCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 16401, 28, "ExpectedCompletionDateAndTime",
				"1"));
		_fldif.add(new DDictEntry(64, 16405, 10,
				"ResultingGeneralPurposePerformedProcedureStepsSequence", "1"));
		_fldif.add(new DDictEntry(64, 16406, 10,
				"ReferencedGeneralPurposeScheduledProcedureStepSequence", "1"));
		_fldif.add(new DDictEntry(64, 16408, 10, "ScheduledWorkitemCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 16409, 10, "PerformedWorkitemCodeSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 16416, 9, "InputAvailabilityFlag", "1"));
		_fldif
				.add(new DDictEntry(64, 16417, 10, "InputInformationSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 16418, 10, "RelevantInformationSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 16419, 2,
				"ReferencedGeneralPurposeScheduledProcedureStepTransactionUID",
				"1"));
		_fldif.add(new DDictEntry(64, 16421, 10,
				"ScheduledStationNameCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16422, 10,
				"ScheduledStationClassCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16423, 10,
				"ScheduledStationGeographicLocationCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16424, 10,
				"PerformedStationNameCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16425, 10,
				"PerformedStationClassCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16432, 10,
				"PerformedStationGeographicLocationCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16433, 10,
				"RequestedSubsequentWorkitemCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 16434, 10, "NonDICOMOutputCodeSequence",
				"1"));
		_fldif
				.add(new DDictEntry(64, 16435, 10, "OutputInformationSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 16436, 10,
				"ScheduledHumanPerformersSequence", "1"));
		_fldif.add(new DDictEntry(64, 16437, 10, "ActualHumanPerformersSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 16438, 6, "HumanPerformersOrganization",
				"1"));
		_fldif.add(new DDictEntry(64, 16439, 14, "HumanPerformersName", "1"));
		_fldif.add(new DDictEntry(64, 33538, 16, "EntranceDoseInMGy", "1"));
		_fldif.add(new DDictEntry(64, 41328, 10,
				"PurposeOfReferenceCodeSequence", "1"));
		_fldif.add(new DDictEntry(256, 1040, 9, "SOPInstanceStatus", "1"));
		_fldif.add(new DDictEntry(256, 1056, 28, "SOPAuthorizationDateAndTime",
				"1"));
		_fldif.add(new DDictEntry(256, 1060, 18, "SOPAuthorizationComment", "1"));
		_fldif.add(new DDictEntry(256, 1062, 6,
				"AuthorizationEquipmentCertificationNumber", "1"));
		_fldif.add(new DDictEntry(1024, 5, 3, "MACIDNumber", "1"));
		_fldif.add(new DDictEntry(1024, 16, 2, "MACCalculationTransferSyntaxUID",
				"1"));
		_fldif.add(new DDictEntry(1024, 21, 9, "MACAlgorithm", "1"));
		_fldif.add(new DDictEntry(1024, 32, 5, "DataElementsSigned", "1-n"));
		_fldif.add(new DDictEntry(1024, 256, 2, "DigitalSignatureUID", "1"));
		_fldif
				.add(new DDictEntry(1024, 261, 28, "DigitalSignatureDateTime",
						"1"));
		_fldif.add(new DDictEntry(1024, 272, 9, "CertificateType", "1"));
		_fldif.add(new DDictEntry(1024, 277, 8, "CertificateOfSigner", "1"));
		_fldif.add(new DDictEntry(1024, 288, 8, "Signature", "1"));
		_fldif.add(new DDictEntry(1024, 773, 9, "CertifiedTimestampType", "1"));
		_fldif.add(new DDictEntry(1024, 784, 8, "CertifiedTimestamp", "1"));
		_fldif.add(new DDictEntry(8192, 97, 15, "MaximumMemoryAllocation", "1"));
		_fldif.add(new DDictEntry(12290, 13, 16, "XRayImageReceptorTranslation",
				"3"));
		_fldif.add(new DDictEntry(12290, 52, 16, "DiaphragmPosition", "4"));
		_fldif.add(new DDictEntry(12294, 72, 15, "ContourNumber", "1"));
		_fldif.add(new DDictEntry(12294, 73, 15, "AttachedContours", "1-n"));
		_fldif.add(new DDictEntry(12296, 16, 10, "MeasuredDoseReferenceSequence",
				"1"));
		_fldif.add(new DDictEntry(12296, 18, 13, "MeasuredDoseDescription", "1"));
		_fldif.add(new DDictEntry(12296, 20, 9, "MeasuredDoseType", "1"));
		_fldif.add(new DDictEntry(12296, 22, 16, "MeasuredDoseValue", "1"));
		_fldif.add(new DDictEntry(12296, 32, 10, "TreatmentSessionBeamSequence",
				"1"));
		_fldif.add(new DDictEntry(12296, 34, 15, "CurrentFractionNumber", "1"));
		_fldif
				.add(new DDictEntry(12296, 36, 11, "TreatmentControlPointDate",
						"1"));
		_fldif
				.add(new DDictEntry(12296, 37, 12, "TreatmentControlPointTime",
						"1"));
		_fldif
				.add(new DDictEntry(12296, 42, 9, "TreatmentTerminationStatus",
						"1"));
		_fldif.add(new DDictEntry(12296, 43, 7, "TreatmentTerminationCode", "1"));
		_fldif.add(new DDictEntry(12296, 44, 9, "TreatmentVerificationStatus",
				"1"));
		_fldif.add(new DDictEntry(12296, 48, 10,
				"ReferencedTreatmentRecordSequence", "1"));
		_fldif
				.add(new DDictEntry(12296, 50, 16, "SpecifiedPrimaryMeterset",
						"1"));
		_fldif.add(new DDictEntry(12296, 51, 16, "SpecifiedSecondaryMeterset",
				"1"));
		_fldif
				.add(new DDictEntry(12296, 54, 16, "DeliveredPrimaryMeterset",
						"1"));
		_fldif.add(new DDictEntry(12296, 55, 16, "DeliveredSecondaryMeterset",
				"1"));
		_fldif.add(new DDictEntry(12296, 58, 16, "SpecifiedTreatmentTime", "1"));
		_fldif.add(new DDictEntry(12296, 59, 16, "DeliveredTreatmentTime", "1"));
		_fldif.add(new DDictEntry(12296, 64, 10, "ControlPointDeliverySequence",
				"1"));
		_fldif.add(new DDictEntry(12296, 66, 16, "SpecifiedMeterset", "1"));
		_fldif.add(new DDictEntry(12296, 68, 16, "DeliveredMeterset", "1"));
		_fldif.add(new DDictEntry(12296, 72, 16, "DoseRateDelivered", "1"));
		_fldif.add(new DDictEntry(12296, 80, 10,
				"TreatmentSummaryCalculatedDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 82, 16, "CumulativeDoseToDoseReference",
				"1"));
		_fldif.add(new DDictEntry(12296, 84, 11, "FirstTreatmentDate", "1"));
		_fldif.add(new DDictEntry(12296, 86, 11, "MostRecentTreatmentDate", "1"));
		_fldif.add(new DDictEntry(12296, 90, 15, "NumberOfFractionsDelivered",
				"1"));
		_fldif.add(new DDictEntry(12296, 96, 10, "OverrideSequence", "1"));
		_fldif.add(new DDictEntry(12296, 98, 5, "OverrideParameterPointer", "1"));
		_fldif.add(new DDictEntry(12296, 100, 15, "MeasuredDoseReferenceNumber",
				"1"));
		_fldif.add(new DDictEntry(12296, 102, 13, "OverrideReason", "1"));
		_fldif.add(new DDictEntry(12296, 112, 10,
				"CalculatedDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 114, 15,
				"CalculatedDoseReferenceNumber", "1"));
		_fldif.add(new DDictEntry(12296, 116, 13,
				"CalculatedDoseReferenceDescription", "1"));
		_fldif.add(new DDictEntry(12296, 118, 16,
				"CalculatedDoseReferenceDoseValue", "1"));
		_fldif.add(new DDictEntry(12296, 120, 16, "StartMeterset", "1"));
		_fldif.add(new DDictEntry(12296, 122, 16, "EndMeterset", "1"));
		_fldif.add(new DDictEntry(12296, 128, 10,
				"ReferencedMeasuredDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 130, 15,
				"ReferencedMeasuredDoseReferenceNumber", "1"));
		_fldif.add(new DDictEntry(12296, 144, 10,
				"ReferencedCalculatedDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 146, 15,
				"ReferencedCalculatedDoseReferenceNumber", "1"));
		_fldif.add(new DDictEntry(12296, 160, 10,
				"BeamLimitingDeviceLeafPairsSequence", "1"));
		_fldif.add(new DDictEntry(12296, 176, 10, "RecordedWedgeSequence", "1"));
		_fldif.add(new DDictEntry(12296, 192, 10, "RecordedCompensatorSequence",
				"1"));
		_fldif.add(new DDictEntry(12296, 208, 10, "RecordedBlockSequence", "1"));
		_fldif.add(new DDictEntry(12296, 224, 10,
				"TreatmentSummaryMeasuredDoseReferenceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 256, 10, "RecordedSourceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 261, 6, "SourceSerialNumber", "1"));
		_fldif.add(new DDictEntry(12296, 272, 10,
				"TreatmentSessionApplicationSetupSequence", "1"));
		_fldif.add(new DDictEntry(12296, 278, 9, "ApplicationSetupCheck", "1"));
		_fldif.add(new DDictEntry(12296, 288, 10,
				"RecordedBrachyAccessoryDeviceSequence", "1"));
		_fldif.add(new DDictEntry(12296, 290, 15,
				"ReferencedBrachyAccessoryDeviceNumber", "1"));
		_fldif
				.add(new DDictEntry(12296, 304, 10, "RecordedChannelSequence",
						"1"));
		_fldif.add(new DDictEntry(12296, 306, 16, "SpecifiedChannelTotalTime",
				"1"));
		_fldif.add(new DDictEntry(12296, 308, 16, "DeliveredChannelTotalTime",
				"1"));
		_fldif
				.add(new DDictEntry(12296, 310, 15, "SpecifiedNumberOfPulses",
						"1"));
		_fldif
				.add(new DDictEntry(12296, 312, 15, "DeliveredNumberOfPulses",
						"1"));
		_fldif.add(new DDictEntry(12296, 314, 16,
				"SpecifiedPulseRepetitionInterval", "1"));
		_fldif.add(new DDictEntry(12296, 316, 16,
				"DeliveredPulseRepetitionInterval", "1"));
		_fldif.add(new DDictEntry(12296, 320, 10,
				"RecordedSourceApplicatorSequence", "1"));
		_fldif.add(new DDictEntry(12296, 322, 15,
				"ReferencedSourceApplicatorNumber", "1"));
		_fldif.add(new DDictEntry(12296, 336, 10,
				"RecordedChannelShieldSequence", "1"));
		_fldif.add(new DDictEntry(12296, 338, 15,
				"ReferencedChannelShieldNumber", "1"));
		_fldif.add(new DDictEntry(12296, 352, 10,
				"BrachyControlPointDeliveredSequence", "1"));
		_fldif.add(new DDictEntry(12296, 354, 11, "SafePositionExitDate", "1"));
		_fldif.add(new DDictEntry(12296, 356, 12, "SafePositionExitTime", "1"));
		_fldif.add(new DDictEntry(12296, 358, 11, "SafePositionReturnDate", "1"));
		_fldif.add(new DDictEntry(12296, 360, 12, "SafePositionReturnTime", "1"));
		_fldif.add(new DDictEntry(12296, 512, 9, "CurrentTreatmentStatus", "1"));
		_fldif.add(new DDictEntry(12296, 514, 13, "TreatmentStatusComment", "1"));
		_fldif.add(new DDictEntry(12296, 544, 10, "FractionGroupSummarySequence",
				"1"));
		_fldif
				.add(new DDictEntry(12296, 547, 15, "ReferencedFractionNumber",
						"1"));
		_fldif.add(new DDictEntry(12296, 548, 9, "FractionGroupType", "1"));
		_fldif.add(new DDictEntry(12296, 560, 9, "BeamStopperPosition", "1"));
		_fldif.add(new DDictEntry(12296, 576, 10,
				"FractionStatusSummarySequence", "1"));
		_fldif.add(new DDictEntry(12296, 592, 11, "TreatmentDate", "1"));
		_fldif.add(new DDictEntry(12296, 593, 12, "TreatmentTime", "1"));
		_fldif.add(new DDictEntry(12298, 21, 9, "NominalBeamEnergyUnit", "1"));
		_fldif.add(new DDictEntry(12298, 199, 9, "HighDoseTechniqueType", "1"));
		_fldif.add(new DDictEntry(12298, 238, 9, "CompensatorType", "1"));
		_fldif.add(new DDictEntry(20478, 1, 10, "MACParametersSequence", "1"));
		_fldif.add(new DDictEntry(65530, 65530, 10, "DigitalSignaturesSequence",
				"1"));
	}

	private static final void _mthdo() {
		_fldif.add(new DDictEntry(8, 150, 10,
				"ReferringPhysicianIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(8, 272, 10,
				"CodingSchemeIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(8, 274, 6, "CodingSchemeRegistry", "1"));
		_fldif.add(new DDictEntry(8, 276, 13, "CodingSchemeExternalID", "1"));
		_fldif.add(new DDictEntry(8, 277, 13, "CodingSchemeName", "1"));
		_fldif.add(new DDictEntry(8, 278, 13, "ResponsibleOrganization", "1"));
		_fldif.add(new DDictEntry(8, 4169, 10,
				"PhysicianSOfRecordIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(8, 4178, 10,
				"PerformingPhysicianIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(8, 4194, 10,
				"PhysicianSReadingStudyIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(8, 4210, 10, "OperatorIdentificationSequence",
				"1"));
		_fldif.add(new DDictEntry(8, 36871, 9, "FrameType", "4"));
		_fldif.add(new DDictEntry(8, 37010, 10,
				"ReferencedImageEvidenceSequence", "1"));
		_fldif
				.add(new DDictEntry(8, 37153, 10, "ReferencedRawDataSequence",
						"1"));
		_fldif.add(new DDictEntry(8, 37155, 2, "CreatorVersionUID", "1"));
		_fldif.add(new DDictEntry(8, 37156, 10, "DerivationImageSequence", "1"));
		_fldif.add(new DDictEntry(8, 37204, 10, "SourceImageEvidenceSequence",
				"1"));
		_fldif.add(new DDictEntry(8, 37381, 9, "PixelPresentation", "1"));
		_fldif.add(new DDictEntry(8, 37382, 9, "VolumetricProperties", "1"));
		_fldif.add(new DDictEntry(8, 37383, 9, "VolumeBasedCalculationTechnique",
				"1"));
		_fldif.add(new DDictEntry(8, 37384, 9, "ComplexImageComponent", "1"));
		_fldif.add(new DDictEntry(8, 37385, 9, "AcquisitionContrast", "1"));
		_fldif.add(new DDictEntry(8, 37397, 10, "DerivationCodeSequence", "1"));
		_fldif.add(new DDictEntry(8, 37431, 10,
				"ReferencedGrayscalePresentationStateSequence", "1"));
		_fldif.add(new DDictEntry(18, 16, 6, "ClinicalTrialSponsorName", "1"));
		_fldif.add(new DDictEntry(18, 32, 6, "ClinicalTrialProtocolID", "1"));
		_fldif.add(new DDictEntry(18, 33, 6, "ClinicalTrialProtocolName", "1"));
		_fldif.add(new DDictEntry(18, 48, 6, "ClinicalTrialSiteID", "1"));
		_fldif.add(new DDictEntry(18, 49, 6, "ClinicalTrialSiteName", "1"));
		_fldif.add(new DDictEntry(18, 64, 6, "ClinicalTrialSubjectID", "1"));
		_fldif
				.add(new DDictEntry(18, 66, 6, "ClinicalTrialSubjectReadingID",
						"1"));
		_fldif.add(new DDictEntry(18, 80, 6, "ClinicalTrialTimePointID", "1"));
		_fldif.add(new DDictEntry(18, 81, 13,
				"ClinicalTrialTimePointDescription", "1"));
		_fldif.add(new DDictEntry(18, 96, 6,
				"ClinicalTrialCoordinatingCenterName", "1"));
		_fldif.add(new DDictEntry(24, 24633, 19, "DopplerSampleVolumeXPosition",
				"1"));
		_fldif.add(new DDictEntry(24, 24635, 19, "DopplerSampleVolumeYPosition",
				"1"));
		_fldif.add(new DDictEntry(24, 24637, 19, "TMLinePositionX0", "1"));
		_fldif.add(new DDictEntry(24, 24639, 19, "TMLinePositionY0", "1"));
		_fldif.add(new DDictEntry(24, 24641, 19, "TMLinePositionX1", "1"));
		_fldif.add(new DDictEntry(24, 24643, 19, "TMLinePositionY1", "1"));
		_fldif.add(new DDictEntry(24, 36868, 9, "ContentQualification", "1"));
		_fldif.add(new DDictEntry(24, 36869, 7, "PulseSequenceName", "1"));
		_fldif
				.add(new DDictEntry(24, 36870, 10, "MRImagingModifierSequence",
						"1"));
		_fldif.add(new DDictEntry(24, 36872, 9, "EchoPulseSequence", "1"));
		_fldif.add(new DDictEntry(24, 36873, 9, "InversionRecovery", "1"));
		_fldif.add(new DDictEntry(24, 36880, 9, "FlowCompensation", "1"));
		_fldif.add(new DDictEntry(24, 36881, 9, "MultipleSpinEcho", "1"));
		_fldif.add(new DDictEntry(24, 36882, 9, "MultiPlanarExcitation", "1"));
		_fldif.add(new DDictEntry(24, 36884, 9, "PhaseContrast", "1"));
		_fldif.add(new DDictEntry(24, 36885, 9, "TimeOfFlightContrast", "1"));
		_fldif.add(new DDictEntry(24, 36886, 9, "Spoiling", "1"));
		_fldif.add(new DDictEntry(24, 36887, 9, "SteadyStatePulseSequence", "1"));
		_fldif.add(new DDictEntry(24, 36888, 9, "EchoPlanarPulseSequence", "1"));
		_fldif.add(new DDictEntry(24, 36889, 20, "TagAngleFirstAxis", "1"));
		_fldif.add(new DDictEntry(24, 36896, 9, "MagnetizationTransfer", "1"));
		_fldif.add(new DDictEntry(24, 36897, 9, "T2Preparation", "1"));
		_fldif.add(new DDictEntry(24, 36898, 9, "BloodSignalNulling", "1"));
		_fldif.add(new DDictEntry(24, 36900, 9, "SaturationRecovery", "1"));
		_fldif.add(new DDictEntry(24, 36901, 9, "SpectrallySelectedSuppression",
				"1"));
		_fldif.add(new DDictEntry(24, 36902, 9, "SpectrallySelectedExcitation",
				"1"));
		_fldif.add(new DDictEntry(24, 36903, 9, "SpatialPreSaturation", "1"));
		_fldif.add(new DDictEntry(24, 36904, 9, "Tagging", "1"));
		_fldif.add(new DDictEntry(24, 36905, 9, "OversamplingPhase", "1"));
		_fldif.add(new DDictEntry(24, 36898, 9, "BloodSignalNulling", "1"));
		_fldif
				.add(new DDictEntry(24, 36912, 20, "TagSpacingFirstDimension",
						"1"));
		_fldif
				.add(new DDictEntry(24, 36914, 9, "GeometryOfKSpaceTraversal",
						"1"));
		_fldif.add(new DDictEntry(24, 36915, 9, "SegmentedKSpaceTraversal", "1"));
		_fldif.add(new DDictEntry(24, 36916, 9,
				"RectilinearPhaseEncodeReordering", "1"));
		_fldif.add(new DDictEntry(24, 36917, 20, "TagThickness", "1"));
		_fldif.add(new DDictEntry(24, 36918, 9, "PartialFourierDirection", "1"));
		_fldif.add(new DDictEntry(24, 36919, 9,
				"CardiacSynchronizationTechnique", "1"));
		_fldif.add(new DDictEntry(24, 36929, 6, "ReceiveCoilManufacturerName",
				"1"));
		_fldif.add(new DDictEntry(24, 36930, 10, "MRReceiveCoilSequence", "1"));
		_fldif.add(new DDictEntry(24, 36931, 9, "ReceiveCoilType", "1"));
		_fldif.add(new DDictEntry(24, 36932, 9, "QuadratureReceiveCoil", "1"));
		_fldif.add(new DDictEntry(24, 36933, 10, "MultiCoilDefinitionSequence",
				"1"));
		_fldif.add(new DDictEntry(24, 36934, 6, "MultiCoilConfiguration", "1"));
		_fldif.add(new DDictEntry(24, 36935, 7, "MultiCoilElementName", "1"));
		_fldif.add(new DDictEntry(24, 36936, 9, "MultiCoilElementUsed", "1"));
		_fldif.add(new DDictEntry(24, 36937, 10, "MRTransmitCoilSequence", "1"));
		_fldif.add(new DDictEntry(24, 36944, 6, "TransmitCoilManufacturerName",
				"1"));
		_fldif.add(new DDictEntry(24, 36945, 9, "TransmitCoilType", "1"));
		_fldif.add(new DDictEntry(24, 36946, 20, "SpectralWidth", "1-2"));
		_fldif
				.add(new DDictEntry(24, 36947, 20, "ChemicalShiftReference",
						"1-2"));
		_fldif.add(new DDictEntry(24, 36948, 9, "VolumeLocalizationTechnique",
				"1"));
		_fldif.add(new DDictEntry(24, 36952, 3,
				"MRAcquisitionFrequencyEncodingSteps", "1"));
		_fldif.add(new DDictEntry(24, 36953, 9, "DeCoupling", "1"));
		_fldif.add(new DDictEntry(24, 36960, 9, "DeCoupledNucleus", "1-2"));
		_fldif.add(new DDictEntry(24, 36961, 20, "DeCouplingFrequency", "1-2"));
		_fldif.add(new DDictEntry(24, 36962, 9, "DeCouplingMethod", "1"));
		_fldif.add(new DDictEntry(24, 36963, 20,
				"DeCouplingChemicalShiftReference", "1-2"));
		_fldif.add(new DDictEntry(24, 36964, 9, "KSpaceFiltering", "1"));
		_fldif.add(new DDictEntry(24, 36965, 9, "TimeDomainFiltering", "1-2"));
		_fldif.add(new DDictEntry(24, 36966, 3, "NumberOfZeroFills", "1-2"));
		_fldif.add(new DDictEntry(24, 36967, 9, "BaselineCorrection", "1"));
		_fldif.add(new DDictEntry(24, 36969, 20,
				"ParallelReductionFactorInPlane", "1"));
		_fldif.add(new DDictEntry(24, 36976, 20, "CardiacRRIntervalSpecified",
				"1"));
		_fldif.add(new DDictEntry(24, 36979, 20, "AcquisitionDuration", "1"));
		_fldif
				.add(new DDictEntry(24, 36980, 28, "FrameAcquisitionDatetime",
						"1"));
		_fldif.add(new DDictEntry(24, 36981, 9, "DiffusionDirectionality", "1"));
		_fldif.add(new DDictEntry(24, 36982, 10,
				"DiffusionGradientDirectionSequence", "1"));
		_fldif.add(new DDictEntry(24, 36983, 9, "ParallelAcquisition", "1"));
		_fldif.add(new DDictEntry(24, 36984, 9, "ParallelAcquisitionTechnique",
				"1"));
		_fldif.add(new DDictEntry(24, 36985, 20, "InversionTimes", "1-n"));
		_fldif
				.add(new DDictEntry(24, 36992, 13, "MetaboliteMapDescription",
						"1"));
		_fldif.add(new DDictEntry(24, 36993, 9, "PartialFourier", "1"));
		_fldif.add(new DDictEntry(24, 36994, 20, "EffectiveEchoTime", "1"));
		_fldif.add(new DDictEntry(24, 36996, 10, "ChemicalShiftSequence", "1"));
		_fldif.add(new DDictEntry(24, 36997, 9, "CardiacSignalSource", "1"));
		_fldif.add(new DDictEntry(24, 36999, 20, "DiffusionBValue", "1"));
		_fldif.add(new DDictEntry(24, 37001, 20, "DiffusionGradientOrientation",
				"3"));
		_fldif
				.add(new DDictEntry(24, 37008, 20, "VelocityEncodingDirection",
						"3"));
		_fldif.add(new DDictEntry(24, 37009, 20, "VelocityEncodingMinimumValue",
				"1"));
		_fldif
				.add(new DDictEntry(24, 37011, 3, "NumberOfKSpaceTrajectories",
						"1"));
		_fldif.add(new DDictEntry(24, 37012, 9, "CoverageOfKSpace", "1"));
		_fldif.add(new DDictEntry(24, 37013, 1,
				"SpectroscopyAcquisitionPhaseRows", "1"));
		_fldif.add(new DDictEntry(24, 37016, 20, "TransmitterFrequency", "1-2"));
		_fldif.add(new DDictEntry(24, 37120, 9, "ResonantNucleus", "1-2"));
		_fldif.add(new DDictEntry(24, 37121, 9, "FrequencyCorrection", "1"));
		_fldif.add(new DDictEntry(24, 37123, 10,
				"MRSpectroscopyFOVGeometrySequence", "1"));
		_fldif.add(new DDictEntry(24, 37124, 20, "SlabThickness", "1"));
		_fldif.add(new DDictEntry(24, 37125, 20, "SlabOrientation", "3"));
		_fldif.add(new DDictEntry(24, 37126, 20, "MidSlabPosition", "3"));
		_fldif.add(new DDictEntry(24, 37127, 10, "MRSpatialSaturationSequence",
				"1"));
		_fldif.add(new DDictEntry(24, 37138, 10,
				"MRTimingAndRelatedParametersSequence", "1"));
		_fldif.add(new DDictEntry(24, 37140, 10, "MREchoSequence", "1"));
		_fldif.add(new DDictEntry(24, 37141, 10, "MRModifierSequence", "1"));
		_fldif.add(new DDictEntry(24, 37143, 10, "MRDiffusionSequence", "1"));
		_fldif.add(new DDictEntry(24, 37144, 10, "CardiacTriggerSequence", "1"));
		_fldif.add(new DDictEntry(24, 37145, 10, "MRAveragesSequence", "1"));
		_fldif.add(new DDictEntry(24, 37157, 10, "MRFOVGeometrySequence", "1"));
		_fldif.add(new DDictEntry(24, 37158, 10, "VolumeLocalizationSequence",
				"1"));
		_fldif.add(new DDictEntry(24, 37159, 1,
				"SpectroscopyAcquisitionDataColumns", "1"));
		_fldif.add(new DDictEntry(24, 37191, 9, "DiffusionAnisotropyType", "1"));
		_fldif.add(new DDictEntry(24, 37201, 28, "FrameReferenceDatetime", "1"));
		_fldif.add(new DDictEntry(24, 37202, 10, "MRMetaboliteMapSequence", "1"));
		_fldif.add(new DDictEntry(24, 37205, 20,
				"ParallelReductionFactorOutOfPlane", "1"));
		_fldif.add(new DDictEntry(24, 37209, 1,
				"SpectroscopyAcquisitionOutOfPlanePhaseSteps", "1"));
		_fldif.add(new DDictEntry(24, 37222, 9, "BulkMotionStatus", "1"));
		_fldif.add(new DDictEntry(24, 37224, 20,
				"ParallelReductionFactorSecondInPlane", "1"));
		_fldif.add(new DDictEntry(24, 37225, 9, "CardiacBeatRejectionTechnique",
				"1"));
		_fldif.add(new DDictEntry(24, 37232, 9,
				"RespiratoryMotionCompensationTechnique", "1"));
		_fldif.add(new DDictEntry(24, 37233, 9, "RespiratorySignalSource", "1"));
		_fldif.add(new DDictEntry(24, 37234, 9,
				"BulkMotionCompensationTechnique", "1"));
		_fldif.add(new DDictEntry(24, 37235, 9, "BulkMotionSignalSource", "1"));
		_fldif.add(new DDictEntry(24, 37236, 9, "ApplicableSafetyStandardAgency",
				"1"));
		_fldif.add(new DDictEntry(24, 37237, 6,
				"ApplicableSafetyStandardDescription", "1"));
		_fldif.add(new DDictEntry(24, 37238, 10, "OperatingModeSequence", "1"));
		_fldif.add(new DDictEntry(24, 37239, 9, "OperatingModeType", "1"));
		_fldif.add(new DDictEntry(24, 37240, 9, "OperatingMode", "1"));
		_fldif.add(new DDictEntry(24, 37241, 9,
				"SpecificAbsorptionRateDefinition", "1"));
		_fldif.add(new DDictEntry(24, 37248, 9, "GradientOutputType", "1"));
		_fldif.add(new DDictEntry(24, 37249, 20, "SpecificAbsorptionRateValue",
				"1"));
		_fldif.add(new DDictEntry(24, 37250, 20, "GradientOutput", "1"));
		_fldif
				.add(new DDictEntry(24, 37251, 9, "FlowCompensationDirection",
						"1"));
		_fldif.add(new DDictEntry(24, 37252, 20, "TaggingDelay", "1"));
		_fldif.add(new DDictEntry(24, 37269, 20,
				"ChemicalShiftsMinimumIntegrationLimit", "1"));
		_fldif.add(new DDictEntry(24, 37270, 20,
				"ChemicalShiftsMaximumIntegrationLimit", "1"));
		_fldif.add(new DDictEntry(24, 37271, 10, "MRVelocityEncodingSequence",
				"1"));
		_fldif
				.add(new DDictEntry(24, 37272, 9, "FirstOrderPhaseCorrection",
						"1"));
		_fldif.add(new DDictEntry(24, 37273, 9, "WaterReferencedPhaseCorrection",
				"1"));
		_fldif.add(new DDictEntry(24, 37376, 9, "MRSpectroscopyAcquisitionType",
				"1"));
		_fldif.add(new DDictEntry(24, 37396, 9, "RespiratoryCyclePosition", "1"));
		_fldif.add(new DDictEntry(24, 37399, 20, "VelocityEncodingMaximumValue",
				"1"));
		_fldif
				.add(new DDictEntry(24, 37400, 23, "TagSpacingSecondDimension",
						"1"));
		_fldif.add(new DDictEntry(24, 37401, 23, "TagAngleSecondAxis", "1"));
		_fldif
				.add(new DDictEntry(24, 37408, 20, "FrameAcquisitionDuration",
						"1"));
		_fldif
				.add(new DDictEntry(24, 37414, 10, "MRImageFrameTypeSequence",
						"1"));
		_fldif.add(new DDictEntry(24, 37415, 10,
				"MRSpectroscopyFrameTypeSequence", "1"));
		_fldif.add(new DDictEntry(24, 37425, 3,
				"MRAcquisitionPhaseEncodingStepsInPlane", "1"));
		_fldif.add(new DDictEntry(24, 37426, 3,
				"MRAcquisitionPhaseEncodingStepsOutOfPlane", "1"));
		_fldif.add(new DDictEntry(24, 37428, 1,
				"SpectroscopyAcquisitionPhaseColumns", "1"));
		_fldif.add(new DDictEntry(24, 37430, 9, "CardiacCyclePosition", "1"));
		_fldif.add(new DDictEntry(24, 37433, 10,
				"SpecificAbsorptionRateSequence", "1"));
		_fldif.add(new DDictEntry(24, 40961, 10, "ContributingEquipmentSequence",
				"1"));
		_fldif.add(new DDictEntry(24, 40962, 28, "ContributionDateTime", "1"));
		_fldif.add(new DDictEntry(24, 40963, 13, "ContributionDescription", "1"));
		_fldif.add(new DDictEntry(32, 36950, 7, "StackID", "1"));
		_fldif.add(new DDictEntry(32, 36951, 1, "InStackPositionNumber", "1"));
		_fldif.add(new DDictEntry(32, 36977, 10, "FrameAnatomySequence", "1"));
		_fldif.add(new DDictEntry(32, 36978, 9, "FrameLaterality", "1"));
		_fldif.add(new DDictEntry(32, 37137, 10, "FrameContentSequence", "1"));
		_fldif.add(new DDictEntry(32, 37139, 10, "PlanePositionSequence", "1"));
		_fldif
				.add(new DDictEntry(32, 37142, 10, "PlaneOrientationSequence",
						"1"));
		_fldif.add(new DDictEntry(32, 37160, 1, "TemporalPositionIndex", "1"));
		_fldif.add(new DDictEntry(32, 37203, 20, "TriggerDelayTime", "1"));
		_fldif.add(new DDictEntry(32, 37206, 3, "FrameAcquisitionNumber", "1"));
		_fldif.add(new DDictEntry(32, 37207, 1, "DimensionIndexValues", "1-n"));
		_fldif.add(new DDictEntry(32, 37208, 18, "FrameComments", "1"));
		_fldif.add(new DDictEntry(32, 37217, 2, "ConcatenationUID", "1"));
		_fldif.add(new DDictEntry(32, 37218, 3, "InConcatenationNumber", "1"));
		_fldif
				.add(new DDictEntry(32, 37219, 3, "InConcatenationTotalNumber",
						"1"));
		_fldif.add(new DDictEntry(32, 37220, 2, "DimensionOrganizationUID", "1"));
		_fldif.add(new DDictEntry(32, 37221, 5, "DimensionIndexPointer", "1"));
		_fldif.add(new DDictEntry(32, 37223, 5, "FunctionalGroupPointer", "1"));
		_fldif.add(new DDictEntry(32, 37395, 6, "DimensionIndexPrivateCreator",
				"1"));
		_fldif.add(new DDictEntry(32, 37409, 10, "DimensionOrganizationSequence",
				"1"));
		_fldif.add(new DDictEntry(32, 37410, 10, "DimensionIndexSequence", "1"));
		_fldif.add(new DDictEntry(32, 37416, 1, "ConcatenationFrameOffsetNumber",
				"1"));
		_fldif.add(new DDictEntry(32, 37432, 6, "FunctionalGroupPrivateCreator",
				"1"));
		_fldif.add(new DDictEntry(40, 36865, 1, "DataPointRows", "1"));
		_fldif.add(new DDictEntry(40, 36866, 1, "DataPointColumns", "1"));
		_fldif.add(new DDictEntry(40, 36867, 9, "SignalDomainColumns", "1"));
		_fldif.add(new DDictEntry(40, 37017, 3, "LargestMonochromePixelValue",
				"1"));
		_fldif.add(new DDictEntry(40, 37128, 9, "DataRepresentation", "1"));
		_fldif.add(new DDictEntry(40, 37136, 10, "PixelMeasuresSequence", "1"));
		_fldif.add(new DDictEntry(40, 37170, 10, "FrameVOILUTSequence", "1"));
		_fldif.add(new DDictEntry(40, 37189, 10,
				"PixelValueTransformationSequence", "1"));
		_fldif.add(new DDictEntry(40, 37429, 9, "SignalDomainRows", "1"));
		_fldif.add(new DDictEntry(50, 4145, 10,
				"RequestingPhysicianIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(64, 11, 10,
				"ScheduledPerformingPhysicianIdentificationSequence", "1"));
		_fldif
				.add(new DDictEntry(
						64,
						641,
						10,
						"PerformedProcedureStepDiscontinuationReasonCodeSequence",
						"1"));
		_fldif.add(new DDictEntry(64, 4113, 10,
				"IntendedRecipientsOfResultsIdentificationSequence", "1"));
		_fldif.add(new DDictEntry(64, 4353, 10,
				"PersonIdentificationCodeSequence", "1"));
		_fldif.add(new DDictEntry(64, 4354, 13, "Person?sAddress", "1"));
		_fldif
				.add(new DDictEntry(64, 4355, 6, "Person?sTelephoneNumbers",
						"1-n"));
		_fldif.add(new DDictEntry(64, 37014, 10, "RealWorldValueMappingSequence",
				"1"));
		_fldif.add(new DDictEntry(64, 37392, 23, "LUTLabel", "1"));
		_fldif.add(new DDictEntry(64, 37393, 21, "RealWorldValueLastValueMapped",
				"1"));
		_fldif.add(new DDictEntry(64, 37394, 20, "RealWorldValueLUTData", "1-n"));
		_fldif.add(new DDictEntry(64, 37398, 21,
				"RealWorldValueFirstValueMapped", "1"));
		_fldif.add(new DDictEntry(64, 37412, 20, "RealWorldValueIntercept", "1"));
		_fldif.add(new DDictEntry(64, 37413, 20, "RealWorldValueSlope", "1"));
		_fldif.add(new DDictEntry(64, 41729, 10,
				"NumericValueQualifierCodeSequence", "1"));
		_fldif.add(new DDictEntry(1024, 1280, 10, "EncryptedAttributesSequence",
				"1"));
		_fldif.add(new DDictEntry(1024, 1296, 2,
				"EncryptedContentTransferSyntaxUID", "1"));
		_fldif.add(new DDictEntry(1024, 1312, 8, "EncryptedContent", "1"));
		_fldif.add(new DDictEntry(1024, 1360, 10, "ModifiedAttributesSequence",
				"1"));
		_fldif.add(new DDictEntry(12298, 251, 9, "BlockMountingPosition", "1"));
		_fldif.add(new DDictEntry(12298, 736, 9, "CompensatorDivergence", "1"));
		_fldif.add(new DDictEntry(12298, 737, 9, "CompensatorMountingPosition",
				"1"));
		_fldif.add(new DDictEntry(12298, 738, 16, "SourceToCompensatorDistance",
				"1-n"));
		_fldif.add(new DDictEntry(20992, 37417, 10,
				"SharedFunctionalGroupsSequence", "1"));
		_fldif.add(new DDictEntry(20992, 37424, 10,
				"PerFrameFunctionalGroupsSequence", "1"));
		_fldif.add(new DDictEntry(22016, 16, 29,
				"FirstOrderPhaseCorrectionAngle", "1"));
		_fldif.add(new DDictEntry(22016, 32, 29, "SpectroscopyData", "1"));
	}

	static String getTypeName(int i) {
		return a[i];
	}

	public static String getTypeCodeName(int i) {
		int j;
		if (i >= _fldif.size())
			j = 0;
		else
			j = ((DDictEntry) _fldif.getAt(i)).type;
		return getTypeName(j);
	}

	public static int lookupDDict(int i, int j) {
		if ((i & 0xff00) == 20480)
			i = 20480;
		else if ((i & 0xff00) == 24576)
			i = 24576;
		int k = _flddo._mthif((i << 16) + j);
		if (k == 0x7fffffff)
			return 1189;
		else
			return k;
	}

	public static int getGroup(int i) {
		return ((DDictEntry) _fldif.getAt(i)).g;
	}

	public static int getElement(int i) {
		return ((DDictEntry) _fldif.getAt(i)).e;
	}

	public static int getTypeCode(int i) {
		if (i >= _fldif.size() || i == 1189)
			return 0;
		else
			return ((DDictEntry) _fldif.getAt(i)).type;
	}

	public static int getTypeCode(int i, int j) {
		int k = getTypeCode(lookupDDict(i, j));
		if (k == 0 && (i & 1) == 1)
			return j != 16 ? 0 : 6;
		else
			return k;
	}

	public static String getDescription(int i) {
		if (i >= _fldif.size())
			return "Undefined";
		else
			return ((DDictEntry) _fldif.getAt(i)).desc;
	}

	public static Enumeration ddictEntries() {
		return _fldif.enumerate();
	}

	public static int addEntry(DDictEntry ddictentry) throws DicomException {
		int i = (ddictentry.getGroup() << 16) + ddictentry.getElement();
		if (_flddo.containsD1D2(i)) {
			throw new DicomException(ddictentry.getGroup(), ddictentry
					.getElement(), "Entry already present in DDict");
		} else {
			int j = _fldif.size();
			_fldif.add(ddictentry);
			_flddo.a(i, j);
			return j;
		}
	}

	static int getTypeFixed(int i) {
		switch (i) {
		case 3: // '\003'
		case 21: // '\025'
		case 23: // '\027'
			return 2;

		case 1: // '\001'
		case 5: // '\005'
		case 17: // '\021'
		case 19: // '\023'
		case 26: // '\032'
			return 4;

		case 20: // '\024'
			return 8;

		case 2: // '\002'
		case 4: // '\004'
		case 6: // '\006'
		case 7: // '\007'
		case 8: // '\b'
		case 9: // '\t'
		case 10: // '\n'
		case 11: // '\013'
		case 12: // '\f'
		case 13: // '\r'
		case 14: // '\016'
		case 15: // '\017'
		case 16: // '\020'
		case 18: // '\022'
		case 22: // '\026'
		case 24: // '\030'
		case 25: // '\031'
		default:
			return 0;
		}
	}

	static int getTypeCode(String s) throws DicomException {
		if (s.equals("UL"))
			return 1;
		if (s.equals("UI"))
			return 2;
		if (s.equals("LO"))
			return 6;
		if (s.equals("US"))
			return 3;
		if (s.equals("AE"))
			return 4;
		if (s.equals("AT"))
			return 5;
		if (s.equals("SH"))
			return 7;
		if (s.equals("IS"))
			return 15;
		if (s.equals("OB"))
			return 8;
		if (s.equals("DA"))
			return 11;
		if (s.equals("TM"))
			return 12;
		if (s.equals("CS"))
			return 9;
		if (s.equals("SQ"))
			return 10;
		if (s.equals("LT"))
			return 18;
		if (s.equals("ST"))
			return 13;
		if (s.equals("PN"))
			return 14;
		if (s.equals("DS"))
			return 16;
		if (s.equals("AS"))
			return 17;
		if (s.equals("SL"))
			return 19;
		if (s.equals("FD"))
			return 20;
		if (s.equals("FL"))
			return 26;
		if (s.equals("OW"))
			return 24;
		if (s.equals("SS"))
			return 23;
		if (s.equals("UN"))
			return 0;
		if (s.equals("UT"))
			return 27;
		if (s.equals("DT"))
			return 28;
		if (s.equals("OF"))
			return 29;
		else
			throw new DicomException("getTypeCode: unexpected type " + s);
	}

	static {
		_mthif();
		a();
		_mthdo();
		_flddo = new DataChunk2Holder();
		_flddo.a(0, 0);
		_flddo.a(2, 1);
		_flddo.a(3, 2);
		_flddo.a(256, 3);
		_flddo.a(272, 4);
		_flddo.a(288, 5);
		_flddo.a(1536, 6);
		_flddo.a(1792, 7);
		_flddo.a(2048, 8);
		_flddo.a(2304, 9);
		_flddo.a(2305, 10);
		_flddo.a(2306, 11);
		_flddo.a(2307, 12);
		_flddo.a(4096, 13);
		_flddo.a(4097, 14);
		_flddo.a(4098, 15);
		_flddo.a(4101, 16);
		_flddo.a(4103, 17);
		_flddo.a(4104, 18);
		_flddo.a(4128, 19);
		_flddo.a(4129, 20);
		_flddo.a(4130, 21);
		_flddo.a(4131, 22);
		_flddo.a(4144, 23);
		_flddo.a(4145, 24);
		_flddo.a(20496, 25);
		_flddo.a(20512, 26);
		_flddo.a(0x20000, 27);
		_flddo.a(0x20001, 28);
		_flddo.a(0x20002, 29);
		_flddo.a(0x20003, 30);
		_flddo.a(0x20010, 31);
		_flddo.a(0x20012, 32);
		_flddo.a(0x20013, 33);
		_flddo.a(0x20016, 34);
		_flddo.a(0x20100, 35);
		_flddo.a(0x20102, 36);
		_flddo.a(0x40000, 37);
		_flddo.a(0x41130, 38);
		_flddo.a(0x41141, 39);
		_flddo.a(0x41142, 40);
		_flddo.a(0x41200, 41);
		_flddo.a(0x41202, 42);
		_flddo.a(0x41212, 43);
		_flddo.a(0x41220, 44);
		_flddo.a(0x41400, 45);
		_flddo.a(0x41410, 46);
		_flddo.a(0x41420, 47);
		_flddo.a(0x41430, 48);
		_flddo.a(0x41432, 49);
		_flddo.a(0x41500, 50);
		_flddo.a(0x41504, 51);
		_flddo.a(0x41510, 52);
		_flddo.a(0x41511, 53);
		_flddo.a(0x41512, 54);
		_flddo.a(0x41600, 55);
		_flddo.a(0x80000, 56);
		_flddo.a(0x80005, 57);
		_flddo.a(0x80008, 58);
		_flddo.a(0x80012, 59);
		_flddo.a(0x80013, 60);
		_flddo.a(0x80014, 61);
		_flddo.a(0x80016, 62);
		_flddo.a(0x80018, 63);
		_flddo.a(0x80020, 64);
		_flddo.a(0x80021, 65);
		_flddo.a(0x80022, 66);
		_flddo.a(0x80023, 67);
		_flddo.a(0x80024, 68);
		_flddo.a(0x80025, 69);
		_flddo.a(0x80030, 70);
		_flddo.a(0x80031, 71);
		_flddo.a(0x80032, 72);
		_flddo.a(0x80033, 73);
		_flddo.a(0x80034, 74);
		_flddo.a(0x80035, 75);
		_flddo.a(0x80042, 76);
		_flddo.a(0x80050, 77);
		_flddo.a(0x80052, 78);
		_flddo.a(0x80054, 79);
		_flddo.a(0x80058, 80);
		_flddo.a(0x80060, 81);
		_flddo.a(0x80061, 82);
		_flddo.a(0x80064, 83);
		_flddo.a(0x80070, 84);
		_flddo.a(0x80080, 85);
		_flddo.a(0x80081, 86);
		_flddo.a(0x80082, 87);
		_flddo.a(0x80090, 88);
		_flddo.a(0x80092, 89);
		_flddo.a(0x80094, 90);
		_flddo.a(0x80100, 91);
		_flddo.a(0x80102, 92);
		_flddo.a(0x80104, 93);
		_flddo.a(0x81010, 94);
		_flddo.a(0x81030, 95);
		_flddo.a(0x81032, 96);
		_flddo.a(0x8103e, 97);
		_flddo.a(0x81040, 98);
		_flddo.a(0x81048, 99);
		_flddo.a(0x81050, 100);
		_flddo.a(0x81060, 101);
		_flddo.a(0x81070, 102);
		_flddo.a(0x81080, 103);
		_flddo.a(0x81084, 104);
		_flddo.a(0x81090, 105);
		_flddo.a(0x81100, 106);
		_flddo.a(0x81110, 107);
		_flddo.a(0x81111, 108);
		_flddo.a(0x81115, 109);
		_flddo.a(0x81120, 110);
		_flddo.a(0x81125, 111);
		_flddo.a(0x81130, 112);
		_flddo.a(0x81140, 113);
		_flddo.a(0x81145, 114);
		_flddo.a(0x81150, 115);
		_flddo.a(0x81155, 116);
		_flddo.a(0x81160, 117);
		_flddo.a(0x81195, 118);
		_flddo.a(0x81197, 119);
		_flddo.a(0x81198, 120);
		_flddo.a(0x81199, 121);
		_flddo.a(0x82111, 122);
		_flddo.a(0x82112, 123);
		_flddo.a(0x82120, 124);
		_flddo.a(0x82122, 125);
		_flddo.a(0x82124, 126);
		_flddo.a(0x82128, 127);
		_flddo.a(0x82129, 128);
		_flddo.a(0x8212a, 129);
		_flddo.a(0x82130, 130);
		_flddo.a(0x82132, 131);
		_flddo.a(0x82142, 132);
		_flddo.a(0x82143, 133);
		_flddo.a(0x82144, 134);
		_flddo.a(0x82200, 135);
		_flddo.a(0x82204, 136);
		_flddo.a(0x82208, 137);
		_flddo.a(0x82218, 138);
		_flddo.a(0x82220, 139);
		_flddo.a(0x82228, 140);
		_flddo.a(0x82230, 141);
		_flddo.a(0x82240, 142);
		_flddo.a(0x82242, 143);
		_flddo.a(0x82244, 144);
		_flddo.a(0x82246, 145);
		_flddo.a(0x100000, 146);
		_flddo.a(0x100010, 147);
		_flddo.a(0x100020, 148);
		_flddo.a(0x100021, 149);
		_flddo.a(0x100030, 150);
		_flddo.a(0x100032, 151);
		_flddo.a(0x100040, 152);
		_flddo.a(0x100050, 153);
		_flddo.a(0x101000, 154);
		_flddo.a(0x101001, 155);
		_flddo.a(0x101005, 156);
		_flddo.a(0x101010, 157);
		_flddo.a(0x101020, 158);
		_flddo.a(0x101030, 159);
		_flddo.a(0x101040, 160);
		_flddo.a(0x101060, 161);
		_flddo.a(0x101080, 162);
		_flddo.a(0x101081, 163);
		_flddo.a(0x101090, 164);
		_flddo.a(0x102000, 165);
		_flddo.a(0x102110, 166);
		_flddo.a(0x102150, 167);
		_flddo.a(0x102152, 168);
		_flddo.a(0x102154, 169);
		_flddo.a(0x102160, 170);
		_flddo.a(0x102180, 171);
		_flddo.a(0x1021a0, 172);
		_flddo.a(0x1021b0, 173);
		_flddo.a(0x1021c0, 174);
		_flddo.a(0x1021d0, 175);
		_flddo.a(0x1021f0, 176);
		_flddo.a(0x104000, 177);
		_flddo.a(0x180000, 178);
		_flddo.a(0x180010, 179);
		_flddo.a(0x180012, 180);
		_flddo.a(0x180014, 181);
		_flddo.a(0x180015, 182);
		_flddo.a(0x180020, 183);
		_flddo.a(0x180021, 184);
		_flddo.a(0x180022, 185);
		_flddo.a(0x180023, 186);
		_flddo.a(0x180024, 187);
		_flddo.a(0x180025, 188);
		_flddo.a(0x180026, 189);
		_flddo.a(0x180027, 190);
		_flddo.a(0x180028, 191);
		_flddo.a(0x180029, 192);
		_flddo.a(0x18002a, 193);
		_flddo.a(0x180030, 194);
		_flddo.a(0x180031, 195);
		_flddo.a(0x180032, 196);
		_flddo.a(0x180033, 197);
		_flddo.a(0x180034, 198);
		_flddo.a(0x180035, 199);
		_flddo.a(0x180036, 200);
		_flddo.a(0x180037, 201);
		_flddo.a(0x180038, 202);
		_flddo.a(0x180039, 203);
		_flddo.a(0x180040, 204);
		_flddo.a(0x180050, 205);
		_flddo.a(0x180060, 206);
		_flddo.a(0x180070, 207);
		_flddo.a(0x180071, 208);
		_flddo.a(0x180072, 209);
		_flddo.a(0x180073, 210);
		_flddo.a(0x180074, 211);
		_flddo.a(0x180075, 212);
		_flddo.a(0x180080, 213);
		_flddo.a(0x180081, 214);
		_flddo.a(0x180082, 215);
		_flddo.a(0x180083, 216);
		_flddo.a(0x180084, 217);
		_flddo.a(0x180085, 218);
		_flddo.a(0x180086, 219);
		_flddo.a(0x180087, 220);
		_flddo.a(0x180088, 221);
		_flddo.a(0x180089, 222);
		_flddo.a(0x180090, 223);
		_flddo.a(0x180091, 224);
		_flddo.a(0x180093, 225);
		_flddo.a(0x180094, 226);
		_flddo.a(0x180095, 227);
		_flddo.a(0x181000, 228);
		_flddo.a(0x181004, 229);
		_flddo.a(0x181010, 230);
		_flddo.a(0x181012, 231);
		_flddo.a(0x181014, 232);
		_flddo.a(0x181016, 233);
		_flddo.a(0x181018, 234);
		_flddo.a(0x181019, 235);
		_flddo.a(0x181020, 236);
		_flddo.a(0x181022, 237);
		_flddo.a(0x181023, 238);
		_flddo.a(0x181030, 239);
		_flddo.a(0x181040, 240);
		_flddo.a(0x181041, 241);
		_flddo.a(0x181042, 242);
		_flddo.a(0x181043, 243);
		_flddo.a(0x181044, 244);
		_flddo.a(0x181045, 245);
		_flddo.a(0x181046, 246);
		_flddo.a(0x181047, 247);
		_flddo.a(0x181048, 248);
		_flddo.a(0x181049, 249);
		_flddo.a(0x181050, 250);
		_flddo.a(0x181060, 251);
		_flddo.a(0x181061, 252);
		_flddo.a(0x181062, 253);
		_flddo.a(0x181063, 254);
		_flddo.a(0x181064, 255);
		_flddo.a(0x181065, 256);
		_flddo.a(0x181066, 257);
		_flddo.a(0x181070, 258);
		_flddo.a(0x181071, 259);
		_flddo.a(0x181072, 260);
		_flddo.a(0x181073, 261);
		_flddo.a(0x181074, 262);
		_flddo.a(0x181075, 263);
		_flddo.a(0x181076, 264);
		_flddo.a(0x181077, 265);
		_flddo.a(0x181080, 266);
		_flddo.a(0x181081, 267);
		_flddo.a(0x181082, 268);
		_flddo.a(0x181083, 269);
		_flddo.a(0x181084, 270);
		_flddo.a(0x181085, 271);
		_flddo.a(0x181086, 272);
		_flddo.a(0x181088, 273);
		_flddo.a(0x181090, 274);
		_flddo.a(0x181094, 275);
		_flddo.a(0x181100, 276);
		_flddo.a(0x181110, 277);
		_flddo.a(0x181111, 278);
		_flddo.a(0x181114, 279);
		_flddo.a(0x181120, 280);
		_flddo.a(0x181121, 281);
		_flddo.a(0x181130, 282);
		_flddo.a(0x181131, 283);
		_flddo.a(0x181134, 284);
		_flddo.a(0x181135, 285);
		_flddo.a(0x181136, 286);
		_flddo.a(0x181137, 287);
		_flddo.a(0x181138, 288);
		_flddo.a(0x181140, 289);
		_flddo.a(0x181141, 290);
		_flddo.a(0x181142, 291);
		_flddo.a(0x181143, 292);
		_flddo.a(0x181144, 293);
		_flddo.a(0x181145, 294);
		_flddo.a(0x181146, 295);
		_flddo.a(0x181147, 296);
		_flddo.a(0x181149, 297);
		_flddo.a(0x181150, 298);
		_flddo.a(0x181151, 299);
		_flddo.a(0x181152, 300);
		_flddo.a(0x181154, 301);
		_flddo.a(0x181155, 302);
		_flddo.a(0x18115a, 303);
		_flddo.a(0x18115e, 304);
		_flddo.a(0x181160, 305);
		_flddo.a(0x181161, 306);
		_flddo.a(0x181162, 307);
		_flddo.a(0x181164, 308);
		_flddo.a(0x181166, 309);
		_flddo.a(0x181170, 310);
		_flddo.a(0x181180, 311);
		_flddo.a(0x181181, 312);
		_flddo.a(0x181182, 313);
		_flddo.a(0x181183, 314);
		_flddo.a(0x181184, 315);
		_flddo.a(0x181190, 316);
		_flddo.a(0x181200, 317);
		_flddo.a(0x181201, 318);
		_flddo.a(0x181210, 319);
		_flddo.a(0x181242, 320);
		_flddo.a(0x181243, 321);
		_flddo.a(0x181244, 322);
		_flddo.a(0x181250, 323);
		_flddo.a(0x181251, 324);
		_flddo.a(0x181260, 325);
		_flddo.a(0x181261, 326);
		_flddo.a(0x181300, 327);
		_flddo.a(0x181301, 328);
		_flddo.a(0x181302, 329);
		_flddo.a(0x181310, 330);
		_flddo.a(0x181312, 331);
		_flddo.a(0x181314, 332);
		_flddo.a(0x181315, 333);
		_flddo.a(0x181316, 334);
		_flddo.a(0x181318, 335);
		_flddo.a(0x181400, 336);
		_flddo.a(0x181401, 337);
		_flddo.a(0x181402, 338);
		_flddo.a(0x181403, 339);
		_flddo.a(0x181404, 340);
		_flddo.a(0x181405, 341);
		_flddo.a(0x181460, 342);
		_flddo.a(0x181470, 343);
		_flddo.a(0x181480, 344);
		_flddo.a(0x181500, 345);
		_flddo.a(0x181510, 346);
		_flddo.a(0x181511, 347);
		_flddo.a(0x181520, 348);
		_flddo.a(0x181521, 349);
		_flddo.a(0x181530, 350);
		_flddo.a(0x181531, 351);
		_flddo.a(0x181600, 352);
		_flddo.a(0x181602, 353);
		_flddo.a(0x181604, 354);
		_flddo.a(0x181606, 355);
		_flddo.a(0x181608, 356);
		_flddo.a(0x181610, 357);
		_flddo.a(0x181612, 358);
		_flddo.a(0x181620, 359);
		_flddo.a(0x181700, 360);
		_flddo.a(0x181702, 361);
		_flddo.a(0x181704, 362);
		_flddo.a(0x181706, 363);
		_flddo.a(0x181708, 364);
		_flddo.a(0x181710, 365);
		_flddo.a(0x181712, 366);
		_flddo.a(0x181720, 367);
		_flddo.a(0x185000, 368);
		_flddo.a(0x185010, 369);
		_flddo.a(0x185012, 370);
		_flddo.a(0x185020, 371);
		_flddo.a(0x185021, 372);
		_flddo.a(0x185022, 373);
		_flddo.a(0x185024, 374);
		_flddo.a(0x185026, 375);
		_flddo.a(0x185027, 376);
		_flddo.a(0x185028, 377);
		_flddo.a(0x185029, 378);
		_flddo.a(0x185050, 379);
		_flddo.a(0x185100, 380);
		_flddo.a(0x185101, 381);
		_flddo.a(0x185210, 382);
		_flddo.a(0x185212, 383);
		_flddo.a(0x186000, 384);
		_flddo.a(0x186011, 385);
		_flddo.a(0x186012, 386);
		_flddo.a(0x186014, 387);
		_flddo.a(0x186016, 388);
		_flddo.a(0x186018, 389);
		_flddo.a(0x18601a, 390);
		_flddo.a(0x18601c, 391);
		_flddo.a(0x18601e, 392);
		_flddo.a(0x186020, 393);
		_flddo.a(0x186022, 394);
		_flddo.a(0x186024, 395);
		_flddo.a(0x186026, 396);
		_flddo.a(0x186028, 397);
		_flddo.a(0x18602a, 398);
		_flddo.a(0x18602c, 399);
		_flddo.a(0x18602e, 400);
		_flddo.a(0x186030, 401);
		_flddo.a(0x186031, 402);
		_flddo.a(0x186032, 403);
		_flddo.a(0x186034, 404);
		_flddo.a(0x186036, 405);
		_flddo.a(0x186038, 406);
		_flddo.a(0x18603a, 407);
		_flddo.a(0x18603c, 408);
		_flddo.a(0x18603e, 409);
		_flddo.a(0x186040, 410);
		_flddo.a(0x186042, 411);
		_flddo.a(0x186044, 412);
		_flddo.a(0x186046, 413);
		_flddo.a(0x186048, 414);
		_flddo.a(0x18604a, 415);
		_flddo.a(0x18604c, 416);
		_flddo.a(0x18604e, 417);
		_flddo.a(0x186050, 418);
		_flddo.a(0x186052, 419);
		_flddo.a(0x186054, 420);
		_flddo.a(0x186056, 421);
		_flddo.a(0x186058, 422);
		_flddo.a(0x18605a, 423);
		_flddo.a(0x200000, 424);
		_flddo.a(0x20000d, 425);
		_flddo.a(0x20000e, 426);
		_flddo.a(0x200010, 427);
		_flddo.a(0x200011, 428);
		_flddo.a(0x200012, 429);
		_flddo.a(0x200013, 430);
		_flddo.a(0x200014, 431);
		_flddo.a(0x200015, 432);
		_flddo.a(0x200016, 433);
		_flddo.a(0x200017, 434);
		_flddo.a(0x200018, 435);
		_flddo.a(0x200020, 436);
		_flddo.a(0x200022, 437);
		_flddo.a(0x200024, 438);
		_flddo.a(0x200026, 439);
		_flddo.a(0x200032, 440);
		_flddo.a(0x200037, 441);
		_flddo.a(0x200052, 442);
		_flddo.a(0x200060, 443);
		_flddo.a(0x200100, 444);
		_flddo.a(0x200105, 445);
		_flddo.a(0x200110, 446);
		_flddo.a(0x201000, 447);
		_flddo.a(0x201002, 448);
		_flddo.a(0x201004, 449);
		_flddo.a(0x201040, 450);
		_flddo.a(0x201041, 451);
		_flddo.a(0x201070, 452);
		_flddo.a(0x201200, 453);
		_flddo.a(0x201202, 454);
		_flddo.a(0x201204, 455);
		_flddo.a(0x201206, 456);
		_flddo.a(0x201208, 457);
		_flddo.a(0x201209, 458);
		_flddo.a(0x204000, 459);
		_flddo.a(0x280000, 460);
		_flddo.a(0x280002, 461);
		_flddo.a(0x280004, 462);
		_flddo.a(0x280006, 463);
		_flddo.a(0x280008, 464);
		_flddo.a(0x280009, 465);
		_flddo.a(0x280010, 466);
		_flddo.a(0x280011, 467);
		_flddo.a(0x280012, 468);
		_flddo.a(0x280014, 469);
		_flddo.a(0x280030, 470);
		_flddo.a(0x280031, 471);
		_flddo.a(0x280032, 472);
		_flddo.a(0x280034, 473);
		_flddo.a(0x280051, 474);
		_flddo.a(0x280100, 475);
		_flddo.a(0x280101, 476);
		_flddo.a(0x280102, 477);
		_flddo.a(0x280103, 478);
		_flddo.a(0x280106, 479);
		_flddo.a(0x280107, 480);
		_flddo.a(0x280108, 481);
		_flddo.a(0x280109, 482);
		_flddo.a(0x280110, 483);
		_flddo.a(0x280111, 484);
		_flddo.a(0x280120, 485);
		_flddo.a(0x281040, 486);
		_flddo.a(0x281050, 487);
		_flddo.a(0x281051, 488);
		_flddo.a(0x281052, 489);
		_flddo.a(0x281053, 490);
		_flddo.a(0x281054, 491);
		_flddo.a(0x281055, 492);
		_flddo.a(0x281090, 493);
		_flddo.a(0x281101, 494);
		_flddo.a(0x281102, 495);
		_flddo.a(0x281103, 496);
		_flddo.a(0x281199, 497);
		_flddo.a(0x281201, 498);
		_flddo.a(0x281202, 499);
		_flddo.a(0x281203, 500);
		_flddo.a(0x281221, 501);
		_flddo.a(0x281222, 502);
		_flddo.a(0x281223, 503);
		_flddo.a(0x282110, 504);
		_flddo.a(0x283000, 505);
		_flddo.a(0x283002, 506);
		_flddo.a(0x283003, 507);
		_flddo.a(0x283004, 508);
		_flddo.a(0x283006, 509);
		_flddo.a(0x283010, 510);
		_flddo.a(0x285000, 511);
		_flddo.a(0x286010, 512);
		_flddo.a(0x286020, 513);
		_flddo.a(0x286022, 514);
		_flddo.a(0x286030, 515);
		_flddo.a(0x286040, 516);
		_flddo.a(0x286100, 517);
		_flddo.a(0x286101, 518);
		_flddo.a(0x286102, 519);
		_flddo.a(0x286110, 520);
		_flddo.a(0x286112, 521);
		_flddo.a(0x286114, 522);
		_flddo.a(0x286120, 523);
		_flddo.a(0x286190, 524);
		_flddo.a(0x320000, 525);
		_flddo.a(0x32000a, 526);
		_flddo.a(0x32000c, 527);
		_flddo.a(0x320012, 528);
		_flddo.a(0x320032, 529);
		_flddo.a(0x320033, 530);
		_flddo.a(0x320034, 531);
		_flddo.a(0x320035, 532);
		_flddo.a(0x321000, 533);
		_flddo.a(0x321001, 534);
		_flddo.a(0x321010, 535);
		_flddo.a(0x321011, 536);
		_flddo.a(0x321020, 537);
		_flddo.a(0x321021, 538);
		_flddo.a(0x321030, 539);
		_flddo.a(0x321032, 540);
		_flddo.a(0x321033, 541);
		_flddo.a(0x321040, 542);
		_flddo.a(0x321041, 543);
		_flddo.a(0x321050, 544);
		_flddo.a(0x321051, 545);
		_flddo.a(0x321055, 546);
		_flddo.a(0x321060, 547);
		_flddo.a(0x321064, 548);
		_flddo.a(0x321070, 549);
		_flddo.a(0x324000, 550);
		_flddo.a(0x380000, 551);
		_flddo.a(0x380004, 552);
		_flddo.a(0x380008, 553);
		_flddo.a(0x380010, 554);
		_flddo.a(0x380011, 555);
		_flddo.a(0x380016, 556);
		_flddo.a(0x38001a, 557);
		_flddo.a(0x38001b, 558);
		_flddo.a(0x38001c, 559);
		_flddo.a(0x38001d, 560);
		_flddo.a(0x38001e, 561);
		_flddo.a(0x380020, 562);
		_flddo.a(0x380021, 563);
		_flddo.a(0x380030, 564);
		_flddo.a(0x380032, 565);
		_flddo.a(0x380040, 566);
		_flddo.a(0x380044, 567);
		_flddo.a(0x380050, 568);
		_flddo.a(0x380300, 569);
		_flddo.a(0x380400, 570);
		_flddo.a(0x380500, 571);
		_flddo.a(0x384000, 572);
		_flddo.a(0x400000, 573);
		_flddo.a(0x400001, 574);
		_flddo.a(0x400002, 575);
		_flddo.a(0x400003, 576);
		_flddo.a(0x400004, 577);
		_flddo.a(0x400005, 578);
		_flddo.a(0x400006, 579);
		_flddo.a(0x400007, 580);
		_flddo.a(0x400008, 581);
		_flddo.a(0x400009, 582);
		_flddo.a(0x400010, 583);
		_flddo.a(0x400011, 584);
		_flddo.a(0x400012, 585);
		_flddo.a(0x400020, 586);
		_flddo.a(0x400100, 587);
		_flddo.a(0x400400, 588);
		_flddo.a(0x401001, 589);
		_flddo.a(0x401002, 590);
		_flddo.a(0x401003, 591);
		_flddo.a(0x401004, 592);
		_flddo.a(0x401005, 593);
		_flddo.a(0x401006, 594);
		_flddo.a(0x401007, 595);
		_flddo.a(0x401008, 596);
		_flddo.a(0x401009, 597);
		_flddo.a(0x401010, 598);
		_flddo.a(0x401400, 599);
		_flddo.a(0x402001, 600);
		_flddo.a(0x402004, 601);
		_flddo.a(0x402005, 602);
		_flddo.a(0x402006, 603);
		_flddo.a(0x402007, 604);
		_flddo.a(0x402008, 605);
		_flddo.a(0x402009, 606);
		_flddo.a(0x402010, 607);
		_flddo.a(0x402400, 608);
		_flddo.a(0x403001, 609);
		_flddo.a(0x500004, 610);
		_flddo.a(0x500010, 611);
		_flddo.a(0x500014, 612);
		_flddo.a(0x500016, 613);
		_flddo.a(0x500017, 614);
		_flddo.a(0x500018, 615);
		_flddo.a(0x500019, 616);
		_flddo.a(0x500020, 617);
		_flddo.a(0x540000, 618);
		_flddo.a(0x540010, 619);
		_flddo.a(0x540011, 620);
		_flddo.a(0x540012, 621);
		_flddo.a(0x540013, 622);
		_flddo.a(0x540014, 623);
		_flddo.a(0x540015, 624);
		_flddo.a(0x540016, 625);
		_flddo.a(0x540017, 626);
		_flddo.a(0x540018, 627);
		_flddo.a(0x540020, 628);
		_flddo.a(0x540021, 629);
		_flddo.a(0x540022, 630);
		_flddo.a(0x540030, 631);
		_flddo.a(0x540031, 632);
		_flddo.a(0x540032, 633);
		_flddo.a(0x540033, 634);
		_flddo.a(0x540036, 635);
		_flddo.a(0x540038, 636);
		_flddo.a(0x540050, 637);
		_flddo.a(0x540051, 638);
		_flddo.a(0x540052, 639);
		_flddo.a(0x540053, 640);
		_flddo.a(0x540060, 641);
		_flddo.a(0x540061, 642);
		_flddo.a(0x540062, 643);
		_flddo.a(0x540063, 644);
		_flddo.a(0x540070, 645);
		_flddo.a(0x540071, 646);
		_flddo.a(0x540072, 647);
		_flddo.a(0x540073, 648);
		_flddo.a(0x540080, 649);
		_flddo.a(0x540081, 650);
		_flddo.a(0x540090, 651);
		_flddo.a(0x540100, 652);
		_flddo.a(0x540101, 653);
		_flddo.a(0x540200, 654);
		_flddo.a(0x540202, 655);
		_flddo.a(0x540210, 656);
		_flddo.a(0x540211, 657);
		_flddo.a(0x540220, 658);
		_flddo.a(0x540222, 659);
		_flddo.a(0x540300, 660);
		_flddo.a(0x540302, 661);
		_flddo.a(0x540304, 662);
		_flddo.a(0x540306, 663);
		_flddo.a(0x540308, 664);
		_flddo.a(0x540400, 665);
		_flddo.a(0x540410, 666);
		_flddo.a(0x540412, 667);
		_flddo.a(0x540414, 668);
		_flddo.a(0x541000, 669);
		_flddo.a(0x541001, 670);
		_flddo.a(0x541002, 671);
		_flddo.a(0x541004, 672);
		_flddo.a(0x541100, 673);
		_flddo.a(0x541101, 674);
		_flddo.a(0x541102, 675);
		_flddo.a(0x541103, 676);
		_flddo.a(0x541104, 677);
		_flddo.a(0x541105, 678);
		_flddo.a(0x541200, 679);
		_flddo.a(0x541201, 680);
		_flddo.a(0x541202, 681);
		_flddo.a(0x541203, 682);
		_flddo.a(0x541210, 683);
		_flddo.a(0x541220, 684);
		_flddo.a(0x541300, 685);
		_flddo.a(0x541310, 686);
		_flddo.a(0x541311, 687);
		_flddo.a(0x541320, 688);
		_flddo.a(0x541321, 689);
		_flddo.a(0x541322, 690);
		_flddo.a(0x541323, 691);
		_flddo.a(0x541324, 692);
		_flddo.a(0x541330, 693);
		_flddo.a(0x541400, 694);
		_flddo.a(0x541401, 695);
		_flddo.a(0x880000, 696);
		_flddo.a(0x880130, 697);
		_flddo.a(0x880140, 698);
		_flddo.a(0x880200, 699);
		_flddo.a(0x880904, 700);
		_flddo.a(0x880906, 701);
		_flddo.a(0x880910, 702);
		_flddo.a(0x880912, 703);
		_flddo.a(0x20000000, 704);
		_flddo.a(0x20000010, 705);
		_flddo.a(0x20000020, 706);
		_flddo.a(0x20000030, 707);
		_flddo.a(0x20000040, 708);
		_flddo.a(0x20000050, 709);
		_flddo.a(0x20000060, 710);
		_flddo.a(0x20000500, 711);
		_flddo.a(0x20100000, 712);
		_flddo.a(0x20100010, 713);
		_flddo.a(0x20100030, 714);
		_flddo.a(0x20100040, 715);
		_flddo.a(0x20100050, 716);
		_flddo.a(0x20100060, 717);
		_flddo.a(0x20100080, 718);
		_flddo.a(0x20100100, 719);
		_flddo.a(0x20100110, 720);
		_flddo.a(0x20100120, 721);
		_flddo.a(0x20100130, 722);
		_flddo.a(0x20100140, 723);
		_flddo.a(0x20100150, 724);
		_flddo.a(0x20100500, 725);
		_flddo.a(0x20100510, 726);
		_flddo.a(0x20100520, 727);
		_flddo.a(0x20200000, 728);
		_flddo.a(0x20200010, 729);
		_flddo.a(0x20200020, 730);
		_flddo.a(0x20200030, 731);
		_flddo.a(0x20200110, 732);
		_flddo.a(0x20200111, 733);
		_flddo.a(0x20200130, 734);
		_flddo.a(0x20200140, 735);
		_flddo.a(0x20300000, 736);
		_flddo.a(0x20300010, 737);
		_flddo.a(0x20300020, 738);
		_flddo.a(0x20400000, 739);
		_flddo.a(0x20400010, 740);
		_flddo.a(0x20400011, 741);
		_flddo.a(0x20400060, 742);
		_flddo.a(0x20400070, 743);
		_flddo.a(0x20400080, 744);
		_flddo.a(0x20400090, 745);
		_flddo.a(0x20400100, 746);
		_flddo.a(0x20400500, 747);
		_flddo.a(0x21000000, 748);
		_flddo.a(0x21000010, 749);
		_flddo.a(0x21000020, 750);
		_flddo.a(0x21000030, 751);
		_flddo.a(0x21000040, 752);
		_flddo.a(0x21000050, 753);
		_flddo.a(0x21000070, 754);
		_flddo.a(0x21000140, 755);
		_flddo.a(0x21000160, 756);
		_flddo.a(0x21000170, 757);
		_flddo.a(0x21000500, 758);
		_flddo.a(0x21100000, 759);
		_flddo.a(0x21100010, 760);
		_flddo.a(0x21100020, 761);
		_flddo.a(0x21100030, 762);
		_flddo.a(0x21100099, 763);
		_flddo.a(0x21200010, 764);
		_flddo.a(0x21200050, 765);
		_flddo.a(0x21200070, 766);
		_flddo.a(0x30020002, 767);
		_flddo.a(0x30020003, 768);
		_flddo.a(0x30020004, 769);
		_flddo.a(0x3002000a, 770);
		_flddo.a(0x3002000c, 771);
		_flddo.a(0x3002000e, 772);
		_flddo.a(0x30020010, 773);
		_flddo.a(0x30020011, 774);
		_flddo.a(0x30020012, 775);
		_flddo.a(0x30020020, 776);
		_flddo.a(0x30020022, 777);
		_flddo.a(0x30020024, 778);
		_flddo.a(0x30020026, 779);
		_flddo.a(0x30020028, 780);
		_flddo.a(0x30020029, 781);
		_flddo.a(0x30020030, 782);
		_flddo.a(0x30020032, 783);
		_flddo.a(0x30040001, 784);
		_flddo.a(0x30040002, 785);
		_flddo.a(0x30040004, 786);
		_flddo.a(0x30040006, 787);
		_flddo.a(0x30040008, 788);
		_flddo.a(0x3004000a, 789);
		_flddo.a(0x3004000c, 790);
		_flddo.a(0x3004000e, 791);
		_flddo.a(0x30040010, 792);
		_flddo.a(0x30040012, 793);
		_flddo.a(0x30040040, 794);
		_flddo.a(0x30040042, 795);
		_flddo.a(0x30040050, 796);
		_flddo.a(0x30040052, 797);
		_flddo.a(0x30040054, 798);
		_flddo.a(0x30040056, 799);
		_flddo.a(0x30040058, 800);
		_flddo.a(0x30040060, 801);
		_flddo.a(0x30040062, 802);
		_flddo.a(0x30040070, 803);
		_flddo.a(0x30040072, 804);
		_flddo.a(0x30040074, 805);
		_flddo.a(0x30060002, 806);
		_flddo.a(0x30060004, 807);
		_flddo.a(0x30060006, 808);
		_flddo.a(0x30060008, 809);
		_flddo.a(0x30060009, 810);
		_flddo.a(0x30060010, 811);
		_flddo.a(0x30060012, 812);
		_flddo.a(0x30060014, 813);
		_flddo.a(0x30060016, 814);
		_flddo.a(0x30060020, 815);
		_flddo.a(0x30060022, 816);
		_flddo.a(0x30060024, 817);
		_flddo.a(0x30060026, 818);
		_flddo.a(0x30060028, 819);
		_flddo.a(0x3006002a, 820);
		_flddo.a(0x3006002c, 821);
		_flddo.a(0x30060030, 822);
		_flddo.a(0x30060033, 823);
		_flddo.a(0x30060036, 824);
		_flddo.a(0x30060038, 825);
		_flddo.a(0x30060039, 826);
		_flddo.a(0x30060040, 827);
		_flddo.a(0x30060042, 828);
		_flddo.a(0x30060044, 829);
		_flddo.a(0x30060045, 830);
		_flddo.a(0x30060046, 831);
		_flddo.a(0x30060050, 832);
		_flddo.a(0x30060080, 833);
		_flddo.a(0x30060082, 834);
		_flddo.a(0x30060084, 835);
		_flddo.a(0x30060085, 836);
		_flddo.a(0x30060086, 837);
		_flddo.a(0x30060088, 838);
		_flddo.a(0x300600a0, 839);
		_flddo.a(0x300600a4, 840);
		_flddo.a(0x300600a6, 841);
		_flddo.a(0x300600b0, 842);
		_flddo.a(0x300600b2, 843);
		_flddo.a(0x300600b4, 844);
		_flddo.a(0x300600c0, 845);
		_flddo.a(0x300600c2, 846);
		_flddo.a(0x300600c4, 847);
		_flddo.a(0x300600c6, 848);
		_flddo.a(0x300600c8, 849);
		_flddo.a(0x300a0002, 850);
		_flddo.a(0x300a0003, 851);
		_flddo.a(0x300a0004, 852);
		_flddo.a(0x300a0006, 853);
		_flddo.a(0x300a0007, 854);
		_flddo.a(0x300a0009, 855);
		_flddo.a(0x300a000a, 856);
		_flddo.a(0x300a000b, 857);
		_flddo.a(0x300a000c, 858);
		_flddo.a(0x300a000e, 859);
		_flddo.a(0x300a0010, 860);
		_flddo.a(0x300a0012, 861);
		_flddo.a(0x300a0014, 862);
		_flddo.a(0x300a0016, 863);
		_flddo.a(0x300a0018, 864);
		_flddo.a(0x300a001a, 865);
		_flddo.a(0x300a0020, 866);
		_flddo.a(0x300a0021, 867);
		_flddo.a(0x300a0022, 868);
		_flddo.a(0x300a0023, 869);
		_flddo.a(0x300a0025, 870);
		_flddo.a(0x300a0026, 871);
		_flddo.a(0x300a0027, 872);
		_flddo.a(0x300a0028, 873);
		_flddo.a(0x300a002a, 874);
		_flddo.a(0x300a002b, 875);
		_flddo.a(0x300a002c, 876);
		_flddo.a(0x300a002d, 877);
		_flddo.a(0x300a0040, 878);
		_flddo.a(0x300a0042, 879);
		_flddo.a(0x300a0043, 880);
		_flddo.a(0x300a0044, 881);
		_flddo.a(0x300a0046, 882);
		_flddo.a(0x300a0048, 883);
		_flddo.a(0x300a004a, 884);
		_flddo.a(0x300a004c, 885);
		_flddo.a(0x300a004e, 886);
		_flddo.a(0x300a0051, 887);
		_flddo.a(0x300a0052, 888);
		_flddo.a(0x300a0053, 889);
		_flddo.a(0x300a0055, 890);
		_flddo.a(0x300a0070, 891);
		_flddo.a(0x300a0071, 892);
		_flddo.a(0x300a0078, 893);
		_flddo.a(0x300a0079, 894);
		_flddo.a(0x300a007a, 895);
		_flddo.a(0x300a007b, 896);
		_flddo.a(0x300a0080, 897);
		_flddo.a(0x300a0082, 898);
		_flddo.a(0x300a0084, 899);
		_flddo.a(0x300a0086, 900);
		_flddo.a(0x300a00a0, 901);
		_flddo.a(0x300a00a2, 902);
		_flddo.a(0x300a00a4, 903);
		_flddo.a(0x300a00b0, 904);
		_flddo.a(0x300a00b2, 905);
		_flddo.a(0x300a00b3, 906);
		_flddo.a(0x300a00b4, 907);
		_flddo.a(0x300a00b6, 908);
		_flddo.a(0x300a00b8, 909);
		_flddo.a(0x300a00ba, 910);
		_flddo.a(0x300a00bc, 911);
		_flddo.a(0x300a00be, 912);
		_flddo.a(0x300a00c0, 913);
		_flddo.a(0x300a00c2, 914);
		_flddo.a(0x300a00c3, 915);
		_flddo.a(0x300a00c4, 916);
		_flddo.a(0x300a00c6, 917);
		_flddo.a(0x300a00c8, 918);
		_flddo.a(0x300a00ca, 919);
		_flddo.a(0x300a00cc, 920);
		_flddo.a(0x300a00ce, 921);
		_flddo.a(0x300a00d0, 922);
		_flddo.a(0x300a00d1, 923);
		_flddo.a(0x300a00d2, 924);
		_flddo.a(0x300a00d3, 925);
		_flddo.a(0x300a00d4, 926);
		_flddo.a(0x300a00d5, 927);
		_flddo.a(0x300a00d6, 928);
		_flddo.a(0x300a00d8, 929);
		_flddo.a(0x300a00da, 930);
		_flddo.a(0x300a00e0, 931);
		_flddo.a(0x300a00e1, 932);
		_flddo.a(0x300a00e2, 933);
		_flddo.a(0x300a00e3, 934);
		_flddo.a(0x300a00e4, 935);
		_flddo.a(0x300a00e5, 936);
		_flddo.a(0x300a00e6, 937);
		_flddo.a(0x300a00e7, 938);
		_flddo.a(0x300a00e8, 939);
		_flddo.a(0x300a00e9, 940);
		_flddo.a(0x300a00ea, 941);
		_flddo.a(0x300a00eb, 942);
		_flddo.a(0x300a00ec, 943);
		_flddo.a(0x300a00ed, 944);
		_flddo.a(0x300a00f0, 945);
		_flddo.a(0x300a00f2, 946);
		_flddo.a(0x300a00f4, 947);
		_flddo.a(0x300a00f5, 948);
		_flddo.a(0x300a00f6, 949);
		_flddo.a(0x300a00f8, 950);
		_flddo.a(0x300a00fa, 951);
		_flddo.a(0x300a00fc, 952);
		_flddo.a(0x300a00fe, 953);
		_flddo.a(0x300a0100, 954);
		_flddo.a(0x300a0102, 955);
		_flddo.a(0x300a0104, 956);
		_flddo.a(0x300a0106, 957);
		_flddo.a(0x300a0107, 958);
		_flddo.a(0x300a0108, 959);
		_flddo.a(0x300a0109, 960);
		_flddo.a(0x300a010a, 961);
		_flddo.a(0x300a010c, 962);
		_flddo.a(0x300a010e, 963);
		_flddo.a(0x300a0110, 964);
		_flddo.a(0x300a0111, 965);
		_flddo.a(0x300a0112, 966);
		_flddo.a(0x300a0114, 967);
		_flddo.a(0x300a0115, 968);
		_flddo.a(0x300a0116, 969);
		_flddo.a(0x300a0118, 970);
		_flddo.a(0x300a011a, 971);
		_flddo.a(0x300a011c, 972);
		_flddo.a(0x300a011e, 973);
		_flddo.a(0x300a011f, 974);
		_flddo.a(0x300a0120, 975);
		_flddo.a(0x300a0121, 976);
		_flddo.a(0x300a0122, 977);
		_flddo.a(0x300a0123, 978);
		_flddo.a(0x300a0124, 979);
		_flddo.a(0x300a0125, 980);
		_flddo.a(0x300a0126, 981);
		_flddo.a(0x300a0128, 982);
		_flddo.a(0x300a0129, 983);
		_flddo.a(0x300a012a, 984);
		_flddo.a(0x300a012c, 985);
		_flddo.a(0x300a012e, 986);
		_flddo.a(0x300a0130, 987);
		_flddo.a(0x300a0134, 988);
		_flddo.a(0x300a0180, 989);
		_flddo.a(0x300a0182, 990);
		_flddo.a(0x300a0184, 991);
		_flddo.a(0x300a0190, 992);
		_flddo.a(0x300a0192, 993);
		_flddo.a(0x300a0194, 994);
		_flddo.a(0x300a0196, 995);
		_flddo.a(0x300a0198, 996);
		_flddo.a(0x300a01a0, 997);
		_flddo.a(0x300a01a2, 998);
		_flddo.a(0x300a01a4, 999);
		_flddo.a(0x300a01a6, 1000);
		_flddo.a(0x300a01a8, 1001);
		_flddo.a(0x300a01b0, 1002);
		_flddo.a(0x300a01b2, 1003);
		_flddo.a(0x300a01b4, 1004);
		_flddo.a(0x300a01b6, 1005);
		_flddo.a(0x300a01b8, 1006);
		_flddo.a(0x300a01ba, 1007);
		_flddo.a(0x300a01bc, 1008);
		_flddo.a(0x300a01d0, 1009);
		_flddo.a(0x300a01d2, 1010);
		_flddo.a(0x300a01d4, 1011);
		_flddo.a(0x300a01d6, 1012);
		_flddo.a(0x300a0200, 1013);
		_flddo.a(0x300a0202, 1014);
		_flddo.a(0x300a0206, 1015);
		_flddo.a(0x300a0210, 1016);
		_flddo.a(0x300a0212, 1017);
		_flddo.a(0x300a0214, 1018);
		_flddo.a(0x300a0216, 1019);
		_flddo.a(0x300a0218, 1020);
		_flddo.a(0x300a021a, 1021);
		_flddo.a(0x300a0222, 1022);
		_flddo.a(0x300a0224, 1023);
		_flddo.a(0x300a0226, 1024);
		_flddo.a(0x300a0228, 1025);
		_flddo.a(0x300a022a, 1026);
		_flddo.a(0x300a022c, 1027);
		_flddo.a(0x300a022e, 1028);
		_flddo.a(0x300a0230, 1029);
		_flddo.a(0x300a0232, 1030);
		_flddo.a(0x300a0234, 1031);
		_flddo.a(0x300a0236, 1032);
		_flddo.a(0x300a0238, 1033);
		_flddo.a(0x300a0240, 1034);
		_flddo.a(0x300a0242, 1035);
		_flddo.a(0x300a0244, 1036);
		_flddo.a(0x300a0250, 1037);
		_flddo.a(0x300a0260, 1038);
		_flddo.a(0x300a0262, 1039);
		_flddo.a(0x300a0263, 1040);
		_flddo.a(0x300a0264, 1041);
		_flddo.a(0x300a0266, 1042);
		_flddo.a(0x300a026a, 1043);
		_flddo.a(0x300a026c, 1044);
		_flddo.a(0x300a0280, 1045);
		_flddo.a(0x300a0282, 1046);
		_flddo.a(0x300a0284, 1047);
		_flddo.a(0x300a0286, 1048);
		_flddo.a(0x300a0288, 1049);
		_flddo.a(0x300a028a, 1050);
		_flddo.a(0x300a028c, 1051);
		_flddo.a(0x300a0290, 1052);
		_flddo.a(0x300a0291, 1053);
		_flddo.a(0x300a0292, 1054);
		_flddo.a(0x300a0294, 1055);
		_flddo.a(0x300a0296, 1056);
		_flddo.a(0x300a0298, 1057);
		_flddo.a(0x300a029c, 1058);
		_flddo.a(0x300a029e, 1059);
		_flddo.a(0x300a02a0, 1060);
		_flddo.a(0x300a02a2, 1061);
		_flddo.a(0x300a02a4, 1062);
		_flddo.a(0x300a02b0, 1063);
		_flddo.a(0x300a02b2, 1064);
		_flddo.a(0x300a02b3, 1065);
		_flddo.a(0x300a02b4, 1066);
		_flddo.a(0x300a02b8, 1067);
		_flddo.a(0x300a02ba, 1068);
		_flddo.a(0x300a02c8, 1069);
		_flddo.a(0x300a02d0, 1070);
		_flddo.a(0x300a02d2, 1071);
		_flddo.a(0x300a02d4, 1072);
		_flddo.a(0x300a02d6, 1073);
		_flddo.a(0x300c0002, 1074);
		_flddo.a(0x300c0004, 1075);
		_flddo.a(0x300c0006, 1076);
		_flddo.a(0x300c0007, 1077);
		_flddo.a(0x300c0008, 1078);
		_flddo.a(0x300c0009, 1079);
		_flddo.a(0x300c000a, 1080);
		_flddo.a(0x300c000c, 1081);
		_flddo.a(0x300c000e, 1082);
		_flddo.a(0x300c0020, 1083);
		_flddo.a(0x300c0022, 1084);
		_flddo.a(0x300c0040, 1085);
		_flddo.a(0x300c0042, 1086);
		_flddo.a(0x300c0050, 1087);
		_flddo.a(0x300c0051, 1088);
		_flddo.a(0x300c0055, 1089);
		_flddo.a(0x300c0060, 1090);
		_flddo.a(0x300c006a, 1091);
		_flddo.a(0x300c0080, 1092);
		_flddo.a(0x300c00a0, 1093);
		_flddo.a(0x300c00b0, 1094);
		_flddo.a(0x300c00c0, 1095);
		_flddo.a(0x300c00d0, 1096);
		_flddo.a(0x300c00e0, 1097);
		_flddo.a(0x300c00f0, 1098);
		_flddo.a(0x300e0002, 1099);
		_flddo.a(0x300e0004, 1100);
		_flddo.a(0x300e0005, 1101);
		_flddo.a(0x300e0008, 1102);
		_flddo.a(0x40080000, 1103);
		_flddo.a(0x40080040, 1104);
		_flddo.a(0x40080042, 1105);
		_flddo.a(0x40080050, 1106);
		_flddo.a(0x40080100, 1107);
		_flddo.a(0x40080101, 1108);
		_flddo.a(0x40080102, 1109);
		_flddo.a(0x40080103, 1110);
		_flddo.a(0x40080108, 1111);
		_flddo.a(0x40080109, 1112);
		_flddo.a(0x4008010a, 1113);
		_flddo.a(0x4008010b, 1114);
		_flddo.a(0x4008010c, 1115);
		_flddo.a(0x40080111, 1116);
		_flddo.a(0x40080112, 1117);
		_flddo.a(0x40080113, 1118);
		_flddo.a(0x40080114, 1119);
		_flddo.a(0x40080115, 1120);
		_flddo.a(0x40080117, 1121);
		_flddo.a(0x40080118, 1122);
		_flddo.a(0x40080119, 1123);
		_flddo.a(0x4008011a, 1124);
		_flddo.a(0x40080200, 1125);
		_flddo.a(0x40080202, 1126);
		_flddo.a(0x40080210, 1127);
		_flddo.a(0x40080212, 1128);
		_flddo.a(0x40080300, 1129);
		_flddo.a(0x40084000, 1130);
		_flddo.a(0x50000000, 1131);
		_flddo.a(0x50000005, 1132);
		_flddo.a(0x50000010, 1133);
		_flddo.a(0x50000020, 1134);
		_flddo.a(0x50000022, 1135);
		_flddo.a(0x50000030, 1136);
		_flddo.a(0x50000040, 1137);
		_flddo.a(0x50000103, 1138);
		_flddo.a(0x50000104, 1139);
		_flddo.a(0x50000105, 1140);
		_flddo.a(0x50000106, 1141);
		_flddo.a(0x50000110, 1142);
		_flddo.a(0x50000112, 1143);
		_flddo.a(0x50000114, 1144);
		_flddo.a(0x50002000, 1145);
		_flddo.a(0x50002002, 1146);
		_flddo.a(0x50002004, 1147);
		_flddo.a(0x50002006, 1148);
		_flddo.a(0x50002008, 1149);
		_flddo.a(0x5000200a, 1150);
		_flddo.a(0x5000200c, 1151);
		_flddo.a(0x5000200e, 1152);
		_flddo.a(0x50002500, 1153);
		_flddo.a(0x50002600, 1154);
		_flddo.a(0x50002610, 1155);
		_flddo.a(0x50003000, 1156);
		_flddo.a(0x60000000, 1157);
		_flddo.a(0x60000010, 1158);
		_flddo.a(0x60000011, 1159);
		_flddo.a(0x60000012, 1160);
		_flddo.a(0x60000015, 1161);
		_flddo.a(0x60000022, 1162);
		_flddo.a(0x60000040, 1163);
		_flddo.a(0x60000045, 1164);
		_flddo.a(0x60000050, 1165);
		_flddo.a(0x60000051, 1166);
		_flddo.a(0x60000052, 1167);
		_flddo.a(0x60000100, 1168);
		_flddo.a(0x60000102, 1169);
		_flddo.a(0x60001100, 1170);
		_flddo.a(0x60001101, 1171);
		_flddo.a(0x60001102, 1172);
		_flddo.a(0x60001103, 1173);
		_flddo.a(0x60001200, 1174);
		_flddo.a(0x60001201, 1175);
		_flddo.a(0x60001202, 1176);
		_flddo.a(0x60001203, 1177);
		_flddo.a(0x60001301, 1178);
		_flddo.a(0x60001302, 1179);
		_flddo.a(0x60001303, 1180);
		_flddo.a(0x60001500, 1181);
		_flddo.a(0x60003000, 1182);
		_flddo.a(0x7fe00000, 1183);
		_flddo.a(0x7fe00010, 1184);
		_flddo.a(0xfffcfffc, 1185);
		_flddo.a(0xfffee000, 1186);
		_flddo.a(0xfffee00d, 1187);
		_flddo.a(0xfffee0dd, 1188);
		_flddo.a(0x82229, 1190);
		_flddo.a(0x181011, 1191);
		_flddo.a(0x181017, 1192);
		_flddo.a(0x18101a, 1193);
		_flddo.a(0x18101b, 1194);
		_flddo.a(0x181153, 1195);
		_flddo.a(0x181450, 1196);
		_flddo.a(0x400220, 1197);
		_flddo.a(0x400241, 1198);
		_flddo.a(0x400242, 1199);
		_flddo.a(0x400243, 1200);
		_flddo.a(0x400244, 1201);
		_flddo.a(0x400245, 1202);
		_flddo.a(0x400250, 1203);
		_flddo.a(0x400251, 1204);
		_flddo.a(0x400252, 1205);
		_flddo.a(0x400253, 1206);
		_flddo.a(0x400254, 1207);
		_flddo.a(0x400255, 1208);
		_flddo.a(0x400260, 1209);
		_flddo.a(0x400270, 1210);
		_flddo.a(0x400275, 1211);
		_flddo.a(0x400280, 1212);
		_flddo.a(0x400293, 1213);
		_flddo.a(0x400294, 1214);
		_flddo.a(0x400295, 1215);
		_flddo.a(0x400296, 1216);
		_flddo.a(0x400300, 1217);
		_flddo.a(0x400301, 1218);
		_flddo.a(0x400302, 1219);
		_flddo.a(0x400303, 1220);
		_flddo.a(0x400306, 1221);
		_flddo.a(0x400310, 1222);
		_flddo.a(0x400320, 1223);
		_flddo.a(0x400321, 1224);
		_flddo.a(0x400324, 1225);
		_flddo.a(0x400330, 1226);
		_flddo.a(0x400340, 1227);
		_flddo.a(0x500000, 1228);
		_flddo.a(0x20000062, 1229);
		_flddo.a(0x20000063, 1230);
		_flddo.a(0x20000065, 1231);
		_flddo.a(0x20000067, 1232);
		_flddo.a(0x20000069, 1233);
		_flddo.a(0x2000006a, 1234);
		_flddo.a(0x20000510, 1235);
		_flddo.a(0x2010015e, 1236);
		_flddo.a(0x20100160, 1237);
		_flddo.a(0x20500000, 1238);
		_flddo.a(0x20500010, 1239);
		_flddo.a(0x20500020, 1240);
		_flddo.a(0x20500500, 1241);
		_flddo.a(0x21200000, 1242);
		_flddo.a(0x21300000, 1243);
		_flddo.a(0x21300010, 1244);
		_flddo.a(0x21300015, 1245);
		_flddo.a(0x21300030, 1246);
		_flddo.a(0x21300040, 1247);
		_flddo.a(0x21300050, 1248);
		_flddo.a(0x21300060, 1249);
		_flddo.a(0x21300080, 1250);
		_flddo.a(0x213000a0, 1251);
		_flddo.a(0x213000c0, 1252);
		_flddo.a(0x30020000, 1253);
		_flddo.a(0x30040000, 1254);
		_flddo.a(0x30060000, 1255);
		_flddo.a(0x300a0000, 1256);
		_flddo.a(0x300c0000, 1257);
		_flddo.a(0x300e0000, 1258);
		_flddo.a(0x82110, 1259);
		_flddo.a(0x80068, 1260);
		_flddo.a(0x18113a, 1261);
		_flddo.a(0x181156, 1262);
		_flddo.a(0x181191, 1263);
		_flddo.a(0x1811a0, 1264);
		_flddo.a(0x1811a2, 1265);
		_flddo.a(0x181490, 1266);
		_flddo.a(0x181491, 1267);
		_flddo.a(0x181495, 1268);
		_flddo.a(0x181508, 1269);
		_flddo.a(0x185104, 1270);
		_flddo.a(0x187000, 1271);
		_flddo.a(0x187001, 1272);
		_flddo.a(0x187004, 1273);
		_flddo.a(0x187005, 1274);
		_flddo.a(0x187006, 1275);
		_flddo.a(0x187008, 1276);
		_flddo.a(0x18700a, 1277);
		_flddo.a(0x18700c, 1278);
		_flddo.a(0x18700e, 1279);
		_flddo.a(0x187010, 1280);
		_flddo.a(0x187011, 1281);
		_flddo.a(0x187012, 1282);
		_flddo.a(0x187014, 1283);
		_flddo.a(0x187016, 1284);
		_flddo.a(0x18701a, 1285);
		_flddo.a(0x187020, 1286);
		_flddo.a(0x187022, 1287);
		_flddo.a(0x187024, 1288);
		_flddo.a(0x187026, 1289);
		_flddo.a(0x187028, 1290);
		_flddo.a(0x187030, 1291);
		_flddo.a(0x187032, 1292);
		_flddo.a(0x187034, 1293);
		_flddo.a(0x187040, 1294);
		_flddo.a(0x187041, 1295);
		_flddo.a(0x187042, 1296);
		_flddo.a(0x187044, 1297);
		_flddo.a(0x187046, 1298);
		_flddo.a(0x187048, 1299);
		_flddo.a(0x18704c, 1300);
		_flddo.a(0x187050, 1301);
		_flddo.a(0x187052, 1302);
		_flddo.a(0x187054, 1303);
		_flddo.a(0x187060, 1304);
		_flddo.a(0x187062, 1305);
		_flddo.a(0x187064, 1306);
		_flddo.a(0x187065, 1307);
		_flddo.a(0x200062, 1308);
		_flddo.a(0x280300, 1309);
		_flddo.a(0x280301, 1310);
		_flddo.a(0x281041, 1311);
		_flddo.a(0x281300, 1312);
		_flddo.a(0x282112, 1313);
		_flddo.a(0x400307, 1314);
		_flddo.a(0x400312, 1315);
		_flddo.a(0x400314, 1316);
		_flddo.a(0x400316, 1317);
		_flddo.a(0x400318, 1318);
		_flddo.a(0x400555, 1319);
		_flddo.a(0x400556, 1320);
		_flddo.a(0x4008ea, 1321);
		_flddo.a(0x40a043, 1322);
		_flddo.a(0x40a121, 1323);
		_flddo.a(0x40a122, 1324);
		_flddo.a(0x40a123, 1325);
		_flddo.a(0x40a136, 1326);
		_flddo.a(0x40a160, 1327);
		_flddo.a(0x40a168, 1328);
		_flddo.a(0x40a30a, 1329);
		_flddo.a(0x603000, 1330);
		_flddo.a(0x603002, 1331);
		_flddo.a(0x603004, 1332);
		_flddo.a(0x603006, 1333);
		_flddo.a(0x603008, 1334);
		_flddo.a(0x603010, 1335);
		_flddo.a(0x603020, 1336);
		_flddo.a(0x80103, 1337);
		_flddo.a(0x80105, 1338);
		_flddo.a(0x80106, 1339);
		_flddo.a(0x80107, 1340);
		_flddo.a(0x8010b, 1341);
		_flddo.a(0x8010c, 1342);
		_flddo.a(0x8010d, 1343);
		_flddo.a(0x8010f, 1344);
		_flddo.a(0x8115a, 1345);
		_flddo.a(0x200019, 1346);
		_flddo.a(0x2000001e, 1347);
		_flddo.a(0x200000a0, 1348);
		_flddo.a(0x200000a1, 1349);
		_flddo.a(0x200000a2, 1350);
		_flddo.a(0x200000a4, 1351);
		_flddo.a(0x200000a8, 1352);
		_flddo.a(0x20100052, 1353);
		_flddo.a(0x20100054, 1354);
		_flddo.a(0x201000a6, 1355);
		_flddo.a(0x201000a7, 1356);
		_flddo.a(0x201000a8, 1357);
		_flddo.a(0x201000a9, 1358);
		_flddo.a(0x20100152, 1359);
		_flddo.a(0x20100154, 1360);
		_flddo.a(0x20100376, 1361);
		_flddo.a(0x20200040, 1362);
		_flddo.a(0x20200050, 1363);
		_flddo.a(0x202000a0, 1364);
		_flddo.a(0x202000a2, 1365);
		_flddo.a(0x20400020, 1366);
		_flddo.a(0x20400072, 1367);
		_flddo.a(0x20400074, 1368);
		_flddo.a(0x20400082, 1369);
		_flddo.a(0x4006fa, 1370);
		_flddo.a(0x40071a, 1371);
		_flddo.a(0x40072a, 1372);
		_flddo.a(0x40073a, 1373);
		_flddo.a(0x40074a, 1374);
		_flddo.a(0x4008d8, 1375);
		_flddo.a(0x4008da, 1376);
		_flddo.a(0x181622, 1377);
		_flddo.a(0x181623, 1378);
		_flddo.a(0x50001001, 1379);
		_flddo.a(0x60001001, 1380);
		_flddo.a(0x700001, 1381);
		_flddo.a(0x700002, 1382);
		_flddo.a(0x700003, 1383);
		_flddo.a(0x700004, 1384);
		_flddo.a(0x700005, 1385);
		_flddo.a(0x700006, 1386);
		_flddo.a(0x700008, 1387);
		_flddo.a(0x700009, 1388);
		_flddo.a(0x700010, 1389);
		_flddo.a(0x700011, 1390);
		_flddo.a(0x700014, 1391);
		_flddo.a(0x700015, 1392);
		_flddo.a(0x700020, 1393);
		_flddo.a(0x700021, 1394);
		_flddo.a(0x700022, 1395);
		_flddo.a(0x700023, 1396);
		_flddo.a(0x700024, 1397);
		_flddo.a(0x700041, 1398);
		_flddo.a(0x700060, 1399);
		_flddo.a(0x700062, 1400);
		_flddo.a(0x700066, 1401);
		_flddo.a(0x700068, 1402);
		_flddo.a(0x700080, 1403);
		_flddo.a(0x700081, 1404);
		_flddo.a(0x700082, 1405);
		_flddo.a(0x700083, 1406);
		_flddo.a(0x700084, 1407);
		_flddo.a(0x283110, 1408);
		_flddo.a(0x700012, 1409);
		_flddo.a(0x700042, 1410);
		_flddo.a(0x700052, 1411);
		_flddo.a(0x700053, 1412);
		_flddo.a(0x70005a, 1413);
		_flddo.a(0x700067, 1414);
		_flddo.a(0x700100, 1415);
		_flddo.a(0x700101, 1416);
		_flddo.a(0x700102, 1417);
		_flddo.a(0x700103, 1418);
		_flddo.a(0x402016, 1419);
		_flddo.a(0x402017, 1420);
		_flddo.a(0x40050a, 1421);
		_flddo.a(0x400550, 1422);
		_flddo.a(0x400551, 1423);
		_flddo.a(0x40059a, 1424);
		_flddo.a(0x40a010, 1425);
		_flddo.a(0x40a027, 1426);
		_flddo.a(0x40a030, 1427);
		_flddo.a(0x40a032, 1428);
		_flddo.a(0x40a040, 1429);
		_flddo.a(0x40a050, 1430);
		_flddo.a(0x40a073, 1431);
		_flddo.a(0x40a075, 1432);
		_flddo.a(0x40a088, 1433);
		_flddo.a(0x40a120, 1434);
		_flddo.a(0x40a124, 1435);
		_flddo.a(0x40a130, 1436);
		_flddo.a(0x40a132, 1437);
		_flddo.a(0x40a138, 1438);
		_flddo.a(0x40a13a, 1439);
		_flddo.a(0x40a300, 1440);
		_flddo.a(0x40a360, 1441);
		_flddo.a(0x40a370, 1442);
		_flddo.a(0x40a372, 1443);
		_flddo.a(0x40a375, 1444);
		_flddo.a(0x40a385, 1445);
		_flddo.a(0x40a491, 1446);
		_flddo.a(0x40a492, 1447);
		_flddo.a(0x40a493, 1448);
		_flddo.a(0x40a504, 1449);
		_flddo.a(0x40a525, 1450);
		_flddo.a(0x40a730, 1451);
		_flddo.a(0x40db00, 1452);
		_flddo.a(0x40db06, 1453);
		_flddo.a(0x40db07, 1454);
		_flddo.a(0x40db0b, 1455);
		_flddo.a(0x40db0c, 1456);
		_flddo.a(0x40db0d, 1457);
		_flddo.a(0x40db73, 1458);
		_flddo.a(0x8002a, 1459);
		_flddo.a(0x181067, 1460);
		_flddo.a(0x181068, 1461);
		_flddo.a(0x181069, 1462);
		_flddo.a(0x18106a, 1463);
		_flddo.a(0x18106c, 1464);
		_flddo.a(0x18106e, 1465);
		_flddo.a(0x181800, 1466);
		_flddo.a(0x181802, 1467);
		_flddo.a(0x181801, 1468);
		_flddo.a(0x200200, 1469);
		_flddo.a(0x3a0004, 1470);
		_flddo.a(0x3a0005, 1471);
		_flddo.a(0x3a0010, 1472);
		_flddo.a(0x3a001a, 1473);
		_flddo.a(0x3a0020, 1474);
		_flddo.a(0x3a0200, 1475);
		_flddo.a(0x3a0202, 1476);
		_flddo.a(0x3a0203, 1477);
		_flddo.a(0x3a0205, 1478);
		_flddo.a(0x3a0208, 1479);
		_flddo.a(0x3a0209, 1480);
		_flddo.a(0x3a020a, 1481);
		_flddo.a(0x3a020c, 1482);
		_flddo.a(0x3a0210, 1483);
		_flddo.a(0x3a0211, 1484);
		_flddo.a(0x3a0212, 1485);
		_flddo.a(0x3a0213, 1486);
		_flddo.a(0x3a0214, 1487);
		_flddo.a(0x3a0215, 1488);
		_flddo.a(0x3a0218, 1489);
		_flddo.a(0x3a021a, 1490);
		_flddo.a(0x3a0220, 1491);
		_flddo.a(0x3a0221, 1492);
		_flddo.a(0x3a0222, 1493);
		_flddo.a(0x3a0223, 1494);
		_flddo.a(0x40a0b0, 1495);
		_flddo.a(0x40a180, 1496);
		_flddo.a(0x40a195, 1497);
		_flddo.a(0x40b020, 1498);
		_flddo.a(0x54000100, 1499);
		_flddo.a(0x54000110, 1500);
		_flddo.a(0x54000112, 1501);
		_flddo.a(0x54001004, 1502);
		_flddo.a(0x54001006, 1503);
		_flddo.a(0x5400100a, 1504);
		_flddo.a(0x54001010, 1505);
		_flddo.a(0x80056, 1506);
		_flddo.a(0x80201, 1507);
		_flddo.a(0x8114a, 1508);
		_flddo.a(0x8113a, 1509);
		_flddo.a(0x82127, 1510);
		_flddo.a(0x100101, 1511);
		_flddo.a(0x100102, 1512);
		_flddo.a(0x182001, 1513);
		_flddo.a(0x182002, 1514);
		_flddo.a(0x182003, 1515);
		_flddo.a(0x182004, 1516);
		_flddo.a(0x182005, 1517);
		_flddo.a(0x182006, 1518);
		_flddo.a(0x182010, 1519);
		_flddo.a(0x182020, 1520);
		_flddo.a(0x182030, 1521);
		_flddo.a(0x183100, 1522);
		_flddo.a(0x183101, 1523);
		_flddo.a(0x183102, 1524);
		_flddo.a(0x183103, 1525);
		_flddo.a(0x183104, 1526);
		_flddo.a(0x183105, 1527);
		_flddo.a(0x188150, 1528);
		_flddo.a(0x188151, 1529);
		_flddo.a(0x281350, 1530);
		_flddo.a(0x281351, 1531);
		_flddo.a(0x40000a, 1532);
		_flddo.a(0x40030e, 1533);
		_flddo.a(0x404001, 1534);
		_flddo.a(0x404002, 1535);
		_flddo.a(0x404003, 1536);
		_flddo.a(0x404004, 1537);
		_flddo.a(0x404005, 1538);
		_flddo.a(0x404006, 1539);
		_flddo.a(0x404007, 1540);
		_flddo.a(0x404009, 1541);
		_flddo.a(0x404011, 1542);
		_flddo.a(0x404015, 1543);
		_flddo.a(0x404016, 1544);
		_flddo.a(0x404018, 1545);
		_flddo.a(0x404019, 1546);
		_flddo.a(0x404020, 1547);
		_flddo.a(0x404021, 1548);
		_flddo.a(0x404022, 1549);
		_flddo.a(0x404023, 1550);
		_flddo.a(0x404025, 1551);
		_flddo.a(0x404026, 1552);
		_flddo.a(0x404027, 1553);
		_flddo.a(0x404028, 1554);
		_flddo.a(0x404029, 1555);
		_flddo.a(0x404030, 1556);
		_flddo.a(0x404031, 1557);
		_flddo.a(0x404032, 1558);
		_flddo.a(0x404033, 1559);
		_flddo.a(0x404034, 1560);
		_flddo.a(0x404035, 1561);
		_flddo.a(0x404036, 1562);
		_flddo.a(0x404037, 1563);
		_flddo.a(0x408302, 1564);
		_flddo.a(0x40a170, 1565);
		_flddo.a(0x1000410, 1566);
		_flddo.a(0x1000420, 1567);
		_flddo.a(0x1000424, 1568);
		_flddo.a(0x1000426, 1569);
		_flddo.a(0x4000005, 1570);
		_flddo.a(0x4000010, 1571);
		_flddo.a(0x4000015, 1572);
		_flddo.a(0x4000020, 1573);
		_flddo.a(0x4000100, 1574);
		_flddo.a(0x4000105, 1575);
		_flddo.a(0x4000110, 1576);
		_flddo.a(0x4000115, 1577);
		_flddo.a(0x4000120, 1578);
		_flddo.a(0x4000305, 1579);
		_flddo.a(0x4000310, 1580);
		_flddo.a(0x20000061, 1581);
		_flddo.a(0x3002000d, 1582);
		_flddo.a(0x30020034, 1583);
		_flddo.a(0x30060048, 1584);
		_flddo.a(0x30060049, 1585);
		_flddo.a(0x30080010, 1586);
		_flddo.a(0x30080012, 1587);
		_flddo.a(0x30080014, 1588);
		_flddo.a(0x30080016, 1589);
		_flddo.a(0x30080020, 1590);
		_flddo.a(0x30080022, 1591);
		_flddo.a(0x30080024, 1592);
		_flddo.a(0x30080025, 1593);
		_flddo.a(0x3008002a, 1594);
		_flddo.a(0x3008002b, 1595);
		_flddo.a(0x3008002c, 1596);
		_flddo.a(0x30080030, 1597);
		_flddo.a(0x30080032, 1598);
		_flddo.a(0x30080033, 1599);
		_flddo.a(0x30080036, 1600);
		_flddo.a(0x30080037, 1601);
		_flddo.a(0x3008003a, 1602);
		_flddo.a(0x3008003b, 1603);
		_flddo.a(0x30080040, 1604);
		_flddo.a(0x30080042, 1605);
		_flddo.a(0x30080044, 1606);
		_flddo.a(0x30080048, 1607);
		_flddo.a(0x30080050, 1608);
		_flddo.a(0x30080052, 1609);
		_flddo.a(0x30080054, 1610);
		_flddo.a(0x30080056, 1611);
		_flddo.a(0x3008005a, 1612);
		_flddo.a(0x30080060, 1613);
		_flddo.a(0x30080062, 1614);
		_flddo.a(0x30080064, 1615);
		_flddo.a(0x30080066, 1616);
		_flddo.a(0x30080070, 1617);
		_flddo.a(0x30080072, 1618);
		_flddo.a(0x30080074, 1619);
		_flddo.a(0x30080076, 1620);
		_flddo.a(0x30080078, 1621);
		_flddo.a(0x3008007a, 1622);
		_flddo.a(0x30080080, 1623);
		_flddo.a(0x30080082, 1624);
		_flddo.a(0x30080090, 1625);
		_flddo.a(0x30080092, 1626);
		_flddo.a(0x300800a0, 1627);
		_flddo.a(0x300800b0, 1628);
		_flddo.a(0x300800c0, 1629);
		_flddo.a(0x300800d0, 1630);
		_flddo.a(0x300800e0, 1631);
		_flddo.a(0x30080100, 1632);
		_flddo.a(0x30080105, 1633);
		_flddo.a(0x30080110, 1634);
		_flddo.a(0x30080116, 1635);
		_flddo.a(0x30080120, 1636);
		_flddo.a(0x30080122, 1637);
		_flddo.a(0x30080130, 1638);
		_flddo.a(0x30080132, 1639);
		_flddo.a(0x30080134, 1640);
		_flddo.a(0x30080136, 1641);
		_flddo.a(0x30080138, 1642);
		_flddo.a(0x3008013a, 1643);
		_flddo.a(0x3008013c, 1644);
		_flddo.a(0x30080140, 1645);
		_flddo.a(0x30080142, 1646);
		_flddo.a(0x30080150, 1647);
		_flddo.a(0x30080152, 1648);
		_flddo.a(0x30080160, 1649);
		_flddo.a(0x30080162, 1650);
		_flddo.a(0x30080164, 1651);
		_flddo.a(0x30080166, 1652);
		_flddo.a(0x30080168, 1653);
		_flddo.a(0x30080200, 1654);
		_flddo.a(0x30080202, 1655);
		_flddo.a(0x30080220, 1656);
		_flddo.a(0x30080223, 1657);
		_flddo.a(0x30080224, 1658);
		_flddo.a(0x30080230, 1659);
		_flddo.a(0x30080240, 1660);
		_flddo.a(0x30080250, 1661);
		_flddo.a(0x30080251, 1662);
		_flddo.a(0x300a0015, 1663);
		_flddo.a(0x300a00c7, 1664);
		_flddo.a(0x300a00ee, 1665);
		_flddo.a(0x4ffe0001, 1666);
		_flddo.a(0xfffafffa, 1667);
		_flddo.a(0x80096, 1668);
		_flddo.a(0x80110, 1669);
		_flddo.a(0x80112, 1670);
		_flddo.a(0x80114, 1671);
		_flddo.a(0x80115, 1672);
		_flddo.a(0x80116, 1673);
		_flddo.a(0x81049, 1674);
		_flddo.a(0x81052, 1675);
		_flddo.a(0x81062, 1676);
		_flddo.a(0x81072, 1677);
		_flddo.a(0x89007, 1678);
		_flddo.a(0x89092, 1679);
		_flddo.a(0x89121, 1680);
		_flddo.a(0x89123, 1681);
		_flddo.a(0x89124, 1682);
		_flddo.a(0x89154, 1683);
		_flddo.a(0x89205, 1684);
		_flddo.a(0x89206, 1685);
		_flddo.a(0x89207, 1686);
		_flddo.a(0x89208, 1687);
		_flddo.a(0x89209, 1688);
		_flddo.a(0x89215, 1689);
		_flddo.a(0x89237, 1690);
		_flddo.a(0x120010, 1691);
		_flddo.a(0x120020, 1692);
		_flddo.a(0x120021, 1693);
		_flddo.a(0x120030, 1694);
		_flddo.a(0x120031, 1695);
		_flddo.a(0x120040, 1696);
		_flddo.a(0x120042, 1697);
		_flddo.a(0x120050, 1698);
		_flddo.a(0x120051, 1699);
		_flddo.a(0x120060, 1700);
		_flddo.a(0x186039, 1701);
		_flddo.a(0x18603b, 1702);
		_flddo.a(0x18603d, 1703);
		_flddo.a(0x18603f, 1704);
		_flddo.a(0x186041, 1705);
		_flddo.a(0x186043, 1706);
		_flddo.a(0x189004, 1707);
		_flddo.a(0x189005, 1708);
		_flddo.a(0x189006, 1709);
		_flddo.a(0x189008, 1710);
		_flddo.a(0x189009, 1711);
		_flddo.a(0x189010, 1712);
		_flddo.a(0x189011, 1713);
		_flddo.a(0x189012, 1714);
		_flddo.a(0x189014, 1715);
		_flddo.a(0x189015, 1716);
		_flddo.a(0x189016, 1717);
		_flddo.a(0x189017, 1718);
		_flddo.a(0x189018, 1719);
		_flddo.a(0x189019, 1720);
		_flddo.a(0x189020, 1721);
		_flddo.a(0x189021, 1722);
		_flddo.a(0x189022, 1723);
		_flddo.a(0x189024, 1724);
		_flddo.a(0x189025, 1725);
		_flddo.a(0x189026, 1726);
		_flddo.a(0x189027, 1727);
		_flddo.a(0x189028, 1728);
		_flddo.a(0x189029, 1729);
		_flddo.a(0x189022, 1730);
		_flddo.a(0x189030, 1731);
		_flddo.a(0x189032, 1732);
		_flddo.a(0x189033, 1733);
		_flddo.a(0x189034, 1734);
		_flddo.a(0x189035, 1735);
		_flddo.a(0x189036, 1736);
		_flddo.a(0x189037, 1737);
		_flddo.a(0x189041, 1738);
		_flddo.a(0x189042, 1739);
		_flddo.a(0x189043, 1740);
		_flddo.a(0x189044, 1741);
		_flddo.a(0x189045, 1742);
		_flddo.a(0x189046, 1743);
		_flddo.a(0x189047, 1744);
		_flddo.a(0x189048, 1745);
		_flddo.a(0x189049, 1746);
		_flddo.a(0x189050, 1747);
		_flddo.a(0x189051, 1748);
		_flddo.a(0x189052, 1749);
		_flddo.a(0x189053, 1750);
		_flddo.a(0x189054, 1751);
		_flddo.a(0x189058, 1752);
		_flddo.a(0x189059, 1753);
		_flddo.a(0x189060, 1754);
		_flddo.a(0x189061, 1755);
		_flddo.a(0x189062, 1756);
		_flddo.a(0x189063, 1757);
		_flddo.a(0x189064, 1758);
		_flddo.a(0x189065, 1759);
		_flddo.a(0x189066, 1760);
		_flddo.a(0x189067, 1761);
		_flddo.a(0x189069, 1762);
		_flddo.a(0x189070, 1763);
		_flddo.a(0x189073, 1764);
		_flddo.a(0x189074, 1765);
		_flddo.a(0x189075, 1766);
		_flddo.a(0x189076, 1767);
		_flddo.a(0x189077, 1768);
		_flddo.a(0x189078, 1769);
		_flddo.a(0x189079, 1770);
		_flddo.a(0x189080, 1771);
		_flddo.a(0x189081, 1772);
		_flddo.a(0x189082, 1773);
		_flddo.a(0x189084, 1774);
		_flddo.a(0x189085, 1775);
		_flddo.a(0x189087, 1776);
		_flddo.a(0x189089, 1777);
		_flddo.a(0x189090, 1778);
		_flddo.a(0x189091, 1779);
		_flddo.a(0x189093, 1780);
		_flddo.a(0x189094, 1781);
		_flddo.a(0x189095, 1782);
		_flddo.a(0x189098, 1783);
		_flddo.a(0x189100, 1784);
		_flddo.a(0x189101, 1785);
		_flddo.a(0x189103, 1786);
		_flddo.a(0x189104, 1787);
		_flddo.a(0x189105, 1788);
		_flddo.a(0x189106, 1789);
		_flddo.a(0x189107, 1790);
		_flddo.a(0x189112, 1791);
		_flddo.a(0x189114, 1792);
		_flddo.a(0x189115, 1793);
		_flddo.a(0x189117, 1794);
		_flddo.a(0x189118, 1795);
		_flddo.a(0x189119, 1796);
		_flddo.a(0x189125, 1797);
		_flddo.a(0x189126, 1798);
		_flddo.a(0x189127, 1799);
		_flddo.a(0x189147, 1800);
		_flddo.a(0x189151, 1801);
		_flddo.a(0x189152, 1802);
		_flddo.a(0x189155, 1803);
		_flddo.a(0x189159, 1804);
		_flddo.a(0x189166, 1805);
		_flddo.a(0x189168, 1806);
		_flddo.a(0x189169, 1807);
		_flddo.a(0x189170, 1808);
		_flddo.a(0x189171, 1809);
		_flddo.a(0x189172, 1810);
		_flddo.a(0x189173, 1811);
		_flddo.a(0x189174, 1812);
		_flddo.a(0x189175, 1813);
		_flddo.a(0x189176, 1814);
		_flddo.a(0x189177, 1815);
		_flddo.a(0x189178, 1816);
		_flddo.a(0x189179, 1817);
		_flddo.a(0x189180, 1818);
		_flddo.a(0x189181, 1819);
		_flddo.a(0x189182, 1820);
		_flddo.a(0x189183, 1821);
		_flddo.a(0x189184, 1822);
		_flddo.a(0x189195, 1823);
		_flddo.a(0x189196, 1824);
		_flddo.a(0x189197, 1825);
		_flddo.a(0x189198, 1826);
		_flddo.a(0x189199, 1827);
		_flddo.a(0x189200, 1828);
		_flddo.a(0x189214, 1829);
		_flddo.a(0x189217, 1830);
		_flddo.a(0x189218, 1831);
		_flddo.a(0x189219, 1832);
		_flddo.a(0x189220, 1833);
		_flddo.a(0x189226, 1834);
		_flddo.a(0x189227, 1835);
		_flddo.a(0x189231, 1836);
		_flddo.a(0x189232, 1837);
		_flddo.a(0x189234, 1838);
		_flddo.a(0x189236, 1839);
		_flddo.a(0x189239, 1840);
		_flddo.a(0x18a001, 1841);
		_flddo.a(0x18a002, 1842);
		_flddo.a(0x18a003, 1843);
		_flddo.a(0x209056, 1844);
		_flddo.a(0x209057, 1845);
		_flddo.a(0x209071, 1846);
		_flddo.a(0x209072, 1847);
		_flddo.a(0x209111, 1848);
		_flddo.a(0x209113, 1849);
		_flddo.a(0x209116, 1850);
		_flddo.a(0x209128, 1851);
		_flddo.a(0x209153, 1852);
		_flddo.a(0x209156, 1853);
		_flddo.a(0x209157, 1854);
		_flddo.a(0x209158, 1855);
		_flddo.a(0x209161, 1856);
		_flddo.a(0x209162, 1857);
		_flddo.a(0x209163, 1858);
		_flddo.a(0x209164, 1859);
		_flddo.a(0x209165, 1860);
		_flddo.a(0x209167, 1861);
		_flddo.a(0x209213, 1862);
		_flddo.a(0x209221, 1863);
		_flddo.a(0x209222, 1864);
		_flddo.a(0x209228, 1865);
		_flddo.a(0x209238, 1866);
		_flddo.a(0x289001, 1867);
		_flddo.a(0x289002, 1868);
		_flddo.a(0x289003, 1869);
		_flddo.a(0x289099, 1870);
		_flddo.a(0x289108, 1871);
		_flddo.a(0x289110, 1872);
		_flddo.a(0x289132, 1873);
		_flddo.a(0x289145, 1874);
		_flddo.a(0x289235, 1875);
		_flddo.a(0x321031, 1876);
		_flddo.a(0x40000b, 1877);
		_flddo.a(0x400281, 1878);
		_flddo.a(0x401011, 1879);
		_flddo.a(0x401101, 1880);
		_flddo.a(0x401102, 1881);
		_flddo.a(0x401103, 1882);
		_flddo.a(0x409096, 1883);
		_flddo.a(0x409210, 1884);
		_flddo.a(0x409211, 1885);
		_flddo.a(0x409212, 1886);
		_flddo.a(0x409216, 1887);
		_flddo.a(0x409224, 1888);
		_flddo.a(0x409225, 1889);
		_flddo.a(0x40a301, 1890);
		_flddo.a(0x4000500, 1891);
		_flddo.a(0x4000510, 1892);
		_flddo.a(0x4000520, 1893);
		_flddo.a(0x4000550, 1894);
		_flddo.a(0x300a00fb, 1895);
		_flddo.a(0x300a02e0, 1896);
		_flddo.a(0x300a02e1, 1897);
		_flddo.a(0x300a02e2, 1898);
		_flddo.a(0x52009229, 1899);
		_flddo.a(0x52009230, 1900);
		_flddo.a(0x56000010, 1901);
		_flddo.a(0x56000020, 1902);
	}
}