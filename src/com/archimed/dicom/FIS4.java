/*
 * b - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.IOException;
import java.io.InputStream;

class FIS4 extends FIS3 {
	FIS4(InputStream inputstream) {
		super(inputstream);
	}

	FIS4(InputStream inputstream, int i) {
		super(inputstream, i);
	}

	String read4String() throws IOException {
		return readString(4);
	}

	Integer read4BInt() throws IOException {
		return new Integer((read01() << 16) + read01());
	}

	Float readFloat() throws IOException {
		return new Float(Float.intBitsToFloat(this.read03()));
	}

	Double readDouble() throws IOException {
		return new Double(Double.longBitsToDouble(read05()));
	}

	Long readLong() throws IOException {
		return new Long((long) read04());
	}

	Short readShort() throws IOException {
		return new Short((short) read02());
	}

	Long readLong3() throws IOException {
		return new Long((long) this.read03());
	}

	Integer readInt1() throws IOException {
		return new Integer(read01());
	}

	Integer readInt12() throws IOException {
		return new Integer(read01());
	}

	Object genericRead(int i) throws IOException {
		switch (i) {
		case 17:
			return read4String();
		case 5:
			return read4BInt();
		case 26:
			return readFloat();
		case 20:
			return readDouble();
		case 19:
			return readLong();
		case 23:
			return readShort();
		case 1:
			return readLong3();
		case 3:
			return readInt1();
		case 21:
			return readInt12();
		default:
			return null;
		}
	}
}