/*
 * g - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.IOException;
import java.io.OutputStream;

class FOS3 extends FOS2 {
	int _fldnew = 8193;

	boolean _fldint = true;

	int _flddo = 0;

	byte[] _fldif;

	byte[] _fldtry;

	byte[] _fldfor;

	FOS3(OutputStream outputstream) {
		super(outputstream);
		_fldif = new byte[2];
		_fldtry = new byte[4];
		_fldfor = new byte[8];
	}

	FOS3(OutputStream outputstream, int i) {
		super(outputstream, i);
		_fldif = new byte[2];
		_fldtry = new byte[4];
		_fldfor = new byte[8];
	}

	void _mthif(int i) {
		_fldnew = i;
	}

	int _mthif() {
		return _fldnew;
	}

	void a(boolean bool) {
		_fldint = bool;
	}

	boolean a() {
		return _fldint;
	}

	int a(int i) throws IOException {
		if (_fldnew == 8195) {
			_fldif[1] = (byte) i;
			_fldif[0] = (byte) (i >>> 8);
		} else {
			_fldif[0] = (byte) i;
			_fldif[1] = (byte) (i >>> 8);
		}
		write(_fldif);
		return 2;
	}

	int _mthdo(int i) throws IOException {
		if (_fldnew == 8195) {
			_fldtry[3] = (byte) i;
			_fldtry[2] = (byte) (i >>> 8);
			_fldtry[1] = (byte) (i >>> 16);
			_fldtry[0] = (byte) (i >>> 24);
		} else {
			_fldtry[0] = (byte) i;
			_fldtry[1] = (byte) (i >>> 8);
			_fldtry[2] = (byte) (i >>> 16);
			_fldtry[3] = (byte) (i >>> 24);
		}
		write(_fldtry);
		return 4;
	}

	int a(long l) throws IOException {
		if (_fldnew == 8195) {
			_fldfor[7] = (byte) (int) l;
			_fldfor[6] = (byte) (int) (l >>> 8);
			_fldfor[5] = (byte) (int) (l >>> 16);
			_fldfor[4] = (byte) (int) (l >>> 24);
			_fldfor[3] = (byte) (int) (l >>> 32);
			_fldfor[2] = (byte) (int) (l >>> 40);
			_fldfor[1] = (byte) (int) (l >>> 48);
			_fldfor[0] = (byte) (int) (l >>> 56);
		} else {
			_fldfor[0] = (byte) (int) l;
			_fldfor[1] = (byte) (int) (l >>> 8);
			_fldfor[2] = (byte) (int) (l >>> 16);
			_fldfor[3] = (byte) (int) (l >>> 24);
			_fldfor[4] = (byte) (int) (l >>> 32);
			_fldfor[5] = (byte) (int) (l >>> 40);
			_fldfor[6] = (byte) (int) (l >>> 48);
			_fldfor[7] = (byte) (int) (l >>> 56);
		}
		write(_fldfor);
		return 8;
	}

	int a(String string) throws IOException {
		write(string.getBytes());
		return string.length();
	}
}