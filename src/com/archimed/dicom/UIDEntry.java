/*
 * UIDEntry - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import com.archimed.tool.StringTool;

public class UIDEntry {
	public static final int SOPClass = 1;

	public static final int WellknownSOPInstance = 2;

	public static final int TransferSyntax = 3;

	public static final int MetaSOPClass = 4;

	public static final int ApplicationContextName = 5;

	public static final int CodingScheme = 6;

	private int _flddo;

	private String _fldint;

	private String a;

	private String _fldif;

	private int _fldfor;

	public UIDEntry(int i, String string, String string_0_, String string_1_,
			int i_2_) {
		_flddo = i;
		_fldint = string;
		a = string_0_;
		_fldif = string_1_;
		_fldfor = i_2_;
	}

	public int getConstant() {
		return _flddo;
	}

	public String getValue() {
		return _fldint;
	}

	public String getName() {
		return a;
	}

	public String getShortName() {
		return _fldif;
	}

	public int getType() {
		return _fldfor;
	}

	public boolean equals(Object object) {
		if (!(object instanceof UIDEntry))
			return false;
		return _flddo == ((UIDEntry) object)._flddo;
	}

	public String toString() {
		return _fldint;
	}

	public String toLongString() {
		return (StringTool.fstr(_fldint, 30) + StringTool.fstr(_fldif, 6) + StringTool
				.fstr(a, 40));
	}
}