/*
 * e - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

class FIS2 extends FilterInputStream {
	private long totalReaded = 0L;

	private long opReaded;

	protected long readed = 0L;

	public FIS2(InputStream inputstream) {
		this(inputstream, 0);
	}

	public FIS2(InputStream inputstream, int i) {
		super(inputstream);
		totalReaded = (long) i;
	}

	public int read() throws IOException {
		totalReaded++;
		return in.read();
	}

	public int read(byte[] is) throws IOException {
		return read(is, 0, is.length);
	}

	public int read(byte[] is, int i, int i_0_) throws IOException {
		opReaded = (long) in.read(is, i, i_0_);
		totalReaded += (long) i + opReaded;
		return (int) opReaded;
	}

	public long skip(long l) throws IOException {
		opReaded = in.skip(l);
		totalReaded += opReaded;
		return opReaded;
	}

	public boolean markSupported() {
		return false;
	}

	public void discard(byte[] is) throws IOException {
		totalReaded += (long) is.length;
		int i = 0;
		boolean bool = false;
		int i_1_;
		for (/**/; i < is.length; i += i_1_) {
			i_1_ = in.read(is, i, is.length - i);
			if (i_1_ == -1)
				throw new EOFException();
		}
	}

	public int skipBytes(int i) throws IOException {
		InputStream inputstream = in;
		for (int i_2_ = 0; i_2_ < i; i_2_ += (int) inputstream
				.skip((long) (i - i_2_))) {
			/* empty */
		}
		totalReaded += (long) i;
		return i;
	}

	public long getReaded() {
		return totalReaded;
	}

	protected void setReaded() {
		readed = totalReaded;
	}

	protected long free() {
		if (totalReaded > readed)
			return totalReaded - readed;
		return 0L;
	}
}