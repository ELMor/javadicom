/*
 * k - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.IOException;
import java.io.InputStream;

class FIS3 extends FIS2 {
	int data1 = 8193;

	int longlong = 0;

	long _fldtry = 0L;

	byte[] buf1;

	byte[] buf2;

	byte[] buf3;

	FIS3(InputStream inputstream) {
		super(inputstream);
		buf1 = new byte[2];
		buf2 = new byte[4];
		buf3 = new byte[8];
	}

	FIS3(InputStream inputstream, int i) {
		super(inputstream, i);
		buf1 = new byte[2];
		buf2 = new byte[4];
		buf3 = new byte[8];
	}

	private int toByte(byte i) {
		return i & 0xff;
	}

	void setData1(int i) {
		data1 = i;
	}

	int getData1() {
		return data1;
	}

	int read01() throws IOException {
		this.discard(buf1);
		if (data1 == 8195)
			return (toByte(buf1[0]) << 8) + toByte(buf1[1]);
		return (toByte(buf1[1]) << 8) + toByte(buf1[0]);
	}

	int read02() throws IOException {
		this.discard(buf1);
		if (data1 == 8195)
			return (buf1[0] << 8) + toByte(buf1[1]);
		return (buf1[1] << 8) + toByte(buf1[0]);
	}

	int read03() throws IOException {
		this.discard(buf2);
		if (data1 == 8195) {
			longlong = toByte(buf2[3]);
			longlong += toByte(buf2[2]) << 8;
			longlong += toByte(buf2[1]) << 16;
			longlong += toByte(buf2[0]) << 24;
		} else {
			longlong = toByte(buf2[0]);
			longlong += toByte(buf2[1]) << 8;
			longlong += toByte(buf2[2]) << 16;
			longlong += toByte(buf2[3]) << 24;
		}
		return longlong;
	}

	int read04() throws IOException {
		this.discard(buf2);
		if (data1 == 8195) {
			longlong = toByte(buf2[3]);
			longlong += toByte(buf2[2]) << 8;
			longlong += toByte(buf2[1]) << 16;
			longlong += buf2[0] << 24;
		} else {
			longlong = toByte(buf2[0]);
			longlong += toByte(buf2[1]) << 8;
			longlong += toByte(buf2[2]) << 16;
			longlong += buf2[3] << 24;
		}
		return longlong;
	}

	long read05() throws IOException {
		this.discard(buf3);
		if (data1 == 8195)
			_fldtry = ((long) toByte(buf3[7]) + ((long) toByte(buf3[6]) << 8)
					+ ((long) toByte(buf3[5]) << 16)
					+ ((long) toByte(buf3[4]) << 24)
					+ ((long) toByte(buf3[3]) << 32)
					+ ((long) toByte(buf3[2]) << 40)
					+ ((long) toByte(buf3[1]) << 48) + ((long) toByte(buf3[0]) << 56));
		else
			_fldtry = ((long) toByte(buf3[0]) + ((long) toByte(buf3[1]) << 8)
					+ ((long) toByte(buf3[2]) << 16)
					+ ((long) toByte(buf3[3]) << 24)
					+ ((long) toByte(buf3[4]) << 32)
					+ ((long) toByte(buf3[5]) << 40)
					+ ((long) toByte(buf3[6]) << 48) + ((long) toByte(buf3[7]) << 56));
		return _fldtry;
	}

	long read06() throws IOException {
		this.discard(buf3);
		long l;
		if (data1 == 8195) {
			l = (long) toByte(buf3[7]);
			l += (long) (toByte(buf3[6]) << 8);
			l += (long) (toByte(buf3[5]) << 16);
			l += (long) (toByte(buf3[4]) << 24);
			l += (long) (toByte(buf3[3]) << 32);
			l += (long) (toByte(buf3[2]) << 40);
			l += (long) (toByte(buf3[1]) << 48);
			l += (long) (toByte(buf3[0]) << 56);
		} else {
			l = (long) toByte(buf3[0]);
			l += (long) (toByte(buf3[1]) << 8);
			l += (long) (toByte(buf3[2]) << 16);
			l += (long) (toByte(buf3[3]) << 24);
			l += (long) (toByte(buf3[4]) << 32);
			l += (long) (toByte(buf3[5]) << 40);
			l += (long) (toByte(buf3[6]) << 48);
			l += (long) (toByte(buf3[7]) << 56);
		}
		return l;
	}

	String readString(int i) throws IOException {
		byte[] is = new byte[i];
		this.discard(is);
		return new String(is);
	}
}