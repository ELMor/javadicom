/*
 * TagValue - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.util.Vector;

public class TagValue {
	protected int group = 0;

	protected int element = 0;

	protected Vector val = new Vector();

	public int getGroup() {
		return group;
	}

	public int getElement() {
		return element;
	}

	public int size() {
		return val.size();
	}

	public Object getValue(int i) throws DicomException {
		if (i < val.size())
			return val.elementAt(i);
		throw new DicomException("Index exceeds bounds of array.");
	}

	public Object getValue() {
		Object object;
		try {
			object = getValue(0);
		} catch (DicomException dicomexception) {
			return null;
		}
		return object;
	}
}