/*
 * r - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.Vector;

class FIS5 extends FIS4 {
	Vector vec;

	FIS5(InputStream inputstream) {
		super(inputstream);
	}

	FIS5(InputStream inputstream, int i) {
		super(inputstream, i);
	}

	Vector _mthfor(int i, int i_0_) throws IOException {
		vec = new Vector();
		this.setReaded();
		for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
			vec.addElement(this.genericRead(i));
		return vec;
	}

	String s4(String string) {
		return string;
	}

	String s9(String string) {
		return string;
	}

	String s6(String string) {
		return string;
	}

	String s18(String string) {
		return string;
	}

	String s7(String string) {
		return string;
	}

	String s13(String string) {
		return string;
	}

	String s12(String string) {
		return string;
	}

	String s27(String string) {
		return string;
	}

	String s28(String string) {
		return string;
	}

	String s2(String string) {
		return string;
	}

	Object s16(String string) {
		if (Jdt.isDSAsString())
			return string;
		return new Float(string);
	}

	Integer s15(String string) {
		return new Integer(string);
	}

	Person s14(String string) {
		return new Person(string);
	}

	Object s11(String string) {
		DDate ddate;
		try {
			ddate = new DDate(string);
		} catch (NumberFormatException numberformatexception) {
			return new DDateRange(string);
		}
		return ddate;
	}

	Object a(String string, int i) {
		switch (i) {
		case 4:
			return s4(string);
		case 9:
			return s9(string);
		case 16:
			return s16(string);
		case 11:
			return s11(string);
		case 15:
			return s15(string);
		case 6:
			return s6(string);
		case 18:
			return s18(string);
		case 14:
			return s14(string);
		case 7:
			return s7(string);
		case 13:
			return s13(string);
		case 12:
			return s12(string);
		case 27:
			return s27(string);
		case 28:
			return s28(string);
		case 2:
			return s2(string);
		default:
			return null;
		}
	}

	Vector getVectorizedString(int i) throws IOException {
		vec = new Vector();
		StringTokenizer stringtokenizer = new StringTokenizer(this.readString(i),
				"\\");
		while (stringtokenizer.hasMoreTokens())
			vec.addElement(stringtokenizer.nextToken());
		return vec;
	}

	Vector readVector2(int i, int i_2_) throws IOException {
		Vector vector;
		if (i == 11 && i_2_ % 8 == 0) {
			String string = this.readString(i_2_);
			vector = new Vector();
			for (int i_3_ = 0; i_3_ < i_2_ / 8; i_3_++)
				vector.addElement(string.substring(i_3_ * 8, (i_3_ + 1) * 8));
		} else
			vector = getVectorizedString(i_2_);
		for (int i_4_ = 0; i_4_ < vector.size(); i_4_++) {
			Object object = a(((String) vector.elementAt(i_4_)).trim(), i);
			vector.setElementAt(object, i_4_);
		}
		return vector;
	}

	Vector readVector3(int i, int i_5_) throws IOException {
		byte[] is = new byte[i_5_];
		this.discard(is);
		if (i == 24 && this.getData1() == 8195)
			ByteSwapper.swapBytes(is);
		vec = new Vector();
		vec.addElement(is);
		return vec;
	}

	Vector readVector4(int i) throws IOException {
		int i_6_ = i / 4;
		float[] fs = new float[i_6_];
		for (int i_7_ = 0; i_7_ < i_6_; i_7_++)
			fs[i_7_] = Float.intBitsToFloat(this.read03());
		if (i % 4 > 0)
			this.discard(new byte[i % 4]);
		vec = new Vector();
		vec.addElement(fs);
		return vec;
	}

	Vector readVector5(int i, int i_8_) throws IOException {
		int i_9_ = DDict.getTypeFixed(i);
		if (i_9_ != 0)
			return _mthfor(i, i_8_ / i_9_);
		switch (i) {
		case 0:
		case 8:
		case 22:
		case 24:
			return readVector3(i, i_8_);
		case 29:
			return readVector4(i_8_);
		default:
			return readVector2(i, i_8_);
		}
	}
}