/*
 * DDate - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DDate {
	private Calendar _fldif;

	private boolean a = false;

	public DDate() {
		_fldif = new GregorianCalendar();
		a = true;
	}

	public DDate(String string) throws NumberFormatException {
		if (string.equals(""))
			a = true;
		else {
			if (string.length() != 8)
				throw new NumberFormatException(
						"Cannot parse string into DDate");
			int i = Integer.parseInt(string.substring(0, 4));
			int i_0_ = Integer.parseInt(string.substring(4, 6));
			int i_1_ = Integer.parseInt(string.substring(6, 8));
			_fldif = new GregorianCalendar(i, i_0_ - 1, i_1_);
		}
	}

	public String toString() {
		if (a)
			return "";
		SimpleDateFormat simpledateformat = new SimpleDateFormat(
				"EEE dd MMM yyyy");
		return simpledateformat.format(_fldif.getTime());
	}

	public String toString(DateFormat dateformat) {
		if (a)
			return "";
		return dateformat.format(_fldif.getTime());
	}

	public String toDICOMString() {
		if (a)
			return "";
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMdd");
		return simpledateformat.format(_fldif.getTime());
	}

	public boolean isEmpty() {
		return a;
	}
}