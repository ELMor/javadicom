/*
 * TransferSyntax - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

public class TransferSyntax extends UID {
	public static final int PrivateTransferSyntax = 8192;

	public static final int ImplicitVRLittleEndian = 8193;

	public static final int ExplicitVRLittleEndian = 8194;

	public static final int ExplicitVRBigEndian = 8195;

	public static final int RLELossless = 8198;

	public static final int JPEG_1 = 8196;

	public static final int JPEGBaseline = 8196;

	public static final int JPEG_14_SelectionValue1 = 8197;

	public static final int JPEGLossless = 8197;

	public static final int JPEG_2_4 = 8199;

	public static final int JPEG_3_5 = 8200;

	public static final int JPEG_6_8 = 8201;

	public static final int JPEG_7_9 = 8202;

	public static final int JPEG_10_12 = 8203;

	public static final int JPEG_11_13 = 8204;

	public static final int JPEG_14 = 8205;

	public static final int JPEG_15 = 8206;

	public static final int JPEG_16_18 = 8207;

	public static final int JPEG_17_19 = 8208;

	public static final int JPEG_20_22 = 8209;

	public static final int JPEG_21_23 = 8210;

	public static final int JPEG_24_26 = 8211;

	public static final int JPEG_25_27 = 8212;

	public static final int JPEG_28 = 8213;

	public static final int JPEG_29 = 8214;

	public static final int DeflatedExplicitVRLittleEndian = 8215;

	public static final int JPEGLSLossless = 8216;

	public static final int JPEGLSLossy = 8217;

	public static final int JPEG2000LosslessOnly = 8224;

	public static final int JPEG2000 = 8225;

	static {
		entries.put(new Integer(8192), new UIDEntry(8192, "private",
				"Private Transfer Syntax", "PR", 3));
		entries.put(new Integer(8193), new UIDEntry(8193, "1.2.840.10008.1.2",
				"Implicit VR Little Endian Transfer Syntax", "IL", 3));
		entries.put(new Integer(8194), new UIDEntry(8194,
				"1.2.840.10008.1.2.1",
				"Explicit VR Little Endian Transfer Syntax", "EL", 3));
		entries.put(new Integer(8195), new UIDEntry(8195,
				"1.2.840.10008.1.2.2",
				"Explicit VR Big Endian Transfer Syntax", "EB", 3));
		entries.put(new Integer(8196), new UIDEntry(8196,
				"1.2.840.10008.1.2.4.50",
				"JPEG Baseline Transfer Syntax (Process 1)", "JB", 3));
		entries
				.put(
						new Integer(8197),
						(new UIDEntry(
								8197,
								"1.2.840.10008.1.2.4.70",
								"JPEG Lossless Transfer Syntax (Process 14, Selection Value 1)",
								"JL", 3)));
		entries
				.put(new Integer(8198), new UIDEntry(8198,
						"1.2.840.10008.1.2.5", "RLE Lossless Transfer Syntax",
						"RL", 3));
		entries.put(new Integer(8199), new UIDEntry(8199,
				"1.2.840.10008.1.2.4.51", "JPEG Extended (Process 2 & 4)",
				"JP_2_4", 3));
		entries.put(new Integer(8200), new UIDEntry(8200,
				"1.2.840.10008.1.2.4.52", "JPEG Extended (Process 3 & 5)",
				"JP_3_5", 3));
		entries.put(new Integer(8201), (new UIDEntry(8201,
				"1.2.840.10008.1.2.4.53",
				"JPEG Spectral Selection, Non-Hierarchical (Process 6 & 8)",
				"JP_6_8", 3)));
		entries.put(new Integer(8202), (new UIDEntry(8202,
				"1.2.840.10008.1.2.4.54",
				"JPEG Spectral Selection, Non-Hierarchical (Process 7 & 9)",
				"JP_7_9", 3)));
		entries.put(new Integer(8203), (new UIDEntry(8203,
				"1.2.840.10008.1.2.4.55",
				"JPEG Full Progression, Non-Hierarchical (Process 10 & 12)",
				"JP_10_12", 3)));
		entries.put(new Integer(8204), (new UIDEntry(8204,
				"1.2.840.10008.1.2.4.56",
				"JPEG Full Progression, Non-Hierarchical (Process 11 & 13)",
				"JP_11_13", 3)));
		entries.put(new Integer(8205), new UIDEntry(8205,
				"1.2.840.10008.1.2.4.57",
				"JPEG Lossless, Non-Hierarchical (Process 14)", "JP_14", 3));
		entries.put(new Integer(8206), new UIDEntry(8206,
				"1.2.840.10008.1.2.4.58",
				"JPEG Lossless, Non-Hierarchical (Process 15)", "JP_15", 3));
		entries
				.put(new Integer(8207), new UIDEntry(8207,
						"1.2.840.10008.1.2.4.59",
						"JPEG Extended, Hierarchical (Process 16 & 18)",
						"JP_16_18", 3));
		entries
				.put(new Integer(8208), new UIDEntry(8208,
						"1.2.840.10008.1.2.4.60",
						"JPEG Extended, Hierarchical (Process 17 & 19)",
						"JP_17_19", 3));
		entries.put(new Integer(8209), (new UIDEntry(8209,
				"1.2.840.10008.1.2.4.61",
				"JPEG Spectral Selection, Hierarchical (Process 20 & 22)",
				"JP_20_22", 3)));
		entries.put(new Integer(8210), (new UIDEntry(8210,
				"1.2.840.10008.1.2.4.62",
				"JPEG Spectral Selection, Hierarchical (Process 21 & 23)",
				"JP_21_23", 3)));
		entries.put(new Integer(8211), (new UIDEntry(8211,
				"1.2.840.10008.1.2.4.63",
				"JPEG Full Progression, Hierarchical (Process 24 & 26)",
				"JP_24_26", 3)));
		entries.put(new Integer(8212), (new UIDEntry(8212,
				"1.2.840.10008.1.2.4.64",
				"JPEG Full Progression, Hierarchical (Process 25 & 27)",
				"JP_25_27", 3)));
		entries.put(new Integer(8213), new UIDEntry(8213,
				"1.2.840.10008.1.2.4.65",
				"JPEG Lossless, Hierarchical (Process 28)", "JP_28", 3));
		entries.put(new Integer(8214), new UIDEntry(8214,
				"1.2.840.10008.1.2.4.66",
				"JPEG Lossless, Hierarchical (Process 29)", "JP_29", 3));
		entries.put(new Integer(8215), new UIDEntry(8215,
				"1.2.840.10008.1.2.1.99", "Deflated Explicit VR Little Endian",
				"DEL", 3));
		entries.put(new Integer(8216), new UIDEntry(8216,
				"1.2.840.10008.1.2.4.80", "JPEG-LS Lossless Image Compression",
				"JPLS", 3));
		entries.put(new Integer(8217), new UIDEntry(8217,
				"1.2.840.10008.1.2.4.81",
				"JPEG-LS Lossy (Near-Lossless) Image Compression", "JPLY", 3));
		entries.put(new Integer(8224), new UIDEntry(8224,
				"1.2.840.10008.1.2.4.90",
				"JPEG 2000 Image Compression (Lossless Only)", "JP2L", 3));
		entries.put(new Integer(8225), new UIDEntry(8225,
				"1.2.840.10008.1.2.4.91", "JPEG 2000 Image Compression",
				"JP20", 3));
	}
}