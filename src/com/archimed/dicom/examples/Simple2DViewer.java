
package com.archimed.dicom.examples;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import com.archimed.dicom.DDict;
import com.archimed.dicom.DicomException;
import com.archimed.dicom.DicomObject;
import com.archimed.dicom.IllegalValueException;
import com.archimed.dicom.UnknownUIDException;

public class Simple2DViewer extends javax.swing.JFrame {
	public Simple2DViewer() {
		/* A call to DDict. */
		DDict.initDDict();
		setJMenuBar(JMenuBar1);
		setTitle("Java2D DICOM Viewer");
		setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setSize(405, 305);
		setVisible(false);
		JPanel2.setLayout(new GridLayout(1, 1, 0, 0));
		getContentPane().add("East", JPanel2);
		JPanel2.setBounds(363, 0, 42, 305);
		windowSlider.setOrientation(javax.swing.JSlider.VERTICAL);
		JPanel2.add(windowSlider);
		windowSlider.setBounds(0, 0, 21, 305);
		levelSlider.setOrientation(javax.swing.JSlider.VERTICAL);
		JPanel2.add(levelSlider);
		levelSlider.setBounds(21, 0, 21, 305);
		fileMenu.setText("File");
		fileMenu.setActionCommand("File");
		JMenuBar1.add(fileMenu);
		openMenuItem.setText("Open");
		openMenuItem.setActionCommand("jmenuItem");
		fileMenu.add(openMenuItem);
		exitMenuItem.setText("Exit");
		exitMenuItem.setActionCommand("Exit");
		fileMenu.add(exitMenuItem);
		dicomMenu.setText("Dicom");
		dicomMenu.setActionCommand("Dicom");
		JMenuBar1.add(dicomMenu);
		tagsMenuItem.setText("View Dicom Tags");
		tagsMenuItem.setActionCommand("View Dicom Tags");
		dicomMenu.add(tagsMenuItem);
		JPanel1.setLayout(new BorderLayout(0, 0));
		getContentPane().add("Center", JPanel1);
		JPanel1.setBounds(0, 0, 405, 305);
		imageScrollPane
				.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		imageScrollPane
				.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		JPanel1.add("Center", imageScrollPane);
		imageScrollPane.setBounds(0, 0, 405, 305);
		imageScrollPane.setViewportView(imagePanel);
		getContentPane().add(toolbar, BorderLayout.NORTH);
		toolbar.add(new ZoomoutAction(null,
				new ImageIcon(getClass().getResource(
						"/com/archimed/dicom/examples/images/zoomout.gif"))));
		toolbar
				.add(new ZoominAction(
						null,
						new ImageIcon(
								getClass()
										.getResource(
												"/com/archimed/dicom/examples/images/zoomin.gif"))));
		toolbar.add(new FitAction(null, new ImageIcon(getClass().getResource(
				"/com/archimed/dicom/examples/images/fit.gif"))));
		toolbar
				.add(new NozoomAction(
						null,
						new ImageIcon(
								getClass()
										.getResource(
												"/com/archimed/dicom/examples/images/nozoom.gif"))));
		SymAction lSymAction = new SymAction();
		exitMenuItem.addActionListener(lSymAction);
		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		openMenuItem.addActionListener(lSymAction);
		SymChange lSymChange = new SymChange();
		windowSlider.addChangeListener(lSymChange);
		levelSlider.addChangeListener(lSymChange);
		tagsMenuItem.addActionListener(lSymAction);
	}

	class ZoomoutAction extends AbstractAction {
		public ZoomoutAction(String name, Icon icon) {
			super(name, icon);
		}

		public void actionPerformed(ActionEvent e) {
			scalefactor = scalefactor / 2;
			imagePanel.scale(scalefactor);
			imageScrollPane.doLayout();
		}
	}

	class ZoominAction extends AbstractAction {
		public ZoominAction(String name, Icon icon) {
			super(name, icon);
		}

		public void actionPerformed(ActionEvent e) {
			scalefactor = scalefactor * 2;
			imagePanel.scale(scalefactor);
			imageScrollPane.doLayout();
		}
	}

	class FitAction extends AbstractAction {
		public FitAction(String name, Icon icon) {
			super(name, icon);
		}

		public void actionPerformed(ActionEvent e) {
			imagePanel.fit(new Dimension(imageScrollPane.getSize().width - 3,
					imageScrollPane.getSize().height - 3));
			imageScrollPane.doLayout();
		}
	}

	class NozoomAction extends AbstractAction {
		public NozoomAction(String name, Icon icon) {
			super(name, icon);
		}

		public void actionPerformed(ActionEvent e) {
			scalefactor = 1.0f;
			imagePanel.scale(1.0f);
			imageScrollPane.doLayout();
		}
	}

	public Simple2DViewer(String sTitle) {
		this();
		setTitle(sTitle);
	}

	public void setVisible(boolean b) {
		if (b) {
			setLocation(50, 50);
		}
		super.setVisible(b);
	}

	static public void main(String args[]) {
		Simple2DViewer viewer = new Simple2DViewer();
		viewer.setVisible(true);
	}

	void open() {
		windowSlider.setEnabled(false);
		levelSlider.setEnabled(false);
		FileDialog fd = new FileDialog(this, "Select DICOM File");
		if (directory != null) {
			fd.setDirectory(directory);
		} else {
			fd.setFile("jdt.dcm");
		}
		fd.setVisible(true);
		if (fd.getFile() == null) {
			return;
		}
		directory = fd.getDirectory();
		File f = new File(directory + File.separator + fd.getFile());
		filename = f.getName();
		try {
			FileInputStream fin = new FileInputStream(f);
			BufferedInputStream bis = new BufferedInputStream(fin);
			dcm = new DicomObject();
			dcm.read(bis, true);
			String name = dcm.getS(DDict.dPatientName);
			if (name != null)
				name = name.replace('^', ' ');
			else
				name = "No name";
			setTitle("Java2D DICOM Viewer - " + name);
			imageIo2 = new ImageIO2(dcm);
			imagePanel.setImage(imageIo2.getBufferedImage(0));

			if ((imageIo2.getPhotometricInterpretation() == ImageIO2.MONOCHROME2)
					|| (imageIo2.getPhotometricInterpretation() == ImageIO2.MONOCHROME1)) {
				opening = true;
				windowSlider.setEnabled(true);
				levelSlider.setEnabled(true);
				windowSlider.setMinimum(0);
				levelSlider.setMinimum(0);
				windowSlider.setMaximum(imageIo2.getMaxWindow());
				levelSlider.setMaximum(imageIo2.getMaxCenter());
				windowSlider.setValue(imageIo2.getWindow());
				levelSlider.setValue(imageIo2.getCenter());
				opening = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		imageScrollPane.doLayout();

	}

	class SymAction implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent event) {
			Object object = event.getSource();
			if (object == exitMenuItem) {
				exitMenuItem_actionPerformed(event);
			} else if (object == openMenuItem) {
				openMenuItem_actionPerformed(event);
			} else if (object == tagsMenuItem) {
				tagsMenuItem_actionPerformed(event);
			}
		}
	}

	void exitMenuItem_actionPerformed(java.awt.event.ActionEvent event) {
		this.setVisible(false);
		System.exit(0);
	}

	void openMenuItem_actionPerformed(java.awt.event.ActionEvent event) {
		open();
	}

	void tagsMenuItem_actionPerformed(java.awt.event.ActionEvent event) {
		if (dcm == null) {
			return;
		}
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		PrintStream pout = new PrintStream(bout);
		try {
			dcm.dumpVRs(pout, true);
		} catch (IOException e) {
		}
		TagsDialog dlg = new TagsDialog(this, "Dicom Tags", true);
		dlg.setText(new String(bout.toByteArray()));
		//dlg.setLocationRelativeTo(this.getParent());
		dlg.setVisible(true);
	}

	class SymWindow extends java.awt.event.WindowAdapter {
		public void windowClosing(java.awt.event.WindowEvent event) {
			Object object = event.getSource();
			if (object == Simple2DViewer.this) {
				Simple2DViewer_windowClosing(event);
			}
		}
	}

	void Simple2DViewer_windowClosing(java.awt.event.WindowEvent event) {
		this.setVisible(false);
		System.exit(0);
	}

	class SymChange implements javax.swing.event.ChangeListener {
		public void stateChanged(javax.swing.event.ChangeEvent event) {
			Object object = event.getSource();
			if (object == windowSlider) {
				windowSlider_stateChanged(event);
			} else if (object == levelSlider) {
				levelSlider_stateChanged(event);
			}
		}
	}

	void windowSlider_stateChanged(javax.swing.event.ChangeEvent event) {
		if (opening == false) {
			setWindow();
		}
	}

	void levelSlider_stateChanged(javax.swing.event.ChangeEvent event) {
		if (opening == false) {
			setLevel();
		}
	}

	void setLevel() {
		imageIo2.setCenter(levelSlider.getValue());
		imagePanel.setImage(imageIo2.calculate8BitImage());
		if ((levelSlider.getValueIsAdjusting() == false)
				&& (imageIo2.getNumberOfBits() > 8)) {
			imagePanel.setImage(imageIo2.calculate8BitFrom16Bit());
		}
	}

	void setWindow() {
		imageIo2.setWindow(windowSlider.getValue());
		imagePanel.setImage(imageIo2.calculate8BitImage());
		if ((windowSlider.getValueIsAdjusting() == false)
				&& (imageIo2.getNumberOfBits() > 8)) {
			imagePanel.setImage(imageIo2.calculate8BitFrom16Bit());
		}
	}

	private ImagePanel imagePanel = new ImagePanel();

	private String directory;

	private ColorModel model;

	private DicomObject dcm;

	private BufferedImage image;

	private ImageIO2 imageIo2;

	private byte[] gray;

	private boolean opening = false;

	private float scalefactor = 1.0f;

	private Object pixels;

	private String filename;

	JPanel JPanel2 = new JPanel();

	JSlider windowSlider = new JSlider();

	JSlider levelSlider = new JSlider();

	JPanel JPanel1 = new JPanel();

	JScrollPane imageScrollPane = new JScrollPane();

	JMenuBar JMenuBar1 = new JMenuBar();

	JMenu fileMenu = new JMenu();

	JMenuItem openMenuItem = new JMenuItem();

	JMenuItem exitMenuItem = new JMenuItem();

	JMenu dicomMenu = new JMenu();

	JMenuItem tagsMenuItem = new JMenuItem();

	JToolBar toolbar = new JToolBar();
}