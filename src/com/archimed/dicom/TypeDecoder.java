/*
 * i - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.dicom;

class TypeDecoder {
	static Object str(String string, int i) throws DicomException {
		string = string.trim();
		if (string.equals(""))
			return null;
		switch (i) {
		case 2:
		case 4:
		case 6:
		case 7:
		case 9:
		case 12:
		case 13:
		case 18:
		case 27:
		case 28:
			return string;
		case 17:
			if (string.length() != 0 && string.length() != 4)
				throw new DicomException("Type AS has to have 4 bytes fixed.");
			return string;
		case 14:
			return new Person(string.trim());
		case 11:
			return new DDate(string);
		case 16:
			if (Jdt.isDSAsString())
				return string;
			return new Float(string);
		case 26:
			return new Float(string);
		case 15:
			return new Integer(string);
		case 23:
			return new Short(string);
		case 3:
		case 21:
			return new Integer(string);
		case 1:
		case 19:
			return new Long(string);
		case 20:
			return new Double(string);
		case 0:
		case 8:
		case 22:
		case 24:
			return string.getBytes();
		default:
			throw new DicomException("String used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object byteArray(byte[] is, int i) throws DicomException {
		switch (i) {
		case 0:
		case 8:
		case 22:
		case 24:
			return is;
		default:
			throw new DicomException("byte[] used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object integ(Integer integer, int i) throws DicomException {
		if (integer == null)
			return null;
		switch (i) {
		case 23:
			return new Short(integer.shortValue());
		case 3:
		case 5:
		case 15:
		case 21:
			return integer;
		case 1:
		case 19:
			return new Long((long) integer.intValue());
		default:
			throw new DicomException("Integer used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object flo(Float var_float, int i) throws DicomException {
		if (var_float == null)
			return null;
		switch (i) {
		case 16:
		case 26:
			return var_float;
		case 20:
			return new Double(var_float.doubleValue());
		default:
			throw new DicomException("Float used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object dou(Double var_double, int i) throws DicomException {
		if (var_double == null)
			return null;
		switch (i) {
		case 20:
			return var_double;
		case 16:
		case 26:
			return new Float(var_double.floatValue());
		default:
			throw new DicomException("Double used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object person(Person person, int i) throws DicomException {
		switch (i) {
		case 14:
			return person;
		default:
			throw new DicomException("Person used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object dat(DDate ddate, int i) throws DicomException {
		switch (i) {
		case 11:
			return ddate;
		default:
			throw new DicomException("DDate used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object dateRange(DDateRange ddaterange, int i) throws DicomException {
		switch (i) {
		case 11:
			return ddaterange;
		default:
			throw new DicomException("DDateRange used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object shor(Short var_short, int i) throws DicomException {
		switch (i) {
		case 23:
			return var_short;
		default:
			throw new DicomException("Short used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object lon(Long var_long, int i) throws DicomException {
		switch (i) {
		case 1:
		case 19:
			return var_long;
		default:
			throw new DicomException("Long used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object a(int[] is, int i) throws DicomException {
		if (is == null)
			return null;
		if (is.length != 2)
			throw new DicomException("Only int[2] can be parsed");
		switch (i) {
		case 5:
			return new Integer((is[0] << 16) + is[1]);
		default:
			throw new DicomException("2 ints used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object floatArray(float[] fs, int i) throws DicomException {
		if (fs == null)
			return null;
		switch (i) {
		case 29:
			return fs;
		default:
			throw new DicomException("float array used for type: "
					+ DDict.getTypeName(i));
		}
	}

	static Object dicomObject(DicomObject dicomobject, int i) throws DicomException {
		if (i == 10)
			return dicomobject;
		throw new DicomException("DicomObject used for type: "
				+ DDict.getTypeName(i));
	}

	static String generic(Object object, int i) {
		String string = "";
		if (object == null)
			return null;
		switch (i) {
		case 2:
		case 4:
		case 6:
		case 7:
		case 9:
		case 12:
		case 13:
		case 17:
		case 18:
		case 27:
		case 28:
			string = ((String) object).trim();
			break;
		case 14:
			string = ((Person) object).toDICOMString('^');
			break;
		case 11:
			try {
				string = ((DDate) object).toDICOMString();
			} catch (ClassCastException classcastexception) {
				string = ((DDateRange) object).toDICOMString();
			}
			break;
		case 16:
		case 26:
			string = object.toString();
			break;
		case 3:
		case 15:
		case 21:
			string = ((Integer) object).toString();
			break;
		case 23:
			string = ((Short) object).toString();
			break;
		case 1:
		case 19:
			string = ((Long) object).toString();
			break;
		case 20:
			string = ((Double) object).toString();
			break;
		case 0:
			string = new String((byte[]) object);
			break;
		case 8:
		case 22:
		case 24: {
			string = "";
			int i_0_ = ((byte[]) object).length;
			if (i_0_ > 12)
				i_0_ = 12;
			for (int i_1_ = 0; i_1_ < i_0_; i_1_++) {
				if (i_1_ != i_0_ - 1)
					string += ObjPrinter.a(((byte[]) object)[i_1_], 2) + "\\";
				else
					string += ObjPrinter.a(((byte[]) object)[i_1_], 2);
			}
			if (i_0_ == 12)
				string += "...";
			break;
		}
		case 5: {
			int i_2_ = ((Integer) object).intValue() >> 16;
			int i_3_ = ((Integer) object).intValue() & 0xffff;
			string = "(" + ObjPrinter.a(i_2_, 4) + "," + ObjPrinter.a(i_3_, 4) + ")";
			break;
		}
		default:
			return null;
		}
		return string;
	}

	static int obj(Object object, int i) throws DicomException {
		if (object == null)
			return 2147483647;
		switch (i) {
		case 23:
			return ((Short) object).intValue();
		case 3:
		case 5:
		case 15:
		case 21:
			return ((Integer) object).intValue();
		case 1:
		case 19:
			return ((Long) object).intValue();
		default:
			throw new DicomException("unable to convert "
					+ DDict.getTypeName(i) + " to int");
		}
	}
}