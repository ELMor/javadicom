/*
 * b - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.utils.helpers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamReader {
	int _fldint = 0;

	int _fldif = 0;

	int a = _fldint * _fldif;

	int type;

	int[] array;

	static final int _fldfor = 128;

	public byte[] _mthint(InputStream inputstream) throws IOException {
		int i = 0;
		boolean bool = false;
		byte[] is = new byte[a];
		while (i < a) {
			int i_0_ = (byte) inputstream.read();
			if (i_0_ >= 0 && i_0_ <= 127) {
				for (int i_1_ = 0; i_1_ < i_0_ + 1; i_1_++) {
					if (i < a)
						is[i++] = (byte) inputstream.read();
				}
			} else if (i_0_ <= -1 && i_0_ >= -127) {
				byte i_2_ = (byte) inputstream.read();
				for (int i_3_ = 0; i_3_ < -i_0_ + 1; i_3_++) {
					if (i < a)
						is[i++] = i_2_;
				}
			}
		}
		return is;
	}

	public byte[] _mthif(InputStream isIn) {
		boolean bool = false;
		int i = 0;
		int i_4_ = 0;
		int i_5_ = 0;
		byte[] is = new byte[128];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		for (;;) {
			int eofMarker;
			try {
				eofMarker = isIn.read();
			} catch (Exception exception) {
				break;
			}
			if (i_4_ == 0) {
				if (i_5_ == 0) {
					i = eofMarker;
					i_5_++;
				} else if (i == eofMarker) {
					if (++i_5_ == 128) {
						baos.write(1 - i_5_);
						baos.write(i);
						i_5_ = 0;
					}
				} else if (i_5_ > 2) {
					baos.write(1 - i_5_);
					baos.write(i);
					i_5_ = 1;
					i = eofMarker;
				} else {
					for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
						is[i_4_++] = (byte) i;
						if (i_4_ == 128) {
							baos.write(i_4_ - 1);
							baos.write(is, 0, i_4_);
							i_4_ = 0;
						}
					}
					i_5_ = 0;
					is[i_4_++] = (byte) eofMarker;
					if (i_4_ == 128) {
						baos.write(i_4_ - 1);
						baos.write(is, 0, i_4_);
						i_4_ = 0;
					}
				}
			} else if (i_5_ != 0) {
				if (eofMarker == i) {
					if (++i_5_ == 128) {
						baos.write(i_4_ - 1);
						baos.write(is, 0, i_4_);
						baos.write(1 - i_5_);
						baos.write(i);
						i_4_ = i_5_ = 0;
					}
				} else {
					baos.write(i_4_ - 1);
					baos.write(is, 0, i_4_);
					baos.write(1 - i_5_);
					baos.write(i);
					i_4_ = 0;
					i_5_ = 1;
					i = eofMarker;
				}
			} else if (is[i_4_ - 1] == eofMarker) {
				i = eofMarker;
				i_5_ = 2;
				i_4_--;
			} else {
				is[i_4_++] = (byte) eofMarker;
				if (i_4_ == 128) {
					baos.write(i_4_ - 1);
					baos.write(is, 0, i_4_);
				}
			}
		}
		return baos.toByteArray();
	}

	public void intArrayRead(InputStream inputstream) throws IOException {
		type = com.archimed.utils.helpers.LongReader.read(inputstream);
		array = new int[15];
		for (int i = 0; i < 15; i++)
			array[i] = com.archimed.utils.helpers.LongReader.read(inputstream);
	}

	public byte[] read2DArray(InputStream inputstream) throws IOException {
		intArrayRead(inputstream);
		byte[][] is = new byte[type][];
		for (int i = 0; i < type; i++)
			is[i] = _mthint(inputstream);
		return is[0];
	}

	public void a(int i, int i_8_) {
		_fldint = i;
		_fldif = i_8_;
		a = i * i_8_;
	}

	public byte[] returnNull(InputStream inputstream) throws IOException {
		return null;
	}
}