/*
 * c - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.utils.img;

import java.io.IOException;

import com.archimed.dicom.Debug;

public class JPEG {
	private static final int q = 57;

	private static final int i = 1;

	private static final int h = 255;

	private static final int _flddo = 255;

	private static final byte _fldelse = -1;

	private static final int m = 65535;

	public static final int _fldtry = 208;

	public static final int e = 195;

	public static final int p = 216;

	public static final int k = 218;

	private static final int _fldnull = 0;

	private long j;

	private int b = 0;

	private int _fldlong = 0;

	private byte w;

	private byte _fldbyte;

	int _fldnew;

	private DataHolder _fldgoto = new DataHolder();

	int u;

	private byte[] x;

	int s;

	private int t;

	private byte[] retHolder;

	private int g = 0;

	int _fldcase;

	int n;

	private int a;

	private int _fldint;

	private int v;

	public static final int o = 224;

	public static final int[] powersMinus = { 0, 1, 3, 7, 15, 31, 63, 127, 255,
			511, 1023, 2047, 4095, 8191, 16383, 32767, 65535 };

	public static final int l = 196;

	public static final int f = 221;

	private static final int c = 8;

	public static final int _fldvoid = 217;

	private static final String err = "bad Huffman code";

	static final int[] negPowers = { 0, -1, -3, -7, -15, -31, -63, -127, -255,
			-511, -1023, -2047, -4095, -8191, -16383, -32767 };

	static final int[] powers = { 0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024,
			2048, 4096, 8192, 16384 };

	public byte[] process01(byte[] is) throws IOException {
		b = 0;
		j = 0L;
		_fldlong = 0;
		x = is;
		_mthif();
		int i;
		if (v > 8)
			i = a * _fldint * t * 2;
		else
			i = a * _fldint * t;
		retHolder = new byte[i];
		processRet();
		return retHolder;
	}

	private void _mthtry() throws IOException {
		boolean bool = false;
		boolean bool_0_ = false;
		boolean bool_1_ = false;
		for (int i = 0; i < t; i++) {
			int i_2_ = _fldgoto.i2Array[i].d1;
			int i_3_ = _mthif(i_2_);
			int i_4_;
			if (i_3_ == 16)
				i_4_ = 32768;
			else if (i_3_ != 0) {
				i_4_ = _mthlong(i_3_);
				i_4_ = a(i_4_, i_3_);
			} else
				i_4_ = 0;
			if (v > 8)
				_mthcase(i_4_);
			else
				a(i_4_);
		}
		for (int i = 1; i < a; i++) {
			for (int i_5_ = 0; i_5_ < t; i_5_++) {
				int i_6_ = _fldgoto.i2Array[i_5_].d1;
				int i_7_ = _mthif(i_6_);
				int i_8_;
				if (i_7_ == 16)
					i_8_ = 32768;
				else if (i_7_ != 0) {
					i_8_ = _mthlong(i_7_);
					i_8_ = a(i_8_, i_7_);
				} else
					i_8_ = 0;
				if (v > 8)
					_mthtry(i_8_);
				else
					_mthint(i_8_);
			}
		}
		if (_fldgoto.i4 != 0)
			_fldgoto.i5--;
	}

	private final int a(int i, int i_9_) {
		if (i < powers[i_9_])
			return i + negPowers[i_9_];
		return i;
	}

	private final void _mthchar(int i) {
		for (/**/; b < 57; b += 8) {
			if (_fldlong == x.length) {
				w = (byte) 0;
				break;
			}
			w = x[_fldlong++];
			if (w == -1) {
				_fldbyte = x[_fldlong++];
				if (_fldbyte != 0) {
					_fldlong = _fldlong - 2;
					if (b >= i)
						break;
					w = (byte) 0;
				}
			}
			j = j << 8 | (long) (w & 0xff);
		}
	}

	private final void _mthvoid(int i) {
		b -= i;
	}

	private final int a() {
		if (b == 0)
			_mthchar(1);
		return (int) (j >> --b) & 0x1;
	}

	private final int _mthlong(int i) {
		if (b < i)
			_mthchar(i);
		return (int) (j >> (b -= i) & (long) powersMinus[i]);
	}

	private int a(byte[] is, int i) {
		return (is[i] & 0xff) + 256 * (is[i + 1] & 0xff);
	}

	private final int _mthint() {
		int i = (x[_fldlong++] & 0xff) << 8;
		i += x[_fldlong++] & 0xff;
		return i;
	}

	private int _mthif(int i) throws IOException {
		_fldnew = _mthfor();
		if (_fldgoto.huffmans[i]._fldif[_fldnew] != 0)
			_mthnull(i);
		else {
			b -= 8;
			u = 8;
			_mthfor(i);
			if (u > 16)
				throw new IOException("bad Huffman code");
			_mthbyte(i);
		}
		return n;
	}

	private void _mthbyte(int i) {
		s = _fldgoto.huffmans[i]._fldtry[u];
		s += _fldnew - _fldgoto.huffmans[i]._fldgoto[u];
		n = _fldgoto.huffmans[i].a[s];
	}

	private void _mthnew(int i) throws IOException {
		int i_10_ = _fldgoto.i2Array[i].d1;
		int i_11_ = _mthif(i_10_);
		int i_12_ = 0;
		if (i_11_ == 16)
			i_12_ = 32768;
		else if (i_11_ != 0) {
			i_12_ = _mthlong(i_11_);
			i_12_ = a(i_12_, i_11_);
		}
		if (v > 8) {
			byte i_13_ = (byte) (a(retHolder, g - 2 * t * a) + i_12_);
			retHolder[g] = i_13_;
			i_13_ = (byte) (a(retHolder, g - 2 * t * a) + i_12_ >> 8);
			retHolder[g + 1] = i_13_;
			g += 2;
		} else {
			byte i_14_ = (byte) ((retHolder[g - t * a] & 0xff) + i_12_);
			retHolder[g] = i_14_;
			g++;
		}
	}

	private void _mthnull(int i) {
		_mthvoid(_fldgoto.huffmans[i]._fldif[_fldnew]);
		n = _fldgoto.huffmans[i]._fldchar[_fldnew];
	}

	private void _mthcase(int i) {
		byte i_15_ = (byte) ((1 << v - 1) + i);
		retHolder[g] = i_15_;
		i_15_ = (byte) ((1 << v - 1) + i >> 8);
		retHolder[g + 1] = i_15_;
		g += 2;
	}

	private void _mthdo(int i) {
		int i_16_ = (a(a(retHolder, g - 2 * t), a(retHolder, g - a * 2 * t), a(
				retHolder, g - a * 2 * t - 2 * t)) + i);
		byte i_17_ = (byte) i_16_;
		retHolder[g] = i_17_;
		i_17_ = (byte) (i_16_ >> 8);
		retHolder[g + 1] = i_17_;
		g += 2;
	}

	private void _mthtry(int i) {
		byte i_18_ = (byte) (a(retHolder, g - 2 * t) + i);
		retHolder[g] = i_18_;
		i_18_ = (byte) (a(retHolder, g - 2 * t) + i >> 8);
		retHolder[g + 1] = i_18_;
		g += 2;
	}

	private void _mthelse(int i) throws IOException {
		int i_19_ = _mthif(_fldgoto.i2Array[i].d1);
		int i_20_ = 0;
		if (i_19_ != 0) {
			i_20_ = _mthlong(i_19_);
			i_20_ = a(i_20_, i_19_);
		}
		if (v > 8)
			_mthdo(i_20_);
		else
			_mthgoto(i_20_);
	}

	private void a(int i) {
		byte i_21_ = (byte) ((1 << v - 1) + i);
		retHolder[g] = i_21_;
		g++;
	}

	private void _mthgoto(int i) {
		retHolder[g] = (byte) (a(retHolder[g - t] & 0xff,
				retHolder[g - a * t] & 0xff, retHolder[g - a * t - t] & 0xff) + i);
		g++;
	}

	private void _mthint(int i) {
		byte i_22_ = (byte) ((retHolder[g - t] & 0xff) + i);
		retHolder[g] = i_22_;
		g++;
	}

	private void _mthfor(int i) {
		do {
			if (_fldnew <= _fldgoto.huffmans[i]._fldbyte[u])
				break;
			_fldcase = a();
			_fldnew = _fldnew << 1 | _fldcase;
			u++;
		} while (_fldnew != 65535);
	}

	private int a(int i, int i_23_, int i_24_) {
		switch (_fldgoto.i3) {
		case 0:
			break;
		case 1:
			return i;
		case 2:
			return i_23_;
		case 3:
			return i_24_;
		case 4:
			return i + i_23_ - i_24_;
		case 5:
			return i + (i_23_ - i_24_ >> 1);
		case 6:
			return i_23_ + (i - i_24_ >> 1);
		case 7:
			return i + i_23_ >> 1;
		}
		return 0;
	}

	private void processRet() throws IOException {
		g = 0;
		_fldgoto.i4 = _fldgoto.i6 / a;
		_fldgoto.i5 = _fldgoto.i4;
		_fldgoto.i7 = 0;
		_mthtry();
		for (int i = 1; i < _fldint; i++) {
			if (i != 0 && _fldgoto.i4 != 0) {
				if (_fldgoto.i5 == 0) {
					_mthdo();
					_mthtry();
					continue;
				}
				_fldgoto.i5--;
			}
			for (int i_25_ = 0; i_25_ < t; i_25_++)
				_mthnew(i_25_);
			for (int i_26_ = 1; i_26_ < a; i_26_++) {
				for (int i_27_ = 0; i_27_ < t; i_27_++)
					_mthelse(i_27_);
			}
		}
	}

	private final void _mthif() throws IOException {
		boolean bool = false;
		boolean bool_28_ = false;
		boolean bool_29_ = false;
		boolean bool_30_ = false;
		int i = _mthint();
		if ((i & 0xff) != 216)
			throw new IOException("Not a valid lossless JPEG file");
		do {
			i = _mthint();
			switch (i & 0xff) {
			case 196: {
				Huffman var_a = new Huffman();
				_fldlong = var_a.a(x, _fldlong);
				_fldgoto.huffmans[var_a._fldcase] = var_a;
				break;
			}
			case 195: {
				int i_31_ = _mthint();
				v = x[_fldlong++];
				_fldint = _mthint();
				a = _mthint();
				int i_32_ = x[_fldlong++];
				_fldgoto.i3Array = new Integers3[i_32_ + 1];
				for (int i_33_ = 0; i_33_ < i_32_; i_33_++) {
					byte i_34_ = x[_fldlong++];
					_fldgoto.i3Array[i_34_] = new Integers3();
					int i_35_ = x[_fldlong++];
					_fldgoto.i3Array[i_34_].i1 = (i_35_ & 0xf0) >> 4;
					_fldgoto.i3Array[i_34_].i2 = i_35_ & 0xf;
					_fldgoto.i3Array[i_34_].i3 = x[_fldlong++];
				}
				break;
			}
			case 218: {
				int i_36_ = _mthint();
				t = x[_fldlong++];
				_fldgoto.i2Array = new Integers2[t];
				for (int i_37_ = 0; i_37_ < t; i_37_++) {
					byte i_38_ = x[_fldlong++];
					_fldgoto.i2Array[i_37_] = new Integers2();
					int i_39_ = x[_fldlong++];
					_fldgoto.i2Array[i_37_].d1 = (i_39_ & 0xf0) >> 4;
					_fldgoto.i2Array[i_37_].d2 = i_39_ & 0xf;
				}
				_fldgoto.i3 = x[_fldlong++];
				_fldlong += 2;
				break;
			}
			case 221: {
				int i_40_ = _mthint();
				_fldgoto.i6 = _mthint();
				break;
			}
			default: {
				if (Debug.DEBUG > 0)
					System.out.println("Received unknown marker "
							+ Integer.toHexString(i) + ", trying to jump over");
				int i_41_ = _mthint();
				_fldlong += i_41_ - 2;
			}
			}
		} while ((i & 0xff) != 218);
	}

	private void _mthdo() throws IOException {
		int i = b / 8;
		b = 0;
		int i_42_;
		for (;;) {
			i_42_ = x[_fldlong++] & 0xff;
			if (i_42_ == 255) {
				do
					i_42_ = x[_fldlong++] & 0xff;
				while (i_42_ == 255);
				if (i_42_ != 0)
					break;
			}
		}
		if (i_42_ != 208 + _fldgoto.i7)
			throw new IOException("messed up restart markers");
		_fldgoto.i5 = _fldgoto.i4;
		_fldgoto.i7 = _fldgoto.i7 + 1 & 0x7;
	}

	private final int _mthfor() {
		if (b < 8)
			_mthchar(8);
		return (int) (j >> b - 8 & 0xffL);
	}
}