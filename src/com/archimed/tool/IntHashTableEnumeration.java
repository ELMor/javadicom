/*
 * d - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;

import java.util.Enumeration;
import java.util.NoSuchElementException;

class IntHashTableEnumeration implements Enumeration {
	boolean _fldfor;

	int a;

	DataChunk2[] _flddo;

	DataChunk2 _fldif;

	IntHashTableEnumeration(DataChunk2[] var_es, boolean bool) {
		_flddo = var_es;
		_fldfor = bool;
		a = var_es.length;
	}

	public boolean hasMoreElements() {
		if (_fldif != null)
			return true;
		while_11_: do {
			do {
				if (a-- <= 0)
					break while_11_;
			} while ((_fldif = _flddo[a]) == null);
			return true;
		} while (false);
		return false;
	}

	public Object nextElement() {
		if (_fldif == null) {
			while (a-- > 0 && (_fldif = _flddo[a]) == null) {
				/* empty */
			}
		}
		if (_fldif != null) {
			DataChunk2 var_e = _fldif;
			_fldif = var_e.next;
			return _fldfor ? new Integer(var_e.d2) : new Integer(var_e.d3);
		}
		throw new NoSuchElementException("IntHashtableEnumerator");
	}
}