
package com.archimed.tool;

class DataChunk2 {
	int d1;

	int d2;

	int d3;

	DataChunk2 next;

	protected Object clone() {
		DataChunk2 aux = new DataChunk2();
		aux.d1 = d1;
		aux.d2 = d2;
		aux.d3 = d3;
		aux.next = next != null ? (DataChunk2) next.clone() : null;
		return aux;
	}
}