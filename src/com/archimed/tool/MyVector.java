
package com.archimed.tool;

import java.util.Enumeration;
import java.util.Vector;

public class MyVector {
	private static final int _fldif = 20;

	protected Vector vHolder = new Vector(20);

	private MyVector(int i) {
		/* empty */
	}

	public MyVector() {
		this(20);
	}

	public int size() {
		return vHolder.size();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public Object getAt(int i) {
		return vHolder.elementAt(i);
	}

	public Object replace(int i, Object object) {
		Object object_0_ = vHolder.elementAt(i);
		vHolder.setElementAt(object, i);
		return object_0_;
	}

	public void add(Object object) {
		vHolder.add(object);
	}

	public void add(int i, Object object) {
		vHolder.add(i, object);
	}

	public Object get(int i) {
		Object object = vHolder.get(i);
		vHolder.remove(i);
		return object;
	}

	public void clear() {
		vHolder.clear();
	}

	public Enumeration enumerate() {
		return vHolder.elements();
	}
}