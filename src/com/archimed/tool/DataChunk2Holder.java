/*
 * b - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.archimed.tool;

import java.util.Enumeration;

public class DataChunk2Holder {
	private DataChunk2[] holder;

	private int _fldif;

	private int product;

	private float initFloat;

	public DataChunk2Holder(int size, float f) {
		if (size <= 0 || (double) f <= 0.0)
			throw new IllegalArgumentException();
		initFloat = f;
		holder = new DataChunk2[size];
		product = (int) ((float) size * f);
	}

	public DataChunk2Holder(int i) {
		this(i, 0.75F);
	}

	public DataChunk2Holder() {
		this(101, 0.75F);
	}

	public int _mthdo() {
		return _fldif;
	}

	public boolean _mthnew() {
		return _fldif == 0;
	}

	public Enumeration enumerate() {
		return new IntHashTableEnumeration(holder, true);
	}

	public Enumeration enumerateNot() {
		return new IntHashTableEnumeration(holder, false);
	}

	public boolean containsD3(int i) {
		DataChunk2[] dch = holder;
		int sz = dch.length;
		while (sz-- > 0) {
			for (DataChunk2 dc = dch[sz]; dc != null; dc = dc.next) {
				if (dc.d3 == i)
					return true;
			}
		}
		return false;
	}

	public boolean containsD1D2(int i) {
		DataChunk2[] var_es = holder;
		int d1 = i;
		int d2 = (d1 & 0x7fffffff) % var_es.length;
		for (DataChunk2 dc = var_es[d2]; dc != null; dc = dc.next) {
			if (dc.d1 == d1 && dc.d2 == i)
				return true;
		}
		return false;
	}

	public int _mthif(int d2) {
		DataChunk2[] dch = holder;
		int d1 = d2;
		int mod = (d1 & 0x7fffffff) % dch.length;
		for (DataChunk2 dc = dch[mod]; dc != null; dc = dc.next) {
			if (dc.d1 == d1 && dc.d2 == d2)
				return dc.d3;
		}
		return 2147483647;
	}

	protected void _mthint() {
		int sz = holder.length;
		DataChunk2[] dch = holder;
		int dl = sz * 2 + 1;
		DataChunk2[] dch2 = new DataChunk2[dl];
		product = (int) ((float) dl * initFloat);
		holder = dch2;
		int j = sz;
		while (j-- > 0) {
			DataChunk2 dc3 = dch[j];
			while (dc3 != null) {
				DataChunk2 dc4 = dc3;
				dc3 = dc3.next;
				int k = (dc4.d1 & 0x7fffffff) % dl;
				dc4.next = dch2[k];
				dch2[k] = dc4;
			}
		}
	}

	public int a(int i, int i_10_) {
		DataChunk2[] var_es = holder;
		int i_11_ = i;
		int i_12_ = (i_11_ & 0x7fffffff) % var_es.length;
		Object object = null;
		for (DataChunk2 var_e = var_es[i_12_]; var_e != null; var_e = var_e.next) {
			if (var_e.d1 == i_11_ && var_e.d2 == i) {
				int i_13_ = var_e.d3;
				var_e.d3 = i_10_;
				return i_13_;
			}
		}
		if (_fldif >= product) {
			_mthint();
			return a(i, i_10_);
		}
		DataChunk2 var_e = new DataChunk2();
		var_e.d1 = i_11_;
		var_e.d2 = i;
		var_e.d3 = i_10_;
		var_e.next = var_es[i_12_];
		var_es[i_12_] = var_e;
		_fldif++;
		return 2147483647;
	}

	public int a(int i) {
		DataChunk2[] var_es = holder;
		int i_14_ = i;
		int i_15_ = (i_14_ & 0x7fffffff) % var_es.length;
		DataChunk2 var_e = var_es[i_15_];
		DataChunk2 var_e_16_ = null;
		for (/**/; var_e != null; var_e = var_e.next) {
			if (var_e.d1 == i_14_ && var_e.d2 == i) {
				if (var_e_16_ != null)
					var_e_16_.next = var_e.next;
				else
					var_es[i_15_] = var_e.next;
				_fldif--;
				return var_e.d3;
			}
			var_e_16_ = var_e;
		}
		return 2147483647;
	}

	public synchronized void a() {
		DataChunk2[] var_es = holder;
		int i = var_es.length;
		while (--i >= 0)
			var_es[i] = null;
		_fldif = 0;
	}
}